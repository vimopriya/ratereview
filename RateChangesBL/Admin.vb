﻿Imports RateChangesDAL

Public Class Admin


    Public Function GetRolesForUser(ByVal strUserName As String)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        Return (rateChangesDal.GetRolesForUser(strUserName))
    End Function
    Public Function GetRightsForUser(ByVal strRoleName As String)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        Return (rateChangesDal.GetRightsForUser(strRoleName))
    End Function
    Public Function GetUsersInRole(ByVal roleName As String)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        Return (rateChangesDal.GetUsersInRole(roleName))
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="strUserName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsUserExists(ByVal strUserName As String)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        Return (rateChangesDal.IsUserExists(strUserName))
    End Function

    Public Function GetRateChangesData()
        Dim dsRateChanges = New DataSet()

        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRateChanges = rateChangesDal.GetRateChangeData()
        Return (dsRateChanges)
    End Function

    Public Function GetCompanyByCompanyID(ByVal strCompanyID As String)
        Dim dsCompany = New DataSet()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsCompany = rateChangesDal.GetCompanyDetailsByCompanyID(strCompanyID)
        Return (dsCompany)
    End Function

    Public Function GetRateFilingsData()
        Dim dsRateFilings = New DataSet()

        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRateFilings = rateChangesDal.GetLatestRateFilings()
        Return (dsRateFilings)
    End Function

    Public Function GetHighestrateChangeData()
        Dim dsRateChanges = New DataSet()

        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRateChanges = rateChangesDal.GetHighestrateChangeData()
        Return (dsRateChanges)
    End Function

    Public Function GetRateDataByInsuranceType()
        Dim dsRateChanges = New DataSet()

        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRateChanges = rateChangesDal.GetRateDataByInsuranceType()
        Return (dsRateChanges)
    End Function

    Public Function GetAuditTrail()
        Dim dsAuditChanges = New DataSet()

        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsAuditChanges = rateChangesDal.GetAuditTrail()
        Return (dsAuditChanges)
    End Function
    Public Function GetAuditTrailByID(ByVal intAuditID As Integer)
        Dim dsAuditChanges = New DataSet()

        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsAuditChanges = rateChangesDal.GetAuditTrailByID(intAuditID)
        Return (dsAuditChanges)
    End Function
    Public Function GetRateChangesByCompIDFromToDate(ByVal strCompanyID As String, ByVal dtFromDate As Date, ByVal dtTodate As Date)
        Dim dsRateChanges = New DataSet()

        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRateChanges = rateChangesDal.GetRateChangesByCompIDFromToDate(strCompanyID, dtFromDate, dtTodate)
        Return (dsRateChanges)
    End Function
    Public Function GetRateChangesByPTypeFromToDate(ByVal strPolicyType As String, ByVal dtFromDate As Date, ByVal dtTodate As Date)
        Dim dsRateChanges = New DataSet()

        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRateChanges = rateChangesDal.GetRateChangesByPTypeFromToDate(strPolicyType, dtFromDate, dtTodate)
        Return (dsRateChanges)
    End Function

    Public Function Export(ByVal rateChangeID As Integer)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        Return (rateChangesDal.Export(rateChangeID))
    End Function


    Public Function ExportByFileID(ByVal fileID As Integer)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        Return (rateChangesDal.ExportFileByFileID(fileID))
    End Function
    Public Function GetAllFileDownloads()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        Return (rateChangesDal.GetAllFileDownloads())
    End Function

    Public Function GetRateChangesByCompID(ByVal strCompanyId As String)
        Dim dsRateChanges = New DataSet()

        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRateChanges = rateChangesDal.GetRateChangeDataByID(strCompanyId)
        Return (dsRateChanges)
    End Function
    Public Function GetRateChangesByRateChangeIDs(ByVal strRateChangeIds As String)
        Dim dsRateChanges = New DataSet()

        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRateChanges = rateChangesDal.GetRateChangeDataByRateChangeID(strRateChangeIds)
        Return (dsRateChanges)
    End Function
    Public Function GetRateChangeDataRateID(ByVal intRateChangeID As Integer)
        Dim dsRateChanges = New DataSet()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRateChanges = rateChangesDal.GetRateChangeDataRateID(intRateChangeID)
        Return (dsRateChanges)
    End Function

    Public Function GetRelatedLinksUrlByID(ByVal strRelatedLinksID As String)
        Dim dsRelatedLinks = New DataSet()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRelatedLinks = rateChangesDal.GetRelatedLinksUrlByID(strRelatedLinksID)
        Return (dsRelatedLinks)
    End Function

    Public Function GetResourceUrlByID(ByVal strResourceID As String)
        Dim dsResources = New DataSet()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsResources = rateChangesDal.GetResourceUrlByID(strResourceID)
        Return (dsResources)
    End Function

    Public Function GetRateChangesByPolicyTypeID(ByVal strPolicyTypeID As String)
        Dim dsRateChanges = New DataSet()

        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRateChanges = rateChangesDal.GetRateChangeByPolicyType(strPolicyTypeID)
        Return (dsRateChanges)
    End Function


    Public Function ImportFile(ByVal uploadDate As Date, ByVal fileName As String, ByVal fileBuffer() As Byte, ByVal fileType As String, ByVal fileSize As Integer, ByVal rateChangeId As Integer)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()

        Return (rateChangesDal.ImportFile(uploadDate, fileName, fileBuffer, fileType, fileSize, rateChangeId))
    End Function
    Public Function DeleteResourcesUrl(ByVal strResourceID As String)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()

        Return (rateChangesDal.DeleteResourcesUrl(strResourceID))
    End Function

    Public Function DeleteRelatedlinksUrl(ByVal strRelatedLinksID As String)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()

        Return (rateChangesDal.DeleteRelatedLinksUrl(strRelatedLinksID))
    End Function

    Public Function DeleteCompany(ByVal strCompanyID As String)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()

        Return (rateChangesDal.DeleteCompany(strCompanyID))
    End Function
    Public Function InsertNewComapny(ByRef strCompanyName As String, ByVal strFileName As String)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()

        Return (rateChangesDal.InsertNewCompany(strCompanyName, strFileName))

    End Function


    Public Function InsertNewResourceUrl(ByVal strResourceUrlName As String, ByVal strSowUrl As String, ByVal dtCreatedDate As Date, ByVal strResourcesDisptxt As String)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()

        Return (rateChangesDal.InsertNewResourceUrl(strResourceUrlName, strSowUrl, dtCreatedDate, strResourcesDisptxt))
    End Function

    Public Function InsertNewRelatedLinksUrl(ByVal strRelatedLinksUrlName As String, ByVal strSowUrl As String, ByVal dtCreatedDate As Date, ByVal strRelatedLinkDispTxt As String)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()

        Return (rateChangesDal.InsertNewRelatedLinksUrl(strRelatedLinksUrlName, strSowUrl, dtCreatedDate, strRelatedLinkDispTxt))
    End Function
    Public Function UpdateCompany(ByVal strCompanyName As String, ByVal strCompanyID As String, ByVal fileName As String, ByVal fileType As String, ByVal fileBuffer() As Byte, Optional ByVal UpdatedBy As String = "", Optional ByVal UpdatedOn As String = "")
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()

        Return (rateChangesDal.UpdateCompany(strCompanyName, strCompanyID, fileName, fileType, fileBuffer, UpdatedBy, UpdatedOn))
    End Function
    Public Function InsertCompany(ByVal strCompanyName As String, ByVal fileName As String, ByVal fileType As String, ByVal fileBuffer() As Byte, Optional ByVal CreatedBy As String = "", Optional ByVal CreatedOn As String = "")
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()

        Return (rateChangesDal.InsertCompany(strCompanyName, fileName, fileType, fileBuffer, CreatedBy, CreatedOn))
    End Function
   
    Public Function GetCompanyDetails()
        Dim dsCompany = New DataSet()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsCompany = rateChangesDal.GetCompanyDetails()
        Return (dsCompany)
    End Function
    Public Function GetCompanyLogoByID(ByVal strCompanyID As String)
        Dim dsCompany = New DataSet()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsCompany = rateChangesDal.GetCompanyLogoByID(strCompanyID)
        Return (dsCompany)
    End Function

    Public Function GetPolicyType()
        Dim dsPolicyType = New DataSet()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsPolicyType = rateChangesDal.GetPolicyType()
        Return (dsPolicyType)
    End Function

    Public Function DeleteFileByID(ByVal intFileID As Integer)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        Return (rateChangesDal.DeleteFileByID(intFileID))
    End Function

    Public Sub DeleteRateChanges(ByVal intRateChangeID As Integer)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        rateChangesDal.DeleteRateChanges(intRateChangeID)
    End Sub


    Public Function InsertRateChanges(ByVal strCompanyID As String, ByVal strPolicyFormNo As String, ByVal strPolicyType As String, ByVal dtDateCurrRateInc As Date, _
                    ByVal PercentCurrtRateInc As Double, ByVal dtLastFiveYrRateIn_2010 As Date, ByVal dtLastFiveYrRateIn_2009 As Date, _
                    ByVal dtLastFiveYrRateIn_2008 As Date, ByVal dtLastFiveYrRateIn_2007 As Date, ByVal dtLastFiveYrRateIn_2006 As Date, _
                    ByVal PcntLast5YrRateInc_2010 As Object, ByVal PcntLast5YrRateInc_2009 As Object, ByVal PcntLast5YrRateInc_2008 As Object, _
                    ByVal PcntLast5YrRateInc_2007 As Object, ByVal PcntLast5YrRateInc_2006 As Object, ByVal last5YrLossRatio_2010 As Object, _
                    ByVal last5YrLossRatio_2009 As Object, ByVal last5YrLossRatio_2008 As Object, _
                    ByVal last5YrLossRatio_2007 As Object, ByVal last5YrLossRatio_2006 As Object, ByVal NumOfMsInsureds_2010 As Object, _
                    ByVal NumOfMsInsureds_2009 As Object, ByVal NumOfMsInsureds_2008 As Object, ByVal NumOfMsInsureds_2007 As Object, _
                    ByVal NumOfMsInsureds_2006 As Object, ByVal strUser As String, ByVal strSummaryRateInc As String, ByVal dtPolicyAprl As Date, ByVal dtDateSubmitted As Date, ByVal strLinkToRateInc As String)
        Dim intRate As Integer
        Try


            Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
            intRate = rateChangesDal.InsertRateChanges(strCompanyID, strPolicyFormNo, strPolicyType, dtDateCurrRateInc, PercentCurrtRateInc, _
                                          dtLastFiveYrRateIn_2010, dtLastFiveYrRateIn_2009, dtLastFiveYrRateIn_2008, dtLastFiveYrRateIn_2007, dtLastFiveYrRateIn_2006, _
                                             PcntLast5YrRateInc_2010, PcntLast5YrRateInc_2009, PcntLast5YrRateInc_2008, PcntLast5YrRateInc_2007, PcntLast5YrRateInc_2006, _
                                             last5YrLossRatio_2010, last5YrLossRatio_2009, last5YrLossRatio_2008, last5YrLossRatio_2007, last5YrLossRatio_2006, NumOfMsInsureds_2010, _
                                             NumOfMsInsureds_2009, NumOfMsInsureds_2008, NumOfMsInsureds_2007, NumOfMsInsureds_2006, strUser, strSummaryRateInc, dtPolicyAprl, dtDateSubmitted, strLinkToRateInc)
        Catch ex As Exception

        End Try

        Return (intRate)
    End Function
    Public Function UpadteRateChanges(ByVal intRateChangesID As Integer, ByVal strCompanyID As String, ByVal strCompanyName As String, _
                    ByVal strPolicyFormNo As String, ByVal strPolicyType As String, ByVal dtDateCurrRateInc As Date, _
                    ByVal PercentCurrtRateInc As Double, ByVal dtLastFiveYrRateIn_2010 As Date, ByVal dtLastFiveYrRateIn_2009 As Date, _
                    ByVal dtLastFiveYrRateIn_2008 As Date, ByVal dtLastFiveYrRateIn_2007 As Date, ByVal dtLastFiveYrRateIn_2006 As Date, _
                    ByVal PcntLast5YrRateInc_2010 As Object, ByVal PcntLast5YrRateInc_2009 As Object, ByVal PcntLast5YrRateInc_2008 As Object, _
                    ByVal PcntLast5YrRateInc_2007 As Object, ByVal PcntLast5YrRateInc_2006 As Object, ByVal last5YrLossRatio_2010 As Object, _
                    ByVal last5YrLossRatio_2009 As Object, ByVal last5YrLossRatio_2008 As Object, _
                    ByVal last5YrLossRatio_2007 As Object, ByVal last5YrLossRatio_2006 As Object, ByVal NumOfMsInsureds_2010 As Object, _
                    ByVal NumOfMsInsureds_2009 As Object, ByVal NumOfMsInsureds_2008 As Object, ByVal NumOfMsInsureds_2007 As Object, _
                    ByVal NumOfMsInsureds_2006 As Object, ByVal strUser As String, ByVal strSummaryRateInc As String, ByVal dtPolicyAprl As Date, ByVal dtDateSubmitted As Date, ByVal strLinkToRateInc As String)
        Try


            Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
            rateChangesDal.UpdateRateChanges(intRateChangesID, strCompanyID, strCompanyName, strPolicyFormNo, strPolicyType, dtDateCurrRateInc, PercentCurrtRateInc, _
                                          dtLastFiveYrRateIn_2010, dtLastFiveYrRateIn_2009, dtLastFiveYrRateIn_2008, dtLastFiveYrRateIn_2007, dtLastFiveYrRateIn_2006, _
                                             PcntLast5YrRateInc_2010, PcntLast5YrRateInc_2009, PcntLast5YrRateInc_2008, PcntLast5YrRateInc_2007, PcntLast5YrRateInc_2006, _
                                             last5YrLossRatio_2010, last5YrLossRatio_2009, last5YrLossRatio_2008, last5YrLossRatio_2007, last5YrLossRatio_2006, NumOfMsInsureds_2010, _
                                             NumOfMsInsureds_2009, NumOfMsInsureds_2008, NumOfMsInsureds_2007, NumOfMsInsureds_2006, strUser, strSummaryRateInc, dtPolicyAprl, dtDateSubmitted, strLinkToRateInc)
        Catch ex As Exception

        End Try

        Return (1)
    End Function

    Public Function UpdateResourceUrl(ByVal intResourceID As Integer, ByVal strResourceUrlName As String, ByVal strSowUrl As String, ByVal dtUpdatedDate As Date, ByVal strResourcesDisptxt As String)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        rateChangesDal.UpdateResourceUrl(intResourceID, strResourceUrlName, strSowUrl, dtUpdatedDate, strResourcesDisptxt)
        Return (1)
    End Function

    Public Function UpdateRelatedLinksUrl(ByVal intRelatedLinksID As Integer, ByVal strRelatedLinksUrlName As String, ByVal strSowUrl As String, ByVal dtUpdatedDate As Date, ByVal strRelatedLinkDispTxt As String)
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        rateChangesDal.UpdateRelatedLinksUrl(intRelatedLinksID, strRelatedLinksUrlName, strSowUrl, dtUpdatedDate, strRelatedLinkDispTxt)
        Return (1)
    End Function
    Public Function GetRateChangeDataByIDAndType(ByVal strCompanyID As String, ByVal strPolicyTypeId As String)
        Dim dsRateChanges = New DataSet()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRateChanges = rateChangesDal.GetRateChangeDataByIDAndType(strCompanyID, strPolicyTypeId)
        Return (dsRateChanges)
    End Function

    Public Function GetResourcesUrl()
        Dim dsResourcesUrl = New DataSet()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsResourcesUrl = rateChangesDal.GetResourcesUrl()
        Return (dsResourcesUrl)
    End Function

    Public Function GetRelatedLinksUrl()
        Dim dsRelatedLinksUrl = New DataSet()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRelatedLinksUrl = rateChangesDal.GetRelatedLinksUrl()
        Return (dsRelatedLinksUrl)
    End Function
    Public Function GetAuditTrailBySearch(ByVal strAction As String, ByVal strActionBy As String, ByVal dtFromDate As String, ByVal dtTodate As String)
        Dim dsRateChanges = New DataSet()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRateChanges = rateChangesDal.GetAuditTrailBySearch(strAction, strActionBy, dtFromDate, dtTodate)
        Return (dsRateChanges)
    End Function
    Public Function GetRateChangeDataByIDTypeDate(ByVal strCompanyID As String, ByVal strPolicyTypeId As String, ByVal dtFromDate As String, ByVal dtTodate As String)
        Dim dsRateChanges = New DataSet()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRateChanges = rateChangesDal.GetRateChangeDataByIDTypeDate(strCompanyID, strPolicyTypeId, dtFromDate, dtTodate)
        Return (dsRateChanges)
    End Function

    Public Function GetRateChangeDataFromToDate(ByVal dtFromDate As Date, ByVal dtTodate As Date)
        Dim dsRateChanges = New DataSet()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsRateChanges = rateChangesDal.GetRateChangeDataFromToDate(dtFromDate, dtTodate)
        Return (dsRateChanges)
    End Function
    Public Shared Sub WriteLog(ByVal ex As Exception)

        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        rateChangesDal.WriteLog(ex)
    End Sub
    Public Function GetErrorLogs(ByVal AppName As String, ByVal ErrorDesc As String, ByVal StartDate As String, _
                                ByVal endDate As String) As DataTable

        Dim dsErrorLogs = New DataTable()
        Dim rateChangesDal As RateChangesDAL.AdminData = New RateChangesDAL.AdminData()
        dsErrorLogs = rateChangesDal.GetErrorLogs(AppName, ErrorDesc, StartDate, endDate)
        Return (dsErrorLogs)

    End Function
End Class
