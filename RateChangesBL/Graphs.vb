﻿Imports RateChangesDAL

Public Class Graphs

    Public Function GetDataForGraphs(ByVal RateIds As String)

        Dim companyData As DataTable
        Dim rateChangesDal As RateChangesDAL.GraphsData

        companyData = New DataTable()
        rateChangesDal = New RateChangesDAL.GraphsData()

        companyData = rateChangesDal.GetDataForGraphs(RateIds)
        Return (companyData)

    End Function

End Class
