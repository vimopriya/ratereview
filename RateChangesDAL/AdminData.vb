﻿Imports System.Data
Imports System.Data.OracleClient
Imports System.Data.OleDb
Imports System.Configuration
Imports System.IO
''' <summary>
''' 
''' </summary>
''' <remarks></remarks>
Public Class AdminData

    Dim MidDBConnStr As String = ConfigurationManager.ConnectionStrings("MidConnectionString").ConnectionString.ToString()
    Dim BlogDbConnStr As String = ConfigurationManager.ConnectionStrings("BlogEngine").ConnectionString.ToString()

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="userName"></param>
    ''' <param name="strPassword"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsValidUser(ByVal userName As String, ByVal strPassword As String)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        Dim oracleCommand = New OracleCommand()

        oracleCommand.Connection = con
        oracleCommand.CommandText = "SP_CHECK_USER_EXISTS"
        oracleCommand.CommandType = CommandType.StoredProcedure
        oracleCommand.Parameters.Add(New OracleParameter("User_name", OleDbType.VarChar)).Value = userName
        oracleCommand.Parameters.Add(New OracleParameter("USER_PASSWORD", OleDbType.VarChar)).Value = strPassword
        Dim retValParam As New OracleParameter("IsUser_Exist", OleDbType.Numeric)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        Dim isUserExists As Integer
        con.Open()
        oracleCommand.ExecuteNonQuery()
        oracleCommand.Parameters.Clear()
        con.Close()
        isUserExists = retValParam.Value
        Return (isUserExists)

    End Function

    'Public Function IsCompanyExists(ByVal companyID As String)
    '    Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

    '    Dim oracleCommand = New OracleCommand()

    '    oracleCommand.Connection = con
    '    oracleCommand.CommandText = "SP_CHECK_COMPANY_EXISTS"
    '    oracleCommand.CommandType = CommandType.StoredProcedure
    '    oracleCommand.Parameters.Add(New OracleParameter("arg_CompanyID", OleDbType.VarChar)).Value = companyID
    '    Dim retValParam As New OracleParameter("IsUser_Exist", OleDbType.Numeric)
    '    retValParam.Direction = ParameterDirection.Output
    '    oracleCommand.Parameters.Add(retValParam)
    '    Dim isUserExists As Integer
    '    con.Open()
    '    oracleCommand.ExecuteNonQuery()
    '    oracleCommand.Parameters.Clear()
    '    con.Close()
    '    isUserExists = retValParam.Value
    '    Return (isUserExists)

    'End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="userName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsUserExists(ByVal userName As String)
        Dim con As OracleConnection = New OracleConnection(BlogDbConnStr)

        Dim oracleCommand = New OracleCommand()

        oracleCommand.Connection = con
        oracleCommand.CommandText = "SP_CHECK_USER_EXISTS"
        oracleCommand.CommandType = CommandType.StoredProcedure
        oracleCommand.Parameters.Add(New OracleParameter("User_name", OleDbType.VarChar)).Value = userName
        Dim retValParam As New OracleParameter("IsUser_Exist", OleDbType.Numeric)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        'oledbCommand.Parameters.Add(New OleDbParameter("IsUser_Exists", OleDbType.Numeric)).Direction = ParameterDirection.Output
        Dim isUserExist As Integer = 0
        con.Open()
        oracleCommand.ExecuteNonQuery()
        oracleCommand.Parameters.Clear()
        con.Close()
        IsUserExists = retValParam.Value
        Return (IsUserExists)

    End Function
    Public Function GetRateChangeDataByRateChangeID(ByVal strRateChangeIds As String)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim oracleCommand = New OracleCommand()
        Dim rateChangesDataSet As New DataSet()
        Try

            oracleCommand.Connection = con
            oracleCommand.CommandText = "Get_RateChangesBy_RateChangeID"
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.Parameters.Add(New OracleParameter("argRateChange_ID", OleDbType.VarChar)).Value = strRateChangeIds
            Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
            retValParam.Direction = ParameterDirection.Output
            oracleCommand.Parameters.Add(retValParam)

            Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
            con.Open()

            oracleDbDataAdapter.Fill(rateChangesDataSet)
            con.Close()

        Catch ex As Exception

        End Try

        Return (rateChangesDataSet)

    End Function

    Public Function GetRateChangeData()

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        Dim oracleCommand = New OracleCommand()

        oracleCommand.Connection = con
        oracleCommand.CommandText = "Get_Rate_Changes"
        oracleCommand.CommandType = CommandType.StoredProcedure
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)

        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
        con.Open()
        Dim rateChangesDataSet As New DataSet()
        oracleDbDataAdapter.Fill(rateChangesDataSet)
        con.Close()
        Return (rateChangesDataSet)


    End Function

    Public Function GetHighestrateChangeData()

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        Dim oracleCommand = New OracleCommand()

        oracleCommand.Connection = con
        oracleCommand.CommandText = "SP_GET_HIGHEST_RATECHANGES"
        oracleCommand.CommandType = CommandType.StoredProcedure
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)

        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
        con.Open()
        Dim rateChangesDataSet As New DataSet()
        oracleDbDataAdapter.Fill(rateChangesDataSet)
        con.Close()
        Return (rateChangesDataSet)


    End Function


    Public Function GetRateDataByInsuranceType()

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        Dim oracleCommand = New OracleCommand()

        oracleCommand.Connection = con
        oracleCommand.CommandText = "SP_GET_DATABY_TYPE"
        oracleCommand.CommandType = CommandType.StoredProcedure
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)

        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
        con.Open()
        Dim rateChangesDataSet As New DataSet()
        oracleDbDataAdapter.Fill(rateChangesDataSet)
        con.Close()
        Return (rateChangesDataSet)


    End Function



    Public Function GetLatestRateFilings()

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        Dim oracleCommand = New OracleCommand()

        oracleCommand.Connection = con
        oracleCommand.CommandText = "GET_RATE_FILINGS"
        oracleCommand.CommandType = CommandType.StoredProcedure
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)

        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
        con.Open()
        Dim rateFilingsDataSet As New DataSet()
        oracleDbDataAdapter.Fill(rateFilingsDataSet)
        con.Close()
        Return (rateFilingsDataSet)


    End Function
    Public Function GetRolesForUser(ByVal strUserName As String)
        Dim roleList As New ArrayList()
        Dim con As OracleConnection = New OracleConnection(BlogDbConnStr)
        Dim oracleCommand = New OracleCommand()

        oracleCommand.Connection = con
        oracleCommand.CommandText = "SP_GETUSER_ROLES"
        oracleCommand.CommandType = CommandType.StoredProcedure
        oracleCommand.Parameters.Add(New OracleParameter("arg_UserName", OleDbType.VarChar)).Value = strUserName
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        con.Open()
        Dim myReader As OracleDataReader = oracleCommand.ExecuteReader()
        While myReader.Read()
            roleList.Add(myReader.GetString(0).ToString())
        End While
        myReader.Close()
        Return (roleList)


    End Function

    Public Function GetRightsForUser(ByVal strRoleName As String)
        Dim roleList As New ArrayList()
        Dim con As OracleConnection = New OracleConnection(BlogDbConnStr)
        Dim oracleCommand = New OracleCommand()

        oracleCommand.Connection = con
        oracleCommand.CommandText = "SP_GETUSER_RIGHTS"
        oracleCommand.CommandType = CommandType.StoredProcedure
        oracleCommand.Parameters.Add(New OracleParameter("arg_RoleName", OleDbType.VarChar)).Value = strRoleName
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        con.Open()
        Dim myReader As OracleDataReader = oracleCommand.ExecuteReader()
        While myReader.Read()
            roleList.Add(myReader.GetString(0).ToString())
        End While
        myReader.Close()
        con.Close()
      
        Return (roleList)


    End Function
    Public Function GetUsersInRole(ByVal roleName As String)
        Dim usersList As New ArrayList()
        Dim con As OracleConnection = New OracleConnection(BlogDbConnStr)
        Dim oracleCommand = New OracleCommand()

        oracleCommand.Connection = con
        oracleCommand.CommandText = "SP_GetUsers_InRole"
        oracleCommand.CommandType = CommandType.StoredProcedure
        oracleCommand.Parameters.Add(New OracleParameter("arg_RoleName", OleDbType.VarChar)).Value = roleName
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        con.Open()
        Dim myReader As OracleDataReader = oracleCommand.ExecuteReader()
        While myReader.Read()
            usersList.Add(myReader.GetString(0).ToString())
        End While
        myReader.Close()
        con.Close()
     
        Return (usersList)


    End Function
    Public Function GetRateChangeDataByID(ByVal strCompanyID As String)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim oracleCommand = New OracleCommand()
        Dim rateChangesDataSet As New DataSet()
        Try

            oracleCommand.Connection = con
            oracleCommand.CommandText = "Get_Rate_ChangesBy_ID"
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.Parameters.Add(New OracleParameter("argCompany_ID", OleDbType.VarChar)).Value = strCompanyID
            Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
            retValParam.Direction = ParameterDirection.Output
            oracleCommand.Parameters.Add(retValParam)
            Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
            con.Open()

            oracleDbDataAdapter.Fill(rateChangesDataSet)
            con.Close()

        Catch ex As Exception

        End Try

        Return (rateChangesDataSet)

    End Function


    Public Function GetResourceUrlByID(ByVal strResourceID As String)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim oracleCommand = New OracleCommand()
        Dim resourcesDataSet As New DataSet()
        Try

            oracleCommand.Connection = con
            oracleCommand.CommandText = "GET_RESOURCEURL_BY_ID"
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.Parameters.Add(New OracleParameter("argResource_Id", OleDbType.VarChar)).Value = strResourceID
            Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
            retValParam.Direction = ParameterDirection.Output
            oracleCommand.Parameters.Add(retValParam)
            Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
            con.Open()

            oracleDbDataAdapter.Fill(resourcesDataSet)
            con.Close()

        Catch ex As Exception

        End Try

        Return (resourcesDataSet)

    End Function

    Public Function GetRelatedLinksUrlByID(ByVal strRelatedLinksID As String)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim oracleCommand = New OracleCommand()
        Dim relatedLinksDataSet As New DataSet()
        Try

            oracleCommand.Connection = con
            oracleCommand.CommandText = "GET_RELATEDLINKS_BY_ID"
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.Parameters.Add(New OracleParameter("argRelatedLinks_Id", OleDbType.VarChar)).Value = strRelatedLinksID
            Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
            retValParam.Direction = ParameterDirection.Output
            oracleCommand.Parameters.Add(retValParam)
            Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
            con.Open()

            oracleDbDataAdapter.Fill(relatedLinksDataSet)
            con.Close()

        Catch ex As Exception

        End Try

        Return (relatedLinksDataSet)

    End Function

    Public Function GetRateChangeDataByIDAndType(ByVal strCompanyID As String, ByVal strPolicyTypeId As String)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim oracleCommand = New OracleCommand()
        Dim rateChangesDataSet As New DataSet()
        Try

            oracleCommand.Connection = con
            oracleCommand.CommandText = "Get_RateChangesBy_ID_PType"
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.Parameters.Add(New OracleParameter("argCompany_ID", OleDbType.VarChar)).Value = strCompanyID
            oracleCommand.Parameters.Add(New OracleParameter("argPolicyType_ID", OleDbType.VarChar)).Value = strPolicyTypeId
            Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
            retValParam.Direction = ParameterDirection.Output
            oracleCommand.Parameters.Add(retValParam)

            Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
            con.Open()

            oracleDbDataAdapter.Fill(rateChangesDataSet)
            con.Close()

        Catch ex As Exception

        End Try

        Return (rateChangesDataSet)

    End Function

    Public Function GetRateChangeDataByIDTypeDate(ByVal strCompanyID As String, ByVal strPolicyTypeId As String, ByVal dtFromDate As String, ByVal dtTodate As String)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim oracleCommand = New OracleCommand()
        Dim rateChangesDataSet As New DataSet()
        Try

            oracleCommand.Connection = con
            oracleCommand.CommandText = "SP_SEARCH_RATEREVIEWDATA"
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.Parameters.Add(New OracleParameter("argCompany_ID", OleDbType.VarChar)).Value = strCompanyID
            oracleCommand.Parameters.Add(New OracleParameter("arg_POLICY_CATEGORY", OleDbType.VarChar)).Value = strPolicyTypeId
            oracleCommand.Parameters.Add(New OracleParameter("arg_FromDate", OleDbType.VarChar)).Value = dtFromDate
            oracleCommand.Parameters.Add(New OracleParameter("arg_ToDate", OleDbType.VarChar)).Value = dtTodate
            Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
            retValParam.Direction = ParameterDirection.Output
            oracleCommand.Parameters.Add(retValParam)

            Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
            con.Open()

            oracleDbDataAdapter.Fill(rateChangesDataSet)
            con.Close()

        Catch ex As Exception

        End Try

        Return (rateChangesDataSet)

    End Function

    Public Function GetAuditTrailBySearch(ByVal strAction As String, ByVal strActionBy As String, ByVal dtFromDate As String, ByVal dtTodate As String)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim oracleCommand = New OracleCommand()
        Dim rateChangesDataSet As New DataSet()
        Try

            oracleCommand.Connection = con
            oracleCommand.CommandText = "Sp_AuditTrailSearch"
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.Parameters.Add(New OracleParameter("ARG_ACTION", OleDbType.VarChar)).Value = strAction
            oracleCommand.Parameters.Add(New OracleParameter("ARG_ACTION_BY", OleDbType.VarChar)).Value = strActionBy
            oracleCommand.Parameters.Add(New OracleParameter("ARG_FROM_DATE", OleDbType.VarChar)).Value = dtFromDate.ToString()
            oracleCommand.Parameters.Add(New OracleParameter("ARG_TO_DATE", OleDbType.VarChar)).Value = dtTodate.ToString()
            Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
            retValParam.Direction = ParameterDirection.Output
            oracleCommand.Parameters.Add(retValParam)

            Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
            con.Open()

            oracleDbDataAdapter.Fill(rateChangesDataSet)
            con.Close()

        Catch ex As Exception

        End Try

        Return (rateChangesDataSet)

    End Function

    Public Function GetRateChangesByCompIDFromToDate(ByVal strCompanyID As String, ByVal dtFromDate As Date, ByVal dtTodate As Date)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim oracleCommand = New OracleCommand()
        Dim rateChangesDataSet As New DataSet()
        Try

            oracleCommand.Connection = con
            oracleCommand.CommandText = "Get_RateBy_CompIDFromToDate"
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.Parameters.Add(New OracleParameter("arg_CompanyID", OleDbType.VarChar)).Value = strCompanyID
            oracleCommand.Parameters.Add(New OracleParameter("arg_FromDate", OleDbType.Date)).Value = dtFromDate
            oracleCommand.Parameters.Add(New OracleParameter("arg_ToDate", OleDbType.Date)).Value = dtTodate
            Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
            retValParam.Direction = ParameterDirection.Output
            oracleCommand.Parameters.Add(retValParam)

            Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
            con.Open()

            oracleDbDataAdapter.Fill(rateChangesDataSet)
            con.Close()

        Catch ex As Exception

        End Try

        Return (rateChangesDataSet)

    End Function

    Public Function GetRateChangesByPTypeFromToDate(ByVal strPolicyType As String, ByVal dtFromDate As Date, ByVal dtTodate As Date)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim oracleCommand = New OracleCommand()
        Dim rateChangesDataSet As New DataSet()
        Try

            oracleCommand.Connection = con
            oracleCommand.CommandText = "Get_RateBy_PTypeFromToDate"
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.Parameters.Add(New OracleParameter("arg_POLICY_TYPE", OleDbType.VarChar)).Value = strPolicyType
            oracleCommand.Parameters.Add(New OracleParameter("arg_FromDate", OleDbType.Date)).Value = dtFromDate
            oracleCommand.Parameters.Add(New OracleParameter("arg_ToDate", OleDbType.Date)).Value = dtTodate
            Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
            retValParam.Direction = ParameterDirection.Output
            oracleCommand.Parameters.Add(retValParam)

            Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
            con.Open()

            oracleDbDataAdapter.Fill(rateChangesDataSet)
            con.Close()

        Catch ex As Exception

        End Try

        Return (rateChangesDataSet)

    End Function

    Public Function GetRateChangeDataFromToDate(ByVal dtFromDate As Date, ByVal dtTodate As Date)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim oracleCommand = New OracleCommand()
        Dim rateChangesDataSet As New DataSet()
        Try

            oracleCommand.Connection = con
            oracleCommand.CommandText = "Get_RateBy_FromToDate"
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.Parameters.Add(New OracleParameter("arg_FromDate", OleDbType.Date)).Value = dtFromDate
            oracleCommand.Parameters.Add(New OracleParameter("arg_ToDate", OleDbType.Date)).Value = dtTodate
            Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
            retValParam.Direction = ParameterDirection.Output
            oracleCommand.Parameters.Add(retValParam)

            Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
            con.Open()

            oracleDbDataAdapter.Fill(rateChangesDataSet)
            con.Close()

        Catch ex As Exception

        End Try

        Return (rateChangesDataSet)

    End Function
    Public Function GetRateChangeDataRateID(ByVal intRateChangeID As Integer)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim oracleCommand = New OracleCommand()
        Dim rateChangesDataSet As New DataSet()
        Try

            oracleCommand.Connection = con
            oracleCommand.CommandText = "Get_Rate_ChangesBy_RateID"
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.Parameters.Add(New OracleParameter("arg_RATECHANGES_ID", OleDbType.Integer)).Value = intRateChangeID
            Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
            retValParam.Direction = ParameterDirection.Output
            oracleCommand.Parameters.Add(retValParam)

            Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
            con.Open()

            oracleDbDataAdapter.Fill(rateChangesDataSet)
            con.Close()
        Catch ex As Exception

        End Try

        Return (rateChangesDataSet)
    End Function

    Public Function GetRateChangeByPolicyType(ByVal strPolicyTypeID As String)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim oracleCommand = New OracleCommand()
        Dim rateChangesDataSet As New DataSet()
        Try

            oracleCommand.Connection = con
            oracleCommand.CommandText = "Get_Rate_ChangesBy_PolicyType"
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.Parameters.Add(New OracleParameter("arg_POLICYTYPE_ID", OleDbType.VarChar)).Value = strPolicyTypeID
            Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
            retValParam.Direction = ParameterDirection.Output
            oracleCommand.Parameters.Add(retValParam)

            Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
            con.Open()

            oracleDbDataAdapter.Fill(rateChangesDataSet)
            con.Close()

        Catch ex As Exception

        End Try

        Return (rateChangesDataSet)

    End Function


    Public Function DeleteCompany(ByVal strCompanyID As String)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim oracleCommand = New OracleCommand()
        Try

            oracleCommand.Connection = con
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.CommandText = "SP_DELETE_COMPANY"

            oracleCommand.Parameters.Add(New OracleParameter("arg_COMPANYID", OracleClient.OracleType.VarChar)).Value = strCompanyID

            con.Open()
            oracleCommand.ExecuteNonQuery()
            oracleCommand.Parameters.Clear()
        Finally
            con.Close()
        End Try
        Return (1)
    End Function
    Public Function InsertNewCompany(ByVal strCompanyName As String, ByVal strCompLogoFileName As String)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Try
            Dim oracleCommand = New OracleCommand()
            oracleCommand.Connection = con
            oracleCommand.CommandText = "SP_INSERT_COMPANY"
            oracleCommand.CommandType = CommandType.StoredProcedure
            Dim paramComapanyName As New OracleParameter("arg_COMPANYNAME", OracleType.VarChar)
            paramComapanyName.Size = 8000
            paramComapanyName.DbType = DbType.String
            paramComapanyName.Value = strCompanyName
            oracleCommand.Parameters.Add(paramComapanyName)

            Dim paramCompanyLogoFileName As New OracleParameter("arg_COMPANY_LOGO_FILENAME", OracleType.VarChar)
            paramCompanyLogoFileName.Size = 8000
            paramCompanyLogoFileName.DbType = DbType.String
            paramCompanyLogoFileName.Value = strCompLogoFileName
            oracleCommand.Parameters.Add(paramCompanyLogoFileName)

            Dim paramCompanyLogoFilePath As New OracleParameter("arg_COMPANY_LOGO_FILEPATH", OracleType.VarChar)
            paramCompanyLogoFilePath.Size = 8000
            paramCompanyLogoFilePath.DbType = DbType.String
            paramCompanyLogoFilePath.Value = "images/" + strCompLogoFileName
            oracleCommand.Parameters.Add(paramCompanyLogoFilePath)
            con.Open()
            oracleCommand.ExecuteNonQuery()
            oracleCommand.Parameters.Clear()
            con.Close()
        Catch ex As Exception

        End Try
        Return (1)

    End Function

    Public Function InsertNewResourceUrl(ByVal strResourceUrlName As String, ByVal strSowUrl As String, ByVal dtCreatedDate As Date, ByVal strResourcesDispUrl As String)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Try
            Dim oracleCommand = New OracleCommand()
            oracleCommand.Connection = con
            oracleCommand.CommandText = "SP_INSERT_RESOURCES_URL"
            oracleCommand.CommandType = CommandType.StoredProcedure
            Dim paramResouceUrl As New OracleParameter("arg_ResouceUrl", OracleType.VarChar)
            paramResouceUrl.Size = 8000
            paramResouceUrl.DbType = DbType.String
            If (strResourceUrlName = "") Then
                paramResouceUrl.Value = DBNull.Value
            Else
                paramResouceUrl.Value = strResourceUrlName
            End If
            oracleCommand.Parameters.Add(paramResouceUrl)

            Dim paramShowUrl As New OracleParameter("arg_ShowUrl", OracleType.VarChar)
            paramShowUrl.Size = 8000
            paramShowUrl.DbType = DbType.String
            paramShowUrl.Value = strSowUrl
            oracleCommand.Parameters.Add(paramShowUrl)

            Dim paramdtCreatedDate As New OracleParameter("arg_Created_DateTime", OracleType.DateTime)
            paramdtCreatedDate.Size = 8000
            paramdtCreatedDate.DbType = DbType.DateTime
            paramdtCreatedDate.Value = Convert.ToDateTime(dtCreatedDate)
            paramdtCreatedDate.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramdtCreatedDate)

            Dim paramRelatedLinkDispUrl As New OracleParameter("arg_Resources_DispTxt", OracleType.VarChar)
            paramRelatedLinkDispUrl.Size = 8000
            paramRelatedLinkDispUrl.DbType = DbType.String
            If (strResourcesDispUrl = "") Then
                paramRelatedLinkDispUrl.Value = DBNull.Value
            Else
                paramRelatedLinkDispUrl.Value = strResourcesDispUrl
            End If

            oracleCommand.Parameters.Add(paramRelatedLinkDispUrl)
            con.Open()
            oracleCommand.ExecuteNonQuery()

            con.Close()
        Catch ex As Exception

        End Try
        Return (1)

    End Function

    Public Function UpdateResourceUrl(ByVal intResourceID As Integer, ByVal strResourceUrlName As String, ByVal strSowUrl As String, ByVal dtUpdatedDate As Date, ByVal strResourcesDispTxt As String)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Try
            Dim oracleCommand = New OracleCommand()
            oracleCommand.Connection = con
            oracleCommand.CommandText = "SP_UPDATE_RESOURCES_URL"
            oracleCommand.CommandType = CommandType.StoredProcedure


            Dim paramResourceID As New OracleParameter("arg_Resource_ID", OracleType.Int32)
            paramResourceID.Size = 8000
            paramResourceID.DbType = DbType.Int32
            paramResourceID.Value = intResourceID
            oracleCommand.Parameters.Add(paramResourceID)

            Dim paramResouceUrl As New OracleParameter("arg_ResouceUrl", OracleType.VarChar)
            paramResouceUrl.Size = 8000
            paramResouceUrl.DbType = DbType.String
            paramResouceUrl.Value = strResourceUrlName
            oracleCommand.Parameters.Add(paramResouceUrl)

            Dim paramShowUrl As New OracleParameter("arg_ShowUrl", OracleType.VarChar)
            paramShowUrl.Size = 8000
            paramShowUrl.DbType = DbType.String
            paramShowUrl.Value = strSowUrl
            oracleCommand.Parameters.Add(paramShowUrl)

            Dim paramdtCreatedDate As New OracleParameter("arg_Updated_DateTime", OracleType.DateTime)
            paramdtCreatedDate.Size = 8000
            paramdtCreatedDate.DbType = DbType.DateTime
            paramdtCreatedDate.Value = Convert.ToDateTime(dtUpdatedDate)
            paramdtCreatedDate.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramdtCreatedDate)


            Dim paramResorcesDispTxt As New OracleParameter("arg_Resources_DispTxt", OracleType.DateTime)
            paramResorcesDispTxt.Size = 8000
            paramResorcesDispTxt.DbType = DbType.String
            paramResorcesDispTxt.Value = strResourcesDispTxt
            paramResorcesDispTxt.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramResorcesDispTxt)

            con.Open()
            oracleCommand.ExecuteNonQuery()

            oracleCommand.Parameters.Clear()
            con.Close()
        Catch ex As Exception

        End Try
        Return (1)

    End Function

    Public Function UpdateRelatedLinksUrl(ByVal intResourceID As Integer, ByVal strResourceUrlName As String, ByVal strSowUrl As String, ByVal dtUpdatedDate As Date, ByVal strRelatedLinkDispTxt As String)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Try
            Dim oracleCommand = New OracleCommand()
            oracleCommand.Connection = con
            oracleCommand.CommandText = "SP_UPDATE_RELATEDLINKS_URL"
            oracleCommand.CommandType = CommandType.StoredProcedure


            Dim paramResourceID As New OracleParameter("arg_RelatedLinks_ID", OracleType.Int32)
            paramResourceID.Size = 8000
            paramResourceID.DbType = DbType.Int32
            paramResourceID.Value = intResourceID
            oracleCommand.Parameters.Add(paramResourceID)

            Dim paramResouceUrl As New OracleParameter("arg_RelatedLinksUrl", OracleType.VarChar)
            paramResouceUrl.Size = 8000
            paramResouceUrl.DbType = DbType.String
            paramResouceUrl.Value = strResourceUrlName
            oracleCommand.Parameters.Add(paramResouceUrl)

            Dim paramShowUrl As New OracleParameter("arg_ShowUrl", OracleType.VarChar)
            paramShowUrl.Size = 8000
            paramShowUrl.DbType = DbType.String
            paramShowUrl.Value = strSowUrl
            oracleCommand.Parameters.Add(paramShowUrl)

            Dim paramdtCreatedDate As New OracleParameter("arg_Updated_DateTime", OracleType.DateTime)
            paramdtCreatedDate.Size = 8000
            paramdtCreatedDate.DbType = DbType.DateTime
            paramdtCreatedDate.Value = Convert.ToDateTime(dtUpdatedDate)
            paramdtCreatedDate.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramdtCreatedDate)

            Dim paramRelatedLinksDispTxt As New OracleParameter("arg_RelatedLinks_Disptxt", OracleType.VarChar)
            paramRelatedLinksDispTxt.Size = 8000
            paramRelatedLinksDispTxt.DbType = DbType.String
            paramRelatedLinksDispTxt.Value = strRelatedLinkDispTxt
            paramRelatedLinksDispTxt.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramRelatedLinksDispTxt)


            con.Open()
            oracleCommand.ExecuteNonQuery()

            oracleCommand.Parameters.Clear()
            con.Close()
        Catch ex As Exception

        End Try
        Return (1)

    End Function
    Public Function InsertNewRelatedLinksUrl(ByVal strRelatedLinksUrlName As String, ByVal strSowUrl As String, ByVal dtCreatedDate As Date, ByVal strRelatedLinkDispUrl As String)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Try
            Dim oracleCommand = New OracleCommand()
            oracleCommand.Parameters.Clear()
            oracleCommand.Connection = con
            oracleCommand.CommandText = "SP_INSERT_RELATEDLINKS_URL"
            oracleCommand.CommandType = CommandType.StoredProcedure
            Dim paramResouceUrl As New OracleParameter("arg_RelatedLinksUrl", OracleType.VarChar)
            paramResouceUrl.Size = 8000
            paramResouceUrl.DbType = DbType.String
            If (strRelatedLinksUrlName = "") Then
                paramResouceUrl.Value = DBNull.Value
            Else
                paramResouceUrl.Value = strRelatedLinksUrlName.ToString()
            End If

            oracleCommand.Parameters.Add(paramResouceUrl)

            Dim paramShowUrl As New OracleParameter("arg_ShowUrl", OracleType.VarChar)
            paramShowUrl.Size = 8000
            paramShowUrl.DbType = DbType.String
            paramShowUrl.Value = strSowUrl.ToString()
            oracleCommand.Parameters.Add(paramShowUrl)

            Dim paramdtCreatedDate As New OracleParameter("arg_Created_DateTime", OracleType.DateTime)
            paramdtCreatedDate.Size = 8000
            paramdtCreatedDate.DbType = DbType.DateTime
            paramdtCreatedDate.Value = Convert.ToDateTime(dtCreatedDate)
            paramdtCreatedDate.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramdtCreatedDate)


            Dim paramRelatedLinkDispUrl As New OracleParameter("arg_RelatedLinks_disptxt", OracleType.VarChar)
            paramRelatedLinkDispUrl.Size = 8000
            paramRelatedLinkDispUrl.DbType = DbType.String
            If (strRelatedLinkDispUrl = "") Then
                paramRelatedLinkDispUrl.Value = DBNull.Value
            Else
                paramRelatedLinkDispUrl.Value = strRelatedLinkDispUrl.ToString()
            End If


            oracleCommand.Parameters.Add(paramRelatedLinkDispUrl)


            con.Open()
            oracleCommand.ExecuteNonQuery()
            oracleCommand.Parameters.Clear()
            con.Close()
        Catch ex As Exception

        End Try
        Return (1)

    End Function

    Public Function InsertCompany(ByVal strCompanyName As String, ByVal fileName As String, ByVal fileType As String, ByVal fileBuffer() As Byte, Optional ByVal CreatedBy As String = "", Optional ByVal CreatedOn As String = "")

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        con.Open()
        Dim transaction As OracleTransaction

        transaction = con.BeginTransaction()

        Dim oracleCommand = New OracleCommand()
        oracleCommand.Transaction = transaction

        oracleCommand.Connection = con
        oracleCommand.CommandText = "declare xx blob; begin dbms_lob.createtemporary(xx, false, 0); :tempblob := xx; end;"
        oracleCommand.Parameters.Add(New OracleParameter("tempblob", OracleType.Blob)).Direction = ParameterDirection.Output
        oracleCommand.ExecuteNonQuery()
        Dim tempLob As OracleLob

        ' Getting the content of the file...
        Dim buffer As Byte() = fileBuffer


        'Oracle object responsible for storing the File content.
        'Assigning tempLob the blob object created on the database.

        tempLob = CType(oracleCommand.Parameters(0).Value, OracleLob)
        tempLob.BeginBatch(OracleLobOpenMode.ReadWrite)
        'Writing the file content to tempLob.
        If Not (buffer Is Nothing) Then
            tempLob.Write(buffer, 0, buffer.Length)
            tempLob.EndBatch()
        End If
        oracleCommand.Parameters.Clear()

        oracleCommand.CommandText = "SP_INSERT_COMPANY"
        oracleCommand.CommandType = CommandType.StoredProcedure
        oracleCommand.Parameters.Add(New OracleParameter("arg_COMPANYNAME", OracleType.VarChar)).Value = strCompanyName
        oracleCommand.Parameters.Add(New OracleParameter("arg_COMPANY_LOGO_FILENAME", OracleType.VarChar)).Value = fileName
        oracleCommand.Parameters.Add(New OracleParameter("arg_LOGO_FILE_TYPE", OracleType.VarChar)).Value = fileType
        oracleCommand.Parameters.Add(New OracleParameter("arg_FILE_CONTENT", OracleType.Blob)).Value = IIf(buffer Is Nothing, DBNull.Value, tempLob)
        oracleCommand.Parameters.Add(New OracleParameter("arg_Created_By", OracleType.VarChar)).Value = CreatedBy
        oracleCommand.Parameters.Add(New OracleParameter("arg_Created_On", OracleType.DateTime)).Value = CreatedOn

        Dim retValParamone As New OracleParameter("arg_Company_ID", OleDbType.Integer)
        retValParamone.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParamone)
        Dim intCompanyD As Integer = 0

        Try
            oracleCommand.ExecuteNonQuery()
            ' intCompanyD = 0
            If (retValParamone.Value.ToString() <> System.DBNull.Value.ToString()) Then
                intCompanyD = CType(retValParamone.Value, Integer)
            End If
        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)


        End Try

        transaction.Commit()

        con.Close()

        Return (intCompanyD)
    End Function

    Public Function UpdateCompany(ByVal strCompanyName As String, ByVal strCompanyID As String, ByVal fileName As String, ByVal fileType As String, ByVal fileBuffer() As Byte, Optional ByVal UpdatedBy As String = "", Optional ByVal UpdatedOn As String = "")

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        con.Open()
        Dim transaction As OracleTransaction

        transaction = con.BeginTransaction()

        Dim oracleCommand = New OracleCommand()
        oracleCommand.Transaction = transaction

        oracleCommand.Connection = con
        oracleCommand.CommandText = "declare xx blob; begin dbms_lob.createtemporary(xx, false, 0); :tempblob := xx; end;"
        oracleCommand.Parameters.Add(New OracleParameter("tempblob", OracleType.Blob)).Direction = ParameterDirection.Output
        oracleCommand.ExecuteNonQuery()
        Dim tempLob As OracleLob = Nothing
        ' Getting the content of the file...
        Dim buffer As Byte() = fileBuffer


        'Oracle object responsible for storing the File content.
        'Assigning tempLob the blob object created on the database.

        tempLob = CType(oracleCommand.Parameters(0).Value, OracleLob)
        tempLob.BeginBatch(OracleLobOpenMode.ReadWrite)
        'Writing the file content to tempLob.
        If Not (buffer Is Nothing) Then
            tempLob.Write(buffer, 0, buffer.Length)
            tempLob.EndBatch()

        End If
        oracleCommand.Parameters.Clear()
        oracleCommand.CommandText = "SP_UPDATE_COMPANY"
        oracleCommand.CommandType = CommandType.StoredProcedure
        oracleCommand.Parameters.Add(New OracleParameter("arg_COMPANYID", OracleType.VarChar)).Value = strCompanyID
        oracleCommand.Parameters.Add(New OracleParameter("arg_COMPANYNAME", OracleType.VarChar)).Value = strCompanyName
        oracleCommand.Parameters.Add(New OracleParameter("arg_COMPANY_LOGO_FILENAME", OracleType.VarChar)).Value = fileName
        oracleCommand.Parameters.Add(New OracleParameter("arg_LOGO_FILE_TYPE", OracleType.VarChar)).Value = fileType
        oracleCommand.Parameters.Add(New OracleParameter("arg_FILE_CONTENT", OracleType.Blob)).Value = IIf(buffer Is Nothing, DBNull.Value, tempLob)
        oracleCommand.Parameters.Add(New OracleParameter("arg_Updated_By", OracleType.VarChar)).Value = UpdatedBy
        oracleCommand.Parameters.Add(New OracleParameter("arg_Updated_On", OracleType.DateTime)).Value = UpdatedOn

        Try
            oracleCommand.ExecuteNonQuery()
        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)


        End Try

        transaction.Commit()

        con.Close()

        Return (1)
    End Function

    'Public Function InsertCompany(ByVal strCompanyName As String, ByVal fileName As String, ByVal fileType As String, ByVal fileBuffer() As Byte)

    '    Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

    '    con.Open()
    '    Dim transaction As OracleTransaction

    '    transaction = con.BeginTransaction()

    '    Dim oracleCommand = New OracleCommand()
    '    oracleCommand.Transaction = transaction

    '    oracleCommand.Connection = con
    '    oracleCommand.CommandText = "declare xx blob; begin dbms_lob.createtemporary(xx, false, 0); :tempblob := xx; end;"
    '    oracleCommand.Parameters.Add(New OracleParameter("tempblob", OracleType.Blob)).Direction = ParameterDirection.Output
    '    oracleCommand.ExecuteNonQuery()
    '    Dim tempLob As OracleLob

    '    ' Getting the content of the file...
    '    Dim buffer As Byte() = fileBuffer


    '    'Oracle object responsible for storing the File content.
    '    'Assigning tempLob the blob object created on the database.

    '    tempLob = CType(oracleCommand.Parameters(0).Value, OracleLob)
    '    tempLob.BeginBatch(OracleLobOpenMode.ReadWrite)
    '    'Writing the file content to tempLob.
    '    If Not (buffer Is Nothing) Then
    '        tempLob.Write(buffer, 0, buffer.Length)
    '        tempLob.EndBatch()
    '    End If
    '    oracleCommand.Parameters.Clear()

    '    oracleCommand.CommandText = "SP_INSERT_COMPANY"
    '    oracleCommand.CommandType = CommandType.StoredProcedure
    '    oracleCommand.Parameters.Add(New OracleParameter("arg_COMPANYNAME", OracleType.VarChar)).Value = strCompanyName
    '    oracleCommand.Parameters.Add(New OracleParameter("arg_COMPANY_LOGO_FILENAME", OracleType.VarChar)).Value = fileName
    '    oracleCommand.Parameters.Add(New OracleParameter("arg_LOGO_FILE_TYPE", OracleType.VarChar)).Value = fileType
    '    oracleCommand.Parameters.Add(New OracleParameter("arg_FILE_CONTENT", OracleType.Blob)).Value = IIf(buffer Is Nothing, DBNull.Value, tempLob)

    '    Dim retValParamone As New OracleParameter("arg_Company_ID", OleDbType.Integer)
    '    retValParamone.Direction = ParameterDirection.Output
    '    oracleCommand.Parameters.Add(retValParamone)
    '    Dim intCompanyD As Integer = 0

    '    Try
    '        oracleCommand.ExecuteNonQuery()
    '        ' intCompanyD = 0
    '        If (retValParamone.Value.ToString() <> System.DBNull.Value.ToString()) Then
    '            intCompanyD = CType(retValParamone.Value, Integer)
    '        End If
    '    Catch ex As Exception
    '        transaction.Rollback()
    '        Throw New Exception(ex.Message)


    '    End Try

    '    transaction.Commit()

    '    con.Close()

    '    Return (intCompanyD)
    'End Function

    'Public Function UpdateCompany(ByVal strCompanyName As String, ByVal strCompanyID As String, ByVal fileName As String, ByVal fileType As String, ByVal fileBuffer() As Byte)

    '    Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

    '    con.Open()
    '    Dim transaction As OracleTransaction

    '    transaction = con.BeginTransaction()

    '    Dim oracleCommand = New OracleCommand()
    '    oracleCommand.Transaction = transaction

    '    oracleCommand.Connection = con
    '    oracleCommand.CommandText = "declare xx blob; begin dbms_lob.createtemporary(xx, false, 0); :tempblob := xx; end;"
    '    oracleCommand.Parameters.Add(New OracleParameter("tempblob", OracleType.Blob)).Direction = ParameterDirection.Output
    '    oracleCommand.ExecuteNonQuery()
    '    Dim tempLob As OracleLob = Nothing
    '    ' Getting the content of the file...
    '    Dim buffer As Byte() = fileBuffer


    '    'Oracle object responsible for storing the File content.
    '    'Assigning tempLob the blob object created on the database.

    '    tempLob = CType(oracleCommand.Parameters(0).Value, OracleLob)
    '    tempLob.BeginBatch(OracleLobOpenMode.ReadWrite)
    '    'Writing the file content to tempLob.
    '    If Not (buffer Is Nothing) Then
    '        tempLob.Write(buffer, 0, buffer.Length)
    '        tempLob.EndBatch()

    '    End If
    '    oracleCommand.Parameters.Clear()
    '    oracleCommand.CommandText = "SP_UPDATE_COMPANY"
    '    oracleCommand.CommandType = CommandType.StoredProcedure
    '    oracleCommand.Parameters.Add(New OracleParameter("arg_COMPANYID", OracleType.VarChar)).Value = strCompanyID
    '    oracleCommand.Parameters.Add(New OracleParameter("arg_COMPANYNAME", OracleType.VarChar)).Value = strCompanyName
    '    oracleCommand.Parameters.Add(New OracleParameter("arg_COMPANY_LOGO_FILENAME", OracleType.VarChar)).Value = fileName
    '    oracleCommand.Parameters.Add(New OracleParameter("arg_LOGO_FILE_TYPE", OracleType.VarChar)).Value = fileType
    '    oracleCommand.Parameters.Add(New OracleParameter("arg_FILE_CONTENT", OracleType.Blob)).Value = IIf(buffer Is Nothing, DBNull.Value, tempLob)

    '    Try
    '        oracleCommand.ExecuteNonQuery()
    '    Catch ex As Exception
    '        transaction.Rollback()
    '        Throw New Exception(ex.Message)


    '    End Try

    '    transaction.Commit()

    '    con.Close()

    '    Return (1)
    'End Function



    Public Function GetAuditTrail()

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        Dim oracleCommand = New OracleCommand()
        oracleCommand.Connection = con
        oracleCommand.CommandText = "Sp_Get_AuditTrail"
        oracleCommand.CommandType = CommandType.StoredProcedure
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
        con.Open()
        Dim auditDataSet As New DataSet()
        oracleDbDataAdapter.Fill(auditDataSet)
        con.Close()

        Return (auditDataSet)


    End Function



    Public Function GetAuditTrailByID(ByVal intAuditID As Integer)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        Dim oracleCommand = New OracleCommand()
        oracleCommand.Connection = con
        oracleCommand.CommandText = "Sp_Get_AuditTrailByID"
        oracleCommand.CommandType = CommandType.StoredProcedure
        oracleCommand.Parameters.Add(New OracleParameter("arg_AuditID", OleDbType.Integer)).Value = intAuditID
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
        con.Open()
        Dim auditDataSet As New DataSet()
        oracleDbDataAdapter.Fill(auditDataSet)
        con.Close()

        Return (auditDataSet)


    End Function


    Public Function GetCompanyLogoByID(ByVal strCompanyID As String)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        Dim oracleCommand = New OracleCommand()
        oracleCommand.Connection = con
        oracleCommand.CommandText = "SP_GET_COMPANYLOGO"
        oracleCommand.CommandType = CommandType.StoredProcedure
        oracleCommand.Parameters.Add(New OracleParameter("arg_company_id", OleDbType.VarChar)).Value = strCompanyID
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
        con.Open()
        Dim companyDataSet As New DataSet()
        oracleDbDataAdapter.Fill(companyDataSet)
        con.Close()

        Return (companyDataSet)


    End Function


    Public Function GetCompanyDetails()

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        Dim oracleCommand = New OracleCommand()
        oracleCommand.Connection = con
        oracleCommand.CommandText = "sp_get_company"
        oracleCommand.CommandType = CommandType.StoredProcedure
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
        con.Open()
        Dim companyDS As New DataSet()
        oracleDbDataAdapter.Fill(companyDS)
        con.Close()

        Return (companyDS)


    End Function

    Public Function GetPolicyType()

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        Dim oracleCommand = New OracleCommand()
        oracleCommand.Connection = con
        oracleCommand.CommandText = "sp_get_TypeOfInsurance"
        oracleCommand.CommandType = CommandType.StoredProcedure
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
        con.Open()
        Dim policyTypeDS As New DataSet()
        oracleDbDataAdapter.Fill(policyTypeDS)
        con.Close()

        Return (policyTypeDS)


    End Function

    Public Function GetResourcesUrl()

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        Dim oracleCommand = New OracleCommand()
        oracleCommand.Connection = con
        oracleCommand.CommandText = "SP_GET_RESOURCES_URL"
        oracleCommand.CommandType = CommandType.StoredProcedure
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
        con.Open()
        Dim resourcesDS As New DataSet()
        oracleDbDataAdapter.Fill(resourcesDS)
        con.Close()

        Return (resourcesDS)


    End Function

    Public Function GetRelatedLinksUrl()

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        Dim oracleCommand = New OracleCommand()
        oracleCommand.Connection = con
        oracleCommand.CommandText = "SP_GET_RELATEDLINKS_URL"
        oracleCommand.CommandType = CommandType.StoredProcedure
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
        con.Open()
        Dim relatedLinksDS As New DataSet()
        oracleDbDataAdapter.Fill(relatedLinksDS)
        con.Close()

        Return (relatedLinksDS)


    End Function

    Public Function DeleteFileByID(ByVal intFileID As Integer)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Try
            Dim oracleCommand = New OracleCommand()
            oracleCommand.Connection = con
            oracleCommand.CommandText = "Sp_Delete_FileBy_ID"
            oracleCommand.CommandType = CommandType.StoredProcedure
            Dim paramFileID As New OracleParameter("arrID", OracleType.Int16)
            paramFileID.Size = 8000
            paramFileID.DbType = DbType.String
            paramFileID.Value = intFileID
            oracleCommand.Parameters.Add(paramFileID)
            con.Open()
            oracleCommand.ExecuteNonQuery()
            oracleCommand.Parameters.Clear()
            con.Close()
        Catch ex As Exception

        End Try
        Return (1)

    End Function


    Public Function DeleteRateChanges(ByVal intRateChangeID As Integer)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Try
            Dim oracleCommand = New OracleCommand()
            oracleCommand.Connection = con
            oracleCommand.CommandText = "SP_Delete_RateChanges"
            oracleCommand.CommandType = CommandType.StoredProcedure
            Dim paramRateID As New OracleParameter("arg_RateChangesID", OracleType.Int32)
            paramRateID.Size = 8000
            paramRateID.DbType = DbType.Int32
            paramRateID.Value = intRateChangeID
            oracleCommand.Parameters.Add(paramRateID)
            con.Open()
            oracleCommand.ExecuteNonQuery()
            oracleCommand.Parameters.Clear()
            con.Close()
        Catch ex As Exception

        End Try
        Return (1)

    End Function


    Public Function DeleteResourcesUrl(ByVal intResourceID As Integer)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Try
            Dim oracleCommand = New OracleCommand()
            oracleCommand.Connection = con
            oracleCommand.CommandText = "SP_DELETE_RESOURCE_URL"
            oracleCommand.CommandType = CommandType.StoredProcedure
            Dim paramRateID As New OracleParameter("arg_ResourceID", OracleType.Int32)
            paramRateID.Size = 8000
            paramRateID.DbType = DbType.Int32
            paramRateID.Value = intResourceID
            oracleCommand.Parameters.Add(paramRateID)
            con.Open()
            oracleCommand.ExecuteNonQuery()
            oracleCommand.Parameters.Clear()
            con.Close()
        Catch ex As Exception

        End Try
        Return (1)

    End Function

    Public Function DeleteRelatedLinksUrl(ByVal intRelatedLinksID As Integer)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Try
            Dim oracleCommand = New OracleCommand()
            oracleCommand.Connection = con
            oracleCommand.CommandText = "SP_DELETE_RELATEDLINKS_URL"
            oracleCommand.CommandType = CommandType.StoredProcedure
            Dim paramRateID As New OracleParameter("arg_RelatedLinks_ID", OracleType.Int32)
            paramRateID.Size = 8000
            paramRateID.DbType = DbType.Int32
            paramRateID.Value = intRelatedLinksID
            oracleCommand.Parameters.Add(paramRateID)
            con.Open()
            oracleCommand.ExecuteNonQuery()
            oracleCommand.Parameters.Clear()
            con.Close()
        Catch ex As Exception

        End Try
        Return (1)

    End Function



    Public Function InsertRateChanges(ByVal strCompanyID As String, ByVal strPolicyFormNo As String, ByVal strPolicyType As String, ByVal dtDateCurrRateInc As Date, _
                    ByVal PercentCurrtRateInc As Double, _
                    ByVal dtLastFiveYrRateIn_2010 As Date, ByVal dtLastFiveYrRateIn_2009 As Date, _
                    ByVal dtLastFiveYrRateIn_2008 As Date, ByVal dtLastFiveYrRateIn_2007 As Date, _
                    ByVal dtLastFiveYrRateIn_2006 As Date, ByVal PcntLast5YrRateInc_2010 As Object, _
                    ByVal PcntLast5YrRateInc_2009 As Object, ByVal PcntLast5YrRateInc_2008 As Object, _
                    ByVal PcntLast5YrRateInc_2007 As Object, ByVal PcntLast5YrRateInc_2006 As Object, _
                    ByVal last5YrLossRatio_2010 As Object, ByVal last5YrLossRatio_2009 As Object, _
                    ByVal last5YrLossRatio_2008 As Object, ByVal last5YrLossRatio_2007 As Object, ByVal last5YrLossRatio_2006 As Object, _
                    ByVal NumOfMsInsureds_2010 As Object, ByVal NumOfMsInsureds_2009 As Object, ByVal NumOfMsInsureds_2008 As Object, _
                     ByVal NumOfMsInsureds_2007 As Object, ByVal NumOfMsInsureds_2006 As Object, ByVal strUser As String, ByVal strSummaryRateInc As String, ByVal dtPolicyApprl As Date, ByVal dtDateSubmitted As Date, ByVal strLinkToRateInc As String)

        Dim intRateID As Integer = 0
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Try
            Dim oracleCommand = New OracleCommand()
            oracleCommand.Connection = con
            oracleCommand.CommandText = "sp_INSERT_RATECHANGES"
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.Parameters.Clear()

            Dim paramCompanyID As New OracleParameter("arg_COMPANYID", OracleType.VarChar)
            paramCompanyID.Size = 8000
            paramCompanyID.DbType = DbType.String
            paramCompanyID.Value = strCompanyID
            paramCompanyID.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramCompanyID)

            Dim paramPolicyForm As New OracleParameter("arg_POLICYFORMNO", OracleType.VarChar)
            paramPolicyForm.Size = 8000
            paramPolicyForm.DbType = DbType.String
            paramPolicyForm.Value = strPolicyFormNo
            paramPolicyForm.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramPolicyForm)


            Dim paramPolicyType As New OracleParameter("arg_PTYPECATEGORY_SECONDARY", OracleType.VarChar)
            paramPolicyType.Size = 8000
            paramPolicyType.DbType = DbType.String
            paramPolicyType.Value = strPolicyType.ToString()
            paramPolicyType.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramPolicyType)


            Dim paramdtCurrRateInc As New OracleParameter("arg_DATEOF_CURRENT_RATEINCR", OracleType.DateTime)
            paramdtCurrRateInc.Size = 8000
            paramdtCurrRateInc.DbType = DbType.DateTime
            paramdtCurrRateInc.Value = Convert.ToDateTime(dtDateCurrRateInc)
            paramdtCurrRateInc.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramdtCurrRateInc)


            Dim paramPercentRateInc As New OracleParameter("arg_PECT_CURRRNT_RATEINCR", OracleType.Double)
            paramPercentRateInc.Size = 8000
            paramPercentRateInc.DbType = DbType.Double
            paramPercentRateInc.Value = PercentCurrtRateInc
            paramPercentRateInc.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramPercentRateInc)

            Dim paramDateLastFiveRateInc10 As New OracleParameter("arg_DATEOF_2010_RATECHANGES", OracleType.DateTime)
            paramDateLastFiveRateInc10.Size = 8000
            paramDateLastFiveRateInc10.DbType = DbType.DateTime
            paramDateLastFiveRateInc10.Value = Convert.ToDateTime(dtLastFiveYrRateIn_2010)
            paramDateLastFiveRateInc10.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramDateLastFiveRateInc10)

            Dim paramDateLastFiveRateInc9 As New OracleParameter("arg_DATEOF_2009_RATECHANGES", OracleType.DateTime)
            paramDateLastFiveRateInc9.Size = 8000
            paramDateLastFiveRateInc9.DbType = DbType.DateTime
            paramDateLastFiveRateInc9.Value = Convert.ToDateTime(dtLastFiveYrRateIn_2009)
            paramDateLastFiveRateInc9.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramDateLastFiveRateInc9)

            Dim paramDateLastFiveRateInc8 As New OracleParameter("arg_DATEOF_2008_RATECHANGES", OracleType.DateTime)
            paramDateLastFiveRateInc8.Size = 8000
            paramDateLastFiveRateInc8.DbType = DbType.DateTime
            paramDateLastFiveRateInc8.Value = Convert.ToDateTime(dtLastFiveYrRateIn_2008)
            paramDateLastFiveRateInc8.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramDateLastFiveRateInc8)

            Dim paramDateLastFiveRateInc7 As New OracleParameter("arg_DATEOF_2007_RATECHANGES", OracleType.DateTime)
            paramDateLastFiveRateInc7.Size = 8000
            paramDateLastFiveRateInc7.DbType = DbType.DateTime
            paramDateLastFiveRateInc7.Value = Convert.ToDateTime(dtLastFiveYrRateIn_2007)
            paramDateLastFiveRateInc7.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramDateLastFiveRateInc7)

            Dim paramDateLastFiveRateInc06 As New OracleParameter("arg_DATEOF_2006_RATECHANGES", OracleType.DateTime)
            paramDateLastFiveRateInc06.Size = 8000
            paramDateLastFiveRateInc06.DbType = DbType.DateTime
            paramDateLastFiveRateInc06.Value = Convert.ToDateTime(dtLastFiveYrRateIn_2006)
            paramDateLastFiveRateInc06.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramDateLastFiveRateInc06)

            'Arguments for Percetage Last Five Years Rate changes
            Dim parmPcntLast5YrRateInc_2010 As New OracleParameter("arg_PERCENT_2010_RATEINC", OracleType.Double)
            parmPcntLast5YrRateInc_2010.Size = 8000
            parmPcntLast5YrRateInc_2010.DbType = DbType.Double
            parmPcntLast5YrRateInc_2010.Value = PcntLast5YrRateInc_2010
            parmPcntLast5YrRateInc_2010.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmPcntLast5YrRateInc_2010)

            Dim parmPcntLast5YrRateInc_2009 As New OracleParameter("arg_PERCENT_2009_RATEINC", OracleType.Double)
            parmPcntLast5YrRateInc_2009.Size = 8000
            parmPcntLast5YrRateInc_2009.DbType = DbType.Double
            parmPcntLast5YrRateInc_2009.Value = PcntLast5YrRateInc_2009
            parmPcntLast5YrRateInc_2009.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmPcntLast5YrRateInc_2009)

            Dim parmPcntLast5YrRateInc_2008 As New OracleParameter("arg_PERCENT_2008_RATEINC", OracleType.Double)
            parmPcntLast5YrRateInc_2008.Size = 8000
            parmPcntLast5YrRateInc_2008.DbType = DbType.Double
            parmPcntLast5YrRateInc_2008.Value = PcntLast5YrRateInc_2008
            parmPcntLast5YrRateInc_2008.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmPcntLast5YrRateInc_2008)

            Dim parmPcntLast5YrRateInc_2007 As New OracleParameter("arg_PERCENT_2007_RATEINC", OracleType.Double)
            parmPcntLast5YrRateInc_2007.Size = 8000
            parmPcntLast5YrRateInc_2007.DbType = DbType.Double
            parmPcntLast5YrRateInc_2007.Value = PcntLast5YrRateInc_2007
            parmPcntLast5YrRateInc_2007.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmPcntLast5YrRateInc_2007)

            Dim parmPcntLast5YrRateInc_2006 As New OracleParameter("arg_PERCENT_2006_RATEINC", OracleType.Double)
            parmPcntLast5YrRateInc_2006.Size = 8000
            parmPcntLast5YrRateInc_2006.DbType = DbType.Double
            parmPcntLast5YrRateInc_2006.Value = PcntLast5YrRateInc_2006
            parmPcntLast5YrRateInc_2006.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmPcntLast5YrRateInc_2006)

            'Arguments for Last Five Years Loss Ratio
            Dim parmLast5YrLossRatio_2010 As New OracleParameter("arg_LAST_LOSSRATIO_2010", OracleType.Double)
            parmLast5YrLossRatio_2010.Size = 8000
            parmLast5YrLossRatio_2010.DbType = DbType.Double
            parmLast5YrLossRatio_2010.Value = last5YrLossRatio_2010
            parmLast5YrLossRatio_2010.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmLast5YrLossRatio_2010)

            Dim parmLast5YrLossRatio_2009 As New OracleParameter("arg_LAST_LOSSRATIO_2009", OracleType.Double)
            parmLast5YrLossRatio_2009.Size = 8000
            parmLast5YrLossRatio_2009.DbType = DbType.Double
            parmLast5YrLossRatio_2009.Value = last5YrLossRatio_2009
            parmLast5YrLossRatio_2009.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmLast5YrLossRatio_2009)

            Dim parmLast5YrLossRatio_2008 As New OracleParameter("arg_LAST_LOSSRATIO_2008", OracleType.Double)
            parmLast5YrLossRatio_2008.Size = 8000
            parmLast5YrLossRatio_2008.DbType = DbType.Double
            parmLast5YrLossRatio_2008.Value = last5YrLossRatio_2008
            parmLast5YrLossRatio_2008.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmLast5YrLossRatio_2008)

            Dim parmLast5YrLossRatio_2007 As New OracleParameter("arg_LAST_LOSSRATIO_2007", OracleType.Double)
            parmLast5YrLossRatio_2007.Size = 8000
            parmLast5YrLossRatio_2007.DbType = DbType.Double
            parmLast5YrLossRatio_2007.Value = last5YrLossRatio_2007
            parmLast5YrLossRatio_2007.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmLast5YrLossRatio_2007)

            Dim parmLast5YrLossRatio_2006 As New OracleParameter("arg_LAST_LOSSRATIO_2006", OracleType.Double)
            parmLast5YrLossRatio_2006.Size = 8000
            parmLast5YrLossRatio_2006.DbType = DbType.Double
            parmLast5YrLossRatio_2006.Value = last5YrLossRatio_2006
            parmLast5YrLossRatio_2006.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmLast5YrLossRatio_2006)


            Dim paramNumOfInsured10 As New OracleParameter("arg_NUMOF_MSINSURED_2010", OracleType.Int32)
            paramNumOfInsured10.Size = 8000
            paramNumOfInsured10.DbType = DbType.Int32
            paramNumOfInsured10.Value = NumOfMsInsureds_2010
            paramNumOfInsured10.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramNumOfInsured10)


            Dim paramNumOfInsured09 As New OracleParameter("arg_NUMOF_MSINSURED_2009", OracleType.Int32)
            paramNumOfInsured09.Size = 8000
            paramNumOfInsured09.DbType = DbType.Int32
            paramNumOfInsured09.Value = NumOfMsInsureds_2009
            paramNumOfInsured09.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramNumOfInsured09)


            Dim paramNumOfInsured08 As New OracleParameter("arg_NUMOF_MSINSURED_2008", OracleType.Int32)
            paramNumOfInsured08.Size = 8000
            paramNumOfInsured08.DbType = DbType.Int32
            paramNumOfInsured08.Value = NumOfMsInsureds_2008
            paramNumOfInsured08.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramNumOfInsured08)


            Dim paramNumOfInsured07 As New OracleParameter("arg_NUMOF_MSINSURED_2007", OracleType.Int32)
            paramNumOfInsured07.Size = 8000
            paramNumOfInsured07.DbType = DbType.Int32
            paramNumOfInsured07.Value = NumOfMsInsureds_2007
            paramNumOfInsured07.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramNumOfInsured07)


            Dim paramNumOfInsured06 As New OracleParameter("arg_NUMOF_MSINSURED_2006", OracleType.Int32)
            paramNumOfInsured06.Size = 8000
            paramNumOfInsured06.DbType = DbType.Int32
            paramNumOfInsured06.Value = NumOfMsInsureds_2006
            paramNumOfInsured06.Direction = ParameterDirection.Input
            Dim retValParam As New OracleParameter("arg_RATTECHNAGES_ID", OleDbType.Integer)
            retValParam.Direction = ParameterDirection.Output
            oracleCommand.Parameters.Add(retValParam)

            oracleCommand.Parameters.Add(paramNumOfInsured06)

            Dim paramActionUser As New OracleParameter("arg_ACTION_USER", OracleType.VarChar)
            paramActionUser.Size = 8000
            paramActionUser.DbType = DbType.String
            paramActionUser.Value = strUser
            paramActionUser.Direction = ParameterDirection.Input

            oracleCommand.Parameters.Add(paramActionUser)

            Dim paramSummaryRateInc As New OracleParameter("arg_SUMMARY_RATEINC", OracleType.VarChar)
            paramSummaryRateInc.Size = 8000
            paramSummaryRateInc.DbType = DbType.String
            paramSummaryRateInc.IsNullable = True
            paramSummaryRateInc.Value = strSummaryRateInc
            paramSummaryRateInc.Direction = ParameterDirection.Input

            oracleCommand.Parameters.Add(paramSummaryRateInc)

            Dim paramPolicyApproval As New OracleParameter("arg_POLICY_APPROVAL", OracleType.DateTime)
            paramPolicyApproval.Size = 8000
            paramPolicyApproval.DbType = DbType.DateTime
            paramPolicyApproval.Value = Convert.ToDateTime(dtPolicyApprl)
            paramPolicyApproval.Direction = ParameterDirection.Input

            oracleCommand.Parameters.Add(paramPolicyApproval)

            Dim retValParamone As New OracleParameter("IsCompany_Exist", OleDbType.Integer)
            retValParamone.Direction = ParameterDirection.Output
            oracleCommand.Parameters.Add(retValParamone)
            ''Dim retValParamCompany As New OracleParameter("IsCompany_Exist", OleDbType.Numeric)
            ''retValParamCompany.Direction = ParameterDirection.Output
            ''oracleCommand.Parameters.Add(retValParamCompany)



            Dim paramDateSubmitted As New OracleParameter("arg_DATE_SUBMITTED", OracleType.DateTime)
            paramDateSubmitted.Size = 8000
            paramDateSubmitted.DbType = DbType.DateTime
            paramDateSubmitted.Value = Convert.ToDateTime(dtDateSubmitted)
            paramDateSubmitted.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramDateSubmitted)

            Dim paramLinkToRateInc As New OracleParameter("arg_LINK_RATE_INCREASE", OracleType.VarChar)
            paramLinkToRateInc.Size = 8000
            paramLinkToRateInc.DbType = DbType.String
            paramLinkToRateInc.IsNullable = True
            paramLinkToRateInc.Value = strLinkToRateInc
            paramLinkToRateInc.Direction = ParameterDirection.Input

            oracleCommand.Parameters.Add(paramLinkToRateInc)

            con.Open()


            'Dim intRowAffted As Integer = New Integer
            'Dim rowid As System.Data.OracleClient.OracleString
            'intRowAffted = oracleCommand.ExecuteOracleNonQuery(rowid)
            oracleCommand.ExecuteNonQuery()
            oracleCommand.Parameters.Clear()
            con.Close()
            If (retValParam.Value.ToString() <> System.DBNull.Value.ToString()) Then
                intRateID = retValParam.Value
            End If
        Catch ex As Exception

        End Try
        Return (intRateID)
    End Function



    Public Function UpdateRateChanges(ByVal intRateChangesID As Integer, ByVal strCompanyID As String, ByVal strCompanyName As String, _
                    ByVal strPolicyFormNo As String, ByVal strPolicyType As String, ByVal dtDateCurrRateInc As Date, _
                    ByVal PercentCurrtRateInc As Double, _
                    ByVal dtLastFiveYrRateIn_2010 As Date, ByVal dtLastFiveYrRateIn_2009 As Date, _
                    ByVal dtLastFiveYrRateIn_2008 As Date, ByVal dtLastFiveYrRateIn_2007 As Date, _
                    ByVal dtLastFiveYrRateIn_2006 As Date, ByVal PcntLast5YrRateInc_2010 As Object, _
                    ByVal PcntLast5YrRateInc_2009 As Object, ByVal PcntLast5YrRateInc_2008 As Object, _
                    ByVal PcntLast5YrRateInc_2007 As Object, ByVal PcntLast5YrRateInc_2006 As Object, _
                    ByVal last5YrLossRatio_2010 As Object, ByVal last5YrLossRatio_2009 As Object, _
                    ByVal last5YrLossRatio_2008 As Object, ByVal last5YrLossRatio_2007 As Object, ByVal last5YrLossRatio_2006 As Object, _
                    ByVal NumOfMsInsureds_2010 As Object, ByVal NumOfMsInsureds_2009 As Object, ByVal NumOfMsInsureds_2008 As Object, _
                    ByVal NumOfMsInsureds_2007 As Object, ByVal NumOfMsInsureds_2006 As Object, ByVal strUser As String, ByVal strSummaryRateInc As String, ByVal dtPolicyApprl As Date, ByVal dtDateSubmitted As Date, ByVal strLinkToRateInc As String)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Try
            Dim oracleCommand = New OracleCommand()
            oracleCommand.Connection = con
            oracleCommand.CommandText = "SP_UPDATE_RATECHANGES"
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.Parameters.Clear()


            Dim paramRateChangesID As New OracleParameter("arg_RATECHANGES_ID", OleDbType.Integer)
            paramRateChangesID.Size = 8000
            paramRateChangesID.DbType = DbType.Int32
            paramRateChangesID.Value = Convert.ToInt32(intRateChangesID)
            paramRateChangesID.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramRateChangesID)

            Dim paramCompanyID As New OracleParameter("arg_COMPANYID", OracleType.VarChar)
            paramCompanyID.Size = 8000
            paramCompanyID.DbType = DbType.String
            paramCompanyID.Value = strCompanyID.ToString()
            paramCompanyID.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramCompanyID)

            Dim paramPolicyForm As New OracleParameter("arg_POLICYFORMNO", OracleType.VarChar)
            paramPolicyForm.Size = 8000
            paramPolicyForm.DbType = DbType.String
            paramPolicyForm.Value = strPolicyFormNo.ToString()
            paramPolicyForm.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramPolicyForm)


            Dim paramPolicyType As New OracleParameter("arg_PTYPECATEGORY_SECONDARY", OracleType.VarChar)
            paramPolicyType.Size = 8000
            paramPolicyType.DbType = DbType.String
            paramPolicyType.Value = strPolicyType.ToString().Trim()
            paramPolicyType.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramPolicyType)


            Dim paramdtCurrRateInc As New OracleParameter("arg_DATEOF_CURRENT_RATEINCR", OracleType.DateTime)
            paramdtCurrRateInc.Size = 8000
            paramdtCurrRateInc.DbType = DbType.DateTime
            paramdtCurrRateInc.Value = Convert.ToDateTime(dtDateCurrRateInc)
            paramdtCurrRateInc.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramdtCurrRateInc)


            Dim paramPercentRateInc As New OracleParameter("arg_PECT_CURRRNT_RATEINCR", OracleType.Double)
            paramPercentRateInc.Size = 8000
            paramPercentRateInc.DbType = DbType.String
            paramPercentRateInc.Value = Convert.ToDouble(PercentCurrtRateInc)
            paramPercentRateInc.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramPercentRateInc)

            Dim paramDateLastFiveRateInc10 As New OracleParameter("arg_DATEOF_2010_RATECHANGES", OracleType.DateTime)
            paramDateLastFiveRateInc10.Size = 8000
            paramDateLastFiveRateInc10.DbType = DbType.DateTime
            paramDateLastFiveRateInc10.Value = Convert.ToDateTime(dtLastFiveYrRateIn_2010)
            paramDateLastFiveRateInc10.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramDateLastFiveRateInc10)

            Dim paramDateLastFiveRateInc9 As New OracleParameter("arg_DATEOF_2009_RATECHANGES", OracleType.DateTime)
            paramDateLastFiveRateInc9.Size = 8000
            paramDateLastFiveRateInc9.DbType = DbType.DateTime
            paramDateLastFiveRateInc9.Value = Convert.ToDateTime(dtLastFiveYrRateIn_2009)
            paramDateLastFiveRateInc9.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramDateLastFiveRateInc9)

            Dim paramDateLastFiveRateInc8 As New OracleParameter("arg_DATEOF_2008_RATECHANGES", OracleType.DateTime)
            paramDateLastFiveRateInc8.Size = 8000
            paramDateLastFiveRateInc8.DbType = DbType.DateTime
            paramDateLastFiveRateInc8.Value = Convert.ToDateTime(dtLastFiveYrRateIn_2008)
            paramDateLastFiveRateInc8.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramDateLastFiveRateInc8)

            Dim paramDateLastFiveRateInc7 As New OracleParameter("arg_DATEOF_2007_RATECHANGES", OracleType.DateTime)
            paramDateLastFiveRateInc7.Size = 8000
            paramDateLastFiveRateInc7.DbType = DbType.DateTime
            paramDateLastFiveRateInc7.Value = Convert.ToDateTime(dtLastFiveYrRateIn_2007)
            paramDateLastFiveRateInc7.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramDateLastFiveRateInc7)

            Dim paramDateLastFiveRateInc06 As New OracleParameter("arg_DATEOF_2006_RATECHANGES", OracleType.DateTime)
            paramDateLastFiveRateInc06.Size = 8000
            paramDateLastFiveRateInc06.DbType = DbType.DateTime
            paramDateLastFiveRateInc06.Value = Convert.ToDateTime(dtLastFiveYrRateIn_2006)
            paramDateLastFiveRateInc06.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramDateLastFiveRateInc06)

            'Arguments for Percetage Last Five Years Rate changes
            Dim parmPcntLast5YrRateInc_2010 As New OracleParameter("arg_PERCENT_2010_RATEINC", OracleType.Double)
            parmPcntLast5YrRateInc_2010.Size = 8000
            parmPcntLast5YrRateInc_2010.DbType = DbType.Double
            parmPcntLast5YrRateInc_2010.Value = PcntLast5YrRateInc_2010
            parmPcntLast5YrRateInc_2010.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmPcntLast5YrRateInc_2010)

            Dim parmPcntLast5YrRateInc_2009 As New OracleParameter("arg_PERCENT_2009_RATEINC", OracleType.Double)
            parmPcntLast5YrRateInc_2009.Size = 8000
            parmPcntLast5YrRateInc_2009.DbType = DbType.Double
            parmPcntLast5YrRateInc_2009.Value = PcntLast5YrRateInc_2009
            parmPcntLast5YrRateInc_2009.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmPcntLast5YrRateInc_2009)

            Dim parmPcntLast5YrRateInc_2008 As New OracleParameter("arg_PERCENT_2008_RATEINC", OracleType.Double)
            parmPcntLast5YrRateInc_2008.Size = 8000
            parmPcntLast5YrRateInc_2008.DbType = DbType.Double
            parmPcntLast5YrRateInc_2008.Value = PcntLast5YrRateInc_2008
            parmPcntLast5YrRateInc_2008.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmPcntLast5YrRateInc_2008)

            Dim parmPcntLast5YrRateInc_2007 As New OracleParameter("arg_PERCENT_2007_RATEINC", OracleType.Double)
            parmPcntLast5YrRateInc_2007.Size = 8000
            parmPcntLast5YrRateInc_2007.DbType = DbType.Double
            parmPcntLast5YrRateInc_2007.Value = PcntLast5YrRateInc_2007
            parmPcntLast5YrRateInc_2007.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmPcntLast5YrRateInc_2007)

            Dim parmPcntLast5YrRateInc_2006 As New OracleParameter("arg_PERCENT_2006_RATEINC", OracleType.Double)
            parmPcntLast5YrRateInc_2006.Size = 8000
            parmPcntLast5YrRateInc_2006.DbType = DbType.Double
            parmPcntLast5YrRateInc_2006.Value = PcntLast5YrRateInc_2006
            parmPcntLast5YrRateInc_2006.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmPcntLast5YrRateInc_2006)

            'Arguments for Last Five Years Loss Ratio
            Dim parmLast5YrLossRatio_2010 As New OracleParameter("arg_LAST_LOSSRATIO_2010", OracleType.Double)
            parmLast5YrLossRatio_2010.Size = 8000
            parmLast5YrLossRatio_2010.DbType = DbType.Double
            parmLast5YrLossRatio_2010.Value = last5YrLossRatio_2010
            parmLast5YrLossRatio_2010.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmLast5YrLossRatio_2010)

            Dim parmLast5YrLossRatio_2009 As New OracleParameter("arg_LAST_LOSSRATIO_2009", OracleType.Double)
            parmLast5YrLossRatio_2009.Size = 8000
            parmLast5YrLossRatio_2009.DbType = DbType.Double
            parmLast5YrLossRatio_2009.Value = last5YrLossRatio_2009
            parmLast5YrLossRatio_2009.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmLast5YrLossRatio_2009)

            Dim parmLast5YrLossRatio_2008 As New OracleParameter("arg_LAST_LOSSRATIO_2008", OracleType.Double)
            parmLast5YrLossRatio_2008.Size = 8000
            parmLast5YrLossRatio_2008.DbType = DbType.Double
            parmLast5YrLossRatio_2008.Value = last5YrLossRatio_2008
            parmLast5YrLossRatio_2008.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmLast5YrLossRatio_2008)

            Dim parmLast5YrLossRatio_2007 As New OracleParameter("arg_LAST_LOSSRATIO_2007", OracleType.Double)
            parmLast5YrLossRatio_2007.Size = 8000
            parmLast5YrLossRatio_2007.DbType = DbType.Double
            parmLast5YrLossRatio_2007.Value = last5YrLossRatio_2007
            parmLast5YrLossRatio_2007.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmLast5YrLossRatio_2007)

            Dim parmLast5YrLossRatio_2006 As New OracleParameter("arg_LAST_LOSSRATIO_2006", OracleType.Double)
            parmLast5YrLossRatio_2006.Size = 8000
            parmLast5YrLossRatio_2006.DbType = DbType.Double
            parmLast5YrLossRatio_2006.Value = last5YrLossRatio_2006
            parmLast5YrLossRatio_2006.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(parmLast5YrLossRatio_2006)


            Dim paramNumOfInsured10 As New OracleParameter("arg_NUMOF_MSINSURED_2010", OracleType.Number)
            paramNumOfInsured10.Size = 8000
            paramNumOfInsured10.DbType = DbType.Int64
            paramNumOfInsured10.Value = NumOfMsInsureds_2010
            paramNumOfInsured10.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramNumOfInsured10)


            Dim paramNumOfInsured09 As New OracleParameter("arg_NUMOF_MSINSURED_2009", OracleType.Number)
            paramNumOfInsured09.Size = 8000
            paramNumOfInsured09.DbType = DbType.Int64
            paramNumOfInsured09.Value = NumOfMsInsureds_2009
            paramNumOfInsured09.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramNumOfInsured09)


            Dim paramNumOfInsured08 As New OracleParameter("arg_NUMOF_MSINSURED_2008", OracleType.Number)
            paramNumOfInsured08.Size = 8000
            paramNumOfInsured08.DbType = DbType.Int64
            paramNumOfInsured08.Value = NumOfMsInsureds_2008
            paramNumOfInsured08.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramNumOfInsured08)


            Dim paramNumOfInsured07 As New OracleParameter("arg_NUMOF_MSINSURED_2007", OracleType.Number)
            paramNumOfInsured07.Size = 8000
            paramNumOfInsured07.DbType = DbType.Int64
            paramNumOfInsured07.Value = NumOfMsInsureds_2007
            paramNumOfInsured07.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramNumOfInsured07)


            Dim paramNumOfInsured06 As New OracleParameter("arg_NUMOF_MSINSURED_2006", OracleType.Number)
            paramNumOfInsured06.Size = 8000
            paramNumOfInsured06.DbType = DbType.Int64
            paramNumOfInsured06.Value = NumOfMsInsureds_2006
            paramNumOfInsured06.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramNumOfInsured06)


            Dim paramActionUser As New OracleParameter("arg_ACTION_USER", OracleType.VarChar)
            paramActionUser.Size = 8000
            paramActionUser.DbType = DbType.String
            paramActionUser.Value = strUser
            paramActionUser.Direction = ParameterDirection.Input

            oracleCommand.Parameters.Add(paramActionUser)

            Dim paramSummaryRateInc As New OracleParameter("arg_SUMMARY_RATEINC", OracleType.VarChar)
            paramSummaryRateInc.Size = 8000
            paramSummaryRateInc.DbType = DbType.String
            paramSummaryRateInc.Value = strSummaryRateInc.ToString()
            paramSummaryRateInc.Direction = ParameterDirection.Input

            oracleCommand.Parameters.Add(paramSummaryRateInc)

            Dim paramPolicyApproval As New OracleParameter("arg_POLICY_APPROVAL", OracleType.DateTime)
            paramPolicyApproval.Size = 8000
            paramPolicyApproval.DbType = DbType.DateTime
            paramPolicyApproval.Value = Convert.ToDateTime(dtPolicyApprl)
            paramPolicyApproval.Direction = ParameterDirection.Input

            oracleCommand.Parameters.Add(paramPolicyApproval)


            Dim paramDateSubmitted As New OracleParameter("arg_DATE_SUBMITTED", OracleType.DateTime)
            paramDateSubmitted.Size = 8000
            paramDateSubmitted.DbType = DbType.DateTime
            paramDateSubmitted.Value = Convert.ToDateTime(dtDateSubmitted)
            paramDateSubmitted.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramDateSubmitted)

            Dim paramLinkToRateInc As New OracleParameter("arg_LINK_RATE_INCREASE", OracleType.VarChar)
            paramLinkToRateInc.Size = 8000
            paramLinkToRateInc.DbType = DbType.String
            paramLinkToRateInc.IsNullable = True
            paramLinkToRateInc.Value = strLinkToRateInc
            paramLinkToRateInc.Direction = ParameterDirection.Input
            oracleCommand.Parameters.Add(paramLinkToRateInc)
            con.Open()


            'Dim intRowAffted As Integer = New Integer
            'Dim rowid As System.Data.OracleClient.OracleString
            'intRowAffted = oracleCommand.ExecuteOracleNonQuery(rowid)
            oracleCommand.ExecuteNonQuery()
            oracleCommand.Parameters.Clear()
            con.Close()
        Catch ex As Exception

        End Try
        Return (1)

    End Function



    Public Function ImportFile(ByVal uploadDate As Date, ByVal fileName As String, ByVal fileBuffer() As Byte, ByVal fileType As String, ByVal fileSize As Integer, ByVal rateChangeId As Integer)

        Export(rateChangeId)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        con.Open()
        Dim transaction As OracleTransaction

        transaction = con.BeginTransaction()

        Dim oracleCommand = New OracleCommand()
        oracleCommand.Transaction = transaction

        oracleCommand.Connection = con
        oracleCommand.CommandText = "declare xx blob; begin dbms_lob.createtemporary(xx, false, 0); :tempblob := xx; end;"
        oracleCommand.Parameters.Add(New OracleParameter("tempblob", OracleType.Blob)).Direction = ParameterDirection.Output
        oracleCommand.ExecuteNonQuery()
        Dim tempLob As OracleLob
        ' Getting the content of the file...
        Dim buffer As Byte() = fileBuffer


        'Oracle object responsible for storing the File content.
        'Assigning tempLob the blob object created on the database.

        tempLob = CType(oracleCommand.Parameters(0).Value, OracleLob)
        tempLob.BeginBatch(OracleLobOpenMode.ReadWrite)
        'Writing the file content to tempLob.
        tempLob.Write(buffer, 0, buffer.Length)
        tempLob.EndBatch()
        oracleCommand.Parameters.Clear()

        oracleCommand.CommandText = "sp_import_file"
        oracleCommand.CommandType = CommandType.StoredProcedure
        oracleCommand.Parameters.Add(New OracleParameter("UPLOAD_DATE", OracleType.DateTime)).Value = uploadDate
        oracleCommand.Parameters.Add(New OracleParameter("ARG_CONTENT", OracleType.Blob)).Value = tempLob
        oracleCommand.Parameters.Add(New OracleParameter("ARG_TYPE", OracleType.VarChar)).Value = fileType
        oracleCommand.Parameters.Add(New OracleParameter("ARG_NAME", OracleType.VarChar)).Value = fileName
        oracleCommand.Parameters.Add(New OracleParameter("ARG_LENGTH", OracleType.Int32)).Value = fileSize
        oracleCommand.Parameters.Add(New OracleParameter("RATECHANGES_ID", OracleType.Int32)).Value = rateChangeId
        Dim retValParam As New OracleParameter("arg_id", OleDbType.Integer)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)


        Try
            oracleCommand.ExecuteNonQuery()
        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)


        End Try
        Dim returnVal As Integer
        returnVal = Convert.ToInt32(oracleCommand.Parameters("arg_id").Value.ToString())

        transaction.Commit()

        con.Close()

        Return (returnVal)
    End Function

    Private Function GetFileContent(ByVal path As String)

        Dim fs As Stream = File.OpenRead(path)
        Dim filesize As Long = fs.Length
        Dim buffer(filesize) As Byte

        Dim q As Integer = fs.Read(buffer, 0, Convert.ToInt32(fs.Length))


        fs.Close()

        Return buffer
    End Function


    Public Function ExportFileByFileID(ByVal fileID As Integer)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim fileDS As New DataSet()
        con.Open()
        Dim oracleCommand = New OracleCommand()
        oracleCommand.Connection = con
        oracleCommand.CommandText = "Sp_Export_File_ByID"
        oracleCommand.CommandType = CommandType.StoredProcedure
        oracleCommand.Parameters.Add(New OracleParameter("arrID", OracleType.Int32)).Value = fileID
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)

        Try

            oracleDbDataAdapter.Fill(fileDS)


        Catch ex As Exception

        End Try
        con.Close()
        Return (fileDS)


    End Function

    Public Function Export(ByVal rateChangeID As Integer)
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim fileDS As New DataSet()
        con.Open()
        Dim arrFile As New ArrayList()
        Dim oracleCommand = New OracleCommand()
        oracleCommand.Connection = con
        oracleCommand.CommandText = "Sp_Export_File"
        oracleCommand.CommandType = CommandType.StoredProcedure
        oracleCommand.Parameters.Add(New OracleParameter("arrRateChangeID", OracleType.Int32)).Value = rateChangeID
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)

        Try

            oracleDbDataAdapter.Fill(fileDS)

        Catch ex As Exception

        End Try
        con.Close()
        Return (fileDS)


    End Function

    Public Function GetAllFileDownloads()
        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Dim fileDS As New DataSet()
        con.Open()
        Dim arrFile As New ArrayList()
        Dim oracleCommand = New OracleCommand()
        oracleCommand.Connection = con
        oracleCommand.CommandText = "Sp_Get_AllFileDownloads"
        oracleCommand.CommandType = CommandType.StoredProcedure
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)

        Try

            oracleDbDataAdapter.Fill(fileDS)


        Catch ex As Exception

        End Try
        con.Close()
        Return (fileDS)


    End Function
    Public Sub WriteLog(ByVal ex As Exception)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)
        Try
            Dim oracleCommand = New OracleCommand()
            oracleCommand.Connection = con
            oracleCommand.CommandType = CommandType.StoredProcedure
            oracleCommand.CommandText = "SP_INSERT_LOG"
            oracleCommand.Parameters.Add(New OracleParameter("pErrorDescription", OracleClient.OracleType.NClob)).Value = ex.ToString()
            oracleCommand.Parameters.Add(New OracleParameter("pApplicationName", OracleClient.OracleType.VarChar)).Value = "MIDRATE WEB"
            oracleCommand.Parameters.Add(New OracleParameter("pSeverity", OracleClient.OracleType.VarChar)).Value = "Error"

            con.Open()
            oracleCommand.ExecuteNonQuery()

        Finally
            con.Close()
        End Try

    End Sub

    Public Function GetCompanyDetailsByCompanyID(ByVal strCompanyID As String)

        Dim con As OracleConnection = New OracleConnection(MidDBConnStr)

        Dim oracleCommand = New OracleCommand()
        oracleCommand.Connection = con
        oracleCommand.CommandText = "sp_get_company_by_id"
        oracleCommand.CommandType = CommandType.StoredProcedure
        oracleCommand.Parameters.Add(New OracleParameter("arg_compid", OleDbType.VarChar)).Value = strCompanyID
        Dim retValParam As New OracleParameter("p_cursor", OracleType.Cursor)
        retValParam.Direction = ParameterDirection.Output
        oracleCommand.Parameters.Add(retValParam)
        Dim oracleDbDataAdapter As OracleDataAdapter = New OracleDataAdapter(oracleCommand)
        con.Open()
        Dim companyDataSet As New DataSet()
        oracleDbDataAdapter.Fill(companyDataSet)
        con.Close()

        Return (companyDataSet)


    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="AppName"></param>
    ''' <param name="ErrorDesc"></param>
    ''' <param name="StartDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetErrorLogs(ByVal AppName As String, ByVal ErrorDesc As String, ByVal StartDate As String, _
                                 ByVal endDate As String) As DataTable

        Dim conn As OracleConnection
        Dim errorLogs As DataTable
        Dim cmd As OracleCommand
        conn = New OracleConnection(MidDBConnStr)
        Try

            errorLogs = New DataTable()
            cmd = New OracleCommand("SP_SEARCH_ERROR_LOG", conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New OracleParameter("pAppName", OracleClient.OracleType.VarChar)).Value = AppName
            cmd.Parameters.Add(New OracleParameter("pErrorDescription", OracleClient.OracleType.VarChar)).Value = ErrorDesc
            cmd.Parameters.Add(New OracleParameter("pStartDate", OracleClient.OracleType.VarChar)).Value = StartDate.ToString()
            cmd.Parameters.Add(New OracleParameter("pEndDate", OracleClient.OracleType.VarChar)).Value = endDate.ToString()
            cmd.Parameters.Add(New OracleParameter("p_cursor", OracleClient.OracleType.Cursor)).Direction = ParameterDirection.Output
            conn.Open()

            Dim cmdAD As New OracleDataAdapter(cmd)
            cmdAD.Fill(errorLogs)

        Finally
            conn.Close()
        End Try

        Return errorLogs
    End Function
End Class
