﻿
Imports System.Data
Imports System.Data.OracleClient
Imports System.Data.OleDb
Imports System.Configuration

Public Class GraphsData

    Dim MidDBConnStr As String = ConfigurationManager.ConnectionStrings("MidConnectionString").ConnectionString.ToString()

    ''' <summary>
    ''' Method to get the data for the graphs based on selected rateChangeIds.
    ''' </summary>
    ''' <param name="RateIds"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDataForGraphs(ByVal RateIds As String) As DataTable

        Dim conn As OracleConnection
        Dim companies As DataTable
        Dim cmd As OracleCommand
        conn = New OracleConnection(MidDBConnStr)
        Try

            companies = New DataTable()
            cmd = New OracleCommand("GET_DATA_FORGRAPHS", conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New OracleParameter("pRateChangeId", OracleClient.OracleType.NVarChar)).Value = RateIds
            cmd.Parameters.Add(New OracleParameter("p_cursor", OracleClient.OracleType.Cursor)).Direction = ParameterDirection.Output
            conn.Open()

            Dim cmdAD As New OracleDataAdapter(cmd)
            cmdAD.Fill(companies)

        Finally
            conn.Close()
        End Try

        Return companies


    End Function
End Class
