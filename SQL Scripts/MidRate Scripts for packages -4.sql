--------------------------------------------------------
--  DDL for Package AUDIT_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "MIDRATE"."AUDIT_PKG" 
    as
      procedure CHECK_VAL_PFORM(
         arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN VARCHAR2,
         ARG_OLD_VALUE IN VARCHAR2,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);   
                                       

     procedure CHECK_VAL_PType(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN VARCHAR2,
         ARG_OLD_VALUE IN VARCHAR2,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);

     procedure CHECK_VAL_DCURRNT(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN DATE,
         ARG_OLD_VALUE IN DATE,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
         
         
     procedure CHECK_VAL_PERCENT(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
         
           procedure CHECK_VAL_DATE1(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN DATE,
         ARG_OLD_VALUE IN DATE,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
         
         
           procedure CHECK_VAL_DATE2(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN DATE,
         ARG_OLD_VALUE IN DATE,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
         
            procedure CHECK_VAL_DATE3(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN DATE,
         ARG_OLD_VALUE IN DATE,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
         
         procedure CHECK_VAL_DATE4(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN DATE,
         ARG_OLD_VALUE IN DATE,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
         
         procedure CHECK_VAL_DATE5(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN DATE,
         ARG_OLD_VALUE IN DATE,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
 
         
          
         procedure CHECK_VAL_PCT1(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
           
         procedure CHECK_VAL_PCT2(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
            
         procedure CHECK_VAL_PCT3(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
         
            procedure CHECK_VAL_PCT4(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
         
         procedure CHECK_VAL_PCT5(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
         
         procedure CHECK_VAL_LOSSR1(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
         
          
         procedure CHECK_VAL_LOSSR2(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
         
          
         procedure CHECK_VAL_LOSSR3(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
          
         procedure CHECK_VAL_LOSSR4(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
            
         procedure CHECK_VAL_LOSSR5(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
              
         procedure CHECK_VAL_NUM1(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN NUMBER,
         ARG_OLD_VALUE IN NUMBER,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
     
         
            procedure CHECK_VAL_NUM2(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN NUMBER,
         ARG_OLD_VALUE IN NUMBER,
         ARG_COLUMN_NAME IN VARCHAR2,
           ARG_TABLE_NAME In VARCHAR2);
       
         
            procedure CHECK_VAL_NUM3(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN NUMBER,
         ARG_OLD_VALUE IN NUMBER,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
         
         
            procedure CHECK_VAL_NUM4(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN NUMBER,
         ARG_OLD_VALUE IN NUMBER,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
         
            procedure CHECK_VAL_NUM5(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN NUMBER,
         ARG_OLD_VALUE IN NUMBER,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2);
         
         
  end;

/

--------------------------------------------------------
--  DDL for Package Body AUDIT_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "MIDRATE"."AUDIT_PKG" 
   as
   
   procedure CHECK_VAL_PFORM(
         arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN VARCHAR2,
         ARG_OLD_VALUE IN VARCHAR2,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
           
 IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
         
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                
      END IF;
 END CHECK_VAL_PFORM ;


     procedure CHECK_VAL_PType(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN VARCHAR2,
         ARG_OLD_VALUE IN VARCHAR2,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
       
IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
        
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;


     procedure CHECK_VAL_DCURRNT(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN DATE,
         ARG_OLD_VALUE IN DATE,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
      
         
         IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
        
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ, sysdate, ARG_ACTION_BY,
          ARG_ACTION,to_char( ARG_NEW_VALUE, 'mm-dd-yyyy hh24:mi:ss'),to_char(ARG_OLD_VALUE,'mm-dd-yyyy hh24:mi:ss'),ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;
  procedure CHECK_VAL_PERCENT(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
   
 IS
  BEGIN
      IF(ARG_NEW_VALUE <> ARG_OLD_VALUE)
         
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;
   procedure CHECK_VAL_DATE1(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN DATE,
         ARG_OLD_VALUE IN DATE,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
     
    
         IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
         
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY,
          ARG_ACTION,to_char( ARG_NEW_VALUE, 'mm-dd-yyyy'),to_char(ARG_OLD_VALUE,'mm-dd-yyyy'),ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 
                           
      END IF;
 END;
  procedure CHECK_VAL_DATE2(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN DATE,
         ARG_OLD_VALUE IN DATE,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
      
    
         IS
  BEGIN
      IF(ARG_NEW_VALUE <> ARG_OLD_VALUE)
       
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY,
          ARG_ACTION,to_char( ARG_NEW_VALUE, 'mm-dd-yyyy'),to_char(ARG_OLD_VALUE,'mm-dd-yyyy'),ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 
                           
      END IF;
 END;
  procedure CHECK_VAL_DATE3(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN DATE,
         ARG_OLD_VALUE IN DATE,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
     
    
         IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
        
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY,
          ARG_ACTION,to_char( ARG_NEW_VALUE, 'mm-dd-yyyy'),to_char(ARG_OLD_VALUE,'mm-dd-yyyy'),ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 
                           
      END IF;
 END;
 procedure CHECK_VAL_DATE4(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN DATE,
         ARG_OLD_VALUE IN DATE,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
     
    
         IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
          
      THEN
         INSERT INTO AUDIT_TRAIL values
          ( ARG_SEQ,sysdate,ARG_ACTION_BY,
          ARG_ACTION,to_char( ARG_NEW_VALUE, 'mm-dd-yyyy'),to_char(ARG_OLD_VALUE,'mm-dd-yyyy'),ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 
                           
      END IF;
 END;
  procedure CHECK_VAL_DATE5(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN DATE,
         ARG_OLD_VALUE IN DATE,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
    
         IS
  BEGIN
      IF(ARG_NEW_VALUE <> ARG_OLD_VALUE )
         
      THEN
         INSERT INTO AUDIT_TRAIL values
          ( ARG_SEQ,sysdate,ARG_ACTION_BY,
          ARG_ACTION,to_char( ARG_NEW_VALUE, 'mm-dd-yyyy'),to_char(ARG_OLD_VALUE,'mm-dd-yyyy'),ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 
                           
      END IF;
 END;
  
         procedure CHECK_VAL_PCT1(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 IS
  BEGIN
      IF(ARG_NEW_VALUE <> ARG_OLD_VALUE)
       
      THEN
         INSERT INTO AUDIT_TRAIL values
          ( ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;
 
  procedure CHECK_VAL_PCT2(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 IS
  BEGIN
      IF(ARG_NEW_VALUE <> ARG_OLD_VALUE OR
          (ARG_NEW_VALUE IS NULL AND ARG_OLD_VALUE IS NOT NULL) OR
           (ARG_NEW_VALUE IS NOT NULL AND ARG_OLD_VALUE IS NULL) )
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;
 
  procedure CHECK_VAL_PCT3(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE OR
          (ARG_NEW_VALUE IS NULL AND ARG_OLD_VALUE IS NOT NULL) OR
           (ARG_NEW_VALUE IS NOT NULL AND ARG_OLD_VALUE IS NULL) )
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;
  procedure CHECK_VAL_PCT4(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
        
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;
 
  procedure CHECK_VAL_PCT5(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
          
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;
 
  
         procedure CHECK_VAL_LOSSR1(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 
 IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
      
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;
 
 
         procedure CHECK_VAL_LOSSR2(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 
 IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
          
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;
 
         procedure CHECK_VAL_LOSSR3(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 
 IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)THEN
          
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;
 
 
         procedure CHECK_VAL_LOSSR4(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 
 IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
         
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END CHECK_VAL_LOSSR4;
 
         procedure CHECK_VAL_LOSSR5(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN FLOAT,
         ARG_OLD_VALUE IN FLOAT,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 
 IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
      
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END CHECK_VAL_LOSSR5;
 
         
         procedure CHECK_VAL_NUM1(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN NUMBER,
         ARG_OLD_VALUE IN NUMBER,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
        
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;
 
 
    procedure CHECK_VAL_NUM2(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN NUMBER,
         ARG_OLD_VALUE IN NUMBER,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
         
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;
 
   procedure CHECK_VAL_NUM3(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN NUMBER,
         ARG_OLD_VALUE IN NUMBER,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
          
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;
 
   procedure CHECK_VAL_NUM4(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN NUMBER,
         ARG_OLD_VALUE IN NUMBER,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
        
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END;
 
   procedure CHECK_VAL_NUM5(
       arg_seq IN NUMBER,
         ARG_ACTION_DATE IN DATE,
         ARG_ACTION_BY IN VARCHAR2,
         ARG_ACTION IN VARCHAR2,
         ARG_NEW_VALUE IN NUMBER,
         ARG_OLD_VALUE IN NUMBER,
         ARG_COLUMN_NAME IN VARCHAR2,
         ARG_TABLE_NAME In VARCHAR2)
 IS
  BEGIN
      IF( ARG_NEW_VALUE <> ARG_OLD_VALUE)
        
      THEN
         INSERT INTO AUDIT_TRAIL values
          (ARG_SEQ,sysdate,ARG_ACTION_BY, ARG_ACTION,ARG_NEW_VALUE,ARG_OLD_VALUE,ARG_COLUMN_NAME,upper(ARG_TABLE_NAME)); 

                            
      END IF;
 END CHECK_VAL_NUM5;

end audit_pkg;

/

