Insert into MIDRATE.CATEGORY (CATAGORY_ID,CATAGORY_NAME) values (1,'Group Health');
Insert into MIDRATE.CATEGORY (CATAGORY_ID,CATAGORY_NAME) values (2,'Individual Health');
Insert into MIDRATE.CATEGORY (CATAGORY_ID,CATAGORY_NAME) values (3,'Group Long Term Care');
Insert into MIDRATE.CATEGORY (CATAGORY_ID,CATAGORY_NAME) values (4,'Individual Long Term Care');
Insert into MIDRATE.CATEGORY (CATAGORY_ID,CATAGORY_NAME) values (5,'Group Medicare Supplement');
Insert into MIDRATE.CATEGORY (CATAGORY_ID,CATAGORY_NAME) values (6,'Individual Medicare Supplement');
Insert into MIDRATE.CATEGORY (CATAGORY_ID,CATAGORY_NAME) values (7,'Other');