

--------------------------------------------------------
--  DDL for Sequence AUDIT_TRAIL_RATE_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "MIDRATE"."AUDIT_TRAIL_RATE_SEQUENCE"  MINVALUE 1 MAXVALUE 
   999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
   
   
   --------------------------------------------------------
--  DDL for Sequence AUDIT_TRAIL_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "MIDRATE"."AUDIT_TRAIL_SEQUENCE"  MINVALUE 1 MAXVALUE 999999999999999999999999999 
   INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;



--------------------------------------------------------
--  DDL for Sequence COMPANYSEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "MIDRATE"."COMPANYSEQUENCE"  MINVALUE 1 MAXVALUE 999999999999999999999999999
   INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;


--------------------------------------------------------
--  DDL for Sequence FILE_UPLOAD_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "MIDRATE"."FILE_UPLOAD_SEQUENCE"  MINVALUE 1 MAXVALUE 999999999999999999999999999 
   INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
   
   
   --------------------------------------------------------
--  DDL for Sequence RATECHANGE_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "MIDRATE"."RATECHANGE_SEQUENCE"  MINVALUE 1 MAXVALUE 999999999999999999999999999 
   INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;

