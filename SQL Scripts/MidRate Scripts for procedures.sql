--------------------------------------------------------
--  DDL for Procedure GET_COMPANY_WITH_RATERECORDS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."GET_COMPANY_WITH_RATERECORDS" 
(p_cursor OUT sys_refcursor)

IS
BEGIN

  OPEN p_cursor FOR
  select distinct(rc.company_id),b.company_name  from Rate_changes RC
      INNER JOIN company b ON 
      b.company_id = RC.company_id
      where RC.IS_ROWDELETED = 'N'
      AND b.IS_ROWDELETED = 'N'
      ORDER BY b.company_name ASC;
  
END GET_COMPANY_WITH_RATERECORDS;

/

--------------------------------------------------------
--  DDL for Procedure GET_DATA_FORGRAPHS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."GET_DATA_FORGRAPHS" 
(
 pRateChangeId IN NVARCHAR2, 
 p_cursor out  SYS_REFCURSOR
)
AS 
BEGIN

  OPEN p_cursor FOR
     select RC.company_id,
     COMPANY_NAME AS "COMPANYNAME",
      TO_CHAR(dateof_currentRateINCR,'YYYY') AS "DATEOF_CURRENTRATEINCR",
      PERCENT_CURRENT_RATEINC,
      TO_CHAR(DATEOF_YR_MINUS1_RATEINC,'YYYY') AS "DATEOF_YR_MINUS1_RATEINC",
      TO_CHAR(DATEOF_YR_MINUS2_RATEINC,'YYYY') AS "DATEOF_YR_MINUS2_RATEINC",
      TO_CHAR(DATEOF_YR_MINUS3_RATEINC,'YYYY') AS "DATEOF_YR_MINUS3_RATEINC",
      TO_CHAR(DATEOF_YR_MINUS4_RATEINC,'YYYY') AS "DATEOF_YR_MINUS4_RATEINC",
      TO_CHAR(DATEOF_YR_MINUS5_RATEINC,'YYYY') AS "DATEOF_YR_MINUS5_RATEINC",
      PERCENT_YR_MINUS1_RATEINC,
      PERCENT_YR_MINUS2_RATEINC,
      PERCENT_YR_MINUS3_RATEINC,
      PERCENT_YR_MINUS4_RATEINC,
      PERCENT_YR_MINUS5_RATEINC,
      LAST_MINUS1_LOSSRATIO,
      LAST_MINUS2_LOSSRATIO,
      LAST_MINUS3_LOSSRATIO,
      LAST_MINUS4_LOSSRATIO,
      LAST_MINUS5_LOSSRATIO,
      RATECHANGES_ID

      from Rate_changes RC
      INNER JOIN company b ON 
      b.company_id = RC.company_id
      where RC.IS_ROWDELETED = 'N'
      AND b.IS_ROWDELETED = 'N'
      AND RC.RATECHANGES_ID IN ( SELECT TO_NUMBER(xt.column_value)
	         FROM   XMLTABLE(pRateChangeId) xt );
      
         
                    
END GET_DATA_FORGRAPHS;

/
--------------------------------------------------------
--  DDL for Procedure GET_RATE_CHANGES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."GET_RATE_CHANGES" ( p_cursor out  SYS_REFCURSOR)
IS
begin
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
              PTYPECATEGORY_SECONDARY  AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
              SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
     end Get_Rate_Changes;

/
--------------------------------------------------------
--  DDL for Procedure GET_RATE_CHANGESBY_ID
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."GET_RATE_CHANGESBY_ID" ( 
p_cursor out  SYS_REFCURSOR,
argCompany_ID IN VARCHAR2)
IS
begin
open p_cursor for select
           RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
             PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
              POLICY_APPROVAL FROM RATE_CHANGES WHERE COMPANY_ID =argCompany_ID AND  IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC; 
            
end Get_Rate_ChangesBy_ID;

/

--------------------------------------------------------
--  DDL for Procedure GET_RATE_CHANGESBY_RATEID
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."GET_RATE_CHANGESBY_RATEID" ( 
p_cursor out  SYS_REFCURSOR,
arg_RATECHANGES_ID IN INTEGER)
IS
begin
open p_cursor for select
           RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
              PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
              SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE RATECHANGES_ID =arg_RATECHANGES_ID AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
         
end Get_Rate_ChangesBy_RateID;

/

--------------------------------------------------------
--  DDL for Procedure GET_RATECHANGESBY_RATECHANGEID
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."GET_RATECHANGESBY_RATECHANGEID" 
( 
p_cursor out  SYS_REFCURSOR,
argRateChange_ID IN VARCHAR2)
IS
begin
open p_cursor for select
           RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
            PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
              to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL
              
               FROM RATE_CHANGES 
              WHERE RATECHANGES_ID IN( SELECT TO_NUMBER(xt.column_value)
	                                 FROM   XMLTABLE(argRateChange_ID) xt ) 
              AND  
              IS_ROWDELETED = 'N' 
              ORDER BY RATECHANGES_ID DESC;
                 
END GET_RATECHANGESBY_RATECHANGEID;

/


--------------------------------------------------------
--  DDL for Procedure SP_AUDITTRAILSEARCH
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_AUDITTRAILSEARCH" (
ARG_ACTION IN VARCHAR2,
ARG_ACTION_BY IN VARCHAR2,
ARG_FROM_DATE IN VARCHAR2,
ARG_TO_DATE IN VARCHAR2,
p_cursor out  SYS_REFCURSOR)
IS
begin
if(ARG_ACTION <>'All' AND ARG_ACTION_BY <> 'All' AND ARG_FROM_DATE <> 'All' AND ARG_TO_DATE <> 'All') then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE ACTION = ARG_ACTION AND ACTION_BY = ARG_ACTION_BY AND 
 ACTION_DATE BETWEEN to_date(ARG_FROM_DATE,'mm/dd/yyyy') AND to_date(ARG_TO_DATE,'mm/dd/yyyy')
ORDER by ACTION_DATE DESC ;

end if;
if(ARG_ACTION <> 'All' AND ARG_ACTION_BY = 'All' AND ARG_FROM_DATE = 'All' AND ARG_TO_DATE = 'All') then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE ACTION = ARG_ACTION 
ORDER by ACTION_DATE DESC ;
end if;

 if(ARG_ACTION <> 'All' AND ARG_ACTION_BY <> 'All' AND ARG_FROM_DATE= 'All' AND ARG_TO_DATE = 'All')then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE  ACTION = ARG_ACTION AND ACTION_BY = ARG_ACTION_BY
ORDER by ACTION_DATE DESC ;
end if;



if(ARG_ACTION = 'All' AND ARG_ACTION_BY = 'All' AND ARG_FROM_DATE <> 'All' AND ARG_TO_DATE <> 'All')then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE ACTION_DATE BETWEEN to_date(ARG_FROM_DATE,'mm/dd/yyyy') AND to_date(ARG_TO_DATE,'mm/dd/yyyy')
ORDER by ACTION_DATE DESC ;
end if;


if(ARG_ACTION <> 'All' AND ARG_ACTION_BY = 'All' AND ARG_FROM_DATE <> 'All' AND ARG_TO_DATE <> 'All')then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE ACTION = ARG_ACTION AND ACTION_DATE BETWEEN to_date(ARG_FROM_DATE,'mm/dd/yyyy') AND to_date(ARG_TO_DATE,'mm/dd/yyyy')
ORDER by ACTION_DATE DESC ;
end if;

 if(ARG_ACTION = 'All' AND ARG_ACTION_BY <> 'All' AND ARG_FROM_DATE <> 'All' AND ARG_TO_DATE <> 'All')then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE ACTION_BY = ARG_ACTION_BY AND  ACTION_DATE BETWEEN to_date(ARG_FROM_DATE,'mm/dd/yyyy') AND to_date(ARG_TO_DATE,'mm/dd/yyyy')
ORDER by ACTION_DATE DESC ;
end if;


if(ARG_ACTION = 'All' AND ARG_ACTION_BY = 'All' AND ARG_FROM_DATE <> 'All' AND ARG_TO_DATE = 'All')then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE ACTION_DATE >= to_date(ARG_FROM_DATE,'mm/dd/yyyy') 
ORDER by ACTION_DATE DESC ;
end if;


if(ARG_ACTION = 'All' AND ARG_ACTION_BY = 'All' AND ARG_FROM_DATE = 'All' AND ARG_TO_DATE <> 'All')then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE ACTION_DATE <= to_date(ARG_TO_DATE,'mm/dd/yyyy') 
ORDER by ACTION_DATE DESC ;
end if;


if(ARG_ACTION <>'All' AND ARG_ACTION_BY = 'All' AND ARG_FROM_DATE = 'All' AND ARG_TO_DATE <> 'All')then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE ACTION = ARG_ACTION AND ACTION_DATE <= to_date(ARG_TO_DATE,'mm/dd/yyyy') 
ORDER by ACTION_DATE DESC ;
end if;

if(ARG_ACTION ='All' AND ARG_ACTION_BY <> 'All' AND ARG_FROM_DATE = 'All' AND ARG_TO_DATE <> 'All')then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE ACTION_BY = ARG_ACTION_BY AND ACTION_DATE <= to_date(ARG_TO_DATE,'mm/dd/yyyy') 
ORDER by ACTION_DATE DESC ;
end if;

if(ARG_ACTION <>'All' AND ARG_ACTION_BY = 'All' AND ARG_FROM_DATE <> 'All' AND ARG_TO_DATE = 'All')then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE ACTION = ARG_ACTION AND ACTION_DATE >= to_date(ARG_FROM_DATE,'mm/dd/yyyy') 
ORDER by ACTION_DATE DESC ;
end if;

if(ARG_ACTION ='All' AND ARG_ACTION_BY <> 'All' AND ARG_FROM_DATE <> 'All' AND ARG_TO_DATE = 'All')then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE ACTION_BY = ARG_ACTION_BY AND ACTION_DATE >= to_date(ARG_FROM_DATE,'mm/dd/yyyy') 
ORDER by ACTION_DATE DESC ;
end if;

if(ARG_ACTION  <>'All' AND ARG_ACTION_BY <> 'All' AND ARG_FROM_DATE <> 'All' AND ARG_TO_DATE = 'All')then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE ACTION = ARG_ACTION AND ACTION_BY = ARG_ACTION_BY AND ACTION_DATE >= to_date(ARG_FROM_DATE,'mm/dd/yyyy') 
ORDER by ACTION_DATE DESC ;
end if;

if(ARG_ACTION  <>'All' AND ARG_ACTION_BY <> 'All' AND ARG_FROM_DATE = 'All' AND ARG_TO_DATE <> 'All')then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE ACTION = ARG_ACTION AND ACTION_BY = ARG_ACTION_BY AND ACTION_DATE <= to_date(ARG_TO_DATE,'mm/dd/yyyy') 
ORDER by ACTION_DATE DESC ;
end if;

if(ARG_ACTION  = 'All' AND ARG_ACTION_BY <> 'All' AND ARG_FROM_DATE = 'All' AND ARG_TO_DATE = 'All')then
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL WHERE ACTION_BY = ARG_ACTION_BY 
ORDER by ACTION_DATE DESC ;
end if;


end;

/
--------------------------------------------------------
--  DDL for Procedure SP_CHECK_COMPANY_EXISTS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_CHECK_COMPANY_EXISTS" (
   arg_CompanyID   IN  VARCHAR2,
   IsCompany_Exist OUT NUMBER )
  AS
   
BEGIN

   SELECT COUNT(*)
      INTO IsCompany_Exist 
      FROM RATE_CHANGES
      WHERE COMPANY_ID = arg_CompanyID;
       
   END;

/
--------------------------------------------------------
--  DDL for Procedure SP_DELETE_COMPANY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_DELETE_COMPANY" 
(
  arg_COMPANYID IN VARCHAR2
)

AS 
BEGIN
  
  UPDATE rate_changes
  SET IS_ROWDELETED = 'Y'
  WHERE COMPANY_ID = arg_COMPANYID;
  
  UPDATE COMPANY
  SET IS_ROWDELETED = 'Y'
  WHERE COMPANY_ID = arg_COMPANYID;
  
  COMMIT;
END SP_DELETE_COMPANY;

/
--------------------------------------------------------
--  DDL for Procedure SP_DELETE_FILEBY_ID
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_DELETE_FILEBY_ID" (
arrID IN integer)

IS
begin
 delete from FILE_UPLOAD_RATEREVIEW where ID = arrID;

end Sp_Delete_FileBy_ID;

/
--------------------------------------------------------
--  DDL for Procedure SP_DELETE_RATECHANGES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_DELETE_RATECHANGES" 
       (
          arg_RateChangesID  IN INTEGER
                        
       )
AS 
BEGIN 

    UPDATE RATE_CHANGES SET IS_ROWDELETED = 'Y'
                            
      WHERE  RATECHANGES_ID  = arg_RateChangesID  ;

      COMMIT ; 

END SP_Delete_RateChanges ; 
/
--------------------------------------------------------
--  DDL for Procedure SP_EXPORT_FILE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_EXPORT_FILE" (
arrRateChangeID IN integer,
p_cursor out  SYS_REFCURSOR)
IS
begin
open p_cursor for select ID,content, name, type,UPLOAD_DATE from FILE_UPLOAD_RATEREVIEW f where f.RATECHANGES_ID = arrRateChangeID
order by f.upload_date desc;
end Sp_Export_File;

/
--------------------------------------------------------
--  DDL for Procedure SP_EXPORT_FILE_BYID
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_EXPORT_FILE_BYID" (
arrID IN integer,
p_cursor out  SYS_REFCURSOR)
IS
begin
open p_cursor for select ID,content, name, type,UPLOAD_DATE from FILE_UPLOAD_RATEREVIEW f where f.ID = arrID
order by f.upload_date desc;
end Sp_Export_File_ByID;

/

--------------------------------------------------------
--  DDL for Procedure SP_GET_AUDITTRAIL
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_GET_AUDITTRAIL" ( p_cursor out  SYS_REFCURSOR)
IS
begin
open p_cursor for select AUDIT_ID,
ACTION_DATE,
ACTION_BY ,
ACTION,
COLUMN_NAME,
OLD_VALUE,NEW_VALUE,
TABLE_NAME from AUDIT_TRAIL
ORDER by ACTION_DATE DESC ;

end Sp_Get_AuditTrail;

/
--------------------------------------------------------
--  DDL for Procedure SP_GET_COMPANY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_GET_COMPANY" (p_cursor OUT sys_refcursor) IS
BEGIN

  OPEN p_cursor FOR
  SELECT company_id,
    company_name
  FROM COMPANY 
  WHERE IS_ROWDELETED = 'N'
  ORDER BY company_name ASC;

END sp_get_company;

/
--------------------------------------------------------
--  DDL for Procedure SP_GET_COMPANYLOGO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_GET_COMPANYLOGO" (
arg_company_id IN VARCHAR2,
p_cursor OUT sys_refcursor) IS
BEGIN

  OPEN p_cursor FOR
  SELECT company_id,COMPANY_LOGO_FILENAME,LOGO_FILE_TYPE,FILE_CONTENT
    
  FROM COMPANY WHERE COMPANY_ID = arg_company_id;

END SP_GET_COMPANYLOGO;

/
--------------------------------------------------------
--  DDL for Procedure SP_GET_TYPEOFINSURANCE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_GET_TYPEOFINSURANCE" (p_cursor OUT sys_refcursor) IS
BEGIN

  OPEN p_cursor FOR
  SELECT CATAGORY_ID,
    CATAGORY_NAME
  FROM CATEGORY ORDER BY CATAGORY_NAME ASC;

END sp_get_TypeOfInsurance;

/
--------------------------------------------------------
--  DDL for Procedure SP_IMPORT_FILE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_IMPORT_FILE" (
upload_date in varchar2,
arg_content     FILE_UPLOAD_RATEREVIEW.content%type,
arg_type        in varchar2,
arg_name        in varchar2,
arg_length      in number,
arg_id          out number,
rateChanges_id in Integer)

is

begin

  SELECT FILE_UPLOAD_SEQUENCE.nextval INTO arg_id FROM dual;

  insert into FILE_UPLOAD_RATEREVIEW
    (UPLOAD_DATE, CONTENT, TYPE, NAME, LENGTH,RATECHANGES_ID)
  values
    (upload_date,
     arg_content,
     arg_type,
     arg_name,
     arg_length,
     rateChanges_id);

end sp_import_file;

/

--------------------------------------------------------
--  DDL for Procedure SP_INSERT_COMPANY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_INSERT_COMPANY" 
(
arg_COMPANYNAME IN VARCHAR2,
arg_COMPANY_LOGO_FILENAME IN VARCHAR2,
arg_LOGO_FILE_TYPE IN VARCHAR2,
arg_FILE_CONTENT IN company.file_content%type,
arg_Company_ID OUT INTEGER)

IS
BEGIN
SELECT COUNT(*)
      INTO arg_Company_ID 
      FROM COMPANY
      WHERE COMPANY_NAME = arg_COMPANYNAME
      AND IS_ROWDELETED = 'N';
      
  if(arg_Company_ID = 0) then
  INSERT INTO COMPANY
  (
  COMPANY_NAME,
  COMPANY_LOGO_FILENAME,
  LOGO_FILE_TYPE,
  FILE_CONTENT
  )
  VALUES
  (arg_COMPANYNAME,arg_COMPANY_LOGO_FILENAME,arg_LOGO_FILE_TYPE,arg_FILE_CONTENT);
  
   SELECT COMPANYSEQUENCE.CURRVAL INTO arg_Company_ID FROM dual; 
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_INSERT_LOG
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_INSERT_LOG" 
(
pErrorDescription IN NCLOB,
pApplicationName IN VARCHAR2,
pSeverity IN VARCHAR2)

AS 
BEGIN
    INSERT INTO EXCEPTION_LOG
    (
    SEVERITY,
    APPLICATIONNAME,
    ERRORDESCRIPTION,
    ERRORDATE
    )
    VALUES
    (pSeverity,pApplicationName,pErrorDescription,sysdate());
    
    COMMIT;
  
END SP_INSERT_LOG;

/
--------------------------------------------------------
--  DDL for Procedure SP_INSERT_RATECHANGES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_INSERT_RATECHANGES" 
( 
       arg_COMPANYID  IN VARCHAR2,
        arg_POLICYFORMNO IN VARCHAR2,
        arg_PTYPECATEGORY_SECONDARY IN VARCHAR2,
        arg_DATEOF_CURRENT_RATEINCR IN DATE,
        arg_PECT_CURRRNT_RATEINCR IN FLOAT,
        arg_DATEOF_2010_RATECHANGES IN DATE,
        arg_DATEOF_2009_RATECHANGES IN DATE,
        arg_DATEOF_2008_RATECHANGES IN DATE,
        arg_DATEOF_2007_RATECHANGES IN DATE,
        arg_DATEOF_2006_RATECHANGES IN DATE,
        arg_PERCENT_2010_RATEINC IN FLOAT,
        arg_PERCENT_2009_RATEINC IN FLOAT,
        arg_PERCENT_2008_RATEINC IN FLOAT,
        arg_PERCENT_2007_RATEINC IN FLOAT,
        arg_PERCENT_2006_RATEINC IN FLOAT,
        arg_LAST_LOSSRATIO_2010 IN FLOAT,
        arg_LAST_LOSSRATIO_2009 IN FLOAT,
        arg_LAST_LOSSRATIO_2008 IN FLOAT,
        arg_LAST_LOSSRATIO_2007 IN FLOAT,
        arg_LAST_LOSSRATIO_2006 IN FLOAT,
        arg_NUMOF_MSINSURED_2010 IN NUMBER,
        arg_NUMOF_MSINSURED_2009 IN NUMBER,
        arg_NUMOF_MSINSURED_2008 IN NUMBER,
        arg_NUMOF_MSINSURED_2007 IN NUMBER,
        arg_NUMOF_MSINSURED_2006 IN NUMBER,
        arg_RATTECHNAGES_ID OUT INTEGER,
        arg_ACTION_USER IN VARCHAR2,
        arg_SUMMARY_RATEINC IN VARCHAR2,
        arg_POLICY_APPROVAL IN Date,
        IsCompany_Exist OUT NUMBER)
              
     AS
BEGIN 
 SELECT COUNT(*)
      INTO IsCompany_Exist 
      FROM RATE_CHANGES
      WHERE COMPANY_ID = arg_COMPANYID AND
      POLICY_FORMNO = arg_POLICYFORMNO AND
      PTYPECATEGORY_SECONDARY = arg_PTYPECATEGORY_SECONDARY AND
      DATEOF_CURRENTRATEINCR = arg_DATEOF_CURRENT_RATEINCR AND
      PERCENT_CURRENT_RATEINC = arg_PECT_CURRRNT_RATEINCR AND
      IS_ROWDELETED = 'N';
     
      if(IsCompany_Exist = 0) then
     INSERT INTO RATE_CHANGES
      ( 
                          
                 
          COMPANY_ID,
    POLICY_FORMNO,
    PTYPECATEGORY_SECONDARY,
    DATEOF_CURRENTRATEINCR,
    PERCENT_CURRENT_RATEINC,
    DATEOF_YR_MINUS1_RATEINC,
    DATEOF_YR_MINUS2_RATEINC,
    DATEOF_YR_MINUS3_RATEINC,
    DATEOF_YR_MINUS4_RATEINC,
    DATEOF_YR_MINUS5_RATEINC,
    PERCENT_YR_MINUS1_RATEINC,
    PERCENT_YR_MINUS2_RATEINC,
    PERCENT_YR_MINUS3_RATEINC,
    PERCENT_YR_MINUS4_RATEINC,
    PERCENT_YR_MINUS5_RATEINC,
    LAST_MINUS1_LOSSRATIO,
    LAST_MINUS2_LOSSRATIO,
    LAST_MINUS3_LOSSRATIO,
    LAST_MINUS4_LOSSRATIO,
    LAST_MINUS5_LOSSRATIO,
    NUMOF_MSINSURED_YR_MINUS1,
    NUMOF_MSINSURED_YR_MINUS2,
    NUMOF_MSINSURED_YR_MINUS3,
    NUMOF_MSINSURED_YR_MINUS4,
    NUMOF_MSINSURED_YR_MINUS5,
    ACTION_USER,
    SUMMARY_RATEINC,
    POLICY_APPROVAL)

           
  

            
      VALUES 
             ( 
        arg_COMPANYID,
        arg_POLICYFORMNO,
        arg_PTYPECATEGORY_SECONDARY,
        arg_DATEOF_CURRENT_RATEINCR,
        arg_PECT_CURRRNT_RATEINCR,
        arg_DATEOF_2010_RATECHANGES,
        arg_DATEOF_2009_RATECHANGES,
        arg_DATEOF_2008_RATECHANGES,
        arg_DATEOF_2007_RATECHANGES,
        arg_DATEOF_2006_RATECHANGES,
        arg_PERCENT_2010_RATEINC,
        arg_PERCENT_2009_RATEINC,
        arg_PERCENT_2008_RATEINC,
        arg_PERCENT_2007_RATEINC,
        arg_PERCENT_2006_RATEINC,
        arg_LAST_LOSSRATIO_2010,
        arg_LAST_LOSSRATIO_2009,
        arg_LAST_LOSSRATIO_2008,
        arg_LAST_LOSSRATIO_2007,
        arg_LAST_LOSSRATIO_2006,
        arg_NUMOF_MSINSURED_2010,
        arg_NUMOF_MSINSURED_2009,
        arg_NUMOF_MSINSURED_2008,
        arg_NUMOF_MSINSURED_2007,
        arg_NUMOF_MSINSURED_2006,
        arg_ACTION_USER,
        arg_SUMMARY_RATEINC,
        arg_POLICY_APPROVAL);
        
          SELECT RATECHANGE_SEQUENCE.CURRVAL INTO arg_RATTECHNAGES_ID FROM dual ;
    end if;
                
  COMMIT;
             
  END;

/
--------------------------------------------------------
--  DDL for Procedure SP_SEARCH_ERROR_LOG
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_SEARCH_ERROR_LOG" 
(
 pAppName IN VARCHAR2,
 pErrorDescription IN VARCHAR2,
 pStartDate IN VARCHAR2,
 pEndDate IN VARCHAR2,
 p_cursor out  SYS_REFCURSOR
 )
AS 
BEGIN


 open p_cursor for 
      SELECT 
      APPLICATIONNAME,
      ERRORDATE,
      ERRORDESCRIPTION,
      SEVERITY
      FROM EXCEPTION_LOG
      
      WHERE
        
         (pAppName = 'ALL' OR APPLICATIONNAME = pAppName) 
          AND
         (pErrorDescription IS NULL OR UPPER(ERRORDESCRIPTION) LIKE '%'||UPPER(pErrorDescription)||'%') 
         AND
         (trunc(ERRORDATE) >= TO_DATE(pStartDate,'mm/dd/yyyy') OR pStartDate IS NULL)
         AND
         (trunc(ERRORDATE) <= TO_DATE(pEndDate,'mm/dd/yyyy') OR pEndDate IS NULL)
         
         ORDER by ERRORDATE DESC;
  
   
END SP_SEARCH_ERROR_LOG;

/
--------------------------------------------------------
--  DDL for Procedure SP_SEARCH_RATEREVIEWDATA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_SEARCH_RATEREVIEWDATA" ( 
p_cursor out  SYS_REFCURSOR,
argCompany_ID IN VARCHAR2,
arg_POLICY_CATEGORY IN VARCHAR2,
arg_FromDate In VARCHAR2,
arg_ToDate IN VARCHAR2)

IS
begin
if(argCompany_ID <> 'All' AND arg_POLICY_CATEGORY <> 'All'  AND arg_FromDate <> 'All' AND arg_ToDate <> 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
              PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE COMPANY_ID =argCompany_ID AND 
           PTYPECATEGORY_SECONDARY LIKE  '%'||arg_POLICY_CATEGORY||'%' AND DATEOF_CURRENTRATEINCR  BETWEEN to_date(arg_FromDate,'mm/dd/yyyy') AND to_date(arg_ToDate,'mm/dd/yyyy')
           AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
        if(argCompany_ID <> 'All' AND arg_POLICY_CATEGORY = 'All'  AND arg_FromDate = 'All' AND arg_ToDate = 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
             PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE COMPANY_ID = argCompany_ID 
                     AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
         if(argCompany_ID <> 'All' AND arg_POLICY_CATEGORY <>'All'  AND arg_FromDate = 'All' AND arg_ToDate = 'All' )then
open p_cursor for SELECT  
           RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
           PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES  WHERE COMPANY_ID =argCompany_ID AND 
           PTYPECATEGORY_SECONDARY LIKE  '%' ||arg_POLICY_CATEGORY||'%' AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
        if(argCompany_ID = 'All' AND arg_POLICY_CATEGORY <>'All'  AND arg_FromDate = 'All' AND arg_ToDate = 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
           PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES  WHERE
           PTYPECATEGORY_SECONDARY LIKE '%'||arg_POLICY_CATEGORY||'%' AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
         if(argCompany_ID = 'All' AND arg_POLICY_CATEGORY = 'All'  AND arg_FromDate <> 'All' AND arg_ToDate <> 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
             PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES  WHERE
           DATEOF_CURRENTRATEINCR  BETWEEN to_date(arg_FromDate,'mm/dd/yyyy') AND to_date(arg_ToDate,'mm/dd/yyyy')
           AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
        if(argCompany_ID = 'All' AND arg_POLICY_CATEGORY = 'All'  AND arg_FromDate = 'All' AND arg_ToDate = 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
              PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
         if(argCompany_ID <> 'All' AND arg_POLICY_CATEGORY = 'All'  AND arg_FromDate <> 'All' AND arg_ToDate <> 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
            PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE COMPANY_ID = argCompany_ID  AND DATEOF_CURRENTRATEINCR BETWEEN to_date(arg_FromDate,'mm/dd/yyyy') AND to_date(arg_ToDate,'mm/dd/yyyy')
           AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
        if(argCompany_ID = 'All' AND arg_POLICY_CATEGORY <> 'All'  AND arg_FromDate <> 'All' AND arg_ToDate <> 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
            PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE  PTYPECATEGORY_SECONDARY LIKE  '%' ||arg_POLICY_CATEGORY||'%' AND DATEOF_CURRENTRATEINCR BETWEEN to_date(arg_FromDate,'mm/dd/yyyy') AND to_date(arg_ToDate,'mm/dd/yyyy')
           AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
          if(argCompany_ID = 'All' AND arg_POLICY_CATEGORY = 'All'  AND arg_FromDate <> 'All' AND arg_ToDate = 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
            PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE   DATEOF_CURRENTRATEINCR >= to_date(arg_FromDate,'mm/dd/yyyy') 
           AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
          if(argCompany_ID = 'All' AND arg_POLICY_CATEGORY = 'All'  AND arg_FromDate = 'All' AND arg_ToDate <> 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
            PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE   DATEOF_CURRENTRATEINCR <= to_date(arg_ToDate,'mm/dd/yyyy')
           AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
        if(argCompany_ID <> 'All' AND arg_POLICY_CATEGORY <> 'All'  AND arg_FromDate <> 'All' AND arg_ToDate = 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
            PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE COMPANY_ID = argCompany_ID AND
             PTYPECATEGORY_SECONDARY LIKE  '%'||arg_POLICY_CATEGORY||'%' AND DATEOF_CURRENTRATEINCR >= to_date(arg_FromDate,'mm/dd/yyyy')
           AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
        
        if(argCompany_ID <> 'All' AND arg_POLICY_CATEGORY <> 'All'  AND arg_FromDate = 'All' AND arg_ToDate <> 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
            PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE COMPANY_ID = argCompany_ID AND
             PTYPECATEGORY_SECONDARY LIKE  '%'||arg_POLICY_CATEGORY||'%' AND DATEOF_CURRENTRATEINCR <= to_date(arg_ToDate,'mm/dd/yyyy')
           AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
         if(argCompany_ID = 'All' AND arg_POLICY_CATEGORY <> 'All'  AND arg_FromDate = 'All' AND arg_ToDate <> 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
            PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE 
             PTYPECATEGORY_SECONDARY LIKE  '%'||arg_POLICY_CATEGORY||'%' AND DATEOF_CURRENTRATEINCR <= to_date(arg_ToDate,'mm/dd/yyyy')
           AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
           if(argCompany_ID = 'All' AND arg_POLICY_CATEGORY <> 'All'  AND arg_FromDate <> 'All' AND arg_ToDate = 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
            PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE 
             PTYPECATEGORY_SECONDARY LIKE  '%'||arg_POLICY_CATEGORY||'%' AND DATEOF_CURRENTRATEINCR >= to_date(arg_FromDate,'mm/dd/yyyy')
           AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        if(argCompany_ID <> 'All' AND arg_POLICY_CATEGORY = 'All'  AND arg_FromDate <> 'All' AND arg_ToDate = 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
            PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE 
           COMPANY_ID = argCompany_ID AND DATEOF_CURRENTRATEINCR >= to_date(arg_FromDate,'mm/dd/yyyy')
           AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
         if(argCompany_ID = 'All' AND arg_POLICY_CATEGORY = 'All'  AND arg_FromDate = 'All' AND arg_ToDate <> 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
            PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE 
          DATEOF_CURRENTRATEINCR <= to_date(arg_ToDate,'mm/dd/yyyy')
           AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
         if(argCompany_ID = 'All' AND arg_POLICY_CATEGORY = 'All'  AND arg_FromDate <> 'All' AND arg_ToDate <> 'All' )then
open p_cursor for select
            RATECHANGES_ID,
             COMPANY_ID,
               (SELECT COMPANY_NAME FROM COMPANY WHERE RATE_CHANGES.COMPANY_ID = COMPANY.COMPANY_ID)AS COMPANY_NAME ,
              POLICY_FORMNO,
            PTYPECATEGORY_SECONDARY AS POLICY_TYPE ,
              to_char(DATEOF_CURRENTRATEINCR,'mm/dd/yyyy') AS DATEOF_CURRENTRATEINCR,
              PERCENT_CURRENT_RATEINC,
               to_char(DATEOF_YR_MINUS1_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS1_RATEINC,
               to_char(DATEOF_YR_MINUS2_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS2_RATEINC,
               to_char(DATEOF_YR_MINUS3_RATEINC,'mm/dd/yyyy')AS DATEOF_YR_MINUS3_RATEINC,
               to_char(DATEOF_YR_MINUS4_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS4_RATEINC,
               to_char(DATEOF_YR_MINUS5_RATEINC,'mm/dd/yyyy') AS DATEOF_YR_MINUS5_RATEINC,
              PERCENT_YR_MINUS1_RATEINC,
              PERCENT_YR_MINUS2_RATEINC,
              PERCENT_YR_MINUS3_RATEINC,
              PERCENT_YR_MINUS4_RATEINC,
              PERCENT_YR_MINUS5_RATEINC,
              LAST_MINUS1_LOSSRATIO,
              LAST_MINUS2_LOSSRATIO,
              LAST_MINUS3_LOSSRATIO,
              LAST_MINUS4_LOSSRATIO,
              LAST_MINUS5_LOSSRATIO,
              NUMOF_MSINSURED_YR_MINUS1,
              NUMOF_MSINSURED_YR_MINUS2,
              NUMOF_MSINSURED_YR_MINUS3,
              NUMOF_MSINSURED_YR_MINUS4,
              NUMOF_MSINSURED_YR_MINUS5,
               SUMMARY_RATEINC,
               to_char(POLICY_APPROVAL,'mm/dd/yyyy') AS POLICY_APPROVAL FROM RATE_CHANGES WHERE 
          DATEOF_CURRENTRATEINCR >= to_date(arg_FromDate,'mm/dd/yyyy') AND DATEOF_CURRENTRATEINCR <= to_date(arg_ToDate,'mm/dd/yyyy')
           AND IS_ROWDELETED = 'N' ORDER BY RATECHANGES_ID DESC;
        end if;
        
        
        
end SP_SEARCH_RATEREVIEWDATA;

/
--------------------------------------------------------
--  DDL for Procedure SP_UPDATE_COMPANY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_UPDATE_COMPANY" 
(
arg_COMPANYID IN VARCHAR2,
arg_COMPANYNAME IN VARCHAR2,
arg_COMPANY_LOGO_FILENAME IN VARCHAR2,
arg_LOGO_FILE_TYPE IN VARCHAR2,
arg_FILE_CONTENT IN company.file_content%type)
AS
BEGIN

  UPDATE COMPANY
  
  SET
 
  COMPANY_NAME = arg_COMPANYNAME,
  COMPANY_LOGO_FILENAME = arg_COMPANY_LOGO_FILENAME,
  LOGO_FILE_TYPE = arg_LOGO_FILE_TYPE,
  FILE_CONTENT = arg_FILE_CONTENT
  
  WHERE COMPANY_ID = arg_COMPANYID;
  COMMIT;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_UPDATE_RATECHANGES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MIDRATE"."SP_UPDATE_RATECHANGES" 
( 
        arg_RATECHANGES_ID IN INT,
        arg_COMPANYID  IN VARCHAR2,
        arg_POLICYFORMNO IN VARCHAR2,
        ARG_PTYPECATEGORY_SECONDARY IN VARCHAR2,
        arg_DATEOF_CURRENT_RATEINCR IN DATE,
        arg_PECT_CURRRNT_RATEINCR IN FLOAT,
   
        arg_DATEOF_2010_RATECHANGES IN DATE,
        arg_DATEOF_2009_RATECHANGES IN DATE,
        arg_DATEOF_2008_RATECHANGES IN DATE,
        arg_DATEOF_2007_RATECHANGES IN DATE,
        arg_DATEOF_2006_RATECHANGES IN DATE,
        arg_PERCENT_2010_RATEINC IN FLOAT,
        arg_PERCENT_2009_RATEINC IN FLOAT,
        arg_PERCENT_2008_RATEINC IN FLOAT,
        arg_PERCENT_2007_RATEINC IN FLOAT,
        arg_PERCENT_2006_RATEINC IN FLOAT,
        arg_LAST_LOSSRATIO_2010 IN FLOAT,
        arg_LAST_LOSSRATIO_2009 IN FLOAT,
        arg_LAST_LOSSRATIO_2008 IN FLOAT,
        arg_LAST_LOSSRATIO_2007 IN FLOAT,
        arg_LAST_LOSSRATIO_2006 IN FLOAT,
        arg_NUMOF_MSINSURED_2010 IN NUMBER,
        arg_NUMOF_MSINSURED_2009 IN NUMBER,
        arg_NUMOF_MSINSURED_2008 IN NUMBER,
        arg_NUMOF_MSINSURED_2007 IN NUMBER,
        arg_NUMOF_MSINSURED_2006 IN NUMBER,
        arg_ACTION_USER IN VARCHAR2,
        arg_SUMMARY_RATEINC IN VARCHAR2,
        arg_POLICY_APPROVAL IN Date)
     
              
     AS
BEGIN 

     UPDATE RATE_CHANGES SET
            COMPANY_ID = arg_COMPANYID,
              POLICY_FORMNO = arg_POLICYFORMNO,
              PTYPECATEGORY_SECONDARY = ARG_PTYPECATEGORY_SECONDARY,
              DATEOF_CURRENTRATEINCR = arg_DATEOF_CURRENT_RATEINCR,
              PERCENT_CURRENT_RATEINC= arg_PECT_CURRRNT_RATEINCR,
              DATEOF_YR_MINUS1_RATEINC = arg_DATEOF_2010_RATECHANGES,
              DATEOF_YR_MINUS2_RATEINC =arg_DATEOF_2009_RATECHANGES,
              DATEOF_YR_MINUS3_RATEINC = arg_DATEOF_2008_RATECHANGES,
              DATEOF_YR_MINUS4_RATEINC = arg_DATEOF_2007_RATECHANGES,
              DATEOF_YR_MINUS5_RATEINC = arg_DATEOF_2006_RATECHANGES,
              PERCENT_YR_MINUS1_RATEINC = arg_PERCENT_2010_RATEINC,
              PERCENT_YR_MINUS2_RATEINC = arg_PERCENT_2009_RATEINC ,
              PERCENT_YR_MINUS3_RATEINC = arg_PERCENT_2008_RATEINC,
              PERCENT_YR_MINUS4_RATEINC = arg_PERCENT_2007_RATEINC,
              PERCENT_YR_MINUS5_RATEINC = arg_PERCENT_2006_RATEINC,
              LAST_MINUS1_LOSSRATIO = arg_LAST_LOSSRATIO_2010,
              LAST_MINUS2_LOSSRATIO = arg_LAST_LOSSRATIO_2009,
              LAST_MINUS3_LOSSRATIO = arg_LAST_LOSSRATIO_2008,
              LAST_MINUS4_LOSSRATIO = arg_LAST_LOSSRATIO_2007,
              LAST_MINUS5_LOSSRATIO = arg_LAST_LOSSRATIO_2006,
              NUMOF_MSINSURED_YR_MINUS1 = arg_NUMOF_MSINSURED_2010,
              NUMOF_MSINSURED_YR_MINUS2 = arg_NUMOF_MSINSURED_2009,
              NUMOF_MSINSURED_YR_MINUS3 = arg_NUMOF_MSINSURED_2008,
              NUMOF_MSINSURED_YR_MINUS4 = arg_NUMOF_MSINSURED_2007,
              NUMOF_MSINSURED_YR_MINUS5 =  arg_NUMOF_MSINSURED_2006,
              ACTION_USER = arg_ACTION_USER,
              SUMMARY_RATEINC = arg_SUMMARY_RATEINC,
              POLICY_APPROVAL = arg_POLICY_APPROVAL
              WHERE RATECHANGES_ID = arg_RATECHANGES_ID;
                               
COMMIT;
             
  END SP_UPDATE_RATECHANGES;

/













































