--------------------------------------------------------
--  DDL for Table AUDIT_TRAIL
--------------------------------------------------------

  CREATE TABLE "MIDRATE"."AUDIT_TRAIL" 
   (	"AUDIT_ID" NUMBER(38,0), 
	"ACTION_DATE" VARCHAR2(4000 BYTE), 
	"ACTION_BY" VARCHAR2(200 BYTE), 
	"ACTION" VARCHAR2(200 BYTE), 
	"NEW_VALUE" VARCHAR2(4000 BYTE), 
	"OLD_VALUE" VARCHAR2(4000 BYTE), 
	"COLUMN_NAME" VARCHAR2(1024 BYTE), 
	"TABLE_NAME" VARCHAR2(1024 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;

  
--------------------------------------------------------
--  DDL for Table COMPANY
--------------------------------------------------------

  CREATE TABLE "MIDRATE"."COMPANY" 
   (	"COMPANY_ID" VARCHAR2(4000 BYTE), 
	"COMPANY_NAME" VARCHAR2(4000 BYTE), 
	"COMPANY_LOGO_FILENAME" VARCHAR2(1024 BYTE), 
	"LOGO_FILE_TYPE" VARCHAR2(1024 BYTE), 
	"FILE_CONTENT" BLOB, 
	"IS_ROWDELETED" CHAR(3 BYTE) DEFAULT 'N'
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" 
 LOB ("FILE_CONTENT") STORE AS BASICFILE (
  TABLESPACE "SYSTEM" ENABLE STORAGE IN ROW CHUNK 8192
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)) ;
--------------------------------------------------------
--  DDL for Index COMPANY_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MIDRATE"."COMPANY_PK" ON "MIDRATE"."COMPANY" ("COMPANY_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table COMPANY
--------------------------------------------------------

  ALTER TABLE "MIDRATE"."COMPANY" ADD CONSTRAINT "COMPANY_PK" PRIMARY KEY ("COMPANY_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
 
  ALTER TABLE "MIDRATE"."COMPANY" MODIFY ("COMPANY_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  DDL for Trigger INSERT_COMPANY_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "MIDRATE"."INSERT_COMPANY_TRIGGER" before insert on "COMPANY"    for each row begin     if inserting then       if :NEW."COMPANY_ID" is null then          select COMPANYSEQUENCE.nextval into :NEW."COMPANY_ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "MIDRATE"."INSERT_COMPANY_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Table CATEGORY
--------------------------------------------------------

  CREATE TABLE "MIDRATE"."CATEGORY" 
   (	"CATAGORY_ID" NUMBER(38,0), 
	"CATAGORY_NAME" VARCHAR2(1024 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index CATAGORY_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MIDRATE"."CATAGORY_PK" ON "MIDRATE"."CATEGORY" ("CATAGORY_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table CATEGORY
--------------------------------------------------------

  ALTER TABLE "MIDRATE"."CATEGORY" ADD CONSTRAINT "CATAGORY_PK" PRIMARY KEY ("CATAGORY_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
 
  ALTER TABLE "MIDRATE"."CATEGORY" MODIFY ("CATAGORY_ID" NOT NULL ENABLE);

--------------------------------------------------------
--  DDL for Table RATE_CHANGES
--------------------------------------------------------

  CREATE TABLE "MIDRATE"."RATE_CHANGES" 
   (	"COMPANY_ID" VARCHAR2(50 BYTE), 
	"POLICY_FORMNO" VARCHAR2(1024 BYTE), 
	"PTYPECATEGORY_SECONDARY" VARCHAR2(2500 BYTE), 
	"DATEOF_CURRENTRATEINCR" DATE, 
	"PERCENT_CURRENT_RATEINC" FLOAT(126), 
	"DATEOF_YR_MINUS1_RATEINC" DATE, 
	"DATEOF_YR_MINUS2_RATEINC" DATE, 
	"DATEOF_YR_MINUS3_RATEINC" DATE, 
	"DATEOF_YR_MINUS4_RATEINC" DATE, 
	"DATEOF_YR_MINUS5_RATEINC" DATE, 
	"PERCENT_YR_MINUS1_RATEINC" FLOAT(126), 
	"PERCENT_YR_MINUS2_RATEINC" FLOAT(126), 
	"PERCENT_YR_MINUS3_RATEINC" FLOAT(126), 
	"PERCENT_YR_MINUS4_RATEINC" FLOAT(126), 
	"PERCENT_YR_MINUS5_RATEINC" FLOAT(126), 
	"LAST_MINUS1_LOSSRATIO" FLOAT(126), 
	"LAST_MINUS2_LOSSRATIO" FLOAT(126), 
	"LAST_MINUS3_LOSSRATIO" FLOAT(126), 
	"LAST_MINUS4_LOSSRATIO" FLOAT(126), 
	"LAST_MINUS5_LOSSRATIO" FLOAT(126), 
	"NUMOF_MSINSURED_YR_MINUS1" NUMBER(38,0), 
	"NUMOF_MSINSURED_YR_MINUS2" NUMBER(38,0), 
	"NUMOF_MSINSURED_YR_MINUS3" NUMBER(38,0), 
	"NUMOF_MSINSURED_YR_MINUS4" NUMBER(38,0), 
	"NUMOF_MSINSURED_YR_MINUS5" NUMBER(38,0), 
	"RATECHANGES_ID" NUMBER(38,0), 
	"IS_ROWDELETED" CHAR(3 CHAR) DEFAULT 'N', 
	"ACTION_USER" VARCHAR2(200 BYTE), 
	"SUMMARY_RATEINC" VARCHAR2(4000 BYTE), 
	"POLICY_APPROVAL" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index RATE_CHANGES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MIDRATE"."RATE_CHANGES_PK" ON "MIDRATE"."RATE_CHANGES" ("RATECHANGES_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table RATE_CHANGES
--------------------------------------------------------

  ALTER TABLE "MIDRATE"."RATE_CHANGES" ADD CONSTRAINT "RATE_CHANGES_PK" PRIMARY KEY ("RATECHANGES_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
 
  ALTER TABLE "MIDRATE"."RATE_CHANGES" MODIFY ("RATECHANGES_ID" NOT NULL ENABLE);
 
  ALTER TABLE "MIDRATE"."RATE_CHANGES" MODIFY ("COMPANY_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table RATE_CHANGES
--------------------------------------------------------

  ALTER TABLE "MIDRATE"."RATE_CHANGES" ADD CONSTRAINT "RATE_CHANGES_FK" FOREIGN KEY ("COMPANY_ID")
	  REFERENCES "MIDRATE"."COMPANY" ("COMPANY_ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUDITTRAIL_INSERT_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "MIDRATE"."AUDITTRAIL_INSERT_TRIGGER" AFTER UPDATE OR INSERT OR DELETE ON RATE_CHANGES 
FOR EACH ROW 
declare
l_seq   number;
v_username VARCHAR2(200);
arg_ACTION VARCHAR2(200);
OLD_VALUE VARCHAR2(3000);
NEW_VALUE VARCHAR2(3000);
ARG_RATECHANGE_ID VARCHAR2(1024);
begin
select AUDIT_TRAIL_RATE_SEQUENCE.nextval into l_seq from dual;

if inserting
then arg_ACTION := 'Insert Rate Review';
elsif updating

then arg_ACTION := 'Update Rate Review';
select AUDIT_TRAIL_RATE_SEQUENCE.CURRVAL into l_seq from dual;
else arg_ACTION := 'Delete Rate Review';
select AUDIT_TRAIL_RATE_SEQUENCE.CURRVAL into l_seq from dual;
end if;
    
          
     audit_pkg.CHECK_VAL_PFORM( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.POLICY_FORMNO,:OLD.POLICY_FORMNO,'Policy Form No','Rate_Changes Table');
     audit_pkg.CHECK_VAL_PType( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.PTYPECATEGORY_SECONDARY,:OLD.PTYPECATEGORY_SECONDARY,'Policy Type','Rate_Changes Table');
      audit_pkg.CHECK_VAL_DCURRNT( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.DATEOF_CURRENTRATEINCR,:OLD.DATEOF_CURRENTRATEINCR,'Date Of Current Rate Increase','Rate_Changes Table');
                                                
      audit_pkg.CHECK_VAL_PERCENT( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.PERCENT_CURRENT_RATEINC,:OLD.PERCENT_CURRENT_RATEINC,'Percent Current Rate Increase','Rate_Changes Table');
     
                                               
      audit_pkg.CHECK_VAL_DATE1( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.DATEOF_YR_MINUS1_RATEINC,:OLD.DATEOF_YR_MINUS1_RATEINC,'Date of 2010 Rate Increase','Rate_Changes Table');
      
                                             
      audit_pkg.CHECK_VAL_DATE2( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.DATEOF_YR_MINUS2_RATEINC,:OLD.DATEOF_YR_MINUS2_RATEINC,'Date of 2009 Rate Increase','Rate_Changes Table');
                                    
      audit_pkg.CHECK_VAL_DATE3( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.DATEOF_YR_MINUS3_RATEINC,:OLD.DATEOF_YR_MINUS3_RATEINC,'Date of 2008 Rate Increase','Rate_Changes Table');
                                                  
      audit_pkg.CHECK_VAL_DATE4( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.DATEOF_YR_MINUS4_RATEINC,:OLD.DATEOF_YR_MINUS4_RATEINC,'Date of 2007 Rate Increase','Rate_Changes Table');
          
                                            
      audit_pkg.CHECK_VAL_DATE5( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.DATEOF_YR_MINUS5_RATEINC,:OLD.DATEOF_YR_MINUS5_RATEINC,'Date of 2006 Rate Increase','Rate_Changes Table');
                                                                                                                         
                                        
      audit_pkg.CHECK_VAL_PCT1( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.PERCENT_YR_MINUS1_RATEINC,:OLD.PERCENT_YR_MINUS1_RATEINC,'Percent 2010 Rate Increase','Rate_Changes Table');
                           
                                        
      audit_pkg.CHECK_VAL_PCT2( l_seq,SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.PERCENT_YR_MINUS2_RATEINC,:OLD.PERCENT_YR_MINUS2_RATEINC,'Percent 2009 Rate Increase','Rate_Changes Table');
                                             
                                       
      audit_pkg.CHECK_VAL_PCT3( l_seq,SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.PERCENT_YR_MINUS3_RATEINC,:OLD.PERCENT_YR_MINUS3_RATEINC,'Percent 2008 Rate Increase','Rate_Changes Table');
           
                                      
      audit_pkg.CHECK_VAL_PCT4( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.PERCENT_YR_MINUS4_RATEINC,:OLD.PERCENT_YR_MINUS4_RATEINC,'Percent 2007 Rate Increase','Rate_Changes Table');
                           
                                                   
      audit_pkg.CHECK_VAL_PCT5( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.PERCENT_YR_MINUS5_RATEINC,:OLD.PERCENT_YR_MINUS5_RATEINC,'Percent 2006 Rate Increase','Rate_Changes Table');
                                                                                                                                                                                                                                               
                                          
      audit_pkg.CHECK_VAL_LOSSR1( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.LAST_MINUS1_LOSSRATIO,:OLD.LAST_MINUS1_LOSSRATIO,'Loss Ratio 2010','Rate_Changes Table');
                                          
           
      audit_pkg.CHECK_VAL_LOSSR2( l_seq,SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.LAST_MINUS2_LOSSRATIO,:OLD.LAST_MINUS2_LOSSRATIO,'Loss Ratio 2009','Rate_Changes Table');
                            
      audit_pkg.CHECK_VAL_LOSSR3( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.LAST_MINUS3_LOSSRATIO,:OLD.LAST_MINUS3_LOSSRATIO,'Loss Ratio 2008','Rate_Changes Table');
                                          
                                           
      audit_pkg.CHECK_VAL_LOSSR4( l_seq,SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.LAST_MINUS4_LOSSRATIO,:OLD.LAST_MINUS4_LOSSRATIO,'Loss Ratio 2007','Rate_Changes Table');
                                      
      audit_pkg.CHECK_VAL_LOSSR5( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.LAST_MINUS5_LOSSRATIO,:OLD.LAST_MINUS5_LOSSRATIO,'Loss Ratio 2006','Rate_Changes Table');
                                          
                                                
      audit_pkg.CHECK_VAL_NUM1( l_seq,SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.NUMOF_MSINSURED_YR_MINUS1,:OLD.NUMOF_MSINSURED_YR_MINUS1,'Number Of Missippi Insureds* 2010','Rate_Changes Table');
                                                     
                   
                                             
      audit_pkg.CHECK_VAL_NUM2( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.NUMOF_MSINSURED_YR_MINUS2,:OLD.NUMOF_MSINSURED_YR_MINUS2,'Number Of Missippi Insureds* 2009','Rate_Changes Table');
                                                     
                                         
                                                                   
      audit_pkg.CHECK_VAL_NUM3( l_seq,SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.NUMOF_MSINSURED_YR_MINUS3,:OLD.NUMOF_MSINSURED_YR_MINUS3,'Number Of Missippi Insureds* 2008','Rate_Changes Table');
                                                     
                                         
                                                                   
      audit_pkg.CHECK_VAL_NUM4( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.NUMOF_MSINSURED_YR_MINUS4,:OLD.NUMOF_MSINSURED_YR_MINUS4,'Number Of Missippi Insureds* 2007','Rate_Changes Table');
                                                     
                                       
                                                                   
      audit_pkg.CHECK_VAL_NUM5( l_seq, SYSDATE,:NEW.ACTION_USER,
                           arg_ACTION,:NEW.NUMOF_MSINSURED_YR_MINUS5,:OLD.NUMOF_MSINSURED_YR_MINUS5,'Number Of Missippi Insureds* 2006','Rate_Changes Table');
                                                                                                    
                                          
end;
/
ALTER TRIGGER "MIDRATE"."AUDITTRAIL_INSERT_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger INSERT_RATECHANGES_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "MIDRATE"."INSERT_RATECHANGES_TRIGGER" before insert on "RATE_CHANGES"    for each row begin     if inserting then       if :NEW."RATECHANGES_ID" is null then          select RATECHANGE_SEQUENCE.nextval into :NEW."RATECHANGES_ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "MIDRATE"."INSERT_RATECHANGES_TRIGGER" ENABLE;

--------------------------------------------------------
--  DDL for Table FILE_UPLOAD_RATEREVIEW
--------------------------------------------------------

  CREATE TABLE "MIDRATE"."FILE_UPLOAD_RATEREVIEW" 
   (	"ID" NUMBER, 
	"UPLOAD_DATE" DATE, 
	"CONTENT" BLOB, 
	"TYPE" VARCHAR2(1024 BYTE), 
	"NAME" VARCHAR2(1024 BYTE), 
	"LENGTH" NUMBER, 
	"RATECHANGES_ID" NUMBER(38,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" 
 LOB ("CONTENT") STORE AS BASICFILE (
  TABLESPACE "SYSTEM" ENABLE STORAGE IN ROW CHUNK 8192
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)) ;
--------------------------------------------------------
--  DDL for Index PK_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "MIDRATE"."PK_ID" ON "MIDRATE"."FILE_UPLOAD_RATEREVIEW" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table FILE_UPLOAD_RATEREVIEW
--------------------------------------------------------

  ALTER TABLE "MIDRATE"."FILE_UPLOAD_RATEREVIEW" ADD CONSTRAINT "PK_ID" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
 
  ALTER TABLE "MIDRATE"."FILE_UPLOAD_RATEREVIEW" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "MIDRATE"."FILE_UPLOAD_RATEREVIEW" MODIFY ("UPLOAD_DATE" NOT NULL ENABLE);
 
  ALTER TABLE "MIDRATE"."FILE_UPLOAD_RATEREVIEW" MODIFY ("CONTENT" NOT NULL ENABLE);
 
  ALTER TABLE "MIDRATE"."FILE_UPLOAD_RATEREVIEW" MODIFY ("TYPE" NOT NULL ENABLE);
 
  ALTER TABLE "MIDRATE"."FILE_UPLOAD_RATEREVIEW" MODIFY ("NAME" NOT NULL ENABLE);
 
  ALTER TABLE "MIDRATE"."FILE_UPLOAD_RATEREVIEW" MODIFY ("LENGTH" NOT NULL ENABLE);
--------------------------------------------------------
--  DDL for Trigger FILE_UPLOAD_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "MIDRATE"."FILE_UPLOAD_TRIGGER" before insert on "FILE_UPLOAD_RATEREVIEW"    for each row begin     if inserting then       if :NEW."ID" is null then          select FILE_UPLOAD_SEQUENCE.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "MIDRATE"."FILE_UPLOAD_TRIGGER" ENABLE;

--------------------------------------------------------
--  DDL for Table EXCEPTION_LOG
--------------------------------------------------------

  CREATE TABLE "MIDRATE"."EXCEPTION_LOG" 
   ("SEVERITY" VARCHAR2(20 BYTE), 
	"APPLICATIONNAME" VARCHAR2(40 BYTE), 
	"ERRORDESCRIPTION" NCLOB, 
	"ERRORDATE" TIMESTAMP (6)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" 
 LOB ("ERRORDESCRIPTION") STORE AS BASICFILE (
  TABLESPACE "SYSTEM" ENABLE STORAGE IN ROW CHUNK 8192
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)) ;
