﻿Imports System.Data
Imports System.Linq
Partial Public Class Companies
    Inherits System.Web.UI.Page
    Dim isEdit As String = ""

    Public Property sortOrder() As String
        Get
            If ViewState("sortOrder").ToString() = "desc" Then
                ViewState("sortOrder") = "asc"
            Else
                ViewState("sortOrder") = "desc"
            End If

            Return ViewState("sortOrder").ToString()
        End Get
        Set(ByVal value As String)
            ViewState("sortOrder") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        hyperLink1.Visible = False

        If Not IsPostBack Then

            Dim strAdmin As String = ""
            If Not (Request.QueryString("ad") Is Nothing) Then
                strAdmin = Request.QueryString("ad").ToString()
            End If

            If Session.Item("PageIndex") <> Nothing Then
                Dim pageind As Integer = CType(Session.Item("PageIndex"), Integer)
                GridViewCompanies.PageIndex = pageind

            End If

            lblCompany.Text = "Add Company"
            txtCompanyName.Text = ""

            ViewState("sortOrder") = ""

            ' Checks whether the use has admin rights.
            ' if yes then the edit and delete button are visible else they are hidden
            CheckForUserRights()

            PopulateGridView()

        Else
            ApplyPaging()
        End If

        If Session.Item("isEdit") <> Nothing Then
            isEdit = CType(Session.Item("isEdit"), String)
            If (isEdit = "Y") Then
                'btnPopupSubmit.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnPopupSubmit.UniqueID, "")

            Else
                lblCompany.Text = "Add Company"
                txtCompanyName.Text = ""

            End If

        Else

            btnPopupSubmit.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnPopupSubmit.UniqueID, "")

        End If
        btnClosePopup.Attributes.Add("style", "visibility : hidden")

    End Sub


    Private Sub CheckForUserRights()
        Dim roleList As New ArrayList()
        Dim rightsList As New ArrayList()
        Dim isUserExists As Integer = 0
        Dim createRight As Boolean = False
        Dim editRight As Boolean = False
        Dim deleteRight As Boolean = False
        Dim strUserName As String = ""
        Dim AnonymousRole As String = ConfigurationManager.AppSettings("MidRateReview.AnonymousRole")
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()

        If Not (Request.Cookies("userInfo") Is Nothing) Then
            strUserName = Convert.ToString(Request.Cookies("userInfo")("userName"))

        End If

        If (strUserName <> "") Then
            isUserExists = rateChangesBL.IsUserExists(strUserName)
        End If

        If (isUserExists > 0) Then
            roleList = rateChangesBL.GetRolesForUser(strUserName)
            Dim strRole As String
            For Each strRole In roleList
                Dim strAssignedRole As String = strRole

                rightsList = rateChangesBL.GetRightsForUser(strAssignedRole)

                If (rightsList.Contains("CreateRateReviewDashBoard")) Then
                    createRight = True
                End If

                If (rightsList.Contains("EditRateReviewDashBoard")) Then
                    editRight = True
                End If

                If (rightsList.Contains("DeleteRateReviewDashBoard")) Then
                    deleteRight = True
                End If

            Next

        End If

        If createRight = True And editRight = True And deleteRight = True Then
            hyperLinkAddUrl.Visible = True
            btnMidBlog.Visible = True
            GridViewCompanies.Columns(0).Visible = True
            GridViewCompanies.Columns(1).Visible = True
        Else
            hyperLinkAddUrl.Visible = False
            GridViewCompanies.Columns(0).Visible = False
            GridViewCompanies.Columns(1).Visible = False
            btnMidBlog.Visible = False
        End If

    End Sub


    Private Sub PopulateGridView(Optional ByVal CompanyId As Integer = 0, Optional ByVal SortExpr As String = "", Optional ByVal SortOrder As String = "")
        Dim companyDs As New DataSet
        Dim companyDsCopy As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        companyDs = rateChangesBL.GetCompanyDetails()

        companyDsCopy = companyDs.Copy()

        If (Not companyDs Is Nothing AndAlso companyDs.Tables(0).Rows.Count > 0) Then

            '' logic for displaying the inserted record first in the grid.
            If (CompanyId <> 0) Then

                Dim dv As DataView = companyDs.Tables(0).DefaultView

                dv.RowFilter = "COMPANY_ID = " + CompanyId.ToString()

                Dim dr As DataRowView = dv(0)
                Dim drDel As DataRow = dr.Row
                Dim drCopy As DataRow = dr.Row

                If (Not dr Is Nothing) Then

                    Dim selectedRow As DataRow = dr.Row
                    Dim newRow As DataRow = companyDs.Tables(0).NewRow()
                    newRow.ItemArray = selectedRow.ItemArray
                    ' copy data
                    companyDs.Tables(0).Rows.Remove(selectedRow)
                    companyDs.Tables(0).Rows.InsertAt(newRow, 0)
                    companyDs.AcceptChanges()

                    companyDsCopy = companyDs.Copy()

                End If

            Else

                companyDsCopy = companyDs.Copy()

            End If

        End If

        Dim dvCompany As DataView = New DataView

        If companyDsCopy.Tables.Count > 0 Then

            dvCompany = companyDsCopy.Tables(0).DefaultView

        End If

        If (SortExpr <> "") Then
            dvCompany.Sort = SortExpr
        End If


        GridViewCompanies.DataSource = dvCompany
        GridViewCompanies.DataBind()

    End Sub



    Protected Sub btnPopupSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPopupSubmit.Click
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        Dim intCompanyID As Integer = 0
        Dim IsEdit As String = ""
        Dim strRelatedUrlTxt As String = ""
        Dim strUserName As String = ""
        Dim strCurrentDate As String = ""

        If Session.Item("isEdit") <> Nothing Then
            IsEdit = CType(Session.Item("isEdit"), String)
        End If

        If Session.Item("CompanyID") <> Nothing Then

            intCompanyID = CType(Session.Item("CompanyID"), Integer)
        End If

        If Not (Request.Cookies("userInfo") Is Nothing) Then
            strUserName = Convert.ToString(Request.Cookies("userInfo")("userName"))

        End If

        strCurrentDate = Date.Now.ToString()

        If (FileUploadLogo.HasFile) Then

            Dim filename As String
            If (FileUploadLogo.PostedFile.ContentType = "image/jpeg") Then

                If (FileUploadLogo.PostedFile.ContentLength < 204800) Then

                    filename = System.IO.Path.GetFileName(FileUploadLogo.FileName)

                    Dim fs As System.IO.Stream
                    fs = FileUploadLogo.PostedFile.InputStream
                    Dim filesize As Long = fs.Length
                    Dim buffer(filesize) As Byte

                    Dim q As Integer = fs.Read(buffer, 0, Convert.ToInt32(fs.Length))
                    fs.Close()

                    If (IsEdit = "Y") Then

                        rateChangesBL.UpdateCompany(txtCompanyName.Text, intCompanyID.ToString(), filename, FileUploadLogo.PostedFile.ContentType, buffer, strUserName, strCurrentDate)
                        'Session.Clear()
                        PopulateGridView()
                        lblUploadStatus.Text = "Company Details Updated Successfully"
                    Else

                        intCompanyID = rateChangesBL.InsertCompany(txtCompanyName.Text, filename, FileUploadLogo.PostedFile.ContentType, buffer, strUserName, strCurrentDate)
                        If intCompanyID <> 0 Then
                            lblUploadStatus.Text = "New Company Details Added Successfully"
                            PopulateGridView(intCompanyID)
                        Else
                            lblUploadStatus.Text = "Company already Exists"
                        End If
                    End If

                Else
                    lblUploadStatus.Text = "The Image file has to be less than 200 kb!"
                End If

            Else
                lblUploadStatus.Text = "Only JPEG files are accepted!"
            End If

        Else
            Dim dsCompany As DataSet = rateChangesBL.GetCompanyLogoByID(intCompanyID.ToString())
            Dim fileName As String = ""
            Dim fileType As String = ""
            Dim buffer() As Byte = Nothing
            Dim dr As DataRow
            Dim dt As DataTable
            If Not (dsCompany Is Nothing) Then
                dt = dsCompany.Tables(0)

                For Each dr In dt.Rows
                    If Not (dr("COMPANY_LOGO_FILENAME") Is Nothing) Then
                        fileName = dr("COMPANY_LOGO_FILENAME").ToString()
                    End If
                    If Not dr("LOGO_FILE_TYPE") Is Nothing Then
                        fileType = dr("LOGO_FILE_TYPE").ToString()
                    End If
                    If dr("FILE_CONTENT").ToString() <> System.DBNull.Value.ToString Then
                        buffer = CType(dr("FILE_CONTENT"), Byte())

                    End If
                Next
            End If

            If (IsEdit = "Y") Then

                rateChangesBL.UpdateCompany(txtCompanyName.Text, intCompanyID.ToString(), fileName, FileUploadLogo.PostedFile.ContentType, buffer, strUserName, strCurrentDate)
                'Session.Clear()
                PopulateGridView()
                lblUploadStatus.Text = "Company Details Updated Successfully"
            Else

                intCompanyID = rateChangesBL.InsertCompany(txtCompanyName.Text, fileName, FileUploadLogo.PostedFile.ContentType, buffer, strUserName, strCurrentDate)
                If intCompanyID <> 0 Then
                    lblUploadStatus.Text = "New Company Details Added Successfully"
                    PopulateGridView(intCompanyID)
                Else
                    lblUploadStatus.Text = "Company already Exists"

                End If
            End If

        End If

        Session.Clear()

        txtCompanyName.Text = ""

    End Sub


    Protected Sub GridViewCompanies_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridViewCompanies.RowCommand
        Dim intCompanyID As Integer
        If e.CommandName.ToLower() = "edt" Then
            'btnPopupSubmit.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnPopupSubmit.UniqueID, "")
            btnPopupSubmit.OnClientClick = String.Format("return confirm('The Company name change will be applied to all the  rate review files under that name .Are you sure you want to edit company name');")
            Session("isEdit") = "Y"
            isEdit = True
            ModalPopupExtender1.Show()
            lblCompany.Text = "Edit Company"
            Dim lb As LinkButton = CType(e.CommandSource, LinkButton)
            'getting clicked link button      
            intCompanyID = CType(lb.Parent.FindControl("hfKey"), HiddenField).Value
            Session("CompanyID") = intCompanyID
            Dim companyDs As New DataSet
            Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
            companyDs = rateChangesBL.GetCompanyByCompanyID(intCompanyID)

            Dim dr As DataRow
            Dim dt As DataTable
            '<code to fill the dataset>
            dt = companyDs.Tables(0)
            For Each dr In dt.Rows
                txtCompanyName.Text = dr("COMPANY_NAME").ToString()
            Next
        ElseIf e.CommandName.ToLower() = "dlt" Then
            Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
            Dim lb As LinkButton = CType(e.CommandSource, LinkButton)
            'getting clicked link button      
            Dim intCompID As Integer = CType(lb.Parent.FindControl("hfKey"), HiddenField).Value
            'getting PK Value   
            rateChangesBL.DeleteCompany(intCompID)
            PopulateGridView()
        End If


    End Sub



    Sub ApplyPaging()

        Dim row As GridViewRow = GridViewCompanies.BottomPagerRow
        Dim ph As PlaceHolder = New PlaceHolder
        Dim lnkPaging As LinkButton
        Dim lnkFirstPage As LinkButton
        Dim lnkPrevPage As LinkButton
        Dim lnkNextPage As LinkButton
        Dim lnkLastPage As LinkButton
        lnkFirstPage = New LinkButton()
        lnkFirstPage.Text = Server.HtmlEncode("<<First")
        lnkFirstPage.Width = Unit.Pixel(50)
        lnkFirstPage.CommandName = "Page"
        lnkFirstPage.CommandArgument = "First"
        lnkPrevPage = New LinkButton()
        lnkPrevPage.Text = Server.HtmlEncode("<Prev")
        lnkPrevPage.Width = Unit.Pixel(50)
        lnkPrevPage.CommandName = "Page"
        lnkPrevPage.CommandArgument = "Prev"

        If (GridViewCompanies.PageCount <> 0) Then
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkFirstPage)
            ph.Controls.Add(lnkPrevPage)
        End If
        If (GridViewCompanies.PageIndex = 0) Then

            lnkFirstPage.Enabled = False
            lnkPrevPage.Enabled = False
        Else
            lnkPrevPage.Enabled = True
        End If


        Dim i As Integer
        For i = 1 To GridViewCompanies.PageCount

            lnkPaging = New LinkButton()
            lnkPaging.Width = Unit.Pixel(20)
            ' lnkPaging.CssClass = "LinkPaging"
            lnkPaging.Text = i.ToString()
            lnkPaging.CommandName = "Page"
            lnkPaging.CommandArgument = i.ToString()
            ' If (i = gridRateChanges.PageIndex + 1) Then
            ' lnkPaging.BackColor = System.Drawing.Color.Blue
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkPaging)
            ' End If

        Next i

        lnkNextPage = New LinkButton()
        lnkNextPage.Text = Server.HtmlEncode("Next>")
        lnkNextPage.Width = Unit.Pixel(50)
        lnkNextPage.CommandName = "Page"
        lnkNextPage.CommandArgument = "Next"
        lnkLastPage = New LinkButton()
        lnkLastPage.Text = Server.HtmlEncode("Last>>")
        lnkLastPage.Width = Unit.Pixel(50)
        lnkLastPage.CommandName = "Page"
        lnkLastPage.CommandArgument = "Last"
        lnkNextPage.Enabled = True
        lnkLastPage.Enabled = True
        If (GridViewCompanies.PageCount <> 0) Then
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkNextPage)
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkLastPage)
        End If
        If (GridViewCompanies.PageIndex + 1 = GridViewCompanies.PageCount) Then

            lnkNextPage.Enabled = False
            lnkLastPage.Enabled = False
        End If



    End Sub
    Protected Sub GridViewCompanies_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewCompanies.PageIndexChanging

        Try
            If e.NewPageIndex <> -1 Then
                GridViewCompanies.PageIndex = e.NewPageIndex
                PopulateGridView()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridViewCompanies_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles GridViewCompanies.DataBound
        ApplyPaging()
    End Sub


    Protected Sub GridViewCompanies_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewCompanies.RowDataBound

        '' logic for highlighting the edited record in the gridview
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim rowView As DataRowView = DirectCast(e.Row.DataItem, DataRowView)

            Dim CompanyID As Integer = Convert.ToInt32(GridViewCompanies.DataKeys(e.Row.RowIndex).Value)
            If (CompanyID = CType(Session.Item("CompanyID"), Integer)) Then

                e.Row.Attributes.Add("style", "background-color: #c7dded")

            End If

        End If
    End Sub

    Protected Sub btnMidRateReviewHome_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMidRateReviewHome.Click

        Dim strLogout As String = ConfigurationManager.AppSettings("CompareRateChanges")
        Dim strLogoutUrl As String = strLogout + "/CompareRateChanges.aspx"
        Response.Redirect(strLogoutUrl, False)
    End Sub

    Protected Sub btnClosePopup_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClosePopup.Click
        Session.Clear()
        txtCompanyName.Text = ""
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Session.Clear()
        txtCompanyName.Text = ""
    End Sub

    Protected Sub btnMidBlog_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMidBlog.Click
        Dim strLogout As String = ConfigurationManager.AppSettings("MidRateReview.logout")
        Dim strLogoutUrl As String = strLogout + "/"
        Response.Redirect(strLogoutUrl, False)
    End Sub


    Private Sub GridViewCompanies_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GridViewCompanies.Sorting

        '' GridView Sorting 
        If (e.SortDirection = SortDirection.Ascending) Then
            e.SortDirection = SortDirection.Descending
        Else
            e.SortDirection = SortDirection.Ascending
        End If

        PopulateGridView(0, e.SortExpression, e.SortDirection)

    End Sub

End Class