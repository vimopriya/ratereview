﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddNewCompany.aspx.vb" Inherits="MidWebApp.AddNewCompany" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>

    <link href="css/grid.css" rel="stylesheet" media="screen" type="text/css" />
    <link href="css/main.css" rel="stylesheet" media="screen" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
       <div class="login">
         <div class="col_8 center-all">
                <h2 class="h2-blu gutter20-t">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Add New Company"></asp:Label></h2>
               
                <div class="col_7 loginbox center-all">
                
                <label class="co-label"><asp:Label ID="Label2" runat="server" Text="Company Name"></asp:Label></label>
                <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox>
                <br /><br />
                <label class="co-label"><asp:Label ID="Label3" runat="server" Text="Upload Company Logo "></asp:Label></label>
                <asp:FileUpload ID="FileUploadLogo" runat="server" />
                <br />
                <dl>
                    <dd>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn-form" />
                    </dd>
                    <dd>
                        <asp:Label ID="StatusLabel" runat="server"></asp:Label>
                    </dd>
                </dl>
           </div>
        </div>
    </div>
    </form>
</body>
</html>
