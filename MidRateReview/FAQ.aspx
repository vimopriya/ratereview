﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master"  CodeBehind="FAQ.aspx.vb" Inherits="MidWebApp.FAQ"  title="FAQ" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
<link href="css/grid.css" rel="stylesheet" media="screen" type="text/css" />
  
    <div id="page_header3">
     <asp:Label ID="lblTitle" runat="server" Text="FAQ" cssClass="h2-form" ></asp:Label>   
         
       <asp:LinkButton ID="btnMidRateReviewHome" CssClass="logout" runat="server"  CausesValidation="False" >MID 
        Rate Review Home</asp:LinkButton>
                   <asp:LinkButton ID="btnMidBlog" CssClass="logout" runat="server"  CausesValidation="False" >MID 
        Blog Home</asp:LinkButton> 
        
    </div>
    
    <div class="spacer"></div>
    <asp:Button ID="btnReturn" runat="server" CssClass="btn-form" Text="Back" 
     CausesValidation="false"/> 
<div class="left col_15x gutter15">
    <div class="tabs-container">
     <asp:Menu id="menuTabs" CssClass="tabs-basic" Orientation="Horizontal"        
            Runat="server" Height="26px" StaticSelectedStyle-CssClass="tab-selected">
             <Items>
                <asp:MenuItem Value="Resources" Text="Resources"/>
                <asp:MenuItem Value="RelatedLinks" Text="Related Links" />  
                <asp:MenuItem Value="FAQ" selected = "true" Text="FAQ" /> 
              </Items>
    </asp:Menu>
    </div>
</div>

<div class="row resources faq-links">
           <br />
           <asp:Literal ID = "LiteralFaq" runat="server"></asp:Literal>
           
           <ul class="list-bullet">
           <li><asp:HyperLink ID="Hlink1"  href="/MidRateChanges/latestFilingsDashBoard.aspx" Text="Link to Mississippi Rate Reviews On Healthcare.gov " runat="server"></asp:HyperLink></li>
             
           </ul>
           <br />
</div>





</asp:Content>
