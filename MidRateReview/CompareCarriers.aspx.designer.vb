﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.3625
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class CompareCarriers

    '''<summary>
    '''headingSpan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents headingSpan As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''linkLogout control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents linkLogout As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnReturn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnReturn As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''menuTabs control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents menuTabs As Global.System.Web.UI.WebControls.Menu

    '''<summary>
    '''CompareChart control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CompareChart As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''hLinkResource control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hLinkResource As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''hLinkFAQ control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hLinkFAQ As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''hLinkFeedBack control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hLinkFeedBack As Global.System.Web.UI.WebControls.HyperLink
End Class
