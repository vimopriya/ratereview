﻿Imports RateChangesBL

Partial Public Class ViewErrorLog
    Inherits System.Web.UI.Page

    Private Const ERROR_PAGE As String = "ErrorPage.aspx"

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            PopulateGridView()
        End If


    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("RateChangesDashBoard.aspx")
    End Sub

   
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub gridErrorLog_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridErrorLog.PageIndexChanging
        gridErrorLog.PageIndex = e.NewPageIndex
        PopulateGridView()
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        PopulateGridView()
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnLink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLink.Click
        Dim aCookie As HttpCookie = New HttpCookie("userInfo")
        aCookie.Expires = DateTime.Now.AddDays(-1)
        Response.Cookies.Add(aCookie)

        If Not (Request.Cookies("userInfo") Is Nothing) Then
            Dim userName As String = Convert.ToString(Request.Cookies("userInfo")("userName"))

        End If
        Dim strLogout As String = ConfigurationManager.AppSettings("MidRateReview.logout")
        Dim strLogoutUrl As String = strLogout + "/Account/login.aspx"
        Response.Redirect(strLogoutUrl, False)
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateGridView()

        Dim dtErrorLog As New DataTable
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        Dim ErrorDesc As String = String.Empty
        Dim StartDate As String = String.Empty
        Dim EndDate As String = String.Empty

        Try

            ErrorDesc = Server.HtmlEncode(txtSearchOn.Text.ToString())
            StartDate = Server.HtmlEncode(txtFromDate.Text.ToString())
            EndDate = Server.HtmlEncode(txtToDate.Text.ToString())

            dtErrorLog = rateChangesBL.GetErrorLogs("ALL", ErrorDesc, StartDate, EndDate)
            gridErrorLog.DataSource = dtErrorLog
            gridErrorLog.DataBind()

        Catch ex As Exception
            Admin.WriteLog(ex)
        End Try

    End Sub

End Class