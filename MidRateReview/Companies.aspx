﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master"
    CodeBehind="Companies.aspx.vb" Inherits="MidWebApp.Companies" Title="Companies" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
<script type="text/javascript">

function fnClickOK(sender, e) {   
 __doPostBack(sender,e);
}

</script>

    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <link href="css/grid.css" rel="stylesheet" media="screen" type="text/css" />
    <div id="page_header3">
        <asp:Label ID="lblTitle" runat="server" Text="Companies" CssClass="h2-form "></asp:Label>
        <asp:LinkButton ID="btnMidRateReviewHome" CssClass="logout" runat="server" CausesValidation="False">MID 
        Rate Review Home</asp:LinkButton>
        <asp:LinkButton ID="btnMidBlog" CssClass="logout" runat="server" CausesValidation="False">MID 
        Blog Home</asp:LinkButton>
    </div>
    <div class="left col_15x gutter15">
        <div class="tabs-container">
        </div>
    </div>
    <div class="col_16x left gutter15">
        <asp:HyperLink CssClass="btn-form left margin0" ID="hyperLink1" runat="server">Back</asp:HyperLink>
        <asp:Label ID="lblUploadStatus" runat="server" CssClass="validators"></asp:Label>
    </div>
    <div class="center-all gutter10 col_11 resources">
        <asp:HyperLink CssClass="btn-wht-form left margin0" ID="hyperLinkAddUrl" runat="server">Add 
    New Company</asp:HyperLink>
        <asp:GridView ID="GridViewCompanies" AllowPaging="True" PageSize="10" EmptyDataText="No Record Available"
            AllowSorting="True" CssClass="grid-vrc gridclass rate-changes col_11" runat="server"
            AutoGenerateColumns="False" DataKeyNames="Company_ID">
            <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:LinkButton ID="Edit" CommandName="edt" CssClass="btn-edit" runat="server" ToolTip="Edit"></asp:LinkButton>
                        &nbsp;&nbsp;
                        <asp:HiddenField ID="hfKey" runat="server" Value='<%#Eval("COMPANY_ID") %>' />
                    </ItemTemplate>
                    <HeaderStyle CssClass="edit-col" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete">
                    <ItemTemplate>
                        <asp:LinkButton ID="Delete" CommandName="dlt" runat="server" CssClass="btn-delete "
                            ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete this Company?');"></asp:LinkButton>
                    </ItemTemplate>
                    <HeaderStyle CssClass="delete-col" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Company ID" ControlStyle-CssClass="txt-left" SortExpression="COMPANY_ID">
                    <ItemTemplate>
                        <%#Container.DataItem("COMPANY_ID")%>
                    </ItemTemplate>
                    <ControlStyle CssClass="url-col" />
                    <HeaderStyle CssClass="url-col" />
                    <ItemStyle CssClass="url-display" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Company Name" ControlStyle-CssClass="txt-left" SortExpression="COMPANY_NAME">
                    <ItemTemplate>
                        <%#Container.DataItem("COMPANY_NAME")%>
                    </ItemTemplate>
                    <ControlStyle CssClass="url-col left" />
                    <HeaderStyle CssClass="url-col" />
                    <ItemStyle CssClass="url-display left" />
                </asp:TemplateField>
            </Columns>
            <PagerStyle HorizontalAlign="left" />
            <PagerTemplate>
                <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </PagerTemplate>
            <AlternatingRowStyle CssClass="rowcoloralt" />
        </asp:GridView>
        <p class="txt-gry col_5 gutter10-l left">
            You are viewing page <strong>
                <%=GridViewCompanies.PageIndex + 1%></strong> of <strong>
                    <%=GridViewCompanies.PageCount%></strong>
        </p>
    </div>
    <div class="spacer col_15">
    </div>
    <asp:Button ID="btnClosePopup" runat="server"></asp:Button>
    <asp:Panel ID="panEdit" runat="server" CssClass="ModalWindow">
        <div class="col_7 center-all login">
            <h2 class="h2-blu gutter20-t">
                <asp:Label ID="lblCompany" runat="server" Font-Bold="True" Text="Add New Company"></asp:Label></h2>
            <div class="col_7 loginbox center-all">
                <label class="co-label">
                    <asp:Label ID="lblCompanyName" runat="server" Text="Company Name"></asp:Label></label>
                <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox>
                <br />
                <br />
                <label class="co-label">
                    <asp:Label ID="lblFileUpload" runat="server" Text="Upload Company Logo "></asp:Label></label>
                <asp:FileUpload ID="FileUploadLogo" runat="server" />
                <br />
                <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
                <dl>
                    <dd>
                        <asp:Button ID="btnPopupSubmit" runat="server" Text="Save" OnClick="btnPopupSubmit_Click"
                            OnClientClick="return confirm('The Company name change will be applied to all the  rate review files under that name .Are you sure you want to edit company name');"
                            CssClass="btn-form" CausesValidation="false" />
                        <asp:Button ID="btnCancel" CssClass="btn-wht-form" runat="server" Text="Cancel" CausesValidation="false" />
                    </dd>
                    <dd>
                        <asp:Label ID="StatusLabel" runat="server"></asp:Label>
                    </dd>
                </dl>
            </div>
        </div>
    </asp:Panel>
    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="hyperLinkAddUrl"
        PopupControlID="panEdit" BackgroundCssClass="modalBackground" CancelControlID="btnClosePopup"
        PopupDragHandleControlID="panEdit">
    </cc1:ModalPopupExtender>
</asp:Content>
