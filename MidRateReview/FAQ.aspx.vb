﻿Imports System.IO
Imports System.Xml
Imports System.Xml.Xsl
Imports System.Xml.XmlReader
Partial Public Class FAQ
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strAdmin As String = ""
        btnReturn.Visible = False
        If Not (Request.QueryString("ad") Is Nothing) Then
            strAdmin = Request.QueryString("ad").ToString()
        End If

        If (strAdmin = "1") Then
      
            menuTabs.Visible = True
            menuTabs.Items(0).NavigateUrl = "Resources.aspx?ad=1"
            menuTabs.Items(1).NavigateUrl = "RelatedLinks.aspx?ad=1"
            menuTabs.Items(2).NavigateUrl = "FAQ.aspx?ad=1"
        Else

            menuTabs.Visible = False
        End If
        BindFAQBlogData()
    End Sub
    ''' <summary>
    ''' This filters the BLOG rrs feed and fetches the feed only for the FAQ category.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindFAQBlogData()
        Dim strXmlSrc As String
        Dim strXslFile As String
        Dim myXmlDoc As New XmlDocument()
        Dim FAQStringBuilder As StringBuilder
        Dim FAQStringWriter As StringWriter

        Try

            strXmlSrc = ConfigurationManager.AppSettings("MidBlogRSSFeedLink").ToString()
            strXslFile = Server.MapPath("XslFiles/FAQTransform.xsl")


            myXmlDoc.Load(strXmlSrc)
            Dim myXslDoc As New XslCompiledTransform()
            myXslDoc.Load(strXslFile)

            FAQStringBuilder = New StringBuilder()
            FAQStringWriter = New StringWriter(FAQStringBuilder)

            myXslDoc.Transform(myXmlDoc, Nothing, FAQStringWriter)
            'FAQStringBuilder = FAQStringBuilder.Replace("http://10.3.0.26", "")
            LiteralFaq.Text = FAQStringBuilder.ToString


        Catch ex As Exception

        Finally
            FAQStringWriter = Nothing
            FAQStringBuilder = Nothing
            myXmlDoc = Nothing

        End Try

    End Sub

    Protected Sub btnMidRateReviewHome_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMidRateReviewHome.Click

        Dim strLogout As String = ConfigurationManager.AppSettings("CompareRateChanges")
        Dim strLogoutUrl As String = strLogout + "/CompareRateChanges.aspx"
        Response.Redirect(strLogoutUrl, False)
    End Sub

    Protected Sub btnMidBlog_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMidBlog.Click
        Dim strLogout As String = ConfigurationManager.AppSettings("MidRateReview.logout")
        Dim strLogoutUrl As String = strLogout + "/"
        Response.Redirect(strLogoutUrl, False)
    End Sub
End Class