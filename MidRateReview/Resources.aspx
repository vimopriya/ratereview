﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Resources.aspx.vb" Inherits="MidWebApp.WebForm4" 
    title="Resources" %>
    
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
  <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <link href="css/grid.css" rel="stylesheet" media="screen" type="text/css" />
  
    <div id="page_header3">
     <asp:Label ID="lblTitle" runat="server" Text="Resources" cssClass="h2-form " ></asp:Label>       
       <asp:LinkButton ID="btnMidRateReviewHome" CssClass="logout" runat="server"  CausesValidation="False" >MID 
        Rate Review Home</asp:LinkButton>
             <asp:LinkButton ID="btnMidBlog" CssClass="logout" runat="server"  CausesValidation="False" >MID 
        Blog Home</asp:LinkButton>  
        </div>
  <div class="left col_15x gutter15">
    <div class="tabs-container">
     <asp:Menu id="menuTabs" CssClass="tabs-basic"   Runat="server" Orientation="Horizontal"        
             Height="26px" StaticSelectedStyle-CssClass="tab-selected">
             <Items>
                <asp:MenuItem Value="Resources"  Selected = true Text="Resources"/>
                <asp:MenuItem Value="RelatedLinks" Text="Related Links"/>  
                <asp:MenuItem Value="FAQ" Text="FAQ" runat = "server"/> 
              </Items>
    </asp:Menu>
    </div>
</div>
        
            
<div class="col_16x left gutter15">
    <asp:hyperlink CssClass="btn-form left margin0"  id = "hyperLink1" runat="server">Back</asp:hyperlink> 
</div>
<div class="center-all gutter10 col_11 resources">
    <asp:hyperlink CssClass="btn-wht-form left margin0"  id = "hyperLinkAddUrl" runat="server">Add 
    New Url</asp:hyperlink>    

    <asp:GridView ID="GridViewDispUrl" AllowPaging="True" PageSize = "10"  EmptyDataText="No Record Available" 
                AllowSorting="True" CssClass="grid-vrc gridclass rate-changes col_11" runat="server" AutoGenerateColumns="False">
        <Columns>
        <asp:TemplateField HeaderText="Edit">
              
              <ItemTemplate>
               
              <asp:LinkButton ID="Edit" CommandName="edt" CssClass="btn-edit" runat="server" ToolTip = "Edit"></asp:LinkButton>
                  &nbsp;&nbsp;
                
 <asp:HiddenField ID="hfKey" runat = "server" Value='<%#Eval("RESOURCE_ID") %>' /> 
               </itemTemplate>
              
              <HeaderStyle CssClass="edit-col" />
              
              </asp:TemplateField>
                               
               <asp:TemplateField HeaderText="Delete">
               <itemTemplate>
               <asp:LinkButton ID="Delete" CommandName="dlt" runat="server"  CssClass = "btn-delete " ToolTip = "Delete"
OnClientClick = "return confirm('Are you sure you want to delete this Url?');"></asp:LinkButton>

               </itemTemplate>
                   <HeaderStyle CssClass="delete-col" />
               </asp:TemplateField>
                
           
                 
               <asp:TemplateField HeaderText="Resources Url" ControlStyle-CssClass="txt-left">
            <ItemTemplate>
                <asp:HyperLink ID="hLinkRateInc" runat="server" Font-Bold="True" Text='<%# Eval("RESOURCES_DISPLAY_TXT", "{0}") %>'
    NavigateUrl='<%# Container.DataItem("RESOURCES_URL") %>'  ></asp:HyperLink>
            </ItemTemplate>
            
                <ControlStyle CssClass="url-col left" />
                <HeaderStyle CssClass="url-col" />
                <ItemStyle CssClass="url-display left" />
            
            </asp:TemplateField>
           
        </Columns>
        <PagerStyle HorizontalAlign="left" />
          <PagerTemplate>
                        <table>
                              <tr>
                                    <td>
                                          <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                              </tr>
                        </table>
                  </PagerTemplate>
        <AlternatingRowStyle CssClass="rowcoloralt" />
    </asp:GridView> 
                  <p class="txt-gry col_5 gutter10-l left">You are viewing page
        <strong><%=GridViewDispUrl.PageIndex + 1%></strong>
                      of
        <strong><%=GridViewDispUrl.PageCount%></strong>
        </p>
     
     </div>
     <div class="spacer col_15"></div>
<asp:Button ID="btnClosePopup"  runat="server"></asp:Button>
<asp:Panel ID = "panEdit" runat="server" CssClass="ModalWindow">

    
         <div class="col_8 center-all login">
                <h2 class="h2-blu gutter20-t">
                <asp:Label ID="LblResouces" runat="server" Font-Bold="True" Text="Add New Url"></asp:Label></h2>
               
                <div class="col_8 loginbox center-all">
                <div class="col_7 padding5-btm bdr-none center-all gutter10-l">
                
                <label class="label"><asp:Label ID="lblUrlName" runat="server" Text="Type URL"></asp:Label></label>
                <asp:TextBox ID="txtUrlName" CssClass="padding5-btm col_4" runat="server"></asp:TextBox>
                
                <div class="spacer"></div>
                <label class="label"><asp:Label ID="lblResourcesDispTxt" runat="server" Text="URL Display Text"></asp:Label></label>
                <asp:TextBox ID="txtUrlDisp" CssClass="padding5-btm col_4" runat="server"></asp:TextBox>
                <br />
                 
                <label class="label"></label><asp:CheckBox ID="ChkBoxShowUrl"  runat="server" /><asp:Label ID="lblShow" runat="server" Text="Display URL on MID Home Page"></asp:Label>
          </div> 
                
                    <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
                <dl>
                    <dd>
                        <asp:Button ID="btnPopupSubmit" runat="server" Text="Save" OnClick = "btnPopupSubmit_Click" CssClass="btn-form" CausesValidation = "false" />
                       <asp:Button ID="btnCancel" CssClass="btn-wht-form" runat="server" Text="Cancel" CausesValidation ="false" />
                        
                    </dd>
                    <dd>
                        <asp:Label ID="StatusLabel" runat="server"></asp:Label>
                    </dd>
                 

                </dl>
                
                </div>
           </div>
        

            </asp:Panel>
 <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server"

        TargetControlID="hyperLinkAddUrl"

        PopupControlID="panEdit"

        BackgroundCssClass="modalBackground"

        CancelControlID="btnClosePopup" 

        PopupDragHandleControlID="panEdit">

        </cc1:ModalPopupExtender>


</asp:Content>
