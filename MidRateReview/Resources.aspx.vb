﻿Public Partial Class WebForm4
    Inherits System.Web.UI.Page
    Dim isEdit As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hyperLink1.Visible = False
        If Not IsPostBack Then
            Dim strAdmin As String = ""
            If Not (Request.QueryString("ad") Is Nothing) Then
                strAdmin = Request.QueryString("ad").ToString()
            End If

            If (strAdmin = "1") Then
                hyperLinkAddUrl.Visible = True
                btnMidBlog.Visible = True
                GridViewDispUrl.Columns(0).Visible = True
                GridViewDispUrl.Columns(1).Visible = True
                menuTabs.Visible = True
                menuTabs.Items(0).NavigateUrl = "Resources.aspx?ad=1"
                menuTabs.Items(1).NavigateUrl = "RelatedLinks.aspx?ad=1"
                menuTabs.Items(2).NavigateUrl = "FAQ.aspx?ad=1"

            Else

                hyperLinkAddUrl.Visible = False
                GridViewDispUrl.Columns(0).Visible = False
                GridViewDispUrl.Columns(1).Visible = False
                btnMidBlog.Visible = False
                menuTabs.Visible = False
            End If

            PopulateGridView()

        Else
            ApplyPaging()
        End If
        'End If
        ' btnPopupSubmit.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnPopupSubmit.UniqueID, "")
        ' btnCancel.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnCancel.UniqueID, "")

        If Session.Item("isEdit") <> Nothing Then
            isEdit = CType(Session.Item("isEdit"), String)
            If (isEdit = "Y") Then
                btnPopupSubmit.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnPopupSubmit.UniqueID, "")

            Else
                LblResouces.Text = "Add Resources Url"
                txtUrlName.Text = ""
                txtUrlDisp.Text = ""
                ChkBoxShowUrl.Checked = False


            End If


        End If
        btnClosePopup.Attributes.Add("style", "visibility : hidden")

    End Sub

    'Protected Sub btnAddUrl_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddUrl.Click

    'End Sub

    Private Sub PopulateGridView()
        Dim resourcesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        resourcesDs = rateChangesBL.GetResourcesUrl()
        GridViewDispUrl.DataSource = resourcesDs
        GridViewDispUrl.DataBind()


    End Sub

    Protected Sub btnPopupSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPopupSubmit.Click
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        Dim intResourceID As Integer = 0
        Dim strShowUrl As String
        Dim IsEdit As String = ""
        Dim strRelatedUrlTxt As String = ""
        If (ChkBoxShowUrl.Checked = True) Then
            strShowUrl = "Y"
        Else
            strShowUrl = "N"
        End If
        If Session.Item("isEdit") <> Nothing Then
            IsEdit = CType(Session.Item("isEdit"), String)
        End If

        If Session.Item("ResourceID") <> Nothing Then
            intResourceID = CType(Session.Item("ResourceID"), Integer)
        End If

        If (txtUrlDisp.Text <> "") Then
            strRelatedUrlTxt = txtUrlDisp.Text.ToString()
        End If


        If (IsEdit = "Y") Then

            rateChangesBL.UpdateResourceUrl(intResourceID, txtUrlName.Text, strShowUrl, Today.Date, strRelatedUrlTxt)
            Session.Clear()
            PopulateGridView()
        Else

            intResourceID = rateChangesBL.InsertNewResourceUrl(txtUrlName.Text, strShowUrl, Today.Date, strRelatedUrlTxt)
        End If
        PopulateGridView()
        Session.Clear()
        txtUrlName.Text = ""
        txtUrlDisp.Text = ""
        ChkBoxShowUrl.Checked = False

    End Sub


    Protected Sub GridViewDispUrl_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridViewDispUrl.RowCommand
        Dim intResourceID As Integer
        If e.CommandName.ToLower() = "edt" Then
            btnPopupSubmit.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnPopupSubmit.UniqueID, "")
            Session("isEdit") = "Y"
            isEdit = True
            ModalPopupExtender1.Show()
            LblResouces.Text = "Edit Resources Url"
            Dim lb As LinkButton = CType(e.CommandSource, LinkButton)
            'getting clicked link button      
            intResourceID = CType(lb.Parent.FindControl("hfKey"), HiddenField).Value
            Session("ResourceID") = intResourceID
            Dim resourcesDs As New DataSet
            Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
            resourcesDs = rateChangesBL.GetResourceUrlByID(intResourceID)

            Dim dr As DataRow
            Dim dt As DataTable
            '<code to fill the dataset>
            dt = resourcesDs.Tables(0)
            For Each dr In dt.Rows
                txtUrlName.Text = dr("RESOURCES_URL").ToString()
                txtUrlDisp.Text = dr("RESOURCES_DISPLAY_TXT").ToString()

                If (dr("IS_SHOWINHOMEPAGE").ToString() = "Y") Then
                    ChkBoxShowUrl.Checked = True
                Else

                    ChkBoxShowUrl.Checked = False
                End If
            Next
        ElseIf e.CommandName.ToLower() = "dlt" Then
            Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
            Dim lb As LinkButton = CType(e.CommandSource, LinkButton)
            'getting clicked link button      
            Dim intResourcesID As Integer = CType(lb.Parent.FindControl("hfKey"), HiddenField).Value
            'getting PK Value   
            rateChangesBL.DeleteResourcesUrl(intResourcesID)
            PopulateGridView()
        End If


    End Sub

    Sub ApplyPaging()

        Dim row As GridViewRow = GridViewDispUrl.BottomPagerRow
        Dim ph As PlaceHolder = New PlaceHolder
        Dim lnkPaging As LinkButton
        Dim lnkFirstPage As LinkButton
        Dim lnkPrevPage As LinkButton
        Dim lnkNextPage As LinkButton
        Dim lnkLastPage As LinkButton
        lnkFirstPage = New LinkButton()
        lnkFirstPage.Text = Server.HtmlEncode("<<First")
        lnkFirstPage.Width = Unit.Pixel(50)
        lnkFirstPage.CommandName = "Page"
        lnkFirstPage.CommandArgument = "First"
        lnkPrevPage = New LinkButton()
        lnkPrevPage.Text = Server.HtmlEncode("<Prev")
        lnkPrevPage.Width = Unit.Pixel(50)
        lnkPrevPage.CommandName = "Page"
        lnkPrevPage.CommandArgument = "Prev"

        If (GridViewDispUrl.PageCount <> 0) Then
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkFirstPage)
            ph.Controls.Add(lnkPrevPage)
        End If
        If (GridViewDispUrl.PageIndex = 0) Then

            lnkFirstPage.Enabled = False
            lnkPrevPage.Enabled = False
        Else
            lnkPrevPage.Enabled = True
        End If


        Dim i As Integer
        For i = 1 To GridViewDispUrl.PageCount

            lnkPaging = New LinkButton()
            lnkPaging.Width = Unit.Pixel(20)
            ' lnkPaging.CssClass = "LinkPaging"
            lnkPaging.Text = i.ToString()
            lnkPaging.CommandName = "Page"
            lnkPaging.CommandArgument = i.ToString()
            ' If (i = gridRateChanges.PageIndex + 1) Then
            ' lnkPaging.BackColor = System.Drawing.Color.Blue
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkPaging)
            ' End If

        Next i

        lnkNextPage = New LinkButton()
        lnkNextPage.Text = Server.HtmlEncode("Next>")
        lnkNextPage.Width = Unit.Pixel(50)
        lnkNextPage.CommandName = "Page"
        lnkNextPage.CommandArgument = "Next"
        lnkLastPage = New LinkButton()
        lnkLastPage.Text = Server.HtmlEncode("Last>>")
        lnkLastPage.Width = Unit.Pixel(50)
        lnkLastPage.CommandName = "Page"
        lnkLastPage.CommandArgument = "Last"
        lnkNextPage.Enabled = True
        lnkLastPage.Enabled = True
        If (GridViewDispUrl.PageCount <> 0) Then
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkNextPage)
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkLastPage)
        End If
        If (GridViewDispUrl.PageIndex + 1 = GridViewDispUrl.PageCount) Then

            lnkNextPage.Enabled = False
            lnkLastPage.Enabled = False
        End If



    End Sub
    Protected Sub GridViewDispUrl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewDispUrl.PageIndexChanging


        Try
            If e.NewPageIndex <> -1 Then
                GridViewDispUrl.PageIndex = e.NewPageIndex
                PopulateGridView()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridViewDispUrl_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles GridViewDispUrl.DataBound
        ApplyPaging()
    End Sub


    Protected Sub GridViewDispUrl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewDispUrl.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim rowView As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
            Dim hLink As HyperLink = e.Row.FindControl("hLinkRateInc")
            hLink.Text = rowView("RESOURCES_DISPLAY_TXT").ToString().Trim()
            hLink.NavigateUrl = rowView("RESOURCES_URL").ToString().Trim()
            Dim strNavigationUrl As String = rowView("RESOURCES_URL").ToString().Trim()
            If Not (strNavigationUrl.Contains("/MidBlog") Or strNavigationUrl.Contains("/MidRateChanges") Or strNavigationUrl.Contains("/MidHome") Or strNavigationUrl.Contains("mid.state.ms.us")) Then

                hLink.Attributes.Add("OnClick", "return confirm('You are now leaving the Mississippi Insurance Department website.');")

            End If
            If (strNavigationUrl.ToLower().Contains("healthcare.gov")) Then

                hLink.Attributes.Add("OnClick", "return confirm('You are now leaving the Mississippi Insurance Department website and being directed to HealthCare.gov, a website operated by\nthe United States Department of Health and Human Services.');")

            End If


        End If
    End Sub

    Protected Sub btnMidRateReviewHome_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMidRateReviewHome.Click

        Dim strLogout As String = ConfigurationManager.AppSettings("CompareRateChanges")
        Dim strLogoutUrl As String = strLogout + "/CompareRateChanges.aspx"
        Response.Redirect(strLogoutUrl, False)
    End Sub

    Protected Sub btnClosePopup_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClosePopup.Click
        Session.Clear()
        txtUrlName.Text = ""
        txtUrlDisp.Text = ""
        ChkBoxShowUrl.Checked = False
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Session.Clear()
        txtUrlName.Text = ""
        txtUrlDisp.Text = ""
        ChkBoxShowUrl.Checked = False
    End Sub

    Protected Sub btnMidBlog_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMidBlog.Click
        Dim strLogout As String = ConfigurationManager.AppSettings("MidRateReview.logout")
        Dim strLogoutUrl As String = strLogout + "/"
        Response.Redirect(strLogoutUrl, False)
    End Sub
End Class