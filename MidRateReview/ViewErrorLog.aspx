﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="ViewErrorLog.aspx.vb" Inherits="MidWebApp.ViewErrorLog" 
    title="Error Log" validateRequest="false"%>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
<link href="css/grid.css" rel="stylesheet" media="screen" type="text/css" />
   
     <div id="page_header3">
       <asp:Label ID="lblTitle" runat="server" Text="View Error Log" 
        CssClass="h2-form"> </asp:Label>
        <asp:LinkButton ID="btnLink"  CssClass="logout" runat="server">Log Out</asp:LinkButton>
     </div>
     
     <div class="spacer"></div>
        <asp:Button ID="btnBack" runat="server" CssClass="btn-form" Text="Back" />    
        
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    
     
    <div class="center-all col_13">
        <div class="rate-changes col_13">
          <ul class="list-hrz">
                <li>
                    <label><asp:Label ID="lblActionBy" runat="server" Text="Search On"></asp:Label></label>
                    <asp:TextBox ID="txtSearchOn" runat="server" Width="200px" ></asp:TextBox>
               </li>
               <li class="date2date">
                    <label><asp:Label ID="lblFromDate" runat="server" Text="From Date <span>To Date</span>"></asp:Label></label>
                    <asp:TextBox ID="txtFromDate" runat="server" cssClass="w60"></asp:TextBox>
                        <input id="btnPick1" type="button" runat = "server" value = "" class="btn-calendar"/>  
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server"  PopupButtonID = "btnPick1" TargetControlID = "txtFromDate"></cc1:calendarextender>    
                         <span id="hyphenSpan" runat="server">-&nbsp;</span>
                        <asp:TextBox ID="txtToDate" runat="server"  cssClass="w60"></asp:TextBox>
                        <input id="btnPick2" type="button" value="" runat = "server" class="btn-calendar" />
                        <cc1:CalendarExtender ID="CalendarExtender3" runat="server"  PopupButtonID = "btnPick2" TargetControlID = "txtToDate"></cc1:calendarextender>    
                        <asp:Button ID="btnSearch" runat="server" CssClass="btn-form" 
                            Text="Search" />
                 </li>
                </ul>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                            ControlToValidate="txtFromDate" CssClass="validators" 
                            ErrorMessage="Please Enter Valid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                            
                            <asp:CompareValidator ID="CompareValidator2" runat="server" 
                          ControlToValidate="txtToDate" CssClass="validators" 
                          ErrorMessage="Please Enter Valid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
              
        </div>
        <div class="spacer"></div>                    
         
         <asp:GridView ID="gridErrorLog" runat="server" AllowPaging="True" 
            AllowSorting="False" CssClass="gridclass grid-vat col_13" AutoGenerateColumns="False" Width="700px" >
            <Columns>
                <asp:BoundField DataField="SEVERITY" HeaderText="SEVERITY" />
                <asp:BoundField DataField="APPLICATIONNAME" HeaderText="APPLICATION NAME" />
                <asp:BoundField DataField="ERRORDATE" HeaderText="DATE ERROR OCCURED" />
                <asp:TemplateField HeaderText="ERROR DESCRIPTION">
                   <HeaderStyle  HorizontalAlign="Right" Width="350px" />
                   <ItemStyle Width="350px"></ItemStyle>     
                    <ItemTemplate>                     
                      <asp:TextBox id="ErrorTxt" runat="server" Width="350px" Height="75px" Text='<%# Bind("ERRORDESCRIPTION") %>'  TextMode="MultiLine"></asp:TextBox>         
                    </ItemTemplate>
              </asp:TemplateField>
            </Columns>
        </asp:GridView>
       </div> 

</asp:Content>
