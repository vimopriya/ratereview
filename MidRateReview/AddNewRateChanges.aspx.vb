﻿Imports RateChangesBL

Partial Public Class AddNewRateChanges
    Inherits System.Web.UI.Page

    Private Const ERROR_PAGE As String = "ErrorPage.aspx"
    Public Event RowCommand As GridViewCommandEventHandler
    Dim userName As String
    Dim isEdit As Boolean
    Dim drpSelectedValue As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                LoadLabelControls()

                userName = CType(Request.QueryString("userName"), String)

                PopulateCompanyComboBox()
                PopulatePolicyTypeComboBox()
                drpDownPolicyType.Items.Insert(0, New ListItem("-Select Insurance Type-", "0"))
                drpDownCompany.Items.Insert(0, New ListItem("-Select Company-", "0"))
                If (Request.QueryString.Count > 0) Then

                    Dim ViewMode As String = CType(Request.QueryString("ViewMode"), String)
                    If (ViewMode = "Yes") Then
                        lblTitle.Text = "View Rate Changes"

                        btnSubmit.Enabled = False
                        hyperLinkAddComp.Visible = False
                        FileUploadPdfDoc.Enabled = False
                        gridDownloadFiles.Enabled = False

                    ElseIf (ViewMode = "edit") Then
                        lblTitle.Text = "Edit Rate Changes"
                        btnSubmit.Enabled = True
                        hyperLinkAddComp.Visible = True
                        FileUploadPdfDoc.Enabled = True
                        gridDownloadFiles.Enabled = True
                    ElseIf (ViewMode = "No") Then

                        btnPopupSubmit.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnPopupSubmit.UniqueID, "")
                        lblTitle.Text = "Add New Rate Changes"
                        btnSubmit.Enabled = True
                        hyperLinkAddComp.Visible = True
                        FileUploadPdfDoc.Enabled = True
                        gridDownloadFiles.Enabled = True

                    End If


                    Dim id As Integer = CType(Request.QueryString("rateid"), Integer)

                    If (id > 0) Then

                        PopulateFileDownloadGrid(id)
                        'PopulateFileDownloadGrid(id)
                        PopulateFormControls(id)
                        drpDownCompany.Enabled = False
                        hyperLinkAddComp.Text = "Edit Company Details"
                        Label1.Text = "Edit Company Details"
                        isEdit = True
                        hyperLinkAddComp.Visible = True
                    Else
                        btnSubmit.Enabled = True
                        FileUploadPdfDoc.Enabled = True
                        gridDownloadFiles.Enabled = True
                        drpDownCompany.Enabled = True
                        hyperLinkAddComp.Visible = True
                        isEdit = False
                        Dim dateField As String = "1/1/"
                        ' txtDateLastFive2010.Text = String.Concat(dateField, (DateTime.Now.Year - 1).ToString())

                        ' txtDateLastFive2009.Text = String.Concat(dateField, (DateTime.Now.Year - 2).ToString())
                        ' txtDateLastFive2008.Text = String.Concat(dateField, (DateTime.Now.Year - 3).ToString())
                        ' txtDateLastFive2007.Text = String.Concat(dateField, (DateTime.Now.Year - 4).ToString())
                        ' txtDateLastFive2006.Text = String.Concat(dateField, (DateTime.Now.Year - 5).ToString())



                    End If

                End If
            End If

        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE, False)
        End Try

    End Sub

    'Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
    '    'Response.Redirect("http://vimoacc44/midapp/RateChangesDashBoard.aspx", True)
    '    Response.Redirect("RateChangesDashBoard.aspx", True)
    'End Sub


    Protected Sub btnSubmit1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click

        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        Dim ratechangesId As Integer
        Dim dblLossRatio2006 As Object
        Dim dblLossRatio2007 As Object
        Dim dblLossRatio2008 As Object
        Dim dblLossRatio2009 As Object
        Dim dblLossRatio2010 As Object


        Dim dblPercent2006 As Object
        Dim dblPercent2007 As Object
        Dim dblPercent2008 As Object
        Dim dblPercent2009 As Object
        Dim dblPercent2010 As Object

        Dim NoOFMsInsureds2006 As Object
        Dim NoOFMsInsureds2007 As Object
        Dim NoOFMsInsureds2008 As Object
        Dim NoOFMsInsureds2009 As Object
        Dim NoOFMsInsureds2010 As Object


        Dim dtLastFiveDate2006 As Date = Nothing
        Dim dtLastFiveDate2007 As Date = Nothing
        Dim dtLastFiveDate2008 As Date = Nothing
        Dim dtLastFiveDate2009 As Date = Nothing
        Dim dtLastFiveDate2010 As Date = Nothing
        Dim dtPolicyApproval As Date = Nothing
        Dim strSummaryRate As String = " "
        Dim strTypeOfInsurance As String = " "
        Dim strLinkToRateInc As String = " "
        Dim dtDateSubmitted As Date = Nothing
        Try
           

            If (txtSecondaryType.Text <> "") Then
                strTypeOfInsurance = drpDownPolicyType.SelectedItem.ToString() + " - " + txtSecondaryType.Text.ToString()
            Else
                strTypeOfInsurance = drpDownPolicyType.SelectedItem.ToString()
            End If
            If (txtDateLastFive2010.Text <> "") Then

                Dim dateParts() As String = txtDateLastFive2010.Text.Split("/")
                Dim strYear = Convert.ToInt32(dateParts(2))
                If (strYear <> DateTime.Now.Year - 1) Then
                    CompareValidator16.IsValid = False
                    CompareValidator20.ValidationGroup = "dateValidation"

                End If

            End If
            If (txtDateLastFive2009.Text <> "") Then

                Dim dateParts() As String = txtDateLastFive2009.Text.Split("/")
                Dim strYear = Convert.ToInt32(dateParts(2))
                If (strYear <> DateTime.Now.Year - 2) Then

                    CompareValidator17.IsValid = False
                End If
            End If
            If (txtDateLastFive2008.Text <> "") Then
                Dim dateParts() As String = txtDateLastFive2008.Text.Split("/")
                Dim strYear = Convert.ToInt32(dateParts(2))
                If (strYear <> DateTime.Now.Year - 3) Then
                    CompareValidator18.IsValid = False


                End If
            End If
            If (txtDateLastFive2007.Text <> "") Then

                Dim dateParts() As String = txtDateLastFive2007.Text.Split("/")
                Dim strYear = Convert.ToInt32(dateParts(2))
                If (strYear <> DateTime.Now.Year - 4) Then
                    CompareValidator19.IsValid = False


                End If
            End If
            If (txtDateLastFive2006.Text <> "") Then
                dtLastFiveDate2006 = Convert.ToDateTime(txtDateLastFive2006.Text)
                Dim dateParts() As String = txtDateLastFive2006.Text.Split("/")
                Dim strYear = Convert.ToInt32(dateParts(2))
                If (strYear <> DateTime.Now.Year - 5) Then
                    CompareValidator20.IsValid = False



                End If
            End If


            If Page.IsValid Then
                ''Last Loss ratio
                If (txtLossRatio2006.Text <> "") Then
                    dblLossRatio2006 = Convert.ToDouble(txtLossRatio2006.Text)
                Else
                    dblLossRatio2006 = DBNull.Value
                End If

                If (txtLossRatio2007.Text <> "") Then
                    dblLossRatio2007 = Convert.ToDouble(txtLossRatio2007.Text)
                Else
                    dblLossRatio2007 = DBNull.Value
                End If

                If (txtLossRatio2008.Text <> "") Then
                    dblLossRatio2008 = Convert.ToDouble(txtLossRatio2008.Text)
                Else
                    dblLossRatio2008 = DBNull.Value
                End If

                If (txtLossRatio2009.Text <> "") Then
                    dblLossRatio2009 = Convert.ToDouble(txtLossRatio2009.Text)
                Else
                    dblLossRatio2009 = DBNull.Value
                End If

                If (txtLossRatio2010.Text <> "") Then
                    dblLossRatio2010 = Convert.ToDouble(txtLossRatio2010.Text)
                Else
                    dblLossRatio2010 = DBNull.Value
                End If

                ''Percentage Rate Increase
                If (txtPertLastFive2006.Text <> "") Then
                    dblPercent2006 = Convert.ToDouble(txtPertLastFive2006.Text)
                Else
                    dblPercent2006 = DBNull.Value
                End If
                If (txtPertLastFive2007.Text <> "") Then
                    dblPercent2007 = Convert.ToDouble(txtPertLastFive2007.Text)
                Else
                    dblPercent2007 = DBNull.Value
                End If

                If (txtPertLastFive2008.Text <> "") Then
                    dblPercent2008 = Convert.ToDouble(txtPertLastFive2008.Text)
                Else
                    dblPercent2008 = DBNull.Value
                End If

                If (txtPertLastFive2009.Text <> "") Then
                    dblPercent2009 = Convert.ToDouble(txtPertLastFive2009.Text)
                Else
                    dblPercent2009 = DBNull.Value
                End If

                If (txtPertLastFive2010.Text <> "") Then
                    dblPercent2010 = Convert.ToDouble(txtPertLastFive2010.Text)
                Else
                    dblPercent2010 = DBNull.Value
                End If

                ''No of Mississippi Insured's
                If (txtNoOfMsInsured2006.Text <> "") Then
                    NoOFMsInsureds2006 = Convert.ToInt64(txtNoOfMsInsured2006.Text.Replace(",", "").Trim())
                Else
                    NoOFMsInsureds2006 = DBNull.Value
                End If

                If (txtNoOfMsInsured2007.Text <> "") Then
                    NoOFMsInsureds2007 = Convert.ToInt64(txtNoOfMsInsured2007.Text.Replace(",", "").Trim())
                Else
                    NoOFMsInsureds2007 = DBNull.Value
                End If

                If (txtNoOfMsInsured2008.Text <> "") Then
                    NoOFMsInsureds2008 = Convert.ToInt64(txtNoOfMsInsured2008.Text.Replace(",", "").Trim())
                Else
                    NoOFMsInsureds2008 = DBNull.Value
                End If

                If (txtNoOfMsInsured2009.Text <> "") Then
                    NoOFMsInsureds2009 = Convert.ToInt64(txtNoOfMsInsured2009.Text.Replace(",", "").Trim())
                Else
                    NoOFMsInsureds2009 = DBNull.Value
                End If

                If (txtNoOfMsInsured2010.Text <> "") Then
                    NoOFMsInsureds2010 = Convert.ToInt64(txtNoOfMsInsured2010.Text.Replace(",", "").Trim())

                Else
                    NoOFMsInsureds2010 = DBNull.Value
                End If

                If (txtDateLastFive2010.Text <> "") Then
                    dtLastFiveDate2010 = Convert.ToDateTime(txtDateLastFive2010.Text)
                End If
                If (txtDateLastFive2009.Text <> "") Then
                    dtLastFiveDate2009 = Convert.ToDateTime(txtDateLastFive2009.Text)
                End If
                If (txtDateLastFive2008.Text <> "") Then
                    dtLastFiveDate2008 = Convert.ToDateTime(txtDateLastFive2008.Text)
                End If
                If (txtDateLastFive2007.Text <> "") Then
                    dtLastFiveDate2007 = Convert.ToDateTime(txtDateLastFive2007.Text)
                End If
                If (txtDateLastFive2006.Text <> "") Then
                    dtLastFiveDate2006 = Convert.ToDateTime(txtDateLastFive2006.Text)
                End If
                If (txtPolicyApp.Text <> "") Then
                    dtPolicyApproval = Convert.ToDateTime(txtPolicyApp.Text)
                Else

                End If



                If (txtReason.Text <> "") Then
                    strSummaryRate = txtReason.Text.ToString()
                End If

                If (txtLinkToRateInc.Text <> "") Then
                    strLinkToRateInc = txtLinkToRateInc.Text.ToString()
                End If

                If (TxtDateSubmitted.Text <> "") Then
                    dtDateSubmitted = Convert.ToDateTime(TxtDateSubmitted.Text)

                End If

                If (Request.QueryString.Count > 0) Then
                    userName = CType(Request.QueryString("userName"), String)
                    ratechangesId = CType(Request.QueryString("rateID"), Integer)
                End If

                If (ratechangesId > 0) Then
                    Dim intStatus As Integer
                    intStatus = rateChangesBL.UpadteRateChanges(ratechangesId, drpDownCompany.SelectedValue, drpDownCompany.SelectedItem.ToString(), txtPolicyFormNo.Text, strTypeOfInsurance, Convert.ToDateTime(txtDateCurRateInc.Text), _
                                          Convert.ToDouble(txtPertCurrRateInc.Text), dtLastFiveDate2010, dtLastFiveDate2009, dtLastFiveDate2008, _
                                             dtLastFiveDate2007, dtLastFiveDate2006, dblPercent2010, dblPercent2009, _
                                            dblPercent2008, dblPercent2007, dblPercent2006, dblLossRatio2010, _
                                            dblLossRatio2009, dblLossRatio2008, dblLossRatio2007, dblLossRatio2006, _
                                            NoOFMsInsureds2010, NoOFMsInsureds2009, NoOFMsInsureds2008, NoOFMsInsureds2007, _
                                            NoOFMsInsureds2006, userName, strSummaryRate, dtPolicyApproval, dtDateSubmitted, strLinkToRateInc)

                    If (intStatus = 1) Then
                        'lblResult.Text = "Rate Changes Data Updated Successfully"
                        Session("rateID") = ratechangesId
                    End If
                    If (FileUploadPdfDoc.HasFile) Then
                        ImportFile(ratechangesId)
                        PopulateFileDownloadGrid(ratechangesId)
                    Else
                        lblUploadStatus.Text = "Rate Review Record Updated Successfully"
                        ' Response.Redirect("RateChangesDashBoard.aspx", False)

                    End If

                Else

                    Dim intRateID As Integer
                    'intRateID = rateChangesBL.InsertRateChanges(drpDownCompany.SelectedValue, txtPolicyFormNo.Text, drpDownPolicyType.SelectedValue, DateTime.Parse(txtDateCurRateInc.Text), _
                    '                        Convert.ToDouble(txtPertCurrRateInc.Text), DateTime.Parse(txtDateLastFive2010.Text), DateTime.Parse(txtDateLastFive2009.Text), DateTime.Parse(txtDateLastFive2008.Text), _
                    '                        DateTime.Parse(txtDateLastFive2007.Text), DateTime.Parse(txtDateLastFive2006.Text), Convert.ToDouble(txtPertLastFive2010.Text), Convert.ToDouble(txtPertLastFive2009.Text), _
                    '                       Double.Parse(txtPertLastFive2008.Text), Convert.ToDouble(txtPertLastFive2007.Text), Convert.ToDouble(txtPertLastFive2006.Text), Convert.ToDouble(txtLossRatio2010.Text), _
                    '                        Convert.ToDouble(txtLossRatio2009.Text), Convert.ToDouble(txtLossRatio2008.Text), Convert.ToDouble(txtLossRatio2007.Text), Convert.ToDouble(txtLossRatio2006.Text), _
                    '                        Convert.ToInt32(txtNoOfMsInsured2010.Text), Convert.ToInt32(txtNoOfMsInsured2009.Text), Convert.ToInt32(txtNoOfMsInsured2008.Text), Convert.ToInt32(txtNoOfMsInsured2007.Text), _
                    '                        Convert.ToInt32(txtNoOfMsInsured2006.Text))
                    intRateID = rateChangesBL.InsertRateChanges(drpDownCompany.SelectedValue, txtPolicyFormNo.Text, strTypeOfInsurance, Convert.ToDateTime(txtDateCurRateInc.Text), _
                                            Convert.ToDouble(txtPertCurrRateInc.Text), dtLastFiveDate2010, dtLastFiveDate2009, dtLastFiveDate2008, _
                                             dtLastFiveDate2007, dtLastFiveDate2006, dblPercent2010, dblPercent2009, _
                                            dblPercent2008, dblPercent2007, dblPercent2006, dblLossRatio2010, _
                                            dblLossRatio2009, dblLossRatio2008, dblLossRatio2007, dblLossRatio2006, _
                                            NoOFMsInsureds2010, NoOFMsInsureds2009, NoOFMsInsureds2008, NoOFMsInsureds2007, _
                                            NoOFMsInsureds2006, userName, strSummaryRate, dtPolicyApproval, dtDateSubmitted, strLinkToRateInc)


                    If (intRateID <> Nothing) Then
                        Session("rateID") = intRateID
                    End If
                    If (intRateID = 0) Then
                        lblUploadStatus.Text = "Rate Review Record Already Exists For This Company"
                    Else
                        If (FileUploadPdfDoc.HasFile) Then
                            ImportFile(intRateID)
                            PopulateFileDownloadGrid(intRateID)
                        End If

                        lblUploadStatus.Text = "New Rate Review Record Created Successfully"
                    End If
                    
                End If

            End If

        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try


    End Sub
    Private Sub ImportFile(ByVal RateChangeID As Integer)
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        If (FileUploadPdfDoc.HasFile) Then


            Try
                Dim filename As String
                Dim strFileType As String = ""
                Dim extension As String = System.IO.Path.GetExtension(FileUploadPdfDoc.PostedFile.FileName)
                If (extension = ".pdf") Or (extension = ".docx") Or (extension = ".doc") Then

                    If (extension = ".pdf") Then
                        strFileType = "pdf File"
                    Else
                        strFileType = "MS Word File"
                    End If


                    If (FileUploadPdfDoc.PostedFile.ContentLength <= 4194304) Then

                        filename = System.IO.Path.GetFileName(FileUploadPdfDoc.FileName)
                        '  FileUploadPdfDoc.SaveAs("c\Uploads" + filename)
                        'FileUploadPdfDoc.SaveAs("/Uploads/" + filename)
                        'Dim filepath As String = ("/Uploads/" + filename)
                        Dim fs As System.IO.Stream
                        fs = FileUploadPdfDoc.PostedFile.InputStream
                        Dim filesize As Long = fs.Length
                        Dim buffer(filesize) As Byte

                        Dim q As Integer = fs.Read(buffer, 0, Convert.ToInt32(fs.Length))
                        fs.Close()

                        'Oracle object responsible for storing the File content.
                        'Assigning tempLob the blob object created on the database.

                        rateChangesBL.ImportFile(System.DateTime.Now.ToString(), filename, buffer, FileUploadPdfDoc.PostedFile.ContentType, FileUploadPdfDoc.PostedFile.ContentLength, RateChangeID)
                        'lblUploadStatus.Text = "File uploaded successfully"
                        lblUploadStatus.Text = "Rate Review Record Updated Successfully"
                       
                        'Response.Redirect("RateChangesDashBoard.aspx", False)
                    Else

                        lblUploadStatus.Text = " Maximun Allowed File  Size is 4MB!"
                    End If
                Else
                    lblUploadStatus.Text = "Only pdf or word files are accepted!"

                End If


            Catch ex As Exception
                Admin.WriteLog(ex)
                Response.Redirect(ERROR_PAGE, False)
            End Try

        End If

    End Sub

    Private Sub PopulatePolicyTypeComboBox()
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        Dim dsPolicyType = New DataSet()
        dsPolicyType = rateChangesBL.GetPolicyType()
        drpDownPolicyType.DataValueField = "CATAGORY_ID"
        drpDownPolicyType.DataTextField = "CATAGORY_NAME"
        drpDownPolicyType.DataSource = dsPolicyType.Tables(0)
        drpDownPolicyType.DataBind()

    End Sub

    Private Sub PopulateCompanyComboBox()
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        Dim dsCompany = New DataSet()
        dsCompany = rateChangesBL.GetCompanyDetails()
        drpDownCompany.DataValueField = "company_id"
        drpDownCompany.DataTextField = "company_name"
        drpDownCompany.DataSource = dsCompany.Tables(0)
        drpDownCompany.DataBind()

    End Sub

    Private Function IsPageValid()

        If (txtDateCurRateInc.Text <> "") Then
        End If

        Return 1
    End Function
    Private Sub PopulateFormControls(ByVal intRateChangeID As Integer)
        Dim strTypeOfInsurance As String = ""
        Dim rateChangesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        rateChangesDs = rateChangesBL.GetRateChangeDataRateID(intRateChangeID)
        isEdit = True
        Dim dr As DataRow
        Dim dt As DataTable
        '<code to fill the dataset>
        dt = rateChangesDs.Tables(0)
        For Each dr In dt.Rows
            drpDownCompany.SelectedValue = drpDownCompany.Items.FindByText(dr("COMPANY_NAME")).Value
            txtPolicyFormNo.Text = dr("POLICY_FORMNO")
            strTypeOfInsurance = dr("POLICY_TYPE").ToString()
            Dim strPolicyType() As String = strTypeOfInsurance.Split("-")
           
            Dim strcategory As String = strPolicyType(0)
            If strPolicyType.Length >= 2 Then
                Dim strSecondaryType As String = strPolicyType(1)
                txtSecondaryType.Text = strSecondaryType.ToString()
            End If

            drpDownPolicyType.SelectedValue = drpDownPolicyType.Items.FindByText(strcategory.Trim()).Value
            Session("drpCompanyID") = drpDownCompany.SelectedValue
            txtDateCurRateInc.Text = dr("DATEOF_CURRENTRATEINCR").ToString()
            txtPertCurrRateInc.Text = dr("PERCENT_CURRENT_RATEINC").ToString()

            If (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/2001") Then
                txtDateLastFive2010.Text = ""
            Else
                txtDateLastFive2010.Text = dr("DATEOF_YR_MINUS1_RATEINC").ToString()
            End If

            If (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/2001") Then
                txtDateLastFive2009.Text = ""
            Else
                txtDateLastFive2009.Text = dr("DATEOF_YR_MINUS2_RATEINC").ToString()
            End If

            If (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/2001") Then
                txtDateLastFive2008.Text = ""
            Else
                txtDateLastFive2008.Text = dr("DATEOF_YR_MINUS3_RATEINC").ToString()

            End If
            If (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/2001") Then
                txtDateLastFive2007.Text = ""
            Else
                txtDateLastFive2007.Text = dr("DATEOF_YR_MINUS4_RATEINC").ToString()
            End If

            If (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/2001") Then
                txtDateLastFive2006.Text = ""
            Else
                txtDateLastFive2006.Text = dr("DATEOF_YR_MINUS5_RATEINC").ToString()
            End If
            If (dr("POLICY_APPROVAL").ToString() = "01/01/0001") Then
                txtPolicyApp.Text = ""
            Else
                txtPolicyApp.Text = dr("POLICY_APPROVAL").ToString()
            End If

            If dr("PERCENT_YR_MINUS1_RATEINC").ToString() = "N/A" Then
                txtPertLastFive2010.Text = ""
            Else
                txtPertLastFive2010.Text = dr("PERCENT_YR_MINUS1_RATEINC").ToString()
            End If



            If dr("PERCENT_YR_MINUS2_RATEINC").ToString() = "N/A" Then
                txtPertLastFive2009.Text = ""
            Else
                txtPertLastFive2009.Text = dr("PERCENT_YR_MINUS2_RATEINC").ToString()
            End If


            If dr("PERCENT_YR_MINUS3_RATEINC").ToString() = "N/A" Then
                txtPertLastFive2008.Text = ""
            Else
                txtPertLastFive2008.Text = dr("PERCENT_YR_MINUS3_RATEINC").ToString()
            End If

            If dr("PERCENT_YR_MINUS4_RATEINC").ToString() = "N/A" Then
                txtPertLastFive2007.Text = ""
            Else
                txtPertLastFive2007.Text = dr("PERCENT_YR_MINUS4_RATEINC").ToString()
            End If

            If dr("PERCENT_YR_MINUS5_RATEINC").ToString() = "N/A" Then
                txtPertLastFive2006.Text = ""
            Else
                txtPertLastFive2006.Text = dr("PERCENT_YR_MINUS5_RATEINC").ToString()
            End If


            If dr("LAST_MINUS1_LOSSRATIO").ToString() = "N/A" Then
                txtLossRatio2010.Text = ""
            Else
                txtLossRatio2010.Text = dr("LAST_MINUS1_LOSSRATIO").ToString()
            End If


            If dr("LAST_MINUS2_LOSSRATIO").ToString() = "N/A" Then
                txtLossRatio2009.Text = ""
            Else
                txtLossRatio2009.Text = dr("LAST_MINUS2_LOSSRATIO").ToString()
            End If

            If dr("LAST_MINUS3_LOSSRATIO").ToString() = "N/A" Then
                txtLossRatio2008.Text = ""
            Else
                txtLossRatio2008.Text = dr("LAST_MINUS3_LOSSRATIO").ToString()
            End If

            If dr("LAST_MINUS4_LOSSRATIO").ToString() = "N/A" Then
                txtLossRatio2007.Text = ""
            Else
                txtLossRatio2007.Text = dr("LAST_MINUS4_LOSSRATIO").ToString()
            End If

            If dr("LAST_MINUS5_LOSSRATIO").ToString() = "N/A" Then
                txtLossRatio2006.Text = ""
            Else
                txtLossRatio2006.Text = dr("LAST_MINUS5_LOSSRATIO").ToString()
            End If


            If dr("NUMOF_MSINSURED_YR_MINUS1").ToString() = "" Then
                txtNoOfMsInsured2010.Text = dr("NUMOF_MSINSURED_YR_MINUS1").ToString()
            Else
                If (dr("NUMOF_MSINSURED_YR_MINUS1").ToString() <> "N/A") Then
                    Dim intMsInsured2010 As Long = CType(dr("NUMOF_MSINSURED_YR_MINUS1"), Long)
                    If (intMsInsured2010 = 0) Then
                        txtNoOfMsInsured2010.Text = dr("NUMOF_MSINSURED_YR_MINUS1").ToString()
                    Else
                        txtNoOfMsInsured2010.Text = (String.Format("{0:#,#}", intMsInsured2010))
                    End If

                Else
                    txtNoOfMsInsured2010.Text = ""
                End If

            End If

            ' txtNoOfMsInsured2010.Text = dr("NUMOF_MSINSURED_YR_MINUS1").ToString()

            If dr("NUMOF_MSINSURED_YR_MINUS2").ToString() = "" Then

                txtNoOfMsInsured2009.Text = dr("NUMOF_MSINSURED_YR_MINUS2").ToString()
            Else
                If (dr("NUMOF_MSINSURED_YR_MINUS2").ToString() <> "N/A") Then
                    Dim intMsInsured2009 As Long = CType(dr("NUMOF_MSINSURED_YR_MINUS2"), Long)
                    If (intMsInsured2009 = 0) Then
                        txtNoOfMsInsured2009.Text = dr("NUMOF_MSINSURED_YR_MINUS2").ToString()
                    Else
                        txtNoOfMsInsured2009.Text = (String.Format("{0:#,#}", intMsInsured2009))

                    End If

                Else
                    txtNoOfMsInsured2009.Text = ""
                End If
            End If



            ' txtNoOfMsInsured2009.Text = dr("NUMOF_MSINSURED_YR_MINUS2").ToString()


            If dr("NUMOF_MSINSURED_YR_MINUS3").ToString() = "" Then
                txtNoOfMsInsured2008.Text = dr("NUMOF_MSINSURED_YR_MINUS3").ToString()
            Else
                If (dr("NUMOF_MSINSURED_YR_MINUS2").ToString() <> "N/A") Then
                    Dim intMsInsured2008 As Long = CType(dr("NUMOF_MSINSURED_YR_MINUS3"), Long)
                    If (intMsInsured2008 = 0) Then
                        txtNoOfMsInsured2008.Text = dr("NUMOF_MSINSURED_YR_MINUS3").ToString()
                    Else
                        txtNoOfMsInsured2008.Text = (String.Format("{0:#,#}", intMsInsured2008))

                    End If

                Else
                    txtNoOfMsInsured2008.Text = ""
                End If


            End If


            ' txtNoOfMsInsured2008.Text = dr("NUMOF_MSINSURED_YR_MINUS3").ToString()

            If dr("NUMOF_MSINSURED_YR_MINUS4").ToString() = "" Then
                txtNoOfMsInsured2007.Text = dr("NUMOF_MSINSURED_YR_MINUS4").ToString()
            Else
                If (dr("NUMOF_MSINSURED_YR_MINUS4").ToString() <> "N/A") Then
                    Dim intMsInsured2007 As Long = CType(dr("NUMOF_MSINSURED_YR_MINUS4"), Long)
                    If (intMsInsured2007 = 0) Then
                        txtNoOfMsInsured2007.Text = dr("NUMOF_MSINSURED_YR_MINUS4").ToString()
                    Else
                        txtNoOfMsInsured2007.Text = (String.Format("{0:#,#}", intMsInsured2007))
                    End If

                Else
                    txtNoOfMsInsured2007.Text = ""
                End If
            End If


            If dr("NUMOF_MSINSURED_YR_MINUS5").ToString() = "" Then
                txtNoOfMsInsured2006.Text = dr("NUMOF_MSINSURED_YR_MINUS5").ToString()
            Else
                If (dr("NUMOF_MSINSURED_YR_MINUS5").ToString() <> "N/A") Then
                    Dim intMsInsured2006 As Long = CType(dr("NUMOF_MSINSURED_YR_MINUS5"), Long)
                    If (intMsInsured2006 = 0) Then
                        txtNoOfMsInsured2006.Text = dr("NUMOF_MSINSURED_YR_MINUS5").ToString()
                    Else
                        txtNoOfMsInsured2006.Text = (String.Format("{0:#,#}", intMsInsured2006))
                    End If

                Else
                    txtNoOfMsInsured2006.Text = ""
                End If
            End If

            ' txtNoOfMsInsured2007.Text = dr("NUMOF_MSINSURED_YR_MINUS4").ToString()


            ' txtNoOfMsInsured2006.Text = dr("NUMOF_MSINSURED_YR_MINUS5").ToString()
            txtReason.Text = dr("SUMMARY_RATEINC").ToString()
            hyperLinkAddComp.Text = " Edit Company Details"
            txtCompanyName.Text = dr("COMPANY_NAME").ToString()
            If (dr("DATE_SUBMITTED").ToString() = "01/01/0001") Or (dr("DATE_SUBMITTED").ToString() = "01/01/2001") Then
                TxtDateSubmitted.Text = ""
            Else
                TxtDateSubmitted.Text = dr("DATE_SUBMITTED").ToString()
            End If

            txtLinkToRateInc.Text = dr("LINK_RATE_INCREASE").ToString()

        Next

    End Sub
    Protected Sub btnPopupSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPopupSubmit.Click
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        Dim intCompID As Integer = 0
        Dim drpSelectedValue As Integer
        If Session.Item("drpCompanyID") <> Nothing Then
            drpSelectedValue = CType(Session.Item("drpCompanyID"), Integer)
        End If
        Try
            If (FileUploadLogo.HasFile) Then

                Dim filename As String
                If (FileUploadLogo.PostedFile.ContentType = "image/jpeg") Then

                    If (FileUploadLogo.PostedFile.ContentLength < 204800) Then

                        filename = System.IO.Path.GetFileName(FileUploadLogo.FileName)
                        'FileUploadLogo.SaveAs(Server.MapPath("images/" + filename))
                        Dim fs As System.IO.Stream
                        fs = FileUploadLogo.PostedFile.InputStream
                        Dim filesize As Long = fs.Length
                        Dim buffer(filesize) As Byte

                        Dim q As Integer = fs.Read(buffer, 0, Convert.ToInt32(fs.Length))
                        fs.Close()
                        Dim ViewMode As String = CType(Request.QueryString("ViewMode"), String)
                        If (ViewMode = "edit") Then
                            ' drpDownCompany.Enabled = True
                            rateChangesBL.UpdateCompany(txtCompanyName.Text, drpDownCompany.SelectedValue.ToString(), filename, FileUploadLogo.PostedFile.ContentType, buffer)
                            PopulateCompanyComboBox()
                            drpDownCompany.SelectedValue = drpSelectedValue

                            lblUploadStatus.Text = "Company Details Updated Successfully"

                        Else
                            intCompID = rateChangesBL.InsertCompany(txtCompanyName.Text, filename, FileUploadLogo.PostedFile.ContentType, buffer)
                            If intCompID <> 0 Then
                                lblUploadStatus.Text = "Company already Exists"
                            Else
                                lblUploadStatus.Text = "New Company Details Added Successfully"
                                PopulateCompanyComboBox()
                            End If


                        End If


                    Else
                        lblUploadStatus.Text = "The Image file has to be less than 200 kb!"
                    End If
                Else
                    lblUploadStatus.Text = "Only JPEG files are accepted!"

                End If

            Else
                Dim dsCompany As DataSet = rateChangesBL.GetCompanyLogoByID(drpDownCompany.SelectedValue.ToString())
                Dim fileName As String = ""
                Dim fileType As String = ""
                Dim buffer() As Byte = Nothing
                Dim dr As DataRow
                Dim dt As DataTable
                If Not (dsCompany Is Nothing) Then
                    dt = dsCompany.Tables(0)

                    For Each dr In dt.Rows
                        If Not (dr("COMPANY_LOGO_FILENAME") Is Nothing) Then
                            fileName = dr("COMPANY_LOGO_FILENAME").ToString()
                        End If
                        If Not dr("LOGO_FILE_TYPE") Is Nothing Then
                            fileType = dr("LOGO_FILE_TYPE").ToString()
                        End If
                        If dr("FILE_CONTENT").ToString() <> System.DBNull.Value.ToString Then
                            buffer = CType(dr("FILE_CONTENT"), Byte())

                        End If
                    Next
                End If


                Dim ViewMode As String = CType(Request.QueryString("ViewMode"), String)

                If (ViewMode = "edit") Then
                    'If (buffer Is Nothing) Then
                    '    lblUploadStatus.Text = "Please Upload A Logo"
                    'Else
                    rateChangesBL.UpdateCompany(txtCompanyName.Text, drpDownCompany.SelectedValue.ToString(), fileName, fileType, buffer)
                    PopulateCompanyComboBox()
                    drpDownCompany.SelectedValue = drpSelectedValue
                    lblUploadStatus.Text = "Company Details Updated Successfully"

                Else
                    'If (buffer Is Nothing) Then
                    '    lblUploadStatus.Text = "Please Upload A Logo"
                    'Else
                    intCompID = rateChangesBL.InsertCompany(txtCompanyName.Text, fileName, fileType, buffer)
                    If intCompID = 1 Then
                        lblUploadStatus.Text = "Company already Exists"
                    Else
                        lblUploadStatus.Text = "New Company Details Added Successfully"
                        PopulateCompanyComboBox()
                    End If


                End If


            End If
            'PopulateCompanyComboBox()



        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE, False)
        End Try


    End Sub

    Sub exportFile(ByVal intFileID As Integer)
        Try


            Dim arrList As New ArrayList()
            Dim dsFile As New DataSet
            Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
            dsFile = rateChangesBL.ExportByFileID(intFileID)
            Dim fileName As String = ""
            Dim fileType As String = ""
            Dim buffer() As Byte = Nothing
            Dim dr As DataRow
            Dim dt As DataTable
            If Not (dsFile Is Nothing) Then
                dt = dsFile.Tables(0)

                For Each dr In dt.Rows
                    fileName = dr("NAME").ToString()
                    fileType = dr("TYPE").ToString()
                    buffer = CType(dr("CONTENT"), Byte())
                Next


                Response.Clear()
                Response.ClearContent()
                Response.ClearHeaders()
                Response.ContentType = fileType
                Response.AddHeader("content-disposition", "attachment; filename=" + _
        fileName)

                Response.BufferOutput = True
                HttpContext.Current.Response.BinaryWrite(buffer)
                HttpContext.Current.ApplicationInstance.CompleteRequest()

            End If
        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try

    End Sub

    Sub PopulateFileDownloadGrid(ByVal intRateID As Integer)

        Dim arrList As New ArrayList()
        Dim dsFile As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        dsFile = rateChangesBL.Export(intRateID)
        If Not (dsFile Is Nothing) Then
            gridDownloadFiles.DataSource = dsFile
            gridDownloadFiles.DataBind()
        End If

    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()

        Try
            rateChangesBL.DeleteCompany(drpDownCompany.SelectedValue.ToString())
            Response.Redirect("RateChangesDashBoard.aspx", False)

        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE, False)
        End Try
    End Sub
    Protected Sub gridDownloadFiles_OnRowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gridDownloadFiles.RowCommand

        Try

            If e.CommandName.ToLower() = "dwnload" Then
                Dim lb As LinkButton = CType(e.CommandSource, LinkButton)
                'getting clicked link button      
                Dim strKey As String = CType(lb.Parent.FindControl("hfKey"), HiddenField).Value
                'getting PK Value   
                exportFile(strKey)

            End If

            If e.CommandName.ToLower() = "delete" Then
                Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
                Dim lb As LinkButton = CType(e.CommandSource, LinkButton)
                'getting clicked link button      
                Dim strKey As String = CType(lb.Parent.FindControl("hfKey"), HiddenField).Value
                'getting PK Value 
                rateChangesBL.DeleteFileByID(strKey)
                Dim rateid As Integer = CType(Request.QueryString("rateid"), Integer)

                If (rateid > 0) Then

                    PopulateFileDownloadGrid(rateid)

                End If
            End If
        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE, False)
        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnError(ByVal args As EventArgs)
        Admin.WriteLog(Server.GetLastError())
        Response.Redirect(ERROR_PAGE, False)
    End Sub

    Protected Sub gridDownloadFiles_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gridDownloadFiles.RowDeleting
        Dim rateid As Integer = CType(Request.QueryString("rateid"), Integer)

        If (rateid > 0) Then

            PopulateFileDownloadGrid(rateid)
        End If
    End Sub

    Sub LoadLabelControls()
        lblDate1.Text = DateTime.Now.Year - 1
        lblDate2.Text = DateTime.Now.Year - 2
        lblDate3.Text = DateTime.Now.Year - 3
        lblDate4.Text = DateTime.Now.Year - 4
        lblDate5.Text = DateTime.Now.Year - 5

        lblprct1.Text = DateTime.Now.Year - 1
        lblprct2.Text = DateTime.Now.Year - 2
        lblprct3.Text = DateTime.Now.Year - 3
        lblprct4.Text = DateTime.Now.Year - 4
        lblprct5.Text = DateTime.Now.Year - 5

        lbllossratio1.Text = DateTime.Now.Year - 1
        lbllossratio2.Text = DateTime.Now.Year - 2
        lbllossratio3.Text = DateTime.Now.Year - 3
        lbllossratio4.Text = DateTime.Now.Year - 4
        lbllossratio5.Text = DateTime.Now.Year - 5

        lblNoOfMS1.Text = DateTime.Now.Year - 1
        lblNoOfMS2.Text = DateTime.Now.Year - 2
        lblNoOfMS3.Text = DateTime.Now.Year - 3
        lblNoOfMS4.Text = DateTime.Now.Year - 4
        lblNoOfMS5.Text = DateTime.Now.Year - 5
    End Sub


    Protected Sub linkLogout_Click(ByVal sender As Object, ByVal e As EventArgs) Handles linkLogout.Click
        Dim aCookie As HttpCookie = New HttpCookie("userInfo")
        aCookie.Expires = DateTime.Now.AddDays(-1)
        Response.Cookies.Add(aCookie)

        For Each key As String In Request.Cookies.AllKeys
            Dim cookie As HttpCookie = New HttpCookie(key)
            cookie.Expires = DateTime.UtcNow.AddDays(-7)
            Response.Cookies.Add(cookie)
        Next


        If Not (Request.Cookies("userInfo") Is Nothing) Then
            userName = Convert.ToString(Request.Cookies("userInfo")("userName"))

        End If
        Dim strLogout As String = ConfigurationManager.AppSettings("MidRateReview.logout")
        Dim strLogoutUrl As String = strLogout + "/Account/login.aspx"
        Response.Redirect(strLogoutUrl, False)
    End Sub

    Private Sub drpDownCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpDownCompany.SelectedIndexChanged

    End Sub

    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReturn.Click
        Response.Redirect("RateChangesDashBoard.aspx", False)
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Response.Redirect("RateChangesDashBoard.aspx", False)
    End Sub

    Protected Sub btnMidBlog_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMidBlog.Click
        Dim strLogout As String = ConfigurationManager.AppSettings("MidRateReview.logout")
        Dim strLogoutUrl As String = strLogout + "/"
        Response.Redirect(strLogoutUrl, False)

    End Sub
End Class