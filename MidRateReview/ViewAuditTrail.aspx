﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="ViewAuditTrail.aspx.vb" Inherits="MidWebApp.WebForm3" 
    title="View Audit Trail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
<link href="css/grid.css" rel="stylesheet" media="screen" type="text/css" />
    
<div id="page_header3">
    <asp:Label ID="lblTitle" runat="server" Text="View Audit Trail" 
        CssClass="h2-form"> </asp:Label>
        <asp:LinkButton ID="btnLink"  CssClass="logout" runat="server">Log Out</asp:LinkButton>
    </div>
    <div class="spacer"></div>
        <asp:Button ID="btnBack" runat="server" CssClass="btn-form" Text="Back" />    
    
    
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <div class="center-all col_13">
        <div class="rate-changes col_13">
            <ul class="list-hrz">
                <li>
                    <label><asp:Label ID="lblSearchByAction" runat="server" Text="Search By Action"></asp:Label></label>
                    <asp:DropDownList ID="drpListAction" runat="server" Width="115px" 
                        AutoPostBack="True" CausesValidation="True">
                        <asp:ListItem>All</asp:ListItem>
                        <asp:ListItem Value="Insert Rate Review">Insert</asp:ListItem>
                        <asp:ListItem Value="Delete Rate Review">Delete</asp:ListItem>
                        <asp:ListItem Value="Update Rate Review">Update</asp:ListItem>
                    </asp:DropDownList>
                </li>
                <li>
                    <label><asp:Label ID="lblActionBy" runat="server" Text="Action By"></asp:Label></label>
                    <asp:TextBox ID="txtActionBy" runat="server" Width="200px"></asp:TextBox>
                </li>
                <li class="date2date">
                    <label><asp:Label ID="lblFromDate" runat="server" Text="From Date <span>To Date</span>"></asp:Label></label>
                    <asp:TextBox ID="txtFromDate" runat="server" cssClass="w60"></asp:TextBox>
                        <input id="btnPick1" type="button" runat = "server" value = "" class="btn-calendar"/>  
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server"  PopupButtonID = "btnPick1" TargetControlID = "txtFromDate"></cc1:calendarextender>    
                         <span id="hyphenSpan" runat="server">-&nbsp;</span>
                        <asp:TextBox ID="txtToDate" runat="server"  cssClass="w60"></asp:TextBox>

                        <input id="btnPick2" type="button" value="" runat = "server" class="btn-calendar" />
                        <cc1:CalendarExtender ID="CalendarExtender3" runat="server"  PopupButtonID = "btnPick2" TargetControlID = "txtToDate"></cc1:calendarextender>    


                        <asp:Button ID="btnSearch" runat="server" CssClass="btn-form" 
                            Text="Search" />
                 </li>
             </ul>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                            ControlToValidate="txtFromDate" CssClass="validators" 
                            ErrorMessage="Please Enter Valid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                            
                            <asp:CompareValidator ID="CompareValidator2" runat="server" 
                    ControlToValidate="txtToDate" CssClass="validators" 
                    ErrorMessage="Please Enter Valid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
              
        </div>
        <div class="spacer"></div>
            <asp:GridView ID="gridAuditTrail" runat="server" AllowPaging="True" PageSize = "10"  EmptyDataText="No Record Available" 
                AllowSorting="True" CssClass="gridclass grid-vat col_13" AutoGenerateColumns="False" >
                <Columns>
                    <asp:BoundField DataField="AUDIT_ID" HeaderText="RATECHANGE ID" />
                    <asp:BoundField DataField="ACTION_DATE" HeaderText="ACTION DATE" />
                    <asp:BoundField DataField="ACTION_BY" HeaderText="ACTION BY" />
                    <asp:BoundField DataField="ACTION" HeaderText="ACTION" />
                    <asp:BoundField DataField="COLUMN_NAME" HeaderText="COLUMN NAME">
                    </asp:BoundField>
                     <asp:BoundField DataField="OLD_VALUE" HeaderText="OLD VALUE" />
                    <asp:BoundField DataField="NEW_VALUE" HeaderText="NEW VALUE" />
                      </Columns>
                       <PagerStyle HorizontalAlign="left" />
          <PagerTemplate>
                        <table>
                              <tr>
                                    <td>
                                          <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                              </tr>
                        </table>
                  </PagerTemplate>
            </asp:GridView>
            <p class="txt-gry col_5 gutter10-l left">You are viewing page
        <strong><%=gridAuditTrail.PageIndex + 1%></strong>
                of
        <strong><%=gridAuditTrail.PageCount%></strong>
        </p>
    </div>

    

</asp:Content>
