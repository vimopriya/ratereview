﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="ViewRateChanges.aspx.vb" Inherits="MidWebApp.ViewRateChange" 
    title="View Rate Changes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
<link href="css/grid.css" rel="stylesheet" media="screen" type="text/css" />

<div id="page_header3">
    <asp:Label ID="lblTitle" runat="server" Text="View Rate Changes" 
        CssClass="h2-form"> </asp:Label>
        <asp:LinkButton ID="btnLink"  CssClass="logout" visible = false runat="server">Log 
    Out</asp:LinkButton>
        <asp:LinkButton ID="btnMidRateReviewHome" CssClass="logout" runat="server"  CausesValidation="False" >MID 
    Rate Review Home</asp:LinkButton>
    </div>
    <div class="spacer"></div>
        <asp:Button ID="btnReturn" runat="server" CssClass="btn-form" Text="Return To Search Results" 
        CausesValidation="false"/>
     
     <div class="center-all gutter10 col_11">
     
      <table class="grid-co col_11">
         <tr>
             <th colspan="4">
              <h4><asp:Label ID="lblCompanyVal" runat="server"></asp:Label></h4>
             </th>
         </tr>
         <tr>
             <td class="w110">
                <label><asp:Label ID="lblPolicyFormNo" runat="server" Text="Policy Form &#35;:"></asp:Label></label>
             </td>
             <td>
                <asp:Label ID="lblPolicyFormVal" runat="server"></asp:Label>
             </td> 
             <td class="w140">
                <label><asp:Label ID="lbldateCurrent" runat="server" 
                     Text="Current Rate Increased:"></asp:Label></label>
             </td>
             <td>
             <asp:Label ID="lbldateCurrentVal" runat="server"></asp:Label>
             </td>    
         </tr>
         <tr>
             <td class="w110">
                <label><asp:Label ID="Label1" runat="server" Text="Type of Insurance:"></asp:Label></label>
             </td>
             <td>
             <asp:Label ID="lblPolicyTypeval" runat="server"></asp:Label>
             </td>     
             <td class="w140">
                <label><asp:Label ID="lblPercent" runat="server" 
                     Text="Current Rate Increase:"></asp:Label></label>
             </td>
             <td>
             <asp:Label ID="lblPercentVal" runat="server"></asp:Label>
             </td>
         </tr>
         <tr>
             <td class="w110">
                <label><asp:Label ID="lblPolicyApproval" runat="server" Text="Policy Approval:"></asp:Label></label>
             </td>
             <td>
             <asp:Label ID="lblPolicyApprlVal" runat="server"></asp:Label>
             </td>     
            <td class="w140">
            
                <label><asp:Label ID="lblLinkRateReview" runat="server" 
                     Text="Link to MS Rate Reviews On HealthCare.gov:"></asp:Label></label>
             </td>
             <td>
              <asp:HyperLink ID="hlinkRateReview" runat="server" OnClick = "return confirm('You are now leaving the Mississippi Insurance Department website and being directed to HealthCare.gov, a website operated by\nthe United States Department of Health and Human Services.')" target="_blank">
              </asp:HyperLink>
             
             </td>
         </tr>         
      </table>
              <div class="spacer"></div>
          
         <table class="grid-vrc gridclass rate-changes col_11">
          <tr class="gridheader">
              <th>
              
              </th>
               <th>
                 <asp:Label ID="lbl2010" runat="server"></asp:Label>
               </th>
               <th>
                   <asp:Label ID="lbl2009" runat="server"></asp:Label>
                 </th>
                <th>
                <asp:Label ID="lbl2008" runat="server"></asp:Label>
               </th>
               <th>
                <asp:Label ID="lbl2007" runat="server"></asp:Label>
               </th>
               <th>
             <asp:Label ID="lbl2006" runat="server"></asp:Label>
               </th>
         </tr>
         <tr>
             <td>
             
                <label><asp:Label ID="lblDate" runat="server" 
                 Text="Date of Last (5) Years Increases"></asp:Label></label>
             </td>
             <td>
                <asp:Label ID="lblDate2010" runat="server"></asp:Label>
              </td>
              <td>
                <asp:Label ID="lblDate2009" runat="server"></asp:Label>
              </td>
              <td>
                <asp:Label ID="lblDate2008" runat="server"></asp:Label>
              </td>
              <td>
                <asp:Label ID="lblDate2007" runat="server"></asp:Label>
              </td>
              <td>
                <asp:Label ID="lblDate2006" runat="server"></asp:Label>
              </td>
         </tr>
         <tr class="rowcoloralt">
               <td>
                <label><asp:Label ID="lblPercentage" runat="server" Text="Percentage of Last (5) Yrs Increases"></asp:Label></label>
                </td>
              <td>
                <asp:Label ID="lblprct2010" runat="server"></asp:Label>
               </td>
             <td>
                <asp:Label ID="lblprct2009" runat="server"></asp:Label>
             </td>
             <td>
                <asp:Label ID="lblprct2008" runat="server"></asp:Label>
             </td>
             <td>
                <asp:Label ID="lblprct2007" runat="server"></asp:Label>
             </td>
             <td>
                <asp:Label ID="lblprct2006" runat="server"></asp:Label>
             </td>
         </tr>
         <tr>
             <td>
                <label><asp:Label ID="lblLossRatio" runat="server" Text="Last (5) Years Loss Ratio"></asp:Label></label>
             </td>
             <td>
               <asp:Label ID="lbllr2010" runat="server"></asp:Label>
             </td>
             <td>
                <asp:Label ID="lbllr2009" runat="server"></asp:Label>
             </td>
             <td>
                <asp:Label ID="lbllr2008" runat="server"></asp:Label>
             </td>
             <td>
              <asp:Label ID="lbllr2007" runat="server"></asp:Label>
             </td>
             <td>
               <asp:Label ID="lbllr2006" runat="server"></asp:Label>
             </td>
         </tr>
         <tr class="rowcoloralt">
             <td>
                <label><asp:Label ID="lblNoOfInsureds" runat="server" Text="No. of MS Insureds"></asp:Label></label>
             </td>
             <td>
             <asp:Label ID="lblms2010" runat="server"></asp:Label>
             </td>
             <td>
             <asp:Label ID="lblms2009" runat="server"></asp:Label>
             </td>
             <td>
              <asp:Label ID="lblms2008" runat="server"></asp:Label>
             </td>
             <td>
             <asp:Label ID="lblms2007" runat="server"></asp:Label>
             </td>
             <td>
              <asp:Label ID="lblms2006" runat="server"></asp:Label>
             </td>
             </tr>
         
         </table>
         <ul class="list-hrz right gutter10 links-secondary">
            <li>
               
                <asp:HyperLink ID="hLinkResource" runat="server" NavigateUrl = "~/Resources.aspx" target="_blank">Resources</asp:HyperLink>
              
            </li>
            <li>
             <asp:HyperLink ID="hLinkFAQ" runat="server" NavigateUrl = "/MidBlog/category/FAQ.aspx" target="_blank" >FAQ's</asp:HyperLink>
              
            </li>
            <li>
             <asp:HyperLink ID="hLinkFeedBack" runat="server" OnClick = "return confirm('All nonpublic personal health information and nonpublic personal financial information shall be held confidential by Mississippi Insurance Department.')" NavigateUrl = "mailto:ratehelp@mid.state.ms.us" target="_blank" >Submit 
                a Comment</asp:HyperLink>
               
            </li>
        </ul>
         </div>
 


</asp:Content>
