﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="HighestRateChange.aspx.vb" Inherits="MidWebApp.HighestRateChange" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<link href="css/grid.css" rel="stylesheet" media="screen" type="text/css" />
<body>
    <form id="form1" runat="server">
    <div>
      <div class="left col_15x gutter15">
    <div class="tabs-container">
     <asp:Menu id="menuTabs" CssClass="tabs-basic"   Runat="server" Orientation="Horizontal"        
             Height="26px" StaticSelectedStyle-CssClass="tab-selected">
             <Items>
                <asp:MenuItem Value="HighestRateChange"  Selected = true Text="Highest Rate Change"/>
                <asp:MenuItem Value="RatesByInsuranceType" Text="Rates By Insurance Type"/>  
                <asp:MenuItem Value="RateChangesByInsurer" Text="Rate Changes By Insurer" runat = "server"/> 
              </Items>
    </asp:Menu>
    </div>
    
   <div class="gridwrap">
    <asp:GridView ID="GridViewHighestRateChange" runat= "server" PersistedSelection="true"
        BackColor="White" AutoGenerateColumns="False" 
        AllowSorting="True" 
       ForeColor="Black">  
      
       <Columns>
         <asp:BoundField DataField="COMPANY_NAME" HeaderText="Company" SortExpression = "COMPANY_NAME">
                <ControlStyle CssClass="gridheader" />
                <HeaderStyle CssClass="gridheader w80" />
            </asp:BoundField>
                                          
              <asp:TemplateField HeaderText="Policy<br /> Form No"  SortExpression="POLICY_FORMNO">
                  <ItemTemplate>
                      <asp:Label ID="lblPolicyForm"  runat="server"   Text='<%# Bind("POLICY_FORMNO") %>'></asp:Label>
                  </ItemTemplate>
                                    <ControlStyle CssClass="gridheader" />
                  <HeaderStyle CssClass="gridheader TextBoxdata w70" />
                  <ItemStyle HorizontalAlign="left" Wrap="True" CssClass  ="w70of"/>
              </asp:TemplateField>
            <asp:BoundField DataField="POLICY_TYPE" HeaderText="Type Of Insurance" SortExpression = "POLICY_TYPE">
                <HeaderStyle CssClass="gridheader w200" />
                <ItemStyle Wrap="true" />
            </asp:BoundField>
            
<asp:BoundField DataField="PERCENT_CURRENT_RATEINC" 
                HeaderText="Percentage Of Rate Change" SortExpression = "PERCENT_CURRENT_RATEINC" ItemStyle-HorizontalAlign = "Center">
    <ItemStyle Wrap="False" />
</asp:BoundField>
</Columns>
   </asp:GridView>
    </div>
</div>
    </div>
 
    </form>
</body>
</html>
