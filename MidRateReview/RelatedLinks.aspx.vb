﻿
Imports System.IO
Imports System.Xml
Imports System.Xml.Xsl
Imports System.Xml.XmlReader
Partial Public Class RelatedLinks
    Inherits System.Web.UI.Page
    Dim isEdit As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnPopupSubmit.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnPopupSubmit.UniqueID, "")
        btnClosePopup.Attributes.Add("style", "visibility : hidden")
        'btnCancel.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnCancel.UniqueID, "")
        hyperLink1.Visible = False
        If (Page.IsPostBack) Then
            ApplyPaging()
        Else

            Dim strAdmin As String = ""
            If Not (Request.QueryString("ad") Is Nothing) Then
                strAdmin = Request.QueryString("ad").ToString()
            End If

            If (strAdmin = "1") Then
                linkBtnAdd.Visible = True
                btnMidBlog.Visible = True
                GridViewDispUrl.Columns(0).Visible = True
                GridViewDispUrl.Columns(1).Visible = True
                menuTabs.Visible = True
                menuTabs.Items(0).NavigateUrl = "Resources.aspx?ad=1"
                menuTabs.Items(1).NavigateUrl = "RelatedLinks.aspx?ad=1"
                menuTabs.Items(2).NavigateUrl = "FAQ.aspx?ad=1"
            Else
                btnMidBlog.Visible = False
                linkBtnAdd.Visible = False
                GridViewDispUrl.Columns(0).Visible = False
                GridViewDispUrl.Columns(1).Visible = False
                menuTabs.Visible = False
            End If
            PopulateGridView()
        End If




        'BindRelatedLinksBlogData()
    End Sub

    'Private Sub BindRelatedLinksBlogData()
    '    Dim strXmlSrc As String
    '    Dim strXslFile As String
    '    Dim myXmlDoc As New XmlDocument()
    '    Dim ResourcesStringBuilder As StringBuilder
    '    Dim ResourcesStringWriter As StringWriter

    '    Try

    '        strXmlSrc = ConfigurationManager.AppSettings("MidBlogRSSFeedLink").ToString()
    '        strXslFile = Server.MapPath("XslFiles/RelatedLinksTransform.xsl")


    '        myXmlDoc.Load(strXmlSrc)
    '        Dim myXslDoc As New XslCompiledTransform()
    '        myXslDoc.Load(strXslFile)

    '        ResourcesStringBuilder = New StringBuilder()
    '        ResourcesStringWriter = New StringWriter(ResourcesStringBuilder)

    '        myXslDoc.Transform(myXmlDoc, Nothing, ResourcesStringWriter)
    '        'FAQStringBuilder = FAQStringBuilder.Replace("http://10.3.0.26", "")
    '        ' LitlRelatedLinks.Text = ResourcesStringBuilder.ToString


    '    Catch ex As Exception

    '    Finally
    '        ResourcesStringWriter = Nothing
    '        ResourcesStringBuilder = Nothing
    '        myXmlDoc = Nothing

    '    End Try

    'End Sub

    Protected Sub btnMidRateReviewHome_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMidRateReviewHome.Click

        Dim strLogout As String = ConfigurationManager.AppSettings("CompareRateChanges")
        Dim strLogoutUrl As String = strLogout + "/CompareRateChanges.aspx"
        Response.Redirect(strLogoutUrl, False)

    End Sub

    Private Sub PopulateGridView()
        Dim resourcesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        resourcesDs = rateChangesBL.GetRelatedLinksUrl()
        GridViewDispUrl.DataSource = resourcesDs
        GridViewDispUrl.DataBind()


    End Sub

    Protected Sub btnPopupSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPopupSubmit.Click
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        Dim intResourceID As Integer = 0
        Dim strShowUrl As String
        Dim IsEdit As String = ""
        Dim strRelatedUrlTxt As String = ""
        Dim strUrlName As String = ""
        If (ChkBoxShowUrl.Checked = True) Then
            strShowUrl = "Y"
        Else
            strShowUrl = "N"
        End If
        If Session.Item("isEdit") <> Nothing Then
            IsEdit = CType(Session.Item("isEdit"), String)
        End If

        If Session.Item("ResourceID") <> Nothing Then
            intResourceID = CType(Session.Item("ResourceID"), Integer)
        End If
        If (txtUrlDisp.Text <> "") Then
            strRelatedUrlTxt = txtUrlDisp.Text.ToString().Trim()
        Else

            strRelatedUrlTxt = DBNull.Value.ToString()
        End If

        If (txtUrlName.Text <> "") Then

            strUrlName = txtUrlName.Text.ToString().Trim()
        Else

            strUrlName = DBNull.Value.ToString()
        End If



        If (IsEdit = "Y") Then
            rateChangesBL.UpdateRelatedLinksUrl(intResourceID, strUrlName, strShowUrl, Today.Date, strRelatedUrlTxt)
            Session.Clear()
            PopulateGridView()
        Else

            intResourceID = rateChangesBL.InsertNewRelatedLinksUrl(strUrlName, strShowUrl, Today.Date, strRelatedUrlTxt)
        End If
        PopulateGridView()
        Session.Clear()
        txtUrlName.Text = ""
        txtUrlDisp.Text = ""
        ChkBoxShowUrl.Checked = False

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Session.Clear()
        txtUrlName.Text = ""
        txtUrlDisp.Text = ""
        ChkBoxShowUrl.Checked = False
    End Sub

  
    Protected Sub GridViewRelatedLinksUrl_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridViewDispUrl.RowCommand

        Dim intRelatedLinksIdID As Integer

        If e.CommandName.ToLower() = "edt" Then
            ' btnPopupSubmit.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnPopupSubmit.UniqueID, "")

            Session("isEdit") = "Y"
            isEdit = True
            ModalPopupExtender1.Show()
            LblResouces.Text = "Edit Related Links Url"
            Dim lb As LinkButton = CType(e.CommandSource, LinkButton)
            'getting clicked link button      
            intRelatedLinksIdID = CType(lb.Parent.FindControl("hfKey"), HiddenField).Value
            Session("ResourceID") = intRelatedLinksIdID
            Dim resourcesDs As New DataSet
            Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
            resourcesDs = rateChangesBL.GetRelatedLinksUrlByID(intRelatedLinksIdID)

            Dim dr As DataRow
            Dim dt As DataTable
            '<code to fill the dataset>
            dt = resourcesDs.Tables(0)
            For Each dr In dt.Rows
                txtUrlName.Text = dr("RELATEDLINKS_URL").ToString()
                txtUrlDisp.Text = dr("RELATEDLINKS_DISP_TXT").ToString()
                If (dr("IS_SHOWINHOMEPAGE").ToString() = "Y") Then
                    ChkBoxShowUrl.Checked = True
                Else

                    ChkBoxShowUrl.Checked = False
                End If
            Next


        ElseIf e.CommandName.ToLower() = "dlt" Then
            Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
            Dim lb As LinkButton = CType(e.CommandSource, LinkButton)
            'getting clicked link button      
            Dim intRelatedLinksID As Integer = CType(lb.Parent.FindControl("hfKey"), HiddenField).Value
            'getting PK Value   
            rateChangesBL.DeleteRelatedlinksUrl(intRelatedLinksID)

        End If
        PopulateGridView()

    End Sub

    Protected Sub linkBtnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles linkBtnAdd.Click
        Session.Clear()
        txtUrlName.Text = ""
        txtUrlDisp.Text = ""
        ChkBoxShowUrl.Checked = False
    End Sub

    Protected Sub btnClosePopup_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClosePopup.Click
        Session.Clear()
        txtUrlName.Text = ""
        txtUrlDisp.Text = ""
        ChkBoxShowUrl.Checked = False
    End Sub

    Protected Sub GridViewDispUrl_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewDispUrl.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            GridViewDispUrl.PagerSettings.Mode = PagerButtons.NumericFirstLast
            GridViewDispUrl.PagerSettings.FirstPageText = "First"
            GridViewDispUrl.PagerSettings.LastPageText = "Last"
            GridViewDispUrl.PagerSettings.NextPageText = "Next"
            GridViewDispUrl.PagerSettings.PreviousPageText = "Previous"
        End If

    End Sub

    Protected Sub GridViewDispUrl_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles GridViewDispUrl.DataBound
        ApplyPaging()
    End Sub

    Protected Sub GridViewDispUrl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewDispUrl.PageIndexChanging

        Try
            If e.NewPageIndex <> -1 Then
                GridViewDispUrl.PageIndex = e.NewPageIndex
                PopulateGridView()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub ApplyPaging()

        Dim row As GridViewRow = GridViewDispUrl.BottomPagerRow
        Dim ph As PlaceHolder = New PlaceHolder
        Dim lnkPaging As LinkButton
        Dim lnkFirstPage As LinkButton
        Dim lnkPrevPage As LinkButton
        Dim lnkNextPage As LinkButton
        Dim lnkLastPage As LinkButton
        lnkFirstPage = New LinkButton()
        lnkFirstPage.Text = Server.HtmlEncode("<<First")
        lnkFirstPage.Width = Unit.Pixel(50)
        lnkFirstPage.CommandName = "Page"
        lnkFirstPage.CommandArgument = "First"
        lnkPrevPage = New LinkButton()
        lnkPrevPage.Text = Server.HtmlEncode("<Prev")
        lnkPrevPage.Width = Unit.Pixel(50)
        lnkPrevPage.CommandName = "Page"
        lnkPrevPage.CommandArgument = "Prev"

        If (GridViewDispUrl.PageCount <> 0) Then
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkFirstPage)
            ph.Controls.Add(lnkPrevPage)
        End If
        If (GridViewDispUrl.PageIndex = 0) Then

            lnkFirstPage.Enabled = False
            lnkPrevPage.Enabled = False
        Else
            lnkPrevPage.Enabled = True
        End If


        Dim i As Integer
        For i = 1 To GridViewDispUrl.PageCount

            lnkPaging = New LinkButton()
            lnkPaging.Width = Unit.Pixel(20)
            ' lnkPaging.CssClass = "LinkPaging"
            lnkPaging.Text = i.ToString()
            lnkPaging.CommandName = "Page"
            lnkPaging.CommandArgument = i.ToString()
            ' If (i = gridRateChanges.PageIndex + 1) Then
            ' lnkPaging.BackColor = System.Drawing.Color.Blue
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkPaging)
            ' End If

        Next i

        lnkNextPage = New LinkButton()
        lnkNextPage.Text = Server.HtmlEncode("Next>")
        lnkNextPage.Width = Unit.Pixel(50)
        lnkNextPage.CommandName = "Page"
        lnkNextPage.CommandArgument = "Next"
        lnkLastPage = New LinkButton()
        lnkLastPage.Text = Server.HtmlEncode("Last>>")
        lnkLastPage.Width = Unit.Pixel(50)
        lnkLastPage.CommandName = "Page"
        lnkLastPage.CommandArgument = "Last"
        lnkNextPage.Enabled = True
        lnkLastPage.Enabled = True
        If (GridViewDispUrl.PageCount <> 0) Then
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkNextPage)
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkLastPage)
        End If
        If (GridViewDispUrl.PageIndex + 1 = GridViewDispUrl.PageCount) Then

            lnkNextPage.Enabled = False
            lnkLastPage.Enabled = False
        End If
    End Sub

    Protected Sub GridViewDispUrl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewDispUrl.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim rowView As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
            Dim hLink As HyperLink = e.Row.FindControl("hLinkRateInc")
            hLink.Text = rowView("RELATEDLINKS_DISP_TXT").ToString().Trim()
            'hLink.Text = rowView("RELATEDLINKS_URL").ToString().Trim()
            hLink.NavigateUrl = rowView("RELATEDLINKS_URL").ToString().Trim()
            ' hLink.Attributes.Add("OnClick", "return confirm('You are now leaving the Mississippi Insurance Department website and being directed to HealthCare.gov, a website operated by\nthe United States Department of Health and Human Services.');")
            Dim strNavigationUrl As String = rowView("RELATEDLINKS_URL").ToString().Trim()
            If Not (strNavigationUrl.Contains("/MidBlog") Or strNavigationUrl.Contains("/MidRateChanges") Or strNavigationUrl.Contains("/MidHome") Or strNavigationUrl.Contains("mid.state.ms.us")) Then

                hLink.Attributes.Add("OnClick", "return confirm('You are now leaving the Mississippi Insurance Department website.');")

            End If
            If (strNavigationUrl.ToLower().Contains("healthcare.gov")) Then

                hLink.Attributes.Add("OnClick", "return confirm('You are now leaving the Mississippi Insurance Department website and being directed to HealthCare.gov, a website operated by\nthe United States Department of Health and Human Services.');")

            End If



        End If
    End Sub

    Protected Sub btnMidBlog_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMidBlog.Click
        Dim strLogout As String = ConfigurationManager.AppSettings("MidRateReview.logout")
        Dim strLogoutUrl As String = strLogout + "/"
        Response.Redirect(strLogoutUrl, False)

    End Sub
End Class