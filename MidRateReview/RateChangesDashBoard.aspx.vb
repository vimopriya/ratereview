﻿Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections.Generic
Imports System.IO
Imports RateChangesBL
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html
Imports iTextSharp.text.html.simpleparser
Imports System.Xml




Partial Public Class WebForm2
    Inherits System.Web.UI.Page

    Private Const ERROR_PAGE As String = "ErrorPage.aspx"
    Private isInPage As Boolean = False
    Private allowEdit As Boolean = True
    Public userName As String
    Dim RateChangeId As String = String.Empty
    Dim mode As String
    Dim searchCriteriaonComp As String = String.Empty

    Protected WithEvents btnRight1 As Global.System.Web.UI.WebControls.Button
    Protected WithEvents btnLeftNumOfInsured As Global.System.Web.UI.WebControls.Button
    Protected WithEvents btnRightLossRatio As Global.System.Web.UI.WebControls.Button
    Protected WithEvents btnLeftLossRatio As Global.System.Web.UI.WebControls.Button
    Protected WithEvents btnLeftPercentage As Global.System.Web.UI.WebControls.Button
    Protected WithEvents btnRightPercentage As Global.System.Web.UI.WebControls.Button
    Protected WithEvents btnLeft1 As Global.System.Web.UI.WebControls.Button


#Region "Private"


    <Serializable()> _
    Private Class MergedColumnsInfo
        ' indexes of merged columns
        Public MergedColumns As New List(Of Integer)()
        ' key-value pairs: key = first column index, value = number of merged columns
        Public StartColumns As New Hashtable()
        ' key-value pairs: key = first column index, value = common title of merged columns 
        Public Titles As New Hashtable()


        'parameters: merged columns's indexes, common title of merged columns 
        Public Sub AddMergedColumns(ByVal columnsIndexes As Integer(), ByVal title As String)
            MergedColumns.AddRange(columnsIndexes)
            StartColumns.Add(columnsIndexes(0), columnsIndexes.Length)
            Titles.Add(columnsIndexes(0), title)
        End Sub
        Public Sub DeleteColumns(ByVal columnsIndex As Integer, ByVal count As Integer)
            MergedColumns.RemoveRange(columnsIndex, count)
        End Sub
    End Class


    'property for storing of information about merged columns
    Private ReadOnly Property info() As MergedColumnsInfo
        Get
            If ViewState("info") Is Nothing Then
                ViewState("info") = New MergedColumnsInfo()
            End If


            Return DirectCast(ViewState("info"), MergedColumnsInfo)
        End Get
    End Property


    'method for rendering of columns's headers 
    Private Sub RenderHeader(ByVal output As HtmlTextWriter, ByVal container As Control)
        For i As Integer = 0 To container.Controls.Count - 1
            Dim cell As TableCell = DirectCast(container.Controls(i), TableCell)
            'stretch non merged columns for two rows
            If Not info.MergedColumns.Contains(i) Then
                cell.Attributes("rowspan") = "2"
                cell.RenderControl(output)
                'render merged columns's common title
            ElseIf info.StartColumns.Contains(i) Then
                output.Write(String.Format("<th align='center' colspan='{0}'>{1}</th>", info.StartColumns(i), info.Titles(i)))
            End If
        Next


        'close first row 
        output.RenderEndTag()
        'set attributes for second row
        gridRateChanges.HeaderStyle.AddAttributesToRender(output)
        'start second row
        output.RenderBeginTag("tr")


        'render second row (only merged columns)
        For i As Integer = 0 To info.MergedColumns.Count - 1
            Dim cell As TableCell = DirectCast(container.Controls(info.MergedColumns(i)), TableCell)
            cell.RenderControl(output)
        Next
    End Sub


#End Region


#Region "Event Handlers"

    

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            Response.BufferOutput = True
            Response.Buffer = True
            'Response.CacheControl = "no-cache"
            'Response.AddHeader("Pragma", "no-cache")
            'Response.Expires = -1441
            'lblResult.Text = ""
            If Not (Request.QueryString("Mode") Is Nothing) Then
                mode = Request.QueryString("Mode").ToString()
            End If
            If Not (Request.QueryString("Id") Is Nothing) Then
                RateChangeId = Request.QueryString("Id").ToString()
            End If
            If Session.Item("PageIndex") <> Nothing Then
                Dim pageind As Integer = CType(Session.Item("PageIndex"), Integer)
                gridRateChanges.PageIndex = pageind

            End If





            If Not IsPostBack Then

                Dim roleList As New ArrayList()
                Dim rightsList As New ArrayList()
                Dim isUserExists As Integer = 0
                Dim createRight As Boolean
                Dim editRight As Boolean
                Dim deleteRight As Boolean
                '  Dim AdminRoleName As String = ConfigurationManager.AppSettings("MidRateReview.AdminRole")
                Dim AnonymousRole As String = ConfigurationManager.AppSettings("MidRateReview.AnonymousRole")
                Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()

                linkLogout.Visible = False
                btnMidBlog.Visible = False
                ' ApplyPaging()
                If Not (Request.Cookies("userInfo") Is Nothing) Then
                    userName = Convert.ToString(Request.Cookies("userInfo")("userName"))

                End If

                If (userName <> "") Then
                    isUserExists = rateChangesBL.IsUserExists(userName)
                End If
                If (isUserExists > 0) Then
                    roleList = rateChangesBL.GetRolesForUser(userName)
                    Dim strRole As String
                    For Each strRole In roleList
                        Dim strAssignedRole As String = strRole
                        'If (strAssignedRole = AdminRoleName) Then
                        'linkLogout.Visible = True
                        'btnViewAudit.Visible = True
                        'btnViewErrorLog.Visible = True
                        'gridRateChanges.Columns(1).Visible = True
                        rightsList = rateChangesBL.GetRightsForUser(strAssignedRole)
                        If (rightsList.Contains("CreateRateReviewDashBoard")) Then
                            createRight = True
                            btnAddNew.Visible = True
                            btnViewAudit.Visible = True
                            btnViewErrorLog.Visible = True
                            gridRateChanges.Columns(1).Visible = True
                            hLinkFAQ.Visible = False
                            hLinkFeedBack.Visible = False
                            hLinkResource.Visible = False

                        End If
                        If (rightsList.Contains("EditRateReviewDashBoard")) Then
                            editRight = True
                            btnViewAudit.Visible = True
                            btnViewErrorLog.Visible = True
                            gridRateChanges.Columns(1).Visible = True
                            hLinkFAQ.Visible = False
                            hLinkFeedBack.Visible = False
                            hLinkResource.Visible = False
                        End If

                        If (rightsList.Contains("DeleteRateReviewDashBoard")) Then
                            deleteRight = True
                            btnViewAudit.Visible = True
                            btnViewErrorLog.Visible = True
                            gridRateChanges.Columns(1).Visible = True
                            hLinkFAQ.Visible = False
                            hLinkFeedBack.Visible = False
                            hLinkResource.Visible = False


                        End If

                        If (createRight = True) And (editRight = True) And (deleteRight = True) Then

                            gridRateChanges.Columns(0).Visible = True
                            gridRateChanges.Columns(1).Visible = True
                            gridRateChanges.Columns(2).Visible = True
                            hLinkFAQ.Visible = False
                            hLinkFeedBack.Visible = False
                            hLinkResource.Visible = False
                            btnCompareRate.Visible = False

                            btnAddNew.Visible = True
                            linkLogout.Visible = True
                            btnMidBlog.Visible = True
                            btnViewAudit.Visible = True
                            PopulateGridView()

                        End If

                        If (createRight = False) And (editRight = True) And (deleteRight = True) Then

                            gridRateChanges.Columns(0).Visible = True
                            gridRateChanges.Columns(1).Visible = True
                            gridRateChanges.Columns(2).Visible = False
                            btnViewAudit.Visible = True
                            btnViewErrorLog.Visible = True
                            hLinkFAQ.Visible = False
                            hLinkFeedBack.Visible = False
                            hLinkResource.Visible = False
                            btnAddNew.Visible = False
                            linkLogout.Visible = True
                            btnMidBlog.Visible = True
                            btnCompareRate.Visible = False

                            PopulateGridView()

                        End If
                        If (createRight = False) And (editRight = False) And (deleteRight = True) Then

                            gridRateChanges.Columns(0).Visible = False
                            gridRateChanges.Columns(1).Visible = True
                            gridRateChanges.Columns(2).Visible = False
                            btnAddNew.Visible = False
                            btnCompareRate.Visible = False

                            PopulateGridView()
                            linkLogout.Visible = True
                            btnMidBlog.Visible = True
                            hLinkFAQ.Visible = False
                            hLinkFeedBack.Visible = False
                            hLinkResource.Visible = False

                        End If
                        If (createRight = True) And (editRight = True) And (deleteRight = False) Then

                            gridRateChanges.Columns(0).Visible = True
                            gridRateChanges.Columns(1).Visible = True
                            gridRateChanges.Columns(2).Visible = False
                            btnAddNew.Visible = True
                            PopulateGridView()
                            hLinkFAQ.Visible = False
                            hLinkFeedBack.Visible = False
                            hLinkResource.Visible = False
                            btnCompareRate.Visible = False

                        End If
                        If (createRight = True) And (editRight = False) And (deleteRight = False) Then

                            gridRateChanges.Columns(0).Visible = False
                            gridRateChanges.Columns(1).Visible = True
                            gridRateChanges.Columns(2).Visible = False
                            btnAddNew.Visible = True
                            linkLogout.Visible = True
                            btnMidBlog.Visible = True
                            PopulateGridView()
                            hLinkFAQ.Visible = False
                            hLinkFeedBack.Visible = False
                            hLinkResource.Visible = False
                            btnCompareRate.Visible = False


                        End If
                        If (createRight = True) And (editRight = False) And (deleteRight = True) Then

                            gridRateChanges.Columns(0).Visible = False
                            gridRateChanges.Columns(1).Visible = True
                            gridRateChanges.Columns(2).Visible = True
                            btnAddNew.Visible = True
                            linkLogout.Visible = True
                            btnMidBlog.Visible = True
                            PopulateGridView()
                            hLinkFAQ.Visible = False
                            hLinkFeedBack.Visible = False
                            hLinkResource.Visible = False
                            btnCompareRate.Visible = False


                        End If
                        If (createRight = False) And (editRight = False) And (deleteRight = False) Then

                            gridRateChanges.Columns(0).Visible = False
                            gridRateChanges.Columns(1).Visible = True
                            gridRateChanges.Columns(2).Visible = False
                            btnAddNew.Visible = False
                            linkLogout.Visible = True
                            btnMidBlog.Visible = True
                            PopulateGridView()
                            hLinkFAQ.Visible = False
                            hLinkFeedBack.Visible = False
                            hLinkResource.Visible = False
                            btnCompareRate.Visible = False


                        End If


                        If (strAssignedRole = AnonymousRole) Then
                            'Allow Editing the Rate Review Dash Board
                            gridRateChanges.Columns(0).Visible = False
                            gridRateChanges.Columns(1).Visible = True
                            gridRateChanges.Columns(2).Visible = False
                            gridRateChanges.Columns(3).Visible = False
                            btnAddNew.Visible = False
                            btnViewAudit.Visible = False
                            btnViewErrorLog.Visible = False
                            btnCompareRate.Visible = True

                            hLinkFAQ.Visible = True
                            hLinkFeedBack.Visible = True
                            hLinkResource.Visible = True
                            PopulateGridView()
                        Else
                            'don't allow Editing the Rate Review Dash Board
                            'PopulateGridView()
                            'gridRateChanges.Columns(0).Visible = False
                            'gridRateChanges.Columns(1).Visible = True
                            'gridRateChanges.Columns(2).Visible = False
                            'hLinkFAQ.Visible = True
                            'hLinkFeedBack.Visible = True
                            'hLinkResource.Visible = True
                            'btnViewAudit.Visible = False
                            'btnCompareRate.Visible = True
                            'btnAddNew.Visible = False
                            'btnViewErrorLog.Visible = False
                            If (Request.QueryString.Count > 0) Then
                                allowEdit = False
                                Dim compId As String = CType(Request.QueryString("compID"), String)
                                If Not (compId Is Nothing) Then
                                    drpListCompany.SelectedValue = compId
                                    Session("CompId") = compId.ToString()
                                    PopulateGridViewByCompID(compId)

                                End If

                               

                                'gridRateChanges.Columns(0).Visible = False
                                'gridRateChanges.Columns(1).Visible = True
                                'gridRateChanges.Columns(2).Visible = False
                                'btnAddNew.Visible = False
                                'btnCompareRate.Visible = True
                                'btnViewAudit.Visible = False
                                'btnViewErrorLog.Visible = False
                                'hLinkFAQ.Visible = True
                                'hLinkFeedBack.Visible = True
                                'hLinkResource.Visible = True

                            End If

                        End If

                    Next
                End If

                If (isUserExists = 0) Then

                    If (Request.QueryString.Count > 0) Then
                        allowEdit = False
                        Dim compId As String = CType(Request.QueryString("compID"), String)
                        If Not (compId Is Nothing) Then
                            drpListCompany.SelectedValue = compId
                            Session("CompId") = compId.ToString()
                            PopulateGridViewByCompID(compId)
                            hLinkFAQ.Visible = True
                            hLinkFeedBack.Visible = True
                            hLinkResource.Visible = True

                        End If

                      
                        gridRateChanges.Columns(0).Visible = False
                        gridRateChanges.Columns(1).Visible = True
                        gridRateChanges.Columns(2).Visible = False
                        btnAddNew.Visible = False
                        btnCompareRate.Visible = True
                        btnViewAudit.Visible = False
                        btnViewErrorLog.Visible = False
                        hLinkFAQ.Visible = True
                        hLinkFeedBack.Visible = True
                        hLinkResource.Visible = True

                    Else
                        PopulateGridView()
                        gridRateChanges.Columns(0).Visible = False
                        gridRateChanges.Columns(1).Visible = True
                        gridRateChanges.Columns(2).Visible = False
                        btnAddNew.Visible = False
                        btnCompareRate.Visible = True
                        btnViewAudit.Visible = False
                        btnViewErrorLog.Visible = False
                        hLinkFAQ.Visible = True
                        hLinkFeedBack.Visible = True
                        hLinkResource.Visible = True
                    End If

                End If

                If (userName = "") Then
                    If (Request.QueryString.Count > 0) Then
                        allowEdit = False
                        Dim compId As String = CType(Request.QueryString("compID"), String)
                        If Not (compId Is Nothing) Then
                            drpListCompany.SelectedValue = compId
                            PopulateGridViewByCompID(compId)
                            btnCompareRate.Visible = True
                            hLinkFAQ.Visible = True
                            hLinkFeedBack.Visible = True
                            hLinkResource.Visible = True

                        End If

                   
                    Else
                        PopulateGridView()
                        gridRateChanges.Columns(0).Visible = False
                        gridRateChanges.Columns(1).Visible = True
                        gridRateChanges.Columns(2).Visible = False
                        btnAddNew.Visible = False
                        btnCompareRate.Visible = True
                        btnViewAudit.Visible = False
                        btnViewErrorLog.Visible = False
                        hLinkFAQ.Visible = True
                        hLinkFeedBack.Visible = True
                        hLinkResource.Visible = True
                    End If
                End If

                'Add Code here for mode = chartView

                If mode = "ChartView" Then
                    Dim companyNames As String = String.Empty
                    gridRateChanges.Columns(0).Visible = False
                    gridRateChanges.Columns(1).Visible = False
                    gridRateChanges.Columns(2).Visible = False
                    gridRateChanges.Columns(3).Visible = False
                    menuTabs.Items(0).NavigateUrl = "CompareCarriers.aspx?ID=" + RateChangeId
                    menuTabs.Items(1).NavigateUrl = "RateChangesDashBoard.aspx?Mode=ChartView&ID=" + RateChangeId
                    btnAddNew.Visible = False
                    btnCompareRate.Visible = False
                    DrpListExport.Visible = False
                    btnExport.Visible = False
                    drpListCompany.Visible = False
                    drpListPolicyType.Visible = False
                    lblFromDate.Visible = False
                    txtFromDate.Visible = False
                    btnPick1.Visible = False
                    lblSearchCompany.Visible = False
                    txtToDate.Visible = False
                    btnPick2.Visible = False
                    lblPolicyType.Visible = False
                    btnSearch.Visible = False
                    btnCompare.Visible = False
                    DrpListExport.Visible = False
                    btnViewAudit.Visible = False
                    btnViewErrorLog.Visible = False
                    hyphenSpan.Visible = False
                    menuPanel.Visible = True
                    menuTabs.Visible = True
                    btnReturn.Visible = True
                    menuTabs.Items(1).Selected = True
                    PopulateGridViewByRateChangeIDs(RateChangeId)

                    For Each row As GridViewRow In gridRateChanges.Rows
                        companyNames = String.Concat(companyNames, " And ", row.Cells(4).Text, " - ", row.Cells(5).Text, " - ", row.Cells(6).Text)
                    Next
                    If companyNames.StartsWith(" And") Then
                        companyNames = companyNames.Remove(0, 4)
                    End If
                    lblTitle.Text = String.Concat("Spreadsheet of ", companyNames, " Rate Changes")
                    Page.Title = String.Concat("Spreadsheet of ", companyNames, " Rate Changes")
                End If


                Dim intSessionRateID As Integer
                If Session.Item("rateID") <> Nothing Then
                    intSessionRateID = CType(Session.Item("rateID"), Integer)
                    For Each row As GridViewRow In gridRateChanges.Rows

                        Dim rateid As Integer = Convert.ToInt32(gridRateChanges.DataKeys(row.RowIndex).Value)
                        If (rateid = intSessionRateID) Then

                            row.Attributes.Add("style", "background-color: #C7DDED")
                            ' gridRateChanges.Columns(4).ControlStyle.BackColor = Drawing.Color.Aqua
                        End If

                    Next
                End If

                'merge second, third and fourth columns with common title "Subjects"
                DisplayDefaultGridControls()


                PopulateCompanyDropDownList()
                PopulatePolicyDropDownList()
                drpListCompany.Items.Insert(0, New System.Web.UI.WebControls.ListItem("All", "All"))
                drpListPolicyType.Items.Insert(0, New System.Web.UI.WebControls.ListItem("All", "All"))

            End If


            If (Page.IsPostBack) Then
                ApplyPaging()
                Session.Clear()
            End If

            If Session.Item("searchComp") <> Nothing Then
                Dim compId As String = CType(Session.Item("SearchComp"), Integer)
                drpListCompany.SelectedValue = compId
                btnSearch_Click(sender, e)
                Session.Clear()

            End If

            'If searchCriteriaonComp <> "" Then

            '    drpListCompany.SelectedValue = searchCriteriaonComp
            '    btnSearch_Click(sender, e)
            'End If


        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try

    End Sub
    Public Property SortDirection() As String
        Get
            If (ViewState("SortDirection") Is Nothing) Then ViewState("SortDirection") = String.Empty
            Return ViewState("SortDirection").ToString()
        End Get
        Set(ByVal value As String)
            ViewState("SortDirection") = value
        End Set
    End Property

    Public Property SortExpressionGrid() As String
        Get
            If (ViewState("SortExpressionGrid") Is Nothing) Then ViewState("SortExpressionGrid") = String.Empty
            Return ViewState("SortExpressionGrid").ToString()
        End Get
        Set(ByVal value As String)
            ViewState("SortExpressionGrid") = value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateGridView()
        Dim rateChangesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        rateChangesDs = rateChangesBL.GetRateChangesData()
        gridRateChanges.DataSource = rateChangesDs

        Dim dr As DataRow
        Dim dt As DataTable = rateChangesDs.Tables(0)
        If Not (gridRateChanges Is Nothing) Then


            For Each dr In dt.Rows
                If Not IsDBNull(dr("DATEOF_YR_MINUS1_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS2_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS3_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS4_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                End If


                If Not IsDBNull(dr("DATEOF_YR_MINUS5_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS5_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS5_RATEINC") = "N/A"
                End If
                If (dr("POLICY_APPROVAL").ToString() = "01/01/0001") Then
                    dr("POLICY_APPROVAL") = "N/A"
                End If

                If (dr("PERCENT_CURRENT_RATEINC").ToString() <> "N/A") Then
                    Dim strPercentage As String = dr("PERCENT_CURRENT_RATEINC").ToString() & "%"
                    dr("PERCENT_CURRENT_RATEINC") = strPercentage.ToString()
                End If
                If (dr("PERCENT_YR_MINUS1_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS1_RATEINC") = dr("PERCENT_YR_MINUS1_RATEINC").ToString() & "%"
                End If
                If (dr("PERCENT_YR_MINUS2_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS2_RATEINC") = dr("PERCENT_YR_MINUS2_RATEINC").ToString() & "%"
                End If

                If (dr("PERCENT_YR_MINUS3_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS3_RATEINC") = dr("PERCENT_YR_MINUS3_RATEINC").ToString() & "%"
                End If

                If (dr("PERCENT_YR_MINUS4_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS4_RATEINC") = dr("PERCENT_YR_MINUS4_RATEINC").ToString() & "%"
                End If

                If (dr("PERCENT_YR_MINUS5_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS5_RATEINC") = dr("PERCENT_YR_MINUS5_RATEINC").ToString() & "%"
                End If
                If (dr("LAST_MINUS1_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS1_LOSSRATIO") = dr("LAST_MINUS1_LOSSRATIO").ToString() & "%"
                End If

                If (dr("LAST_MINUS2_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS2_LOSSRATIO") = dr("LAST_MINUS2_LOSSRATIO").ToString() & "%"
                End If

                If (dr("LAST_MINUS3_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS3_LOSSRATIO") = dr("LAST_MINUS3_LOSSRATIO").ToString() & "%"
                End If
                If (dr("LAST_MINUS4_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS4_LOSSRATIO") = dr("LAST_MINUS4_LOSSRATIO").ToString() & "%"
                End If
                If (dr("LAST_MINUS5_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS5_LOSSRATIO") = dr("LAST_MINUS5_LOSSRATIO").ToString() & "%"
                End If

            Next
        End If

        gridRateChanges.Columns(9).HeaderText = (DateTime.Now.Year - 1).ToString()
        gridRateChanges.Columns(10).HeaderText = (DateTime.Now.Year - 2).ToString()
        gridRateChanges.Columns(11).HeaderText = (DateTime.Now.Year - 3).ToString()
        gridRateChanges.Columns(12).HeaderText = (DateTime.Now.Year - 4).ToString()
        gridRateChanges.Columns(13).HeaderText = (DateTime.Now.Year - 5).ToString()


        gridRateChanges.Columns(16).HeaderText = DateTime.Now.Year - 1
        gridRateChanges.Columns(17).HeaderText = DateTime.Now.Year - 2
        gridRateChanges.Columns(18).HeaderText = DateTime.Now.Year - 3
        gridRateChanges.Columns(19).HeaderText = DateTime.Now.Year - 4
        gridRateChanges.Columns(20).HeaderText = DateTime.Now.Year - 5


        gridRateChanges.Columns(23).HeaderText = DateTime.Now.Year - 1
        gridRateChanges.Columns(24).HeaderText = DateTime.Now.Year - 2
        gridRateChanges.Columns(25).HeaderText = DateTime.Now.Year - 3
        gridRateChanges.Columns(26).HeaderText = DateTime.Now.Year - 4
        gridRateChanges.Columns(27).HeaderText = DateTime.Now.Year - 5

        gridRateChanges.Columns(30).HeaderText = DateTime.Now.Year - 1
        gridRateChanges.Columns(31).HeaderText = DateTime.Now.Year - 2
        gridRateChanges.Columns(32).HeaderText = DateTime.Now.Year - 3
        gridRateChanges.Columns(33).HeaderText = DateTime.Now.Year - 4
        gridRateChanges.Columns(34).HeaderText = DateTime.Now.Year - 5
        If (rateChangesDs.Tables.Count > 0) Then

            gridRateChanges.DataBind()

        End If
    End Sub

    Private Sub PopulateGridViewForExport()

        Dim rateChangesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        Try

            Dim strFromDate As String = ""
            Dim strToDate As String = ""
            Dim strSymbol = "%"
            strFromDate = txtFromDate.Text
            strToDate = txtToDate.Text
            If (txtFromDate.Text = "") Then
                strFromDate = "All"
            End If
            If (txtToDate.Text = "") Then
                strToDate = "All"
            End If

            If mode = "ChartView" Then
                rateChangesDs = rateChangesBL.GetRateChangesByRateChangeIDs(RateChangeId)
            Else
                rateChangesDs = rateChangesBL.GetRateChangeDataByIDTypeDate(drpListCompany.SelectedValue, drpListPolicyType.SelectedItem.ToString(), strFromDate, strToDate)
            End If


            If (rateChangesDs.Tables.Count > 0) Then
                Dim dr As DataRow
                Dim dt As DataTable = rateChangesDs.Tables(0)
                If Not (gridRateChanges Is Nothing) Then


                    For Each dr In dt.Rows
                        If Not IsDBNull(dr("DATEOF_YR_MINUS1_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                        End If

                        If Not IsDBNull(dr("DATEOF_YR_MINUS2_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                        End If

                        If Not IsDBNull(dr("DATEOF_YR_MINUS3_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                        End If

                        If Not IsDBNull(dr("DATEOF_YR_MINUS4_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                        End If


                        If Not IsDBNull(dr("DATEOF_YR_MINUS5_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS5_RATEINC") = "N/A"
                            End If
                        End If
                        If (dr("POLICY_APPROVAL").ToString() = "01/01/0001") Then
                            dr("POLICY_APPROVAL") = "N/A"
                        End If
                        If (dr("LINk_RATE_INCREASE").ToString() = " ") Then
                            dr("LINk_RATE_INCREASE") = "N/A"
                        End If
                        If (dr("PERCENT_CURRENT_RATEINC").ToString() <> "N/A") Then
                            ' Dim strPercentage As String = dr("PERCENT_CURRENT_RATEINC").ToString() & "%"
                            dr("PERCENT_CURRENT_RATEINC") = dr("PERCENT_CURRENT_RATEINC").ToString() & "%"
                        End If
                        If (dr("PERCENT_YR_MINUS1_RATEINC").ToString() <> "N/A") Then
                            dr("PERCENT_YR_MINUS1_RATEINC") = dr("PERCENT_YR_MINUS1_RATEINC").ToString() & "%"
                        End If
                        If (dr("PERCENT_YR_MINUS2_RATEINC").ToString() <> "N/A") Then
                            dr("PERCENT_YR_MINUS2_RATEINC") = dr("PERCENT_YR_MINUS2_RATEINC").ToString() & "%"
                        End If

                        If (dr("PERCENT_YR_MINUS3_RATEINC").ToString() <> "N/A") Then
                            dr("PERCENT_YR_MINUS3_RATEINC") = dr("PERCENT_YR_MINUS3_RATEINC").ToString() & "%"
                        End If

                        If (dr("PERCENT_YR_MINUS4_RATEINC").ToString() <> "N/A") Then
                            dr("PERCENT_YR_MINUS4_RATEINC") = dr("PERCENT_YR_MINUS4_RATEINC").ToString() & "%"
                        End If

                        If (dr("PERCENT_YR_MINUS5_RATEINC").ToString() <> "N/A") Then
                            dr("PERCENT_YR_MINUS5_RATEINC") = dr("PERCENT_YR_MINUS5_RATEINC").ToString() & "%"
                        End If
                        If (dr("LAST_MINUS1_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS1_LOSSRATIO") = dr("LAST_MINUS1_LOSSRATIO").ToString() & "%"
                        End If

                        If (dr("LAST_MINUS2_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS2_LOSSRATIO") = dr("LAST_MINUS2_LOSSRATIO").ToString() & "%"
                        End If

                        If (dr("LAST_MINUS3_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS3_LOSSRATIO") = dr("LAST_MINUS3_LOSSRATIO").ToString() & "%"
                        End If
                        If (dr("LAST_MINUS4_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS4_LOSSRATIO") = dr("LAST_MINUS4_LOSSRATIO").ToString() & "%"
                        End If
                        If (dr("LAST_MINUS5_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS5_LOSSRATIO") = dr("LAST_MINUS5_LOSSRATIO").ToString() & "%"
                        End If

                    Next
                End If

                gridRateChanges.DataSource = rateChangesDs
                gridRateChanges.DataBind()

            End If

        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try

    End Sub

    Public Function GetSortableData(ByVal Expression As String) As DataView
        Dim rateChangesDs As New DataSet
        Dim SortableView As DataView
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        Try
            rateChangesDs = rateChangesBL.GetRateChangesData()

            SortableView = rateChangesDs.Tables(0).DefaultView
            If (Not String.IsNullOrEmpty(Expression)) Then
                If (SortDirection.ToUpper() = "ASC") Then
                    SortDirection = "DESC"
                Else
                    SortDirection = "ASC"
                End If
                SortableView.Sort = Expression & " " & SortDirection
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return SortableView
    End Function

    Private Sub gridRateChanges_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gridRateChanges.RowEditing

    End Sub
    Protected Sub GridView1_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gridRateChanges.Sorting
        'GetDataOnNavigation()
        Dim SortableView As DataView
        Dim rateChangesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        Try

            Dim strFromDate As String = ""
            Dim strToDate As String = ""
            strFromDate = txtFromDate.Text
            strToDate = txtToDate.Text
            If (txtFromDate.Text = "") Then
                strFromDate = "All"
            End If
            If (txtToDate.Text = "") Then
                strToDate = "All"
            End If

            If mode = "ChartView" Then
                rateChangesDs = rateChangesBL.GetRateChangesByRateChangeIDs(RateChangeId)
            Else
                rateChangesDs = rateChangesBL.GetRateChangeDataByIDTypeDate(drpListCompany.SelectedValue, drpListPolicyType.SelectedItem.ToString(), strFromDate, strToDate)
            End If


            If (rateChangesDs.Tables.Count > 0) Then
                Dim dr As DataRow
                Dim dt As DataTable = rateChangesDs.Tables(0)
                If Not (gridRateChanges Is Nothing) Then


                    For Each dr In dt.Rows
                        If Not IsDBNull(dr("DATEOF_YR_MINUS1_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                        End If

                        If Not IsDBNull(dr("DATEOF_YR_MINUS2_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                        End If

                        If Not IsDBNull(dr("DATEOF_YR_MINUS3_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                        End If

                        If Not IsDBNull(dr("DATEOF_YR_MINUS4_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                        End If


                        If Not IsDBNull(dr("DATEOF_YR_MINUS5_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS5_RATEINC") = "N/A"
                            End If
                        End If
                        If (dr("POLICY_APPROVAL").ToString() = "01/01/0001") Then
                            dr("POLICY_APPROVAL") = "N/A"
                        End If
                        If (dr("PERCENT_CURRENT_RATEINC") <> "N/A") Then
                            Dim strPercentage As String = dr("PERCENT_CURRENT_RATEINC").ToString() & "%"
                            dr("PERCENT_CURRENT_RATEINC") = strPercentage.ToString()
                        End If
                        If (dr("PERCENT_YR_MINUS1_RATEINC") <> "N/A") Then
                            dr("PERCENT_YR_MINUS1_RATEINC") = dr("PERCENT_YR_MINUS1_RATEINC").ToString() & "%"
                        End If
                        If (dr("PERCENT_YR_MINUS2_RATEINC").ToString() <> "N/A") Then
                            dr("PERCENT_YR_MINUS2_RATEINC") = dr("PERCENT_YR_MINUS2_RATEINC").ToString() & "%"
                        End If

                        If (dr("PERCENT_YR_MINUS3_RATEINC").ToString() <> "N/A") Then
                            dr("PERCENT_YR_MINUS3_RATEINC") = dr("PERCENT_YR_MINUS3_RATEINC").ToString() & "%"
                        End If

                        If (dr("PERCENT_YR_MINUS4_RATEINC").ToString() <> "N/A") Then
                            dr("PERCENT_YR_MINUS4_RATEINC") = dr("PERCENT_YR_MINUS4_RATEINC").ToString() & "%"
                        End If

                        If (dr("PERCENT_YR_MINUS5_RATEINC").ToString() <> "N/A") Then
                            dr("PERCENT_YR_MINUS5_RATEINC") = dr("PERCENT_YR_MINUS5_RATEINC").ToString() & "%"
                        End If
                        If (dr("LAST_MINUS1_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS1_LOSSRATIO") = dr("LAST_MINUS1_LOSSRATIO").ToString() & "%"
                        End If

                        If (dr("LAST_MINUS2_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS2_LOSSRATIO") = dr("LAST_MINUS2_LOSSRATIO").ToString() & "%"
                        End If

                        If (dr("LAST_MINUS3_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS3_LOSSRATIO") = dr("LAST_MINUS3_LOSSRATIO").ToString() & "%"
                        End If
                        If (dr("LAST_MINUS4_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS4_LOSSRATIO") = dr("LAST_MINUS4_LOSSRATIO").ToString() & "%"
                        End If
                        If (dr("LAST_MINUS5_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS5_LOSSRATIO") = dr("LAST_MINUS5_LOSSRATIO").ToString() & "%"
                        End If

                    Next
                End If
                SortableView = rateChangesDs.Tables(0).DefaultView
                'Dim sortExpression As String = "PERCENT_CURRENT_RATEINC"
                If (Not String.IsNullOrEmpty(e.SortExpression)) Then
                    If (SortDirection.ToUpper() = "ASC") Then
                        SortDirection = "desc"
                        SortExpressionGrid = e.SortExpression
                    Else
                        SortDirection = "asc"
                        SortExpressionGrid = e.SortExpression
                    End If
                    SortableView.Sort = e.SortExpression & " " & SortDirection
                    gridRateChanges.DataSource = SortableView
                    gridRateChanges.DataBind()
                End If

            End If




        Catch ex As Exception
            ' rateChangesBL.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="strCompID"></param>
    ''' <remarks></remarks>
    Private Sub PopulateGridViewByCompID(ByVal strCompID As String)
        Dim rateChangesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        rateChangesDs = rateChangesBL.GetRateChangesByCompID(strCompID)
        gridRateChanges.DataSource = rateChangesDs
        Dim tbl As DataTable = rateChangesDs.Tables(0)

        Dim dr As DataRow
        If Not (tbl Is Nothing) Then
            For Each dr In tbl.Rows
                If Not IsDBNull(dr("DATEOF_YR_MINUS1_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS2_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS3_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS4_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                End If


                If Not IsDBNull(dr("DATEOF_YR_MINUS5_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS5_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS5_RATEINC") = "N/A"
                End If
                If (dr("POLICY_APPROVAL").ToString() = "01/01/0001") Then
                    dr("POLICY_APPROVAL") = "N/A"
                End If
                If (dr("PERCENT_CURRENT_RATEINC").ToString() <> "N/A") Then
                    Dim strPercentage As String = dr("PERCENT_CURRENT_RATEINC").ToString() & "%"
                    dr("PERCENT_CURRENT_RATEINC") = strPercentage.ToString()
                End If
                If (dr("PERCENT_YR_MINUS1_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS1_RATEINC") = dr("PERCENT_YR_MINUS1_RATEINC").ToString() & "%"
                End If
                If (dr("PERCENT_YR_MINUS2_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS2_RATEINC") = dr("PERCENT_YR_MINUS2_RATEINC").ToString() & "%"
                End If

                If (dr("PERCENT_YR_MINUS3_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS3_RATEINC") = dr("PERCENT_YR_MINUS3_RATEINC").ToString() & "%"
                End If

                If (dr("PERCENT_YR_MINUS4_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS4_RATEINC") = dr("PERCENT_YR_MINUS4_RATEINC").ToString() & "%"
                End If

                If (dr("PERCENT_YR_MINUS5_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS5_RATEINC") = dr("PERCENT_YR_MINUS5_RATEINC").ToString() & "%"
                End If
                If (dr("LAST_MINUS1_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS1_LOSSRATIO") = dr("LAST_MINUS1_LOSSRATIO").ToString() & "%"
                End If

                If (dr("LAST_MINUS2_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS2_LOSSRATIO") = dr("LAST_MINUS2_LOSSRATIO").ToString() & "%"
                End If

                If (dr("LAST_MINUS3_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS3_LOSSRATIO") = dr("LAST_MINUS3_LOSSRATIO").ToString() & "%"
                End If
                If (dr("LAST_MINUS4_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS4_LOSSRATIO") = dr("LAST_MINUS4_LOSSRATIO").ToString() & "%"
                End If
                If (dr("LAST_MINUS5_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS5_LOSSRATIO") = dr("LAST_MINUS5_LOSSRATIO").ToString() & "%"
                End If
            Next
        End If
        gridRateChanges.Columns(9).HeaderText = (DateTime.Now.Year - 1).ToString()
        gridRateChanges.Columns(10).HeaderText = (DateTime.Now.Year - 2).ToString()
        gridRateChanges.Columns(11).HeaderText = (DateTime.Now.Year - 3).ToString()
        gridRateChanges.Columns(12).HeaderText = (DateTime.Now.Year - 4).ToString()
        gridRateChanges.Columns(13).HeaderText = (DateTime.Now.Year - 5).ToString()


        gridRateChanges.Columns(16).HeaderText = DateTime.Now.Year - 1
        gridRateChanges.Columns(17).HeaderText = DateTime.Now.Year - 2
        gridRateChanges.Columns(18).HeaderText = DateTime.Now.Year - 3
        gridRateChanges.Columns(19).HeaderText = DateTime.Now.Year - 4
        gridRateChanges.Columns(20).HeaderText = DateTime.Now.Year - 5


        gridRateChanges.Columns(23).HeaderText = DateTime.Now.Year - 1
        gridRateChanges.Columns(24).HeaderText = DateTime.Now.Year - 2
        gridRateChanges.Columns(25).HeaderText = DateTime.Now.Year - 3
        gridRateChanges.Columns(26).HeaderText = DateTime.Now.Year - 4
        gridRateChanges.Columns(27).HeaderText = DateTime.Now.Year - 5

        gridRateChanges.Columns(30).HeaderText = DateTime.Now.Year - 1
        gridRateChanges.Columns(31).HeaderText = DateTime.Now.Year - 2
        gridRateChanges.Columns(32).HeaderText = DateTime.Now.Year - 3
        gridRateChanges.Columns(33).HeaderText = DateTime.Now.Year - 4
        gridRateChanges.Columns(34).HeaderText = DateTime.Now.Year - 5
        gridRateChanges.DataSource = rateChangesDs
        gridRateChanges.DataBind()
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="strRateChangeIds"></param>
    ''' <remarks></remarks>
    Private Sub PopulateGridViewByRateChangeIDs(ByVal strRateChangeIds As String)
        Dim rateChangesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        rateChangesDs = rateChangesBL.GetRateChangesByRateChangeIDs(strRateChangeIds)
        Dim tbl As DataTable = rateChangesDs.Tables(0).DefaultView.Table

        Dim dr As DataRow
        If Not (tbl Is Nothing) Then
            For Each dr In tbl.Rows
                If Not IsDBNull(dr("DATEOF_YR_MINUS1_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS2_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS3_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS4_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                End If


                If Not IsDBNull(dr("DATEOF_YR_MINUS5_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS5_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS5_RATEINC") = "N/A"
                End If
                If (dr("POLICY_APPROVAL").ToString() = "01/01/0001") Then
                    dr("POLICY_APPROVAL") = "N/A"
                End If
                If (dr("PERCENT_CURRENT_RATEINC").ToString() <> "N/A") Then
                    Dim strPercentage As String = dr("PERCENT_CURRENT_RATEINC").ToString() & "%"
                    dr("PERCENT_CURRENT_RATEINC") = strPercentage.ToString()
                End If
                If (dr("PERCENT_YR_MINUS1_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS1_RATEINC") = dr("PERCENT_YR_MINUS1_RATEINC").ToString() & "%"
                End If
                If (dr("PERCENT_YR_MINUS2_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS2_RATEINC") = dr("PERCENT_YR_MINUS2_RATEINC").ToString() & "%"
                End If

                If (dr("PERCENT_YR_MINUS3_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS3_RATEINC") = dr("PERCENT_YR_MINUS3_RATEINC").ToString() & "%"
                End If

                If (dr("PERCENT_YR_MINUS4_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS4_RATEINC") = dr("PERCENT_YR_MINUS4_RATEINC").ToString() & "%"
                End If

                If (dr("PERCENT_YR_MINUS5_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS5_RATEINC") = dr("PERCENT_YR_MINUS5_RATEINC").ToString() & "%"
                End If
                If (dr("LAST_MINUS1_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS1_LOSSRATIO") = dr("LAST_MINUS1_LOSSRATIO").ToString() & "%"
                End If

                If (dr("LAST_MINUS2_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS2_LOSSRATIO") = dr("LAST_MINUS2_LOSSRATIO").ToString() & "%"
                End If

                If (dr("LAST_MINUS3_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS3_LOSSRATIO") = dr("LAST_MINUS3_LOSSRATIO").ToString() & "%"
                End If
                If (dr("LAST_MINUS4_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS4_LOSSRATIO") = dr("LAST_MINUS4_LOSSRATIO").ToString() & "%"
                End If
                If (dr("LAST_MINUS5_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS5_LOSSRATIO") = dr("LAST_MINUS5_LOSSRATIO").ToString() & "%"
                End If
            Next
        End If
        gridRateChanges.Columns(9).HeaderText = (DateTime.Now.Year - 1).ToString()
        gridRateChanges.Columns(10).HeaderText = (DateTime.Now.Year - 2).ToString()
        gridRateChanges.Columns(11).HeaderText = (DateTime.Now.Year - 3).ToString()
        gridRateChanges.Columns(12).HeaderText = (DateTime.Now.Year - 4).ToString()
        gridRateChanges.Columns(13).HeaderText = (DateTime.Now.Year - 5).ToString()


        gridRateChanges.Columns(16).HeaderText = DateTime.Now.Year - 1
        gridRateChanges.Columns(17).HeaderText = DateTime.Now.Year - 2
        gridRateChanges.Columns(18).HeaderText = DateTime.Now.Year - 3
        gridRateChanges.Columns(19).HeaderText = DateTime.Now.Year - 4
        gridRateChanges.Columns(20).HeaderText = DateTime.Now.Year - 5


        gridRateChanges.Columns(23).HeaderText = DateTime.Now.Year - 1
        gridRateChanges.Columns(24).HeaderText = DateTime.Now.Year - 2
        gridRateChanges.Columns(25).HeaderText = DateTime.Now.Year - 3
        gridRateChanges.Columns(26).HeaderText = DateTime.Now.Year - 4
        gridRateChanges.Columns(27).HeaderText = DateTime.Now.Year - 5

        gridRateChanges.Columns(30).HeaderText = DateTime.Now.Year - 1
        gridRateChanges.Columns(31).HeaderText = DateTime.Now.Year - 2
        gridRateChanges.Columns(32).HeaderText = DateTime.Now.Year - 3
        gridRateChanges.Columns(33).HeaderText = DateTime.Now.Year - 4
        gridRateChanges.Columns(34).HeaderText = DateTime.Now.Year - 5
        gridRateChanges.DataSource = rateChangesDs
        gridRateChanges.DataBind()
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateCompanyDropDownList()
        Dim companyDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        companyDs = rateChangesBL.GetCompanyDetails()
        drpListCompany.DataTextField = "company_name"
        drpListCompany.DataValueField = "company_id"
        drpListCompany.DataSource = companyDs
        drpListCompany.DataBind()
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulatePolicyDropDownList()
        Dim populateDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        populateDs = rateChangesBL.GetPolicyType()
        drpListPolicyType.DataValueField = "CATAGORY_ID"
        drpListPolicyType.DataTextField = "CATAGORY_NAME"
        drpListPolicyType.DataSource = populateDs
        drpListPolicyType.DataBind()
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnError(ByVal args As EventArgs)
        RateChangesBL.Admin.WriteLog(Server.GetLastError())
        Response.Redirect(ERROR_PAGE)
    End Sub

#End Region


    Private Sub ExportToExcel()
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", "RateReview.xls"))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)

        info.Titles.Clear()
        info.StartColumns.Clear()
        info.MergedColumns.Clear()

        gridRateChanges.Columns(3).Visible = True
        gridRateChanges.Columns(4).Visible = True
        gridRateChanges.Columns(5).Visible = True
        gridRateChanges.Columns(6).Visible = True
        gridRateChanges.Columns(7).Visible = True
        gridRateChanges.Columns(8).Visible = True
        gridRateChanges.Columns(9).Visible = True
        gridRateChanges.Columns(10).Visible = True
        gridRateChanges.Columns(11).Visible = True
        gridRateChanges.Columns(12).Visible = True
        gridRateChanges.Columns(13).Visible = True
        gridRateChanges.Columns(14).Visible = True
        gridRateChanges.Columns(15).Visible = True
        gridRateChanges.Columns(16).Visible = True
        gridRateChanges.Columns(17).Visible = True
        gridRateChanges.Columns(18).Visible = True
        gridRateChanges.Columns(19).Visible = True

        gridRateChanges.Columns(20).Visible = True
        gridRateChanges.Columns(21).Visible = True
        gridRateChanges.Columns(22).Visible = True
        gridRateChanges.Columns(23).Visible = True
        gridRateChanges.Columns(24).Visible = True

        gridRateChanges.Columns(25).Visible = True
        gridRateChanges.Columns(26).Visible = True
        gridRateChanges.Columns(27).Visible = True
        gridRateChanges.Columns(28).Visible = True
        gridRateChanges.Columns(29).Visible = True
        gridRateChanges.Columns(30).Visible = True
        gridRateChanges.Columns(31).Visible = True
        gridRateChanges.Columns(32).Visible = True
        gridRateChanges.Columns(33).Visible = True
        gridRateChanges.Columns(34).Visible = True
        gridRateChanges.Columns(35).Visible = True
        gridRateChanges.Columns(36).Visible = True

        info.AddMergedColumns(New Integer() {9, 10, 11, 12, 13}, "Date of Last Five(5) Years Increases")
        info.AddMergedColumns(New Integer() {16, 17, 18, 19, 20}, "Percentage of Last Five(5) Years Increases")

        info.AddMergedColumns(New Integer() {23, 24, 25, 26, 27}, "Last Five(5) Years Loss Ratio")

        info.AddMergedColumns(New Integer() {30, 31, 32, 33, 34}, "No. Of MS Insureds")
        Dim table As Table = New Table
        table.GridLines = gridRateChanges.GridLines
        gridRateChanges.AllowPaging = False
        PopulateGridViewForExport()
        '  add the header row to the table
        If (Not (gridRateChanges.HeaderRow) Is Nothing) Then

            PrepareControlForExport(gridRateChanges.HeaderRow)
            table.Rows.Add(gridRateChanges.HeaderRow)


        End If

        '  add each of the data rows to the table
        For Each row As GridViewRow In gridRateChanges.Rows
            PrepareControlForExport(row)
            row.Cells(0).Width = 0
            row.Cells(1).Width = 0
            row.Cells(2).Width = 0
            row.Cells(3).Width = 0
            row.Cells(14).Width = 0
            row.Cells(15).Width = 0
            row.Cells(21).Width = 0
            row.Cells(22).Width = 0
            row.Cells(28).Width = 0
            row.Cells(29).Width = 0
            table.Rows.Add(row)
        Next

        '  add the footer row to the table
        If (Not (gridRateChanges.FooterRow) Is Nothing) Then
            PrepareControlForExport(gridRateChanges.FooterRow)
            table.Rows.Add(gridRateChanges.FooterRow)
        End If

        '  render the table into the htmlwriter
        table.Attributes("runat") = "server"
        htw.AddAttribute("runat", "server")

        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()

    End Sub


    Sub exportpdf()
        Dim noOfColumns As Integer
        Dim noOfRows As Integer
        ' Dim tbl As DataTable
        Dim rateChangesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()


        Dim strFromDate As String = ""
        Dim strToDate As String = ""
        Dim strSymbol = "%"
        strFromDate = txtFromDate.Text
        strToDate = txtToDate.Text
        If (txtFromDate.Text = "") Then
            strFromDate = "All"
        End If
        If (txtToDate.Text = "") Then
            strToDate = "All"
        End If

        rateChangesDs = rateChangesBL.GetRateChangeDataByIDTypeDate(drpListCompany.SelectedValue, drpListPolicyType.SelectedItem.ToString(), strFromDate, strToDate)

        Dim tbl As DataTable = rateChangesDs.Tables(0).DefaultView.Table

        Dim dr As DataRow
        If Not (tbl Is Nothing) Then
            For Each dr In tbl.Rows

                If Not IsDBNull(dr("DATEOF_YR_MINUS1_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS2_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS3_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS4_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                End If


                If Not IsDBNull(dr("DATEOF_YR_MINUS5_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS5_RATEINC") = "N/A"
                    End If
                End If
                If (dr("POLICY_APPROVAL").ToString() = "01/01/0001") Then
                    dr("POLICY_APPROVAL") = "N/A"
                End If
            Next
        End If


        noOfColumns = tbl.Columns.Count - 3
        noOfRows = tbl.Rows.Count

        Dim HeaderTextSize As Integer = 5
        Dim ReportNameSize As Integer = 10
        Dim ReportTextSize As Integer = 5
        Dim ApplicationNameSize As Integer = 7
        'Creates a PDF document     
        Dim document As Document = Nothing

        document = New Document(PageSize.LETTER.Rotate(), 0, 0, 15, 5)

        ' Creates a PdfPTable with column count of the table equal to no of columns of the gridview or gridview datasource.  
        Dim mainTable As iTextSharp.text.pdf.PdfPTable = New iTextSharp.text.pdf.PdfPTable(noOfColumns)
        ' Sets the first 4 rows of the table as the header rows which will be repeated in all the pages.    
        mainTable.HeaderRows = 5
        ' Creates a PdfPTable with 2 columns to hold the header in the exported PDF.   
        Dim headerTable As iTextSharp.text.pdf.PdfPTable = New iTextSharp.text.pdf.PdfPTable(2)
        'Creates a phrase to hold the application name at the left hand side of the header.  
        Dim phApplicationName As Phrase = New Phrase("", FontFactory.GetFont("Arial", ApplicationNameSize, iTextSharp.text.Font.NORMAL))
        ' Creates a PdfPCell which accepts a phrase as a parameter.   
        Dim clApplicationName As PdfPCell = New PdfPCell(phApplicationName)
        ' Sets the border of the cell to zero.     
        clApplicationName.Border = PdfPCell.NO_BORDER
        ' Sets the Horizontal Alignment of the PdfPCell to left.  
        clApplicationName.HorizontalAlignment = Element.ALIGN_LEFT
        'Creates a phrase to show the current date at the right hand side of the header.   
        Dim phDate As Phrase = New Phrase(DateTime.Now.Date.ToString("MM/dd/yyyy"), FontFactory.GetFont("Arial", ApplicationNameSize, iTextSharp.text.Font.NORMAL))
        ' Creates a PdfPCell which accepts the date phrase as a parameter. 
        Dim clDate As PdfPCell = New PdfPCell(phDate)
        'Sets the Horizontal Alignment of the PdfPCell to right. 
        clDate.HorizontalAlignment = Element.ALIGN_RIGHT
        'Sets the border of the cell to zero.      
        clDate.Border = PdfPCell.NO_BORDER
        ' Adds the cell which holds the application name to the headerTable.    
        headerTable.AddCell(clApplicationName)
        ' Adds the cell which holds the date to the headerTable. 
        headerTable.AddCell(clDate)
        ' Sets the border of the headerTable to zero.   
        headerTable.DefaultCell.Border = PdfPCell.NO_BORDER
        ' Creates a PdfPCell that accepts the headerTable as a parameter and then adds that cell to the main PdfPTable. 
        Dim cellHeader As PdfPCell = New PdfPCell(headerTable)
        cellHeader.Border = PdfPCell.NO_BORDER
        ' Sets the column span of the header cell to noOfColumns.    
        cellHeader.Colspan = noOfColumns
        ' Adds the above header cell to the table.      
        mainTable.AddCell(cellHeader)
        ' Creates a phrase which holds the file name.     
        Dim phHeader As Phrase = New Phrase("Rate Review", FontFactory.GetFont("Arial", ReportNameSize, iTextSharp.text.Font.BOLD))
        Dim clHeader As PdfPCell = New PdfPCell(phHeader)
        clHeader.Colspan = noOfColumns
        clHeader.Border = PdfPCell.NO_BORDER
        clHeader.HorizontalAlignment = Element.ALIGN_CENTER
        mainTable.AddCell(clHeader)
        ' Creates a phrase for a new line.    
        Dim phSpace As Phrase = New Phrase("")
        Dim clSpace As PdfPCell = New PdfPCell(phSpace)
        clSpace.Border = PdfPCell.NO_BORDER
        clSpace.Colspan = noOfColumns
        mainTable.AddCell(clSpace)
        ' Sets the gridview column names as table headers. 

        mainTable.WidthPercentage = 100

        Dim i As Integer
        Dim rowNo As Integer
        Dim columnNo As Integer
        Dim PdfPCell1 As PdfPCell = Nothing

        For i = 0 To noOfColumns - 1

            Select Case i

                Case 0
                    PdfPCell1 = New PdfPCell(New Phrase(New Chunk("Company ", FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
                    PdfPCell1.Rowspan = 2

                Case 1
                    PdfPCell1 = New PdfPCell(New Phrase(New Chunk("Policy Form No", FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
                    PdfPCell1.Rowspan = 2
                Case 2
                    PdfPCell1 = New PdfPCell(New Phrase(New Chunk("Type Of Insurance", FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
                    PdfPCell1.Rowspan = 2
                Case 3
                    PdfPCell1 = New PdfPCell(New Phrase(New Chunk("Date Of Current Rate Increase", FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
                    PdfPCell1.Rowspan = 2
                Case 4
                    PdfPCell1 = New PdfPCell(New Phrase(New Chunk("Percentage Of Current Rate Increase", FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
                    PdfPCell1.Rowspan = 2
                Case 5
                    PdfPCell1 = New PdfPCell(New Phrase(New Chunk("Date of Last Five(5) Years Increases", FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
                    PdfPCell1.Colspan = 5
                    PdfPCell1.HorizontalAlignment = 1
                    i = i + 4
                Case 10
                    PdfPCell1 = New PdfPCell(New Phrase(New Chunk("Percentage of Last Five(5) Years Increases", FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
                    PdfPCell1.Colspan = 5
                    PdfPCell1.HorizontalAlignment = 1
                    i = i + 4
                Case 15
                    PdfPCell1 = New PdfPCell(New Phrase(New Chunk("Last Five(5) Years Loss Ratio", FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
                    PdfPCell1.Colspan = 5
                    PdfPCell1.HorizontalAlignment = 1
                    i = i + 4
                Case 20
                    PdfPCell1 = New PdfPCell(New Phrase(New Chunk("No. Of MS Insureds", FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
                    PdfPCell1.Colspan = 5
                    PdfPCell1.HorizontalAlignment = 1
                    i = i + 4
                Case 25
                    PdfPCell1 = New PdfPCell(New Phrase(New Chunk("Policy Approval", FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
                    PdfPCell1.Rowspan = 2

            End Select
            mainTable.AddCell(PdfPCell1)
        Next i       ' Reads the gridview rows and adds them to the mainTable    

        mainTable.CompleteRow()

        For i = 1 To 4

            PdfPCell1 = New PdfPCell(New Phrase(New Chunk(Today.Year - 1, FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
            mainTable.AddCell(PdfPCell1)
            PdfPCell1 = New PdfPCell(New Phrase(New Chunk(Today.Year - 2, FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
            mainTable.AddCell(PdfPCell1)
            PdfPCell1 = New PdfPCell(New Phrase(New Chunk(Today.Year - 3, FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
            mainTable.AddCell(PdfPCell1)
            PdfPCell1 = New PdfPCell(New Phrase(New Chunk(Today.Year - 4, FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
            mainTable.AddCell(PdfPCell1)
            PdfPCell1 = New PdfPCell(New Phrase(New Chunk(Today.Year - 5, FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD))))
            mainTable.AddCell(PdfPCell1)
        Next i
        mainTable.CompleteRow()

        For rowNo = 0 To noOfRows - 1

            For columnNo = 2 To tbl.Columns.Count - 1

                Dim s As String = tbl.Rows(rowNo)(columnNo).ToString()
                PdfPCell1 = New PdfPCell(New Phrase(New Chunk(s, FontFactory.GetFont("Arial", ReportTextSize, iTextSharp.text.Font.NORMAL))))
                mainTable.AddCell(PdfPCell1)
                If columnNo = 26 Then
                    columnNo = columnNo + 1
                End If
            Next columnNo
        Next rowNo
        'Tells the mainTable to complete the row even if any cell is left incomplete.    
        mainTable.CompleteRow()

        ' Gets the instance of the document created and writes it to the output stream of the Response object.  
        PdfWriter.GetInstance(document, Response.OutputStream)
        ' Creates a footer for the PDF document.   
        'Dim pdfFooter As HeaderFooter = New HeaderFooter()
        'pdfFooter.Alignment = Element.ALIGN_CENTER
        'pdfFooter.Border = iTextSharp.text.Rectangle.NO_BORDER
        ' Sets the document footer to pdfFooter.  
        ' document.Footer = pdfFooter;        
        ' Opens the document.       
        document.Open()

        ' Adds the mainTable to the document.      
        document.Add(mainTable)
        ' Closes the document.       
        document.Close()
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment; filename= RateReview.pdf")
        HttpContext.Current.ApplicationInstance.CompleteRequest()
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.ClearContent()


    End Sub
    Private Sub ExpportToXML()

        Dim rateChangesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()


        Dim strFromDate As String = ""
        Dim strToDate As String = ""
        Dim strSymbol = "%"
        strFromDate = txtFromDate.Text
        strToDate = txtToDate.Text
        If (txtFromDate.Text = "") Then
            strFromDate = "All"
        End If
        If (txtToDate.Text = "") Then
            strToDate = "All"
        End If

        rateChangesDs = rateChangesBL.GetRateChangeDataByIDTypeDate(drpListCompany.SelectedValue, drpListPolicyType.SelectedItem.ToString(), strFromDate, strToDate)
        gridRateChanges.DataSource = rateChangesDs


        Dim tbl As DataTable = rateChangesDs.Tables(0).DefaultView.Table

        Dim dr As DataRow
        If Not (rateChangesDs Is Nothing) Then
            For Each dr In rateChangesDs.Tables(0).Rows
                If Not IsDBNull(dr("DATEOF_YR_MINUS1_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS2_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS3_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS4_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                End If


                If Not IsDBNull(dr("DATEOF_YR_MINUS5_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS5_RATEINC") = "N/A"
                    End If
                End If
                If (dr("POLICY_APPROVAL").ToString() = "01/01/0001") Then
                    dr("POLICY_APPROVAL") = "N/A"
                End If
            Next
        End If
        rateChangesDs.Tables(0).Columns.Remove("COMPANY_ID")
        rateChangesDs.Tables(0).Columns.Remove("RATECHANGES_ID")
        rateChangesDs.Tables(0).Columns.Remove("SUMMARY_RATEINC")

        rateChangesDs.Tables(0).Columns(0).ColumnName = "Company"
        rateChangesDs.Tables(0).Columns(1).ColumnName = "Policy_Form_No"
        rateChangesDs.Tables(0).Columns(2).ColumnName = "Type_Of_Insurance"
        rateChangesDs.Tables(0).Columns(3).ColumnName = "Date_Of_Current_Rate_Increase"
        rateChangesDs.Tables(0).Columns(4).ColumnName = "Percentage_Of_Current_Rate_Increase"
        rateChangesDs.Tables(0).Columns(5).ColumnName = "Date_Of_" & Today.Year - 1 & "_Rate_Increase"
        rateChangesDs.Tables(0).Columns(6).ColumnName = "Date_Of_" & Today.Year - 2 & "_Rate_Increase"
        rateChangesDs.Tables(0).Columns(7).ColumnName = "Date_Of_" & Today.Year - 3 & "_Rate_Increase"
        rateChangesDs.Tables(0).Columns(8).ColumnName = "Date_Of_" & Today.Year - 4 & "_Rate_Increase"
        rateChangesDs.Tables(0).Columns(9).ColumnName = "Date_Of_" & Today.Year - 5 & "_Rate_Increase"
        rateChangesDs.Tables(0).Columns(10).ColumnName = "Percentage_Of_" & Today.Year - 1 & "_Rate_Increase"
        rateChangesDs.Tables(0).Columns(11).ColumnName = "Percentage_Of_" & Today.Year - 2 & "_Rate_Increase"
        rateChangesDs.Tables(0).Columns(12).ColumnName = "Percentage_Of_" & Today.Year - 3 & "_Rate_Increase"
        rateChangesDs.Tables(0).Columns(13).ColumnName = "Percentage_Of_" & Today.Year - 4 & "_Rate_Increase"
        rateChangesDs.Tables(0).Columns(14).ColumnName = "Percentage_Of_" & Today.Year - 5 & "_Rate_Increase"
        rateChangesDs.Tables(0).Columns(15).ColumnName = "Loss_ratio_" & Today.Year - 1
        rateChangesDs.Tables(0).Columns(16).ColumnName = "Loss_ratio_" & Today.Year - 2
        rateChangesDs.Tables(0).Columns(17).ColumnName = "Loss_ratio_" & Today.Year - 3
        rateChangesDs.Tables(0).Columns(18).ColumnName = "Loss_ratio_" & Today.Year - 4
        rateChangesDs.Tables(0).Columns(19).ColumnName = "Loss_ratio_" & Today.Year - 5
        rateChangesDs.Tables(0).Columns(20).ColumnName = "No_Of_Mississippi_Insureds_" & Today.Year - 1
        rateChangesDs.Tables(0).Columns(21).ColumnName = "No_Of_Mississippi_Insureds_" & Today.Year - 2
        rateChangesDs.Tables(0).Columns(22).ColumnName = "No_Of_Mississippi_Insureds_" & Today.Year - 3
        rateChangesDs.Tables(0).Columns(23).ColumnName = "No_Of_Mississippi_Insureds_" & Today.Year - 4
        rateChangesDs.Tables(0).Columns(24).ColumnName = "No_Of_Mississippi_Insureds_" & Today.Year - 5
        rateChangesDs.Tables(0).Columns(25).ColumnName = "Policy_Approval"
        Dim l As Literal = New Literal()
        l.Text = rateChangesDs.GetXml()

        'Preparing to xml the file
        Response.Clear()

        Response.AddHeader("content-disposition", "attachment;filename=FileName.xml")
        HttpContext.Current.Response.AppendHeader("Content-Length", l.Text.Length.ToString())
        Response.Charset = ""

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Response.ContentType = "application/RateReview.xml"

        Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter()

        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)

        l.RenderControl(htmlWrite)


        Response.Write(stringWrite.ToString())
        HttpContext.Current.ApplicationInstance.CompleteRequest()
        Response.Flush()


    End Sub

    Private Sub ExportToCSV()
        HttpContext.Current.Response.Clear()
        Dim FileName As String = "RateReview.csv"

        HttpContext.Current.Response.ContentType = "text/csv"


        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + FileName)
        Dim objSw As StringWriter = New StringWriter
        Dim sb As New StringBuilder()
        'Dim objSw As StreamWriter = New StreamWriter(Server.MapPath("~/demo.csv"))
        Dim rateChangesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()


        Dim strFromDate As String = ""
        Dim strToDate As String = ""
        Dim strSymbol = "%"
        strFromDate = txtFromDate.Text
        strToDate = txtToDate.Text
        If (txtFromDate.Text = "") Then
            strFromDate = "All"
        End If
        If (txtToDate.Text = "") Then
            strToDate = "All"
        End If

            rateChangesDs = rateChangesBL.GetRateChangeDataByIDTypeDate(drpListCompany.SelectedValue, drpListPolicyType.SelectedItem.ToString(), strFromDate, strToDate)


        gridRateChanges.DataSource = rateChangesDs
        ' rateChangesDs = CType(gridRateChanges.DataSource, DataSet)
        Dim objdt As DataTable = rateChangesDs.Tables(0).DefaultView.Table

        Dim dr As DataRow
        If Not (objdt Is Nothing) Then
            For Each dr In objdt.Rows
                If Not IsDBNull(dr("DATEOF_YR_MINUS1_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS2_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS3_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS4_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                    End If
                Else
                    dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                End If


                If Not IsDBNull(dr("DATEOF_YR_MINUS5_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/2001") Then
                        dr("DATEOF_YR_MINUS5_RATEINC") = "N/A"
                    End If
                End If

                If (dr("POLICY_APPROVAL").ToString() = "01/01/0001") Then
                    dr("POLICY_APPROVAL") = "N/A"
                End If
                If (dr("PERCENT_CURRENT_RATEINC") <> "N/A") Then
                    Dim strPercentage As String = dr("PERCENT_CURRENT_RATEINC").ToString() & "%"
                    dr("PERCENT_CURRENT_RATEINC") = strPercentage.ToString()
                End If
                If (dr("PERCENT_YR_MINUS1_RATEINC") <> "N/A") Then
                    dr("PERCENT_YR_MINUS1_RATEINC") = dr("PERCENT_YR_MINUS1_RATEINC").ToString() & "%"
                End If
                If (dr("PERCENT_YR_MINUS2_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS2_RATEINC") = dr("PERCENT_YR_MINUS1_RATEINC").ToString() & "%"
                End If

                If (dr("PERCENT_YR_MINUS3_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS3_RATEINC") = dr("PERCENT_YR_MINUS1_RATEINC").ToString() & "%"
                End If

                If (dr("PERCENT_YR_MINUS4_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS4_RATEINC") = dr("PERCENT_YR_MINUS1_RATEINC").ToString() & "%"
                End If

                If (dr("PERCENT_YR_MINUS5_RATEINC").ToString() <> "N/A") Then
                    dr("PERCENT_YR_MINUS5_RATEINC") = dr("PERCENT_YR_MINUS1_RATEINC").ToString() & "%"
                End If
                If (dr("LAST_MINUS1_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS1_LOSSRATIO") = dr("LAST_MINUS1_LOSSRATIO").ToString() & "%"
                End If

                If (dr("LAST_MINUS2_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS2_LOSSRATIO") = dr("LAST_MINUS2_LOSSRATIO").ToString() & "%"
                End If

                If (dr("LAST_MINUS3_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS3_LOSSRATIO") = dr("LAST_MINUS3_LOSSRATIO").ToString() & "%"
                End If
                If (dr("LAST_MINUS4_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS4_LOSSRATIO") = dr("LAST_MINUS4_LOSSRATIO").ToString() & "%"
                End If
                If (dr("LAST_MINUS5_LOSSRATIO").ToString() <> "N/A") Then
                    dr("LAST_MINUS5_LOSSRATIO") = dr("LAST_MINUS5_LOSSRATIO").ToString() & "%"
                End If
                dr("POLICY_FORMNO") = dr("POLICY_FORMNO").ToString().Trim()
                dr("POLICY_TYPE") = dr("POLICY_TYPE").ToString().Trim()


            Next
        End If
        rateChangesDs.Tables(0).Columns.Remove("COMPANY_ID")
        rateChangesDs.Tables(0).Columns.Remove("RATECHANGES_ID")
        rateChangesDs.Tables(0).Columns.Remove("SUMMARY_RATEINC")

        rateChangesDs.Tables(0).Columns(0).ColumnName = "Company"
        rateChangesDs.Tables(0).Columns(1).ColumnName = "Policy Form No "
        rateChangesDs.Tables(0).Columns(2).ColumnName = "Type Of Insurance "
        rateChangesDs.Tables(0).Columns(3).ColumnName = "Date Of Current Rate Increase "
        rateChangesDs.Tables(0).Columns(4).ColumnName = "Percentage Of Current Rate Increase "
        rateChangesDs.Tables(0).Columns(5).ColumnName = "Date Of" & Today.Year - 1 & "Rate Increase "
        rateChangesDs.Tables(0).Columns(6).ColumnName = "Date Of" & Today.Year - 2 & "Rate Increase "
        rateChangesDs.Tables(0).Columns(7).ColumnName = "Date Of" & Today.Year - 3 & "Rate Increase "
        rateChangesDs.Tables(0).Columns(8).ColumnName = "Date Of" & Today.Year - 4 & "Rate Increase "
        rateChangesDs.Tables(0).Columns(9).ColumnName = "Date Of" & Today.Year - 5 & "Rate Increase "
        rateChangesDs.Tables(0).Columns(10).ColumnName = "Percentage Of" & Today.Year - 1 & "Rate Increase "
        rateChangesDs.Tables(0).Columns(11).ColumnName = "Percentage Of" & Today.Year - 2 & "Rate Increase "
        rateChangesDs.Tables(0).Columns(12).ColumnName = "Percentage Of" & Today.Year - 3 & "Rate Increase "
        rateChangesDs.Tables(0).Columns(13).ColumnName = "Percentage Of" & Today.Year - 4 & "Rate Increase "
        rateChangesDs.Tables(0).Columns(14).ColumnName = "Percentage Of" & Today.Year - 5 & "Rate Increase "
        rateChangesDs.Tables(0).Columns(15).ColumnName = " Loss ratio" & Today.Year - 1
        rateChangesDs.Tables(0).Columns(16).ColumnName = " Loss ratio" & Today.Year - 2
        rateChangesDs.Tables(0).Columns(17).ColumnName = " Loss ratio" & Today.Year - 3
        rateChangesDs.Tables(0).Columns(18).ColumnName = " Loss ratio" & Today.Year - 4
        rateChangesDs.Tables(0).Columns(19).ColumnName = " Loss ratio" & Today.Year - 5
        rateChangesDs.Tables(0).Columns(20).ColumnName = " No Of Mississippi Insureds" & Today.Year - 1
        rateChangesDs.Tables(0).Columns(21).ColumnName = " No Of Mississippi Insureds" & Today.Year - 2
        rateChangesDs.Tables(0).Columns(22).ColumnName = " No Of Mississippi Insureds" & Today.Year - 3
        rateChangesDs.Tables(0).Columns(23).ColumnName = " No Of Mississippi Insureds" & Today.Year - 4
        rateChangesDs.Tables(0).Columns(24).ColumnName = " No Of Mississippi Insureds" & Today.Year - 5
        rateChangesDs.Tables(0).Columns(25).ColumnName = " PolicyApproval "



        Dim NoOfColumn As Integer = objdt.Columns.Count
        Dim i As Integer = 0

        For i = 0 To NoOfColumn - 1
            sb.Append(objdt.Columns(i).ColumnName)
            If (i < NoOfColumn - 1) Then
                sb.Append(",")
            End If
        Next i
        sb.Remove(sb.Length - 1, 1)
        sb.Append(Environment.NewLine)


        Dim drow As DataRow
        Dim j As Integer
        For Each drow In objdt.Rows

            For j = 0 To NoOfColumn - 1
                sb.Append(drow(j).ToString())
                If (j < NoOfColumn - 1) Then
                    sb.Append(" ,")
                End If
            Next
            sb.Append(Environment.NewLine)

        Next

        HttpContext.Current.Response.AppendHeader("Content-Length", sb.Length.ToString())

        HttpContext.Current.Response.Write(sb.ToString)
        HttpContext.Current.ApplicationInstance.CompleteRequest()



        ''''

        'Dim response As HttpResponse = HttpContext.Current.Response
        Response.Flush()
        'response.Clear()
        'response.AddHeader("content-disposition", String.Format("attachment; filename={0}", FileName))
        'response.ContentType = "text/csv"
        'Dim sb As New StringBuilder()

        'If (Not (gridRateChanges.HeaderRow) Is Nothing) Then
        '    PrepareControlForExport(gridRateChanges.HeaderRow)
        '    sb.Append(gridRateChanges.Columns.ToString + ",")
        'End If
        'sb.Remove(sb.Length - 1, 1)
        'sb.Append(Environment.NewLine)

        'For Each row As GridViewRow In gridRateChanges.Rows
        '    For i As Integer = 0 To gridRateChanges.Columns.Count - 1
        '        sb.Append(gridRateChanges.Columns.ToString + ",")
        '    Next
        '    sb.Append(Environment.NewLine)
        'Next
        'response.AppendHeader("Content-Length", sb.Length.ToString())
        'response.Write(sb.ToString())
        'response.End()
    End Sub
    '//Create CSV file 
    '  StreamWriter  = new StreamWriter(Server.MapPath("~/demo.csv"));
    '  //get table from GridView1
    '  DataTable objDt = ((DataSet)GridView1.DataSource).Tables[0];
    '  //Get No Of Column in GridView
    '  int NoOfColumn = objDt.Columns.Count;
    '  //Create Header
    '  for (int i = 0; i < NoOfColumn; i++)
    '  {
    '      objSw.Write(objDt.Columns[i]);
    '      //check not last column
    '   If (i < NoOfColumn - 1) Then
    '      {
    '          objSw.Write(",");
    '      }
    '  }
    '  objSw.Write(objSw.NewLine);
    '  //Create Data  
    '  foreach (DataRow dr in objDt.Rows)
    '  {
    '      for (int i = 0; i < NoOfColumn; i++)
    '      {
    '          objSw.Write(dr[i].ToString());

    '           If (i < NoOfColumn - 1) Then
    '          {
    '              objSw.Write(",");
    '          }
    '      }
    '      objSw.Write(objSw.NewLine);
    '  }
    '  objSw.Close();
    ' End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="control"></param>
    ''' <remarks></remarks>
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0

        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))

            ElseIf (TypeOf current Is HiddenField) Then
                control.Controls.Remove(current)

            ElseIf (TypeOf current Is System.Web.UI.WebControls.Image) Then
                control.Controls.Remove(current)
            ElseIf (TypeOf current Is Button) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, Button).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))

            End If



            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddNew.Click
        'Response.Redirect("http://vimoacc44/midapp/AddRateChanges.aspx", True)
        If Not (Request.Cookies("userInfo") Is Nothing) Then
            userName = Convert.ToString(Request.Cookies("userInfo")("userName"))

        End If
        Dim viewMode As String = "No"
        Response.Redirect("AddNewRateChanges.aspx?rateid=" & 0 & "&userName=" & userName & "&ViewMode=" & viewMode)
        lblResult.Text = "Record Created Successfully"
    End Sub

    Private Sub gridRateChanges_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridRateChanges.DataBound
        ApplyPaging()
        ' ChangeGridCellColor(sender, e)
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub gridRateChanges_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridRateChanges.Load
       

    End Sub
    Private Sub SortImage(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try

            Dim senderGridView As GridView = DirectCast(sender, GridView)
            Dim space As New Literal()
            space.Text = " "
            If e.Row IsNot Nothing AndAlso e.Row.RowType = DataControlRowType.Header Then
                For i As Integer = 0 To gridRateChanges.Columns.Count - 1
                    Dim cell As TableCell = e.Row.Cells(i)
                    Dim column As DataControlField = gridRateChanges.Columns(i)

                    If cell.HasControls() Then
                        Dim button As LinkButton = TryCast(cell.Controls(0), LinkButton)
                        If button IsNot Nothing Then
                            Dim image As New System.Web.UI.WebControls.Image()
                            Dim direction As String = SortDirection
                            image.ImageUrl = "~/images/aro-blank.gif"
                            If SortDirection = "asc" Then
                                image.CssClass = "aro-up"
                                'image.ImageUrl = "~/images/ico_asc.png"

                            End If
                            If SortDirection = "desc" Then
                                image.CssClass = "aro-down"
                                'image.ImageUrl = "~/images/ico_desc.png"

                            End If
                            cell.Controls.Add(image)
                        End If
                    End If
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridRateChanges_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gridRateChanges.RowCreated
        'call the method for custom rendering of columns's headers 
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.SetRenderMethodDelegate(AddressOf RenderHeader)
        End If


        If e.Row.RowType = DataControlRowType.Pager Then
            gridRateChanges.PagerSettings.Mode = PagerButtons.NumericFirstLast
            gridRateChanges.PagerSettings.FirstPageText = "First"
            gridRateChanges.PagerSettings.LastPageText = "Last"
            gridRateChanges.PagerSettings.NextPageText = "Next"
            gridRateChanges.PagerSettings.PreviousPageText = "Previous"
        End If

        Try

            'Dim senderGridView As GridView = DirectCast(sender, GridView)
            'Dim space As New Literal()
            'space.Text = " "
            'Dim imgAsc As String = " <img src='images/ico_asc.png' title='Ascending' />"
            'Dim imgDes As String = " <img src='images/ico_desc.png' title='Descendng' />"
            'If e.Row IsNot Nothing AndAlso e.Row.RowType = DataControlRowType.Header Then
            '    For i As Integer = 0 To gridRateChanges.Columns.Count - 1
            '        Dim cell As TableCell = e.Row.Cells(i)
            '        Dim column As DataControlField = gridRateChanges.Columns(i)

            '        If cell.HasControls() Then
            '            Dim lbSort As LinkButton = TryCast(cell.Controls(0), LinkButton)
            '            If lbSort IsNot Nothing Then
            '                Dim exp As String = SortExpressionGrid

            '                If lbSort.Text.Trim() = SortExpressionGrid.Replace("_", "").Trim().ToLower() Then

            '                    Dim image As New System.Web.UI.WebControls.Image()
            '                    Dim direction As String = SortDirection
            '                    If SortDirection = "asc" Then
            '                        image.CssClass = "aro-up"
            '                        image.ImageUrl = "~/images/ico_asc.png"

            '                    End If
            '                    If SortDirection = "desc" Then
            '                        image.CssClass = "aro-down"
            '                        image.ImageUrl = "~/images/ico_desc.png"

            '                    End If
            '                    cell.Controls.Add(image)
            '                End If
            '            End If
            '        End If
            '    Next
            'End If
            SortImage(sender, e)

            'Dim senderGridView As GridView = DirectCast(sender, GridView)
            'Dim space As New Literal()
            'space.Text = " "
            'If e.Row IsNot Nothing AndAlso e.Row.RowType = DataControlRowType.Header Then
            '    For i As Integer = 0 To gridRateChanges.Columns.Count - 1
            '        Dim cell As TableCell = e.Row.Cells(i)
            '        Dim column As DataControlField = gridRateChanges.Columns(i)

            '        If cell.HasControls() Then
            '            Dim button As LinkButton = TryCast(cell.Controls(0), LinkButton)
            '            If button IsNot Nothing Then
            '                Dim image As New System.Web.UI.WebControls.Image()
            '                Dim direction As String = SortDirection
            '                If SortDirection = "asc" Then
            '                    image.CssClass = "aro-up"
            '                    image.ImageUrl = "~/images/ico_asc.png"

            '                End If
            '                If SortDirection = "desc" Then
            '                    image.CssClass = "aro-down"
            '                    image.ImageUrl = "~/images/ico_desc.png"

            '                End If
            '                cell.Controls.Add(image)
            '            End If
            '        End If
            '    Next
            'End If

        Catch ex As Exception

        End Try

    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub drpListCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles drpListCompany.SelectedIndexChanged
        '
        btnSearch_Click(sender, e)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub drpListPolicyType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles drpListPolicyType.SelectedIndexChanged

        btnSearch_Click(sender, e)

    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridRateChanges_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridRateChanges.PageIndexChanging

        gridRateChanges.PageIndex = e.NewPageIndex
        btnSearch_Click(sender, e)


    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        ' Dim SortableView As DataView
        Dim rateChangesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        Try

            Dim strFromDate As String = ""
            Dim strToDate As String = ""
            Dim strSymbol = "%"
            strFromDate = txtFromDate.Text
            strToDate = txtToDate.Text
            If (txtFromDate.Text = "") Then
                strFromDate = "All"
            End If
            If (txtToDate.Text = "") Then
                strToDate = "All"
            End If

            If mode = "ChartView" Then
                rateChangesDs = rateChangesBL.GetRateChangesByRateChangeIDs(RateChangeId)
            Else
                rateChangesDs = rateChangesBL.GetRateChangeDataByIDTypeDate(drpListCompany.SelectedValue, drpListPolicyType.SelectedItem.ToString(), strFromDate, strToDate)
            End If


            If (rateChangesDs.Tables.Count > 0) Then
                Dim dr As DataRow
                Dim dt As DataTable = rateChangesDs.Tables(0)
                If Not (gridRateChanges Is Nothing) Then


                    For Each dr In dt.Rows
                        If Not IsDBNull(dr("DATEOF_YR_MINUS1_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                        End If

                        If Not IsDBNull(dr("DATEOF_YR_MINUS2_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                        End If

                        If Not IsDBNull(dr("DATEOF_YR_MINUS3_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                        End If

                        If Not IsDBNull(dr("DATEOF_YR_MINUS4_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                        End If


                        If Not IsDBNull(dr("DATEOF_YR_MINUS5_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS5_RATEINC") = "N/A"
                            End If
                        End If
                        If (dr("POLICY_APPROVAL").ToString() = "01/01/0001") Then
                            dr("POLICY_APPROVAL") = "N/A"
                        End If
                        If (dr("LINk_RATE_INCREASE").ToString() = " ") Then
                            dr("LINk_RATE_INCREASE") = "N/A"
                        End If
                        If (dr("PERCENT_CURRENT_RATEINC").ToString() <> "N/A") Then
                            ' Dim strPercentage As String = dr("PERCENT_CURRENT_RATEINC").ToString() & "%"
                            dr("PERCENT_CURRENT_RATEINC") = dr("PERCENT_CURRENT_RATEINC").ToString() & "%"
                        End If
                        If (dr("PERCENT_YR_MINUS1_RATEINC").ToString() <> "N/A") Then
                            dr("PERCENT_YR_MINUS1_RATEINC") = dr("PERCENT_YR_MINUS1_RATEINC").ToString() & "%"
                        End If
                        If (dr("PERCENT_YR_MINUS2_RATEINC").ToString() <> "N/A") Then
                            dr("PERCENT_YR_MINUS2_RATEINC") = dr("PERCENT_YR_MINUS2_RATEINC").ToString() & "%"
                        End If

                        If (dr("PERCENT_YR_MINUS3_RATEINC").ToString() <> "N/A") Then
                            dr("PERCENT_YR_MINUS3_RATEINC") = dr("PERCENT_YR_MINUS3_RATEINC").ToString() & "%"
                        End If

                        If (dr("PERCENT_YR_MINUS4_RATEINC").ToString() <> "N/A") Then
                            dr("PERCENT_YR_MINUS4_RATEINC") = dr("PERCENT_YR_MINUS4_RATEINC").ToString() & "%"
                        End If

                        If (dr("PERCENT_YR_MINUS5_RATEINC").ToString() <> "N/A") Then
                            dr("PERCENT_YR_MINUS5_RATEINC") = dr("PERCENT_YR_MINUS5_RATEINC").ToString() & "%"
                        End If
                        If (dr("LAST_MINUS1_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS1_LOSSRATIO") = dr("LAST_MINUS1_LOSSRATIO").ToString() & "%"
                        End If

                        If (dr("LAST_MINUS2_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS2_LOSSRATIO") = dr("LAST_MINUS2_LOSSRATIO").ToString() & "%"
                        End If

                        If (dr("LAST_MINUS3_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS3_LOSSRATIO") = dr("LAST_MINUS3_LOSSRATIO").ToString() & "%"
                        End If
                        If (dr("LAST_MINUS4_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS4_LOSSRATIO") = dr("LAST_MINUS4_LOSSRATIO").ToString() & "%"
                        End If
                        If (dr("LAST_MINUS5_LOSSRATIO").ToString() <> "N/A") Then
                            dr("LAST_MINUS5_LOSSRATIO") = dr("LAST_MINUS5_LOSSRATIO").ToString() & "%"
                        End If



                    Next
                End If

                gridRateChanges.DataSource = rateChangesDs
                gridRateChanges.DataBind()


            End If

        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try

    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridRateChanges_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gridRateChanges.RowCommand
        Dim viewMode As String
        If e.CommandName.ToLower() = "edt" Then
            viewMode = "edit"
            Dim lb As LinkButton = CType(e.CommandSource, LinkButton)
            'getting clicked link button      
            Dim strKey As String = CType(lb.Parent.FindControl("hfKey"), HiddenField).Value
            'getting PK Value   
            If Not (Request.Cookies("userInfo") Is Nothing) Then
                userName = Convert.ToString(Request.Cookies("userInfo")("userName"))

            End If
            Dim pageIndex As Integer = gridRateChanges.PageIndex
            Session("PageIndex") = pageIndex.ToString()
            Response.Redirect("AddNewRateChanges.aspx?rateid=" & strKey & "&userName=" & userName & "&ViewMode=" & viewMode, False)
            lblResult.Text = "Record Updated Successfully"
        ElseIf e.CommandName.ToLower() = "dlt" Then
            Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
            Dim lb As LinkButton = CType(e.CommandSource, LinkButton)
            'getting clicked link button      
            Dim intRateChangeKey As Integer = CType(lb.Parent.FindControl("hfKey"), HiddenField).Value
            'getting PK Value   
            rateChangesBL.DeleteRateChanges(intRateChangeKey)
            PopulateGridView()


        ElseIf e.CommandName.ToLower() = "view" Then
            Dim lb As LinkButton = CType(e.CommandSource, LinkButton)
            viewMode = "Yes"
            'getting clicked link button      
            Dim strKey As String = CType(lb.Parent.FindControl("hfKey"), HiddenField).Value
            'getting PK Value   
            If Not (Request.Cookies("userInfo") Is Nothing) Then
                userName = Convert.ToString(Request.Cookies("userInfo")("userName"))

            End If
            searchCriteriaonComp = drpListCompany.SelectedValue.ToString()
            Response.Redirect("ViewRateChanges.aspx?rateid=" & strKey, False)

        End If
    End Sub
    Private Sub ChangeGridCellColor(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        'Dim cell As TableCell
        'Try

        '    If (e.Row.RowType = DataControlRowType.DataRow) Then
        '        For Each cell In e.Row.Cells

        '            If (Convert.ToInt32(e.Row.Cells(2).Text.Trim) < 0) Then
        '                cell.ForeColor = Drawing.Color.Red
        '            End If
        '        Next
        '    End If

        'Catch ex As Exception

        'End Try
    End Sub
    '    public void dgBoundItems(object sender, DataGridItemEventArgs e) 
    '{ 
    '    foreach(TableCell cell in e.Item.Cells) 
    '    { 
    '        ///e.Item.DataSetIndex = row 

    '        ///e.Item.Cells.GetCellIndex(cell) = column 

    '        if (e.Item.DataSetIndex > 0) 
    '        {
    '            ///If the salary is less than 5000 the color must be red. 

    '            if (Convert.ToInt32(cell.Text.Trim()) < 5000) 
    '            { 
    '                cell.ForeColor = Color.Red; 
    '            } 
    '        } 
    '    } 
    '}
    Private Sub GetDataOnNavigation()
        Dim SortableView As DataView
        Dim rateChangesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        Try

            Dim strFromDate As String = ""
            Dim strToDate As String = ""
            strFromDate = txtFromDate.Text
            strToDate = txtToDate.Text
            If (txtFromDate.Text = "") Then
                strFromDate = "All"
            End If
            If (txtToDate.Text = "") Then
                strToDate = "All"
            End If

            If mode = "ChartView" Then
                rateChangesDs = rateChangesBL.GetRateChangesByRateChangeIDs(RateChangeId)
            Else
                rateChangesDs = rateChangesBL.GetRateChangeDataByIDTypeDate(drpListCompany.SelectedValue, drpListPolicyType.SelectedItem.ToString(), strFromDate, strToDate)
            End If


            If (rateChangesDs.Tables.Count > 0) Then
                Dim dr As DataRow
                Dim dt As DataTable = rateChangesDs.Tables(0)
                If Not (gridRateChanges Is Nothing) Then


                    For Each dr In dt.Rows
                        If Not IsDBNull(dr("DATEOF_YR_MINUS1_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS1_RATEINC") = "N/A"
                        End If

                        If Not IsDBNull(dr("DATEOF_YR_MINUS2_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS2_RATEINC") = "N/A"
                        End If

                        If Not IsDBNull(dr("DATEOF_YR_MINUS3_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS3_RATEINC") = "N/A"
                        End If

                        If Not IsDBNull(dr("DATEOF_YR_MINUS4_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                            End If
                        Else
                            dr("DATEOF_YR_MINUS4_RATEINC") = "N/A"
                        End If


                        If Not IsDBNull(dr("DATEOF_YR_MINUS5_RATEINC")) Then
                            If (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/2001") Then
                                dr("DATEOF_YR_MINUS5_RATEINC") = "N/A"
                            End If
                        End If

                        If (dr("POLICY_APPROVAL").ToString() = "01/01/0001") Then
                            dr("POLICY_APPROVAL") = "N/A"
                        End If

                    Next
                End If
                SortableView = rateChangesDs.Tables(0).DefaultView
                Dim sortExpression As String = "PERCENT_CURRENT_RATEINC"

                If (Not String.IsNullOrEmpty(sortExpression)) Then
                    If (SortDirection.ToUpper() = "ASC") Then
                        SortDirection = "DESC"
                    Else
                        SortDirection = "ASC"
                    End If
                    SortableView.Sort = sortExpression & " " & SortDirection

                    gridRateChanges.DataSource = SortableView
                    gridRateChanges.DataBind()
                End If
                'If (Not String.IsNullOrEmpty(sortexpressCompany)) Then
                '    If (SortDirection.ToUpper() = "ASC") Then
                '        SortDirection = "DESC"
                '    Else
                '        SortDirection = "ASC"
                '    End If
                '    SortableView.Sort = sortexpressCompany & " " & SortDirection

                '    gridRateChanges.DataSource = SortableView
                '    gridRateChanges.DataBind()
                'End If




            End If

        Catch ex As Exception
            ' rateChangesBL.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try

    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCompare_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCompare.Click

        Dim RateChangeId As String = ""

        'Find the rowid of the all the checked rows.
        For Each row As GridViewRow In gridRateChanges.Rows
            Dim chkItem As CheckBox = DirectCast(row.FindControl("ChartChkBox"), CheckBox)
            Dim intRateChangeKey As String = CType(row.FindControl("hfKey"), HiddenField).Value
            If chkItem IsNot Nothing Then
                If chkItem.Checked Then
                    RateChangeId = String.Concat(RateChangeId, ",", intRateChangeKey.ToString())
                End If
            End If
        Next
        If RateChangeId.StartsWith(",") Then
            RateChangeId = RateChangeId.Remove(0, 1)
        End If
        Response.Redirect("CompareCarriers.aspx?ID=" + RateChangeId, False)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnViewAudit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewAudit.Click
        Response.Redirect("ViewAuditTrail.aspx", False)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnViewErrorLog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewErrorLog.Click
        Response.Redirect("ViewErrorLog.aspx", False)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub linkLogout_Click(ByVal sender As Object, ByVal e As EventArgs) Handles linkLogout.Click
        Dim aCookie As HttpCookie = New HttpCookie("userInfo")
        aCookie.Expires = DateTime.Now.AddDays(-1)
        Response.Cookies.Add(aCookie)

        For Each key As String In Request.Cookies.AllKeys
            Dim cookie As HttpCookie = New HttpCookie(key)
            cookie.Expires = DateTime.UtcNow.AddDays(-7)
            Response.Cookies.Add(cookie)
        Next

        If Not (Request.Cookies("userInfo") Is Nothing) Then
            userName = Convert.ToString(Request.Cookies("userInfo")("userName"))

        End If
        Dim strLogout As String = ConfigurationManager.AppSettings("MidRateReview.logout")
        Dim strLogoutUrl As String = strLogout + "/Account/login.aspx"
        Response.Redirect(strLogoutUrl, False)

    End Sub

    Sub ApplyPaging()

        Dim row As GridViewRow = gridRateChanges.BottomPagerRow
        Dim ph As PlaceHolder = New PlaceHolder
        Dim lnkPaging As LinkButton
        Dim lnkFirstPage As LinkButton
        Dim lnkPrevPage As LinkButton
        Dim lnkNextPage As LinkButton
        Dim lnkLastPage As LinkButton
        lnkFirstPage = New LinkButton()
        lnkFirstPage.Text = Server.HtmlEncode("<<First")
        lnkFirstPage.Width = Unit.Pixel(50)
        lnkFirstPage.CommandName = "Page"
        lnkFirstPage.CommandArgument = "First"
        lnkPrevPage = New LinkButton()
        lnkPrevPage.Text = Server.HtmlEncode("<Prev")
        lnkPrevPage.Width = Unit.Pixel(50)
        lnkPrevPage.CommandName = "Page"
        lnkPrevPage.CommandArgument = "Prev"
        If (gridRateChanges.PageCount <> 0) Then
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkFirstPage)
            ph.Controls.Add(lnkPrevPage)
        End If
        If (gridRateChanges.PageIndex = 0) Then

            lnkFirstPage.Enabled = False
            lnkPrevPage.Enabled = False
        End If
        Dim i As Integer
        For i = 1 To gridRateChanges.PageCount

            lnkPaging = New LinkButton()
            lnkPaging.Width = Unit.Pixel(20)
            ' lnkPaging.CssClass = "LinkPaging"
            lnkPaging.Text = i.ToString()
            lnkPaging.CommandName = "Page"
            lnkPaging.CommandArgument = i.ToString()
            ' If (i = gridRateChanges.PageIndex + 1) Then
            ' lnkPaging.BackColor = System.Drawing.Color.Blue
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkPaging)
            ' End If

        Next i

        lnkNextPage = New LinkButton()
        lnkNextPage.Text = Server.HtmlEncode("Next>")
        lnkNextPage.Width = Unit.Pixel(50)
        lnkNextPage.CommandName = "Page"
        lnkNextPage.CommandArgument = "Next"
        lnkLastPage = New LinkButton()
        lnkLastPage.Text = Server.HtmlEncode("Last>>")
        lnkLastPage.Width = Unit.Pixel(50)
        lnkLastPage.CommandName = "Page"
        lnkLastPage.CommandArgument = "Last"
        If (gridRateChanges.PageCount <> 0) Then
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkNextPage)
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkLastPage)
        End If
        If (gridRateChanges.PageIndex = gridRateChanges.PageCount - 1) Then

            lnkNextPage.Enabled = False
            'lnkLastPage.Enabled = False
        End If
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnLeft1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLeft1.Click

        gridRateChanges.Columns(4).Visible = True
        gridRateChanges.Columns(5).Visible = True
        gridRateChanges.Columns(6).Visible = True
        gridRateChanges.Columns(7).Visible = True
        gridRateChanges.Columns(10).Visible = True
        gridRateChanges.Columns(11).Visible = True
        gridRateChanges.Columns(12).Visible = True
        gridRateChanges.Columns(13).Visible = False
        gridRateChanges.Columns(14).Visible = False
        gridRateChanges.Columns(15).Visible = False
        gridRateChanges.Columns(16).Visible = False
        gridRateChanges.Columns(17).Visible = False
        gridRateChanges.Columns(18).Visible = False
        gridRateChanges.Columns(19).Visible = False
        gridRateChanges.Columns(20).Visible = False
        gridRateChanges.Columns(21).Visible = False
        gridRateChanges.Columns(22).Visible = False
        gridRateChanges.Columns(23).Visible = False
        gridRateChanges.Columns(24).Visible = False
        gridRateChanges.Columns(25).Visible = False
        gridRateChanges.Columns(26).Visible = False
        gridRateChanges.Columns(27).Visible = False
        gridRateChanges.Columns(28).Visible = False
        gridRateChanges.Columns(29).Visible = False
        gridRateChanges.Columns(30).Visible = False
        gridRateChanges.Columns(31).Visible = False
        gridRateChanges.Columns(32).Visible = False
        gridRateChanges.Columns(33).Visible = False
        gridRateChanges.Columns(34).Visible = False
        gridRateChanges.Columns(35).Visible = False
        gridRateChanges.Columns(36).Visible = False
        info.Titles.Clear()
        info.StartColumns.Clear()
        info.MergedColumns.Clear()
        'GetDataOnNavigation()
        btnSearch_Click(sender, e)

    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRight1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRight1.Click


        info.Titles.Clear()
        info.StartColumns.Clear()
        info.MergedColumns.Clear()
        gridRateChanges.Columns(13).Visible = False
        gridRateChanges.Columns(14).Visible = False
        gridRateChanges.Columns(15).Visible = True
        gridRateChanges.Columns(16).Visible = True
        gridRateChanges.Columns(17).Visible = True
        gridRateChanges.Columns(18).Visible = True

        gridRateChanges.Columns(19).Visible = True
        gridRateChanges.Columns(20).Visible = True
        gridRateChanges.Columns(21).Visible = True
        gridRateChanges.Columns(22).Visible = False

        gridRateChanges.Columns(23).Visible = False
        gridRateChanges.Columns(24).Visible = False
        gridRateChanges.Columns(25).Visible = False

        gridRateChanges.Columns(26).Visible = False
        gridRateChanges.Columns(27).Visible = False

        gridRateChanges.Columns(28).Visible = False
        gridRateChanges.Columns(29).Visible = False
        gridRateChanges.Columns(30).Visible = False
        gridRateChanges.Columns(31).Visible = False
        gridRateChanges.Columns(32).Visible = False
        gridRateChanges.Columns(33).Visible = False


        gridRateChanges.Columns(3).Visible = True
        gridRateChanges.Columns(4).Visible = True
        gridRateChanges.Columns(5).Visible = True
        gridRateChanges.Columns(6).Visible = True
        gridRateChanges.Columns(7).Visible = True
        gridRateChanges.Columns(8).Visible = False
        gridRateChanges.Columns(9).Visible = False
        gridRateChanges.Columns(10).Visible = False
        gridRateChanges.Columns(11).Visible = False
        gridRateChanges.Columns(12).Visible = False


        info.AddMergedColumns(New Integer() {16, 17, 18, 19, 20}, "Percentage of Last Five(5) Years Increases")
        btnSearch_Click(sender, e)

    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRightPercentage_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRightPercentage.Click
        info.Titles.Clear()
        info.StartColumns.Clear()
        info.MergedColumns.Clear()
        gridRateChanges.Columns(13).Visible = False
        gridRateChanges.Columns(14).Visible = False
        gridRateChanges.Columns(15).Visible = False
        gridRateChanges.Columns(16).Visible = False
        gridRateChanges.Columns(17).Visible = False

        gridRateChanges.Columns(18).Visible = False
        gridRateChanges.Columns(19).Visible = False
        gridRateChanges.Columns(20).Visible = False
        gridRateChanges.Columns(21).Visible = False
        gridRateChanges.Columns(22).Visible = True
        gridRateChanges.Columns(23).Visible = True
        gridRateChanges.Columns(24).Visible = True
        gridRateChanges.Columns(25).Visible = True
        gridRateChanges.Columns(26).Visible = True
        gridRateChanges.Columns(27).Visible = True
        gridRateChanges.Columns(28).Visible = True
        gridRateChanges.Columns(29).Visible = False
        gridRateChanges.Columns(30).Visible = False
        gridRateChanges.Columns(31).Visible = False
        gridRateChanges.Columns(32).Visible = False
        gridRateChanges.Columns(33).Visible = False
        gridRateChanges.Columns(34).Visible = False
        gridRateChanges.Columns(35).Visible = False
        gridRateChanges.Columns(36).Visible = False

        gridRateChanges.Columns(3).Visible = True
        gridRateChanges.Columns(4).Visible = True
        gridRateChanges.Columns(5).Visible = True
        gridRateChanges.Columns(6).Visible = True
        gridRateChanges.Columns(7).Visible = True
        gridRateChanges.Columns(8).Visible = False
        gridRateChanges.Columns(9).Visible = False
        gridRateChanges.Columns(10).Visible = False
        gridRateChanges.Columns(11).Visible = False
        gridRateChanges.Columns(12).Visible = False

        info.AddMergedColumns(New Integer() {23, 24, 25, 26, 27}, "Last Five(5) Years Loss Ratio")
        'GetDataOnNavigation()
        btnSearch_Click(sender, e)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnLeftPercentage_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLeftPercentage.Click
        info.Titles.Clear()
        info.StartColumns.Clear()
        info.MergedColumns.Clear()

        gridRateChanges.Columns(14).Visible = True
        gridRateChanges.Columns(15).Visible = False
        gridRateChanges.Columns(16).Visible = False
        gridRateChanges.Columns(17).Visible = False
        gridRateChanges.Columns(18).Visible = False

        gridRateChanges.Columns(19).Visible = False
        gridRateChanges.Columns(20).Visible = False
        gridRateChanges.Columns(21).Visible = False
        gridRateChanges.Columns(22).Visible = False
        gridRateChanges.Columns(23).Visible = False

        gridRateChanges.Columns(24).Visible = False
        gridRateChanges.Columns(25).Visible = False
        gridRateChanges.Columns(26).Visible = False
        gridRateChanges.Columns(27).Visible = False
        gridRateChanges.Columns(28).Visible = False
        gridRateChanges.Columns(29).Visible = False
        gridRateChanges.Columns(30).Visible = False
        gridRateChanges.Columns(31).Visible = False
        gridRateChanges.Columns(32).Visible = False
        gridRateChanges.Columns(33).Visible = False
        gridRateChanges.Columns(34).Visible = False
        gridRateChanges.Columns(35).Visible = False
        gridRateChanges.Columns(36).Visible = False

        gridRateChanges.Columns(3).Visible = True
        gridRateChanges.Columns(4).Visible = True
        gridRateChanges.Columns(5).Visible = True
        gridRateChanges.Columns(6).Visible = True
        gridRateChanges.Columns(7).Visible = True
        gridRateChanges.Columns(8).Visible = True
        gridRateChanges.Columns(9).Visible = True
        gridRateChanges.Columns(10).Visible = True
        gridRateChanges.Columns(11).Visible = True
        gridRateChanges.Columns(12).Visible = True
        gridRateChanges.Columns(13).Visible = True

        info.AddMergedColumns(New Integer() {9, 10, 11, 12, 13}, "Date of Last Five(5) Years Increases")
        ' GetDataOnNavigation()
        btnSearch_Click(sender, e)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnLeftLossRatio_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLeftLossRatio.Click
        info.Titles.Clear()
        info.StartColumns.Clear()
        info.MergedColumns.Clear()
        gridRateChanges.Columns(13).Visible = False
        gridRateChanges.Columns(14).Visible = False
        gridRateChanges.Columns(15).Visible = True
        gridRateChanges.Columns(16).Visible = True
        gridRateChanges.Columns(17).Visible = True
        gridRateChanges.Columns(18).Visible = True

        gridRateChanges.Columns(19).Visible = True
        gridRateChanges.Columns(20).Visible = True
        gridRateChanges.Columns(21).Visible = True
        gridRateChanges.Columns(22).Visible = False

        gridRateChanges.Columns(23).Visible = False
        gridRateChanges.Columns(24).Visible = False
        gridRateChanges.Columns(25).Visible = False

        gridRateChanges.Columns(26).Visible = False
        gridRateChanges.Columns(27).Visible = False

        gridRateChanges.Columns(28).Visible = False
        gridRateChanges.Columns(29).Visible = False
        gridRateChanges.Columns(30).Visible = False
        gridRateChanges.Columns(31).Visible = False
        gridRateChanges.Columns(31).Visible = False
        gridRateChanges.Columns(32).Visible = False
        gridRateChanges.Columns(33).Visible = False
        gridRateChanges.Columns(34).Visible = False
        gridRateChanges.Columns(35).Visible = False
        gridRateChanges.Columns(36).Visible = False



        gridRateChanges.Columns(3).Visible = True
        gridRateChanges.Columns(4).Visible = True
        gridRateChanges.Columns(5).Visible = True
        gridRateChanges.Columns(6).Visible = True
        gridRateChanges.Columns(7).Visible = True
        gridRateChanges.Columns(8).Visible = False
        gridRateChanges.Columns(9).Visible = False
        gridRateChanges.Columns(10).Visible = False
        gridRateChanges.Columns(11).Visible = False
        gridRateChanges.Columns(12).Visible = False


        info.AddMergedColumns(New Integer() {16, 17, 18, 19, 20}, "Percentage of Last Five(5) Years Increases")
        ' GetDataOnNavigation()
        btnSearch_Click(sender, e)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRightLossRatio_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRightLossRatio.Click
        info.Titles.Clear()
        info.StartColumns.Clear()
        info.MergedColumns.Clear()
        gridRateChanges.Columns(13).Visible = False
        gridRateChanges.Columns(14).Visible = False
        gridRateChanges.Columns(15).Visible = False
        gridRateChanges.Columns(16).Visible = False
        gridRateChanges.Columns(17).Visible = False

        gridRateChanges.Columns(18).Visible = False
        gridRateChanges.Columns(19).Visible = False
        gridRateChanges.Columns(20).Visible = False
        gridRateChanges.Columns(21).Visible = False
        gridRateChanges.Columns(22).Visible = False
        gridRateChanges.Columns(23).Visible = False
        gridRateChanges.Columns(24).Visible = False
        gridRateChanges.Columns(25).Visible = False
        gridRateChanges.Columns(26).Visible = False
        gridRateChanges.Columns(27).Visible = False
        gridRateChanges.Columns(28).Visible = False
        gridRateChanges.Columns(29).Visible = True
        gridRateChanges.Columns(30).Visible = True
        gridRateChanges.Columns(31).Visible = True
        gridRateChanges.Columns(32).Visible = True
        gridRateChanges.Columns(33).Visible = True
        gridRateChanges.Columns(34).Visible = True
        gridRateChanges.Columns(35).Visible = True
        gridRateChanges.Columns(36).Visible = True

        gridRateChanges.Columns(3).Visible = True
        gridRateChanges.Columns(4).Visible = True
        gridRateChanges.Columns(5).Visible = True
        gridRateChanges.Columns(6).Visible = True
        gridRateChanges.Columns(7).Visible = True
        gridRateChanges.Columns(8).Visible = False
        gridRateChanges.Columns(9).Visible = False
        gridRateChanges.Columns(10).Visible = False
        gridRateChanges.Columns(11).Visible = False
        gridRateChanges.Columns(12).Visible = False

        info.AddMergedColumns(New Integer() {30, 31, 32, 33, 34}, "No. Of MS Insureds")
        ' GetDataOnNavigation()
        btnSearch_Click(sender, e)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnLeftNumOfInsured_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLeftNumOfInsured.Click

        info.Titles.Clear()
        info.StartColumns.Clear()
        info.MergedColumns.Clear()
        gridRateChanges.Columns(13).Visible = False
        gridRateChanges.Columns(14).Visible = False
        gridRateChanges.Columns(15).Visible = False
        gridRateChanges.Columns(16).Visible = False
        gridRateChanges.Columns(17).Visible = False

        gridRateChanges.Columns(18).Visible = False
        gridRateChanges.Columns(19).Visible = False
        gridRateChanges.Columns(20).Visible = False
        gridRateChanges.Columns(21).Visible = False
        gridRateChanges.Columns(22).Visible = True
        gridRateChanges.Columns(23).Visible = True
        gridRateChanges.Columns(24).Visible = True
        gridRateChanges.Columns(25).Visible = True
        gridRateChanges.Columns(26).Visible = True
        gridRateChanges.Columns(27).Visible = True
        gridRateChanges.Columns(28).Visible = True
        gridRateChanges.Columns(29).Visible = False
        gridRateChanges.Columns(30).Visible = False
        gridRateChanges.Columns(31).Visible = False
        gridRateChanges.Columns(32).Visible = False
        gridRateChanges.Columns(33).Visible = False
        gridRateChanges.Columns(34).Visible = False
        gridRateChanges.Columns(35).Visible = False
        gridRateChanges.Columns(36).Visible = False

        gridRateChanges.Columns(3).Visible = True
        gridRateChanges.Columns(4).Visible = True
        gridRateChanges.Columns(5).Visible = True
        gridRateChanges.Columns(6).Visible = True
        gridRateChanges.Columns(7).Visible = True
        gridRateChanges.Columns(8).Visible = False
        gridRateChanges.Columns(9).Visible = False
        gridRateChanges.Columns(10).Visible = False
        gridRateChanges.Columns(11).Visible = False
        gridRateChanges.Columns(12).Visible = False

        info.AddMergedColumns(New Integer() {23, 24, 25, 26, 27}, "Last Five(5) Years Loss Ratio")
        'GetDataOnNavigation()
        btnSearch_Click(sender, e)

    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DisplayDefaultGridControls()
        info.Titles.Clear()
        info.StartColumns.Clear()
        info.MergedColumns.Clear()

        gridRateChanges.Columns(15).Visible = False
        gridRateChanges.Columns(16).Visible = False
        gridRateChanges.Columns(17).Visible = False
        gridRateChanges.Columns(18).Visible = False
        gridRateChanges.Columns(19).Visible = False

        gridRateChanges.Columns(20).Visible = False
        gridRateChanges.Columns(21).Visible = False
        gridRateChanges.Columns(22).Visible = False
        gridRateChanges.Columns(23).Visible = False
        gridRateChanges.Columns(24).Visible = False

        gridRateChanges.Columns(25).Visible = False
        gridRateChanges.Columns(26).Visible = False
        gridRateChanges.Columns(27).Visible = False
        gridRateChanges.Columns(28).Visible = False
        gridRateChanges.Columns(29).Visible = False
        gridRateChanges.Columns(30).Visible = False
        gridRateChanges.Columns(31).Visible = False
        gridRateChanges.Columns(32).Visible = False
        gridRateChanges.Columns(33).Visible = False
        gridRateChanges.Columns(34).Visible = False
        gridRateChanges.Columns(35).Visible = False
        gridRateChanges.Columns(36).Visible = False

        gridRateChanges.Columns(3).Visible = True
        gridRateChanges.Columns(4).Visible = True
        gridRateChanges.Columns(5).Visible = True
        gridRateChanges.Columns(6).Visible = True
        gridRateChanges.Columns(7).Visible = True
        gridRateChanges.Columns(8).Visible = True
        gridRateChanges.Columns(9).Visible = True
        gridRateChanges.Columns(10).Visible = True
        gridRateChanges.Columns(11).Visible = True
        gridRateChanges.Columns(12).Visible = True
        gridRateChanges.Columns(13).Visible = True
        gridRateChanges.Columns(14).Visible = True

        info.AddMergedColumns(New Integer() {9, 10, 11, 12, 13}, "Date of Last Five(5) Years Increases")

    End Sub

    'Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportpdf.Click
    '    Try
    '        Response.Clear()
    '        ' ExportToCSV()
    '        ' exportpdf()
    '        ExpportToXML()
    '    Catch ex As Exception
    '        Admin.WriteLog(ex)
    '        Response.Redirect(ERROR_PAGE)
    '    End Try
    'End Sub

    Protected Sub DrpListExport_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        If (DrpListExport.SelectedItem.ToString() = "Excel") Then
            ExportToExcel()


        End If
        If (DrpListExport.SelectedItem.ToString() = "PDF") Then
            exportpdf()
        End If
        If (DrpListExport.SelectedItem.ToString() = "XML") Then
            ExpportToXML()
        End If

        If (DrpListExport.SelectedItem.ToString() = "CSV") Then
            ExportToCSV()
        End If
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExport.Click

        If (DrpListExport.SelectedItem.ToString() = "Excel") Then
            ExportToExcel()
        End If
        If (DrpListExport.SelectedItem.ToString() = "PDF") Then
            exportpdf()
        End If
        If (DrpListExport.SelectedItem.ToString() = "XML") Then
            ExpportToXML()
        End If

        If (DrpListExport.SelectedItem.ToString() = "CSV") Then
            ExportToCSV()
        End If
    End Sub

    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReturn.Click
        Dim intCompId As Integer
        If Session.Item("CompId") <> Nothing Then
            intCompId = CType(Session.Item("CompId"), Integer)
        End If
        If (intCompId <> 0) Then
            Response.Redirect("RateChangesDashBoard.aspx?compID=" + intCompId.ToString(), False)
        Else
            Response.Redirect("RateChangesDashBoard.aspx", False)

        End If



    End Sub


    Protected Sub btnCompareRate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCompareRate.Click
        Dim strpath As String = ConfigurationManager.AppSettings("CompareRateChanges")
        Dim strpathUrl As String = strpath + "/CompareRateChanges.aspx"
        Dim companyID As String = drpListCompany.SelectedValue.ToString()
        Session("CompId") = companyID
        Response.Redirect(strpathUrl + "?compID=" & companyID)

    End Sub

    Protected Sub gridRateChanges_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gridRateChanges.RowDataBound
        'ChangeGridCellColor(sender, e)
        Try

            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim rowView As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
                Dim hLink As HyperLink = e.Row.FindControl("hLinkRateInc")
                hLink.Enabled = True
                hLink.Text = rowView("LINK_RATE_INCREASE").ToString().Trim()

                hLink.Enabled = True
                If (hLink.Text <> "N/A") Then
                    hLink.Enabled = True
                    hLink.NavigateUrl = rowView("LINK_RATE_INCREASE").ToString().Trim()
                    hLink.Attributes.Add("OnClick", "return confirm('You are now leaving the Mississippi Insurance Department website and being directed to HealthCare.gov, a website operated by\nthe United States Department of Health and Human Services.');")
                Else
                    hLink.Enabled = False
                End If


            End If


            If e.Row.RowType = DataControlRowType.DataRow Then
                If (e.Row.DataItem("PERCENT_CURRENT_RATEINC").ToString() <> "N/A") Then
                    Dim ratePercentage As String = e.Row.DataItem("PERCENT_CURRENT_RATEINC").ToString()
                    Dim ratePercent() As String = ratePercentage.Split("%")
                    Dim rateperValue As Decimal = Convert.ToDecimal(ratePercent(0))
                    If (rateperValue < 0) Then

                        e.Row.Cells(8).ForeColor = Drawing.Color.Red
                    End If
                End If

                If (e.Row.DataItem("PERCENT_YR_MINUS1_RATEINC").ToString() <> "N/A") Then
                    Dim ratePercentYearMinus1() As String = e.Row.DataItem("PERCENT_YR_MINUS1_RATEINC").ToString().Split("%")
                    Dim PercentYearMinus1 As Decimal = Convert.ToDecimal(ratePercentYearMinus1(0))
                    If (PercentYearMinus1 < 0) Then

                        e.Row.Cells(16).ForeColor = Drawing.Color.Red
                    End If
                End If

                If (e.Row.DataItem("PERCENT_YR_MINUS2_RATEINC").ToString() <> "N/A") Then
                    Dim ratePercentYearMinus2() As String = e.Row.DataItem("PERCENT_YR_MINUS2_RATEINC").ToString().Split("%")
                    Dim PercentYearMinus2 As Decimal = Convert.ToDecimal(ratePercentYearMinus2(0))
                    If (PercentYearMinus2 < 0) Then

                        e.Row.Cells(17).ForeColor = Drawing.Color.Red
                    End If
                End If
                If (e.Row.DataItem("PERCENT_YR_MINUS3_RATEINC").ToString() <> "N/A") Then
                    Dim ratePercentYearMinus3() As String = e.Row.DataItem("PERCENT_YR_MINUS3_RATEINC").ToString().Split("%")
                    Dim PercentYearMinus3 As Decimal = Convert.ToDecimal(ratePercentYearMinus3(0))
                    If (PercentYearMinus3 < 0) Then

                        e.Row.Cells(18).ForeColor = Drawing.Color.Red
                    End If
                End If

                If (e.Row.DataItem("PERCENT_YR_MINUS4_RATEINC").ToString() <> "N/A") Then
                    Dim ratePercentYearMinus4() As String = e.Row.DataItem("PERCENT_YR_MINUS4_RATEINC").ToString().Split("%")
                    Dim PercentYearMinus4 As Decimal = Convert.ToDecimal(ratePercentYearMinus4(0))
                    If (PercentYearMinus4 < 0) Then

                        e.Row.Cells(19).ForeColor = Drawing.Color.Red
                    End If

                End If
                If (e.Row.DataItem("PERCENT_YR_MINUS5_RATEINC").ToString() <> "N/A") Then
                    Dim ratePercentYearMinus5() As String = e.Row.DataItem("PERCENT_YR_MINUS5_RATEINC").ToString().Split("%")
                    Dim PercentYearMinus5 As Decimal = Convert.ToDecimal(ratePercentYearMinus5(0))
                    If (PercentYearMinus5 < 0) Then

                        e.Row.Cells(20).ForeColor = Drawing.Color.Red
                    End If
                End If

                If (e.Row.DataItem("LAST_MINUS1_LOSSRATIO").ToString() <> "N/A") Then
                    Dim lossratioYearMinus1() As String = e.Row.DataItem("LAST_MINUS1_LOSSRATIO").ToString().Split("%")
                    Dim ratioYearMinus1 As Decimal = Convert.ToDecimal(lossratioYearMinus1(0))
                    If (ratioYearMinus1 < 0) Then

                        e.Row.Cells(23).ForeColor = Drawing.Color.Red
                    End If
                End If

                If (e.Row.DataItem("LAST_MINUS2_LOSSRATIO").ToString() <> "N/A") Then
                    Dim lossratioYearMinus2() As String = e.Row.DataItem("LAST_MINUS2_LOSSRATIO").ToString().Split("%")
                    Dim ratioYearMinus2 As Decimal = Convert.ToDecimal(lossratioYearMinus2(0))
                    If (ratioYearMinus2 < 0) Then

                        e.Row.Cells(24).ForeColor = Drawing.Color.Red
                    End If
                End If

                If (e.Row.DataItem("LAST_MINUS3_LOSSRATIO").ToString() <> "N/A") Then
                    Dim lossratioYearMinus3() As String = e.Row.DataItem("LAST_MINUS3_LOSSRATIO").ToString().Split("%")
                    Dim ratioYearMinus3 As Decimal = Convert.ToDecimal(lossratioYearMinus3(0))
                    If (ratioYearMinus3 < 0) Then

                        e.Row.Cells(25).ForeColor = Drawing.Color.Red
                    End If
                End If

                If (e.Row.DataItem("LAST_MINUS4_LOSSRATIO").ToString() <> "N/A") Then
                    Dim lossratioYearMinus4() As String = e.Row.DataItem("LAST_MINUS4_LOSSRATIO").ToString().Split("%")
                    Dim ratioYearMinus4 As Decimal = Convert.ToDecimal(lossratioYearMinus4(0))
                    If (ratioYearMinus4 < 0) Then

                        e.Row.Cells(26).ForeColor = Drawing.Color.Red
                    End If
                End If

                If (e.Row.DataItem("LAST_MINUS5_LOSSRATIO").ToString() <> "N/A") Then
                    Dim lossratioYearMinus5() As String = e.Row.DataItem("LAST_MINUS5_LOSSRATIO").ToString().Split("%")
                    Dim ratioYearMinus5 As Decimal = Convert.ToDecimal(lossratioYearMinus5(0))
                    If (ratioYearMinus5 < 0) Then

                        e.Row.Cells(27).ForeColor = Drawing.Color.Red
                    End If
                End If
            End If
         




        Catch ex As Exception

        End Try


    End Sub

    Protected Sub btnMidBlog_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMidBlog.Click
        Dim strLogout As String = ConfigurationManager.AppSettings("MidRateReview.logout")
        Dim strLogoutUrl As String = strLogout + "/"
        Response.Redirect(strLogoutUrl, False)

    End Sub


    Protected Sub btnMidRateReviewHome_Click1(ByVal sender As Object, ByVal e As EventArgs) Handles btnMidRateReviewHome.Click
        Dim strMidHome As String = ConfigurationManager.AppSettings("CompareRateChanges")
        Dim strLogoutUrl As String = strMidHome + "/CompareRateChanges.aspx"
        Response.Redirect(strLogoutUrl, False)
    End Sub
End Class

