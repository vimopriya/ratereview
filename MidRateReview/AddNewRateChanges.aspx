<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="AddNewRateChanges.aspx.vb" Inherits="MidWebApp.AddNewRateChanges" 
    title="Add New Rate Changes" %>
 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <link href="css/grid.css" rel="stylesheet" media="screen" type="text/css" />
    <script type ="text/javascript" >

 function fnClickOK(sender, e) {   
 __doPostBack(sender,e);
}

function ConfirmDelete() {
    var agree = confirm("Are you sure you want to Delete this Company and all its associated rate review records?");
    if (agree)
        return true;
    else
        return false;
}
</script>
            
    
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <div id="page_header3">
    <asp:Label ID="lblTitle" runat="server"  Text="Add New Rate Changes" 
        CssClass="h2-form"> </asp:Label>
         <asp:LinkButton ID="linkLogout" CssClass="logout" runat="server" 
            CausesValidation="False">LogOut</asp:LinkButton>
             <asp:LinkButton ID="btnMidBlog" CssClass="logout" runat="server"  CausesValidation="False" >MID Blog Home</asp:LinkButton>  
    </div>
    <div class="gutter10">
        <asp:Button ID="btnReturn" runat="server" CssClass="btn-input" Text="Back" 
        CausesValidation="False"/>
          <asp:Label ID="lblUploadStatus" runat="server" CssClass="validators"></asp:Label>
      
    </div>
    <div class="form">
    <dl class="col_14 center-all">
        <dd>
            <asp:Label ID="company" runat="server" Text="Company" CssClass="label-form" ></asp:Label>
            <asp:DropDownList ID="drpDownCompany" runat="server" CssClass="dropdown" 
                    Width="300px">
            </asp:DropDownList>
            
            <asp:hyperlink CssClass="txt-small gutter10-l"  id = "hyperLinkAddComp" runat="server">Add 
            New Company</asp:hyperlink>


<asp:Panel ID = "panEdit" runat="server" CssClass="ModalWindow">

    
         <div class="col_7 center-all login">
                <h2 class="h2-blu gutter20-t">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Add New Company"></asp:Label></h2>
               
                <div class="col_7 loginbox center-all">
                
                <label class="co-label"><asp:Label ID="lblCompanyName" runat="server" Text="Company Name"></asp:Label></label>
                <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox>
                <br /><br />
                <label class="co-label"><asp:Label ID="lblFileUpload" runat="server" Text="Upload Company Logo "></asp:Label></label>
                <asp:FileUpload ID="FileUploadLogo" runat="server" />
                <br />
                    <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
                <dl>
                    <dd>
                        <asp:Button ID="btnPopupSubmit" runat="server" Text="Save" OnClick = "btnPopupSubmit_Click" OnClientClick = "return confirm('The Company name change will be applied to all the  rate review files under that name .Are you sure you want to edit company name');" CssClass="btn-form" CausesValidation = "false" />
                        <asp:Button ID="btnDelete" CssClass="btn-form" runat="server" Text="Delete" CausesValidation ="false" OnClientClick="return ConfirmDelete();"/>
                        <asp:Button ID="btnCancel" CssClass="btn-wht-form" runat="server" Text="Cancel" CausesValidation ="false" />
                        
                    </dd>
                    <dd>
                        <asp:Label ID="StatusLabel" runat="server"></asp:Label>
                    </dd>
                 

                </dl>
           </div>
        

            </asp:Panel>
            

        </dd>

        <dd>

        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server"

        TargetControlID="hyperLinkAddComp"

        PopupControlID="panEdit"

        BackgroundCssClass="modalBackground"

        CancelControlID="btnCancel" 

        PopupDragHandleControlID="panEdit">

        </cc1:ModalPopupExtender>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    
            <asp:RequiredFieldValidator ID="ReqValidatorCompany" runat="server" 
                ControlToValidate="drpDownCompany" ErrorMessage="Please Select A Company" 
                Enabled="true" InitialValue = "0" CssClass="validators"></asp:RequiredFieldValidator>
         

        </dd>
           
        <dd>
   
              <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                    SelectMethod="GetCompanyDetails" TypeName="RateChangesBL.Admin">
                    </asp:ObjectDataSource>
   
               
        </dd>
       
        <dd>
                <asp:Label ID="Label3" runat="server" Text="Policy Form No" 
                CssClass="label-form" ></asp:Label>


            <asp:TextBox ID="txtPolicyFormNo" runat="server" Width="296px" cssClass="txtbox-form" 
                        TextMode="MultiLine" MaxLength = "300"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server"  controltovalidate = "txtPolicyFormNo"
                ErrorMessage="Please Enter Policy Form No" CssClass="validators"></asp:RequiredFieldValidator>

        </dd>
        <dd>
        <asp:Label ID="lblPolicyType" runat="server" Text="Type Of Insurance" 
            CssClass="label-form"></asp:Label>
        <asp:DropDownList ID="drpDownPolicyType" runat="server" Width="300px">
        </asp:DropDownList>
     <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
            SelectMethod="GetPolicyType" TypeName="RateChangesBL.Admin">
        </asp:ObjectDataSource>
            <asp:RequiredFieldValidator ID="reqFieldValidatorPolicyType" runat="server" CssClass = "validators"
                ControlToValidate="drpDownPolicyType" 
                ErrorMessage="Please Select A Type Of Insurance" Enabled="true" InitialValue = "0"></asp:RequiredFieldValidator>
        </dd>   
        <dd>
        <asp:Label ID="Label4" runat="server" 
            Text="Secondary Type Description" CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtSecondaryType" runat="server" cssClass="txtbox-form" MaxLength = "100"
                Width="215px"></asp:TextBox>

        </dd>   
        <dd>
        <asp:Label ID="Lbl1" CssClass="label-form" runat="server" Text="Date Of Current Rate Increase">Date 
            Of Current Rate Increase</asp:Label><asp:TextBox ID="txtDateCurRateInc" runat="server" cssClass="txtbox-form"></asp:TextBox>

        <input id="btnPick1" type="button" value=""class="btn-calendar"/>
                <cc1:calendarextender ID="Calendarextender1" runat="server"  PopupButtonID = "btnPick1" TargetControlID = "txtDateCurRateInc"></cc1:calendarextender>    
                <asp:RequiredFieldValidator 
            ID="RequiredFieldValidator23" runat="server"  controltovalidate = "txtDateCurRateInc"
            ErrorMessage="Please Enter Current Date Rate Increase" CssClass="validators "></asp:RequiredFieldValidator>
     
            <asp:CompareValidator ID="CompareValidator21" runat = "server" ControlToValidate="txtDateCurRateInc" CssClass="validators" 
                ErrorMessage="Please Enter Valid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
        </dd>
        <dd>
        <asp:Label ID="lbl7" runat="server" 
            Text="Percentage Of Current Rate Increase" CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtPertCurrRateInc" runat="server" cssClass="txtbox-form" MaxLength = "8"></asp:TextBox>

        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" controltovalidate = "txtPertCurrRateInc"
            ErrorMessage="Please Enter The % Of Current Rate Increase" CssClass="validators "></asp:RequiredFieldValidator>

    
            <asp:CompareValidator ID="CompareValidator22" runat="server" 
                ControlToValidate="txtPertCurrRateInc" CssClass="validators" 
                ErrorMessage="Please Enter Valid Number" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>

        </dd>
        <dd class="subhead-line">
        <asp:Label ID="lblLastFiveyrsRateInc0" runat="server" 
            Text="Date of Last Five Yrs Rate Increase" CssClass="label-form"></asp:Label>
        </dd>
        <dd>
        <asp:Label ID="lblDate1" runat="server"  CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtDateLastFive2010" runat="server" cssClass="txtbox-form" MaxLength = "12"></asp:TextBox>
        

        <input id="btnPick2" type="button" value="" class="btn-calendar"/>
        
        <cc1:calendarextender ID="Calendarextender2" runat="server"  PopupButtonID = "btnPick2" TargetControlID = "txtDateLastFive2010"></cc1:calendarextender>    
                 
            <asp:CompareValidator ID="CompareValidator16" runat="server" 
                ControlToValidate="txtDateLastFive2010" CssClass="validators" 
                ErrorMessage="    Please Enter A Valid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
        </dd>
        <dd>
        <asp:Label ID="lblDate2" runat="server"  CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtDateLastFive2009" runat="server" cssClass="txtbox-form" MaxLength = "12"></asp:TextBox>
        

        <input id="btnPick3" type="button" value="" class="btn-calendar"/>
        <cc1:calendarextender ID="Calendarextender3" runat="server"  PopupButtonID = "btnPick3" TargetControlID = "txtDateLastFive2009"></cc1:calendarextender>    
                      <asp:CompareValidator ID="CompareValidator17" runat="server" 
                ControlToValidate="txtDateLastFive2009" CssClass="validators" 
                ErrorMessage="    Please Enter A Valid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
        </dd>
        <dd>
        <asp:Label ID="lblDate3" runat="server"  CssClass="label-form"></asp:Label>         
        <asp:TextBox ID="txtDateLastFive2008" runat="server" cssClass="txtbox-form" MaxLength = "12"></asp:TextBox>
        

        <input id="btnPick4" type="button" value="" class="btn-calendar"/>
        <cc1:calendarextender ID="Calendarextender4" runat="server"  PopupButtonID = "btnPick4" TargetControlID = "txtDateLastFive2008"></cc1:calendarextender>  
          
            <asp:CompareValidator ID="CompareValidator18" runat="server" 
                ControlToValidate="txtDateLastFive2008" CssClass="validators" 
                ErrorMessage="    Please Enter A Valid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>

        </dd>
        <dd>
        <asp:Label ID="lblDate4" runat="server"  CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtDateLastFive2007" runat="server" cssClass="txtbox-form" MaxLength = "12"></asp:TextBox>


        <input id="btnPick5" type="button" value="" class="btn-calendar"/>
        <cc1:calendarextender ID="Calendarextender5" runat="server"  PopupButtonID = "btnPick5" TargetControlID = "txtDateLastFive2007"></cc1:calendarextender>    
            
            <asp:CompareValidator ID="CompareValidator19" runat="server" 
                ControlToValidate="txtDateLastFive2007" CssClass="validators" 
                ErrorMessage="    Please Enter A Valid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>

        </dd>
        <dd>
        <asp:Label ID="lblDate5" runat="server" CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtDateLastFive2006" runat="server" cssClass="txtbox-form" MaxLength = "12"></asp:TextBox>
        

        <input id="btnPick6" type="button" value="" class="btn-calendar"/>
  
    
  
        <cc1:calendarextender ID="Calendarextender6" runat="server"  PopupButtonID = "btnPick6" TargetControlID = "txtDateLastFive2006"></cc1:calendarextender>    
           
            <asp:CompareValidator ID="CompareValidator20" runat="server" 
                ControlToValidate="txtDateLastFive2006" CssClass="validators" 
                ErrorMessage="    Please Enter A Valid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>

        </dd>
        <dd class="subhead-line">
        <asp:Label ID="LblPercentage" runat="server" 
            Text="Percent Of Last Five Yrs Rate Increase" CssClass="label-form"></asp:Label>
        </dd>
        <dd>
        <asp:Label ID="lblprct1" runat="server" CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtPertLastFive2010" runat="server" cssClass="txtbox-form" MaxLength = "8"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" controltovalidate ="txtPertLastFive2010"
            ErrorMessage="Please Enter Percentage(%) 2010 Rate Increase" 
                CssClass="validators " Enabled="False" ></asp:RequiredFieldValidator>
     
            <asp:CompareValidator ID="CompareValidator11" runat="server" 
                ControlToValidate="txtPertLastFive2010" CssClass="validators" 
                ErrorMessage="Please Enter A Valid Number" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
        </dd>
        <dd>
        <asp:Label ID="lblprct2" runat="server"  CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtPertLastFive2009" runat="server" cssClass="txtbox-form" MaxLength = "8"></asp:TextBox>
        
        <asp:RequiredFieldValidator ID="RequiredFieldValidator24"  controltovalidate ="txtPertLastFive2009"
            runat="server" ErrorMessage="Please Enter Percentage(%) 2009 Rate Increase" 
                CssClass="validators " Enabled="False"></asp:RequiredFieldValidator>
    
            <asp:CompareValidator ID="CompareValidator12" runat="server" 
                ControlToValidate="txtPertLastFive2009" CssClass="validators" 
                ErrorMessage="Please Enter A Valid Number" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
        </dd>
        <dd>
        <asp:Label ID="lblprct3" runat="server"  CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtPertLastFive2008" runat="server" cssClass="txtbox-form" MaxLength = "8"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"  controltovalidate = "txtPertLastFive2008"
            ErrorMessage="Please Enter Percentage(%) 2008 Rate Increase" 
                CssClass="validators" Enabled="False"></asp:RequiredFieldValidator>
     
            <asp:CompareValidator ID="CompareValidator13" runat="server" 
                ControlToValidate="txtPertLastFive2009" CssClass="validators" 
                ErrorMessage="Please Enter A Valid Number" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
        </dd>
        <dd>
        <asp:Label ID="lblprct4" runat="server" CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtPertLastFive2007" runat="server" cssClass="txtbox-form" MaxLength = "8"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server"  controltovalidate = "txtPertLastFive2007"
            ErrorMessage="Please Enter Percentage(%) 2007 Rate Increase" 
                CssClass="validators " Enabled="False"></asp:RequiredFieldValidator>
        
            <asp:CompareValidator ID="CompareValidator14" runat="server" 
                ControlToValidate="txtPertLastFive2007" CssClass="validators" 
                ErrorMessage="Please Enter A Valid Number" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>

        </dd>
        <dd>
        <asp:Label ID="lblprct5" runat="server"  CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtPertLastFive2006" runat="server" cssClass="txtbox-form" MaxLength = "8"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server"  controltovalidate = "txtPertLastFive2006"
            ErrorMessage="Please Enter Percentage(%) 2006 Rate Increase" 
                CssClass="validators " Enabled="False"></asp:RequiredFieldValidator>
        
            <asp:CompareValidator ID="CompareValidator15" runat="server" 
                CssClass="validators" ErrorMessage="Please Enter A Valid Number" 
                Operator="DataTypeCheck" Type="Double" 
                ControlToValidate="txtPertLastFive2006"></asp:CompareValidator>

        </dd>
        <dd class="subhead-line">
        <asp:Label ID="LblLossRatio" runat="server" Text="Last Five Years Loss Ratio" 
            CssClass="label-form"></asp:Label>
        </dd>
        <dd>
        <asp:Label ID="lbllossratio1" runat="server" CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtLossRatio2010" runat="server" cssClass="txtbox-form" MaxLength = "8"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" controltovalidate = "txtLossRatio2010"
            ErrorMessage="Please Enter Loss Ratio 2010"  CssClass="validators " 
                Enabled="False"></asp:RequiredFieldValidator>
   
            <asp:CompareValidator ID="CompareValidator6" runat="server" 
                ControlToValidate="txtLossRatio2010" CssClass="validators" 
                ErrorMessage="Please Enter A Valid Number" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
        </dd>
        <dd>
        <asp:Label ID="lbllossratio2" runat="server" CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtLossRatio2009" runat="server" cssClass="txtbox-form" MaxLength = "8"></asp:TextBox>
        
        <asp:RequiredFieldValidator ID="RequiredFieldValidator13"   runat="server" controltovalidate = "txtLossRatio2009"
            ErrorMessage="Please Enter Loss Ratio 2009" CssClass="validators " 
                Enabled="False"></asp:RequiredFieldValidator>
      
            <asp:CompareValidator ID="CompareValidator7" runat="server" 
                ControlToValidate="txtLossRatio2009" CssClass="validators" 
                ErrorMessage="Please Enter A Valid Number" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
        </dd>
        <dd>
        <asp:Label ID="lbllossratio3" runat="server"  CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtLossRatio2008" runat="server" cssClass="txtbox-form" MaxLength = "8"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator14"  runat="server" controltovalidate = "txtLossRatio2008"
            ErrorMessage="Please Enter Loss Ratio 2008" CssClass="validators " 
                Enabled="False"></asp:RequiredFieldValidator>
        
            <asp:CompareValidator ID="CompareValidator8" runat="server" 
                ControlToValidate="txtLossRatio2008" CssClass="validators" 
                ErrorMessage="Please Enter A Valid Number" Operator="DataTypeCheck" 
                Type="Double"></asp:CompareValidator>

        </dd>
        <dd>
        <asp:Label ID="lbllossratio4" runat="server" CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtLossRatio2007" runat="server" cssClass="txtbox-form" MaxLength = "8"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator15"  runat="server" controltovalidate = "txtLossRatio2007"
            ErrorMessage="Please Enter Loss Ratio 2007" CssClass="validators " 
                Enabled="False"></asp:RequiredFieldValidator>
      
            <asp:CompareValidator ID="CompareValidator9" runat="server" 
                ControlToValidate="txtLossRatio2007" CssClass="validators" 
                ErrorMessage="Please Enter A Valid Number" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
        </dd>
        <dd>
        <asp:Label ID="lbllossratio5" runat="server"  CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtLossRatio2006" runat="server" cssClass="txtbox-form" MaxLength = "8"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator16"   runat="server" controltovalidate = "txtLossRatio2006"
            ErrorMessage="Please Enter Loss Ratio 2006" CssClass="validators " 
                Enabled="False"></asp:RequiredFieldValidator>
      
            <asp:CompareValidator ID="CompareValidator10" runat="server" 
                ControlToValidate="txtLossRatio2006" CssClass="validators" 
                ErrorMessage="Please Enter A Valid Number" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
        </dd>
        <dd class="subhead-line">
        <asp:Label ID="LblMsInsured" runat="server" Text="No Of Mississippi Insureds*" 
            CssClass="label-form"></asp:Label>
        </dd>
        <dd>
        <asp:Label ID="lblNoOfMS1" runat="server"  CssClass="label-form left30"></asp:Label>
        <asp:TextBox ID="txtNoOfMsInsured2010" runat="server" cssClass="txtbox-form" MaxLength = "15"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator17"  runat="server" controltovalidate = "txtNoOfMsInsured2010"
            ErrorMessage="Please Enter No Of MS Insureds 2010"  CssClass="validators" 
                Enabled="False"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegExpNoOFmsInsured2010" runat="server" ValidationExpression = "[\d,]*"  ControlToValidate="txtNoOfMsInsured2010" CssClass="validators" ErrorMessage="Please Enter A Valid Number"></asp:RegularExpressionValidator>
        </dd>
        <dd>
        
        <asp:Label ID="lblNoOfMS2" runat="server"  CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtNoOfMsInsured2009" runat="server" cssClass="txtbox-form" MaxLength = "15"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator18"  runat="server" controltovalidate = "txtNoOfMsInsured2009"
            ErrorMessage="Please Enter No Of MS Insureds 2009" CssClass="validators" 
                Enabled="False"></asp:RequiredFieldValidator>
           
             <asp:RegularExpressionValidator ID="RegExpNoOFmsInsured2009" runat="server" ValidationExpression = "[\d,]*"  ControlToValidate="txtNoOfMsInsured2009" CssClass="validators" ErrorMessage="Please Enter A Valid Number"></asp:RegularExpressionValidator>
            </dd>
        <dd>
    
        <asp:Label ID="lblNoOfMS3" runat="server" Text="" CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtNoOfMsInsured2008" runat="server" cssClass="txtbox-form" MaxLength = "15"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator19"  runat="server" controltovalidate="txtNoOfMsInsured2008"
            ErrorMessage="Please Enter No Of MS Insureds 2008" CssClass="validators" 
                Enabled="False"></asp:RequiredFieldValidator>
           
                 <asp:RegularExpressionValidator ID="RegExpNoOFmsInsured2008" runat="server" ValidationExpression = "[\d,]*"  ControlToValidate="txtNoOfMsInsured2008" CssClass="validators" ErrorMessage="Please Enter A Valid Number"></asp:RegularExpressionValidator>
        </dd>
        <dd>
        <asp:Label ID="lblNoOfMS4" runat="server" Text="" CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtNoOfMsInsured2007" runat="server" cssClass="txtbox-form" MaxLength = "15"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2007"   runat="server" controltovalidate="txtNoOfMsInsured2007"
            ErrorMessage="Please Enter No Of MS Insureds 2007" CssClass="validators" 
                Enabled="False"></asp:RequiredFieldValidator>
                     
                <asp:RegularExpressionValidator ID="RegExpNoOFmsInsured2007" runat="server" ValidationExpression = "[\d,]*"  ControlToValidate="txtNoOfMsInsured2007" CssClass="validators" ErrorMessage="Please Enter A Valid Number"></asp:RegularExpressionValidator>
        </dd>
        <dd>
        <asp:Label ID="lblNoOfMS5" runat="server" Text="" CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtNoOfMsInsured2006" runat="server" cssClass="txtbox-form" MaxLength = "15"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator21"  runat="server" controltovalidate="txtNoOfMsInsured2006"
            ErrorMessage="Please Enter No Of MS Insureds 2006" CssClass="validators" 
                Enabled="False"></asp:RequiredFieldValidator>
      
          <asp:RegularExpressionValidator ID="RegExpNoOFmsInsured2006" runat="server" ValidationExpression = "[\d,]*"  ControlToValidate="txtNoOfMsInsured2006" CssClass="validators" ErrorMessage="Please Enter A Valid Number"></asp:RegularExpressionValidator>
        <asp:Label ID="lblResult" runat="server"></asp:Label>
         
        </dd>
       <dd class="subhead-line">
        <asp:Label ID="Label2" runat="server" Text="Policy Approval" 
            CssClass="label-form"></asp:Label>
        </dd>        
         <dd>
        <asp:Label ID="lblReason" runat="server" Text="Summary of rate increase explanation" CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtReason" runat="server" cssClass="txtbox-form" MaxLength = "500" 
                 TextMode="MultiLine" Width="314px"></asp:TextBox>
              
        </dd>
         <dd>
        <asp:Label ID="lblPolicyAprlDate" runat="server"  Text = "Policy Approval" CssClass="label-form" ></asp:Label>
        <asp:TextBox ID="txtPolicyApp" runat="server" cssClass="txtbox-form" MaxLength = "12"></asp:TextBox>
        

        <input id="btnPickApprl" type="button" value="" class="btn-calendar"/>
  
        <cc1:calendarextender ID="Calendarextender7" runat="server"  PopupButtonID = "btnPickApprl" TargetControlID = "txtPolicyApp"></cc1:calendarextender>    
        <asp:RequiredFieldValidator 
            ID="RequiredFieldValidator7" runat="server" controltovalidate = "txtPolicyApp" 
            ErrorMessage="Please Enter The Date" CssClass="validators "  Enabled="False"></asp:RequiredFieldValidator>
       
            <asp:CompareValidator ID="CompareValidator23" runat="server" 
                ControlToValidate="txtPolicyApp" CssClass="validators" 
                ErrorMessage="  Please Enter A Valid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>

        </dd>
          <dd class="subhead-line">
        <span class="label-form">MS Rate Review on Healthcare.gov</span>     
        </dd>
         <dd>
        <asp:Label ID="lblDateSubmitted" runat="server"  Text = "Date Submitted" CssClass="label-form" ></asp:Label>
        <asp:TextBox ID="TxtDateSubmitted" runat="server" cssClass="txtbox-form" MaxLength = "12"></asp:TextBox>
        

        <input id="btnDateSubmitted" type="button" value="" class="btn-calendar"/>
  
        <cc1:calendarextender ID="CalendarextenderDateSubmitted" runat="server"  PopupButtonID = "btnDateSubmitted" TargetControlID = "TxtDateSubmitted"></cc1:calendarextender>    
              
            <asp:CompareValidator ID="CompareValidatorDateSubmitted" runat="server" 
                ControlToValidate="TxtDateSubmitted" CssClass="validators" 
                ErrorMessage="  Please Enter A Valid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>

        </dd>
       
        <dd>
        <asp:Label ID="lblLinkToRateInc" runat="server" Text="Link To Rate Increase" CssClass="label-form"></asp:Label>
        <asp:TextBox ID="txtLinkToRateInc" runat="server" cssClass="txtbox-form" 
                MaxLength = "400" Width="308px"></asp:TextBox>
                          
                
        </dd>
        <dd class="subhead-line">
        <span class="label-form">Edit Uploaded files</span>     
        </dd>
        <dd class="center">
           
                            <asp:GridView ID="gridDownloadFiles" CssClass="uploader center-all" runat="server" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="NAME" HeaderText="Name" />
                                    <asp:BoundField DataField="TYPE" HeaderText="Type" />
                                    <asp:BoundField DataField="UPLOAD_DATE" HeaderText="Upload Date" />
                                    <asp:TemplateField HeaderText="Download">
                                    <ItemTemplate>
                                    <asp:HiddenField ID="hfKey" runat = "server" Value='<%#Eval("ID")%>' /> 
                                      <asp:LinkButton ID="DownLoad" CommandName="dwnload"  runat="server" ToolTip = "DownLoad">Download</asp:LinkButton>
                                    </ItemTemplate>
                                    
                                    </asp:TemplateField>
                                                                      
                                
                                    <asp:TemplateField>
                                     <ItemTemplate>
                                          <asp:LinkButton ID="linkDelete" CommandName="Delete"  runat="server" ToolTip = "Delete">Remove</asp:LinkButton>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ID" HeaderText="ID" Visible="False" />
                                </Columns>
                            </asp:GridView>
        </dd>
       
        <dd>
            <asp:Label ID="Label25" runat="server" Text="Upload pdf/doc" CssClass="label-form"></asp:Label>
            <asp:FileUpload ID="FileUploadPdfDoc" runat="server" Width="245px" CssClass="txtbox-form" />
        </dd>
       <dd>
       <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick = "btnSubmit1_Click"
       CssClass="btn-form-m margin10-t margin330-l" 
                Width="110px" Height="30px"/>
            &nbsp;
           
            <asp:Button ID="Button1" runat="server" CssClass="btn-form-m margin10-t" 
                height = 30px Text="Back To Results" CausesValidation="False" />
      </dd>

         
        
        <dd class="center"><br />*Number Of Mississippi Insureds refers to the number of 
            insureds covered under individual policies and/or group policies.
        <dd>
    </dl>

       </div>

   <br />
    


</asp:Content>

   

