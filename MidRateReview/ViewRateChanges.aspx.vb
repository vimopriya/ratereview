﻿Imports RateChangesBL


Partial Public Class ViewRateChange
    Inherits System.Web.UI.Page

    Private Const ERROR_PAGE As String = "ErrorPage.aspx"
    Dim strCompId As String = ""
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hlinkRateReview.Enabled = True
        Dim rateid As Integer = CType(Request.QueryString("rateid"), Integer)

        Try
            If (rateid > 0) Then
                PopulateFormControls(rateid)
            End If

            If Not (Request.Cookies("userInfo") Is Nothing) Then
                Dim userName As String = Convert.ToString(Request.Cookies("userInfo")("userName"))

            End If
            'hlinkRateReview.Enabled = True
            'hlinkRateReview.Attributes.Add("OnClick", "return confirm(You are now leaving the Mississippi Insurance Department website and being directed to HealthCare.gov, a website operated by\nthe United States Department of Health and Human Services.');")
            'If (hlinkRateReview.Text = "N/A") Then
            '    hlinkRateReview.Enabled = False
            'End If
        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="intRateChangeID"></param>
    ''' <remarks></remarks>
    Private Sub PopulateFormControls(ByVal intRateChangeID As Integer)
        Dim rateChangesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()

        Try

            rateChangesDs = rateChangesBL.GetRateChangeDataRateID(intRateChangeID)
            lbl2010.Text = DateTime.Now.Year - 1
            lbl2009.Text = DateTime.Now.Year - 2
            lbl2008.Text = DateTime.Now.Year - 3
            lbl2007.Text = DateTime.Now.Year - 4
            lbl2006.Text = DateTime.Now.Year - 5


            Dim dr As DataRow
            Dim dt As DataTable
            '<code to fill the dataset>
            dt = rateChangesDs.Tables(0)
            For Each dr In dt.Rows
                'Response.Write(dr("COMPANY_ID"))
                strCompId = dr("COMPANY_ID").ToString()
                Session("searchComp") = strCompId
                lblCompanyVal.Text = dr("COMPANY_NAME").ToString()
                lblPolicyFormVal.Text = dr("POLICY_FORMNO").ToString()
                lblPolicyTypeval.Text = dr("POLICY_TYPE").ToString()
                If Not IsDBNull(dr("DATEOF_CURRENTRATEINCR")) Then
                    lbldateCurrentVal.Text = dr("DATEOF_CURRENTRATEINCR")
                End If

                lblPercentVal.Text = dr("PERCENT_CURRENT_RATEINC")
                If (lblPercentVal.Text.ToString() <> "N/A") Then
                    Dim percent As Decimal = Convert.ToDecimal(lblPercentVal.Text)
                    If (percent < 0) Then
                        lblPercentVal.ForeColor = Drawing.Color.Red

                    End If
                    lblPercentVal.Text = lblPercentVal.Text.ToString() & "%"
                End If
                If Not IsDBNull(dr("DATEOF_YR_MINUS1_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS1_RATEINC").ToString() = "01/01/2001") Then
                        lblDate2010.Text = "N/A"
                    Else
                        lblDate2010.Text = dr("DATEOF_YR_MINUS1_RATEINC")
                    End If
                Else
                    lblDate2010.Text = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS2_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS2_RATEINC").ToString() = "01/01/2001") Then
                        lblDate2009.Text = "N/A"
                    Else
                        lblDate2009.Text = dr("DATEOF_YR_MINUS2_RATEINC")
                    End If
                Else
                    lblDate2009.Text = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS3_RATEINC")) Then
                    If (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS3_RATEINC").ToString() = "01/01/2001") Then
                        lblDate2008.Text = "N/A"
                    Else
                        lblDate2008.Text = dr("DATEOF_YR_MINUS3_RATEINC")
                    End If
                Else
                    lblDate2008.Text = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS4_RATEINC")) Then

                    If (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS4_RATEINC").ToString() = "01/01/2001") Then
                        lblDate2007.Text = "N/A"
                    Else
                        lblDate2007.Text = dr("DATEOF_YR_MINUS4_RATEINC")
                    End If
                Else
                    lblDate2007.Text = "N/A"
                End If

                If Not IsDBNull(dr("DATEOF_YR_MINUS5_RATEINC")) Then

                    If (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/0001") Or (dr("DATEOF_YR_MINUS5_RATEINC").ToString() = "01/01/2001") Then
                        lblDate2006.Text = "N/A"
                    Else
                        lblDate2006.Text = dr("DATEOF_YR_MINUS5_RATEINC")
                    End If
                Else
                    lblDate2006.Text = "N/A"
                End If

                If (dr("POLICY_APPROVAL").ToString() = "01/01/0001") Then
                    lblPolicyApprlVal.Text = "N/A"
                Else
                    lblPolicyApprlVal.Text = dr("POLICY_APPROVAL").ToString()
                End If

                lblprct2010.Text = IIf((String.IsNullOrEmpty(dr("PERCENT_YR_MINUS1_RATEINC").ToString())), "N/A", dr("PERCENT_YR_MINUS1_RATEINC"))
                lblprct2009.Text = IIf((String.IsNullOrEmpty(dr("PERCENT_YR_MINUS2_RATEINC").ToString())), "N/A", dr("PERCENT_YR_MINUS2_RATEINC"))
                lblprct2008.Text = IIf((String.IsNullOrEmpty(dr("PERCENT_YR_MINUS3_RATEINC").ToString())), "N/A", dr("PERCENT_YR_MINUS3_RATEINC"))
                lblprct2007.Text = IIf((String.IsNullOrEmpty(dr("PERCENT_YR_MINUS4_RATEINC").ToString())), "N/A", dr("PERCENT_YR_MINUS4_RATEINC"))
                lblprct2006.Text = IIf((String.IsNullOrEmpty(dr("PERCENT_YR_MINUS5_RATEINC").ToString())), "N/A", dr("PERCENT_YR_MINUS5_RATEINC"))

                lbllr2010.Text = IIf((String.IsNullOrEmpty(dr("LAST_MINUS1_LOSSRATIO").ToString())), "N/A", dr("LAST_MINUS1_LOSSRATIO"))
                lbllr2009.Text = IIf((String.IsNullOrEmpty(dr("LAST_MINUS2_LOSSRATIO").ToString())), "N/A", dr("LAST_MINUS2_LOSSRATIO"))
                lbllr2008.Text = IIf((String.IsNullOrEmpty(dr("LAST_MINUS3_LOSSRATIO").ToString())), "N/A", dr("LAST_MINUS3_LOSSRATIO"))
                lbllr2007.Text = IIf((String.IsNullOrEmpty(dr("LAST_MINUS4_LOSSRATIO").ToString())), "N/A", dr("LAST_MINUS4_LOSSRATIO"))
                lbllr2006.Text = IIf((String.IsNullOrEmpty(dr("LAST_MINUS5_LOSSRATIO").ToString())), "N/A", dr("LAST_MINUS5_LOSSRATIO"))

                lblms2010.Text = IIf((String.IsNullOrEmpty(dr("NUMOF_MSINSURED_YR_MINUS1").ToString())), "N/A", dr("NUMOF_MSINSURED_YR_MINUS1"))
                lblms2009.Text = IIf((String.IsNullOrEmpty(dr("NUMOF_MSINSURED_YR_MINUS2").ToString())), "N/A", dr("NUMOF_MSINSURED_YR_MINUS2"))
                lblms2008.Text = IIf((String.IsNullOrEmpty(dr("NUMOF_MSINSURED_YR_MINUS3").ToString())), "N/A", dr("NUMOF_MSINSURED_YR_MINUS3"))
                lblms2007.Text = IIf((String.IsNullOrEmpty(dr("NUMOF_MSINSURED_YR_MINUS4").ToString())), "N/A", dr("NUMOF_MSINSURED_YR_MINUS4"))
                lblms2006.Text = IIf((String.IsNullOrEmpty(dr("NUMOF_MSINSURED_YR_MINUS5").ToString())), "N/A", dr("NUMOF_MSINSURED_YR_MINUS5"))
                hlinkRateReview.Text = IIf((String.IsNullOrEmpty(dr("LINK_RATE_INCREASE").ToString())), "", dr("LINK_RATE_INCREASE").ToString().Trim())
                hlinkRateReview.NavigateUrl = IIf((String.IsNullOrEmpty(dr("LINK_RATE_INCREASE").ToString())), "", dr("LINK_RATE_INCREASE").ToString().Trim())

                'hlinkRateReview.Enabled = True
                'hlinkRateReview.Attributes.Add("OnClick", "return confirm('You are now leaving the Mississippi Insurance Department website and being directed to HealthCare.gov, a website operated by\nthe United States Department of Health and Human Services.');")

                'If (hlinkRateReview.Text = "N/A") Then
                '    hlinkRateReview.Enabled = False
                'Else
                '    hlinkRateReview.Enabled = True
                '    hlinkRateReview.Attributes.Add("OnClick", "return confirm('You are now leaving the Mississippi Insurance Department website and being directed to HealthCare.gov, a website operated by\nthe United States Department of Health and Human Services.');")
                'End If

                If dr("NUMOF_MSINSURED_YR_MINUS1").ToString() = "" Then
                    lblms2010.Text = "N/A"
                Else

                    If lblms2010.Text.ToString() <> "N/A" Then
                        Dim intMsInsured2010 As Long = CType(dr("NUMOF_MSINSURED_YR_MINUS1"), Long)
                        If (intMsInsured2010 <> 0) Then
                            lblms2010.Text = (String.Format("{0:#,#}", intMsInsured2010))
                        End If
                    End If

                    End If

                    ' txtNoOfMsInsured2010.Text = dr("NUMOF_MSINSURED_YR_MINUS1").ToString()

                    If dr("NUMOF_MSINSURED_YR_MINUS2").ToString() = "" Then

                        lblms2009.Text = "N/A"
                Else
                    If lblms2009.Text.ToString() <> "N/A" Then
                        Dim intMsInsured2009 As Long = CType(dr("NUMOF_MSINSURED_YR_MINUS2"), Long)
                        If (intMsInsured2009 <> 0) Then
                            lblms2009.Text = (String.Format("{0:#,#}", intMsInsured2009))
                        End If
                    End If
                    End If


                    ' txtNoOfMsInsured2009.Text = dr("NUMOF_MSINSURED_YR_MINUS2").ToString()


                    If dr("NUMOF_MSINSURED_YR_MINUS3").ToString() = "" Then
                        lblms2008.Text = "N/A"
                Else
                    If lblms2008.Text.ToString() <> "N/A" Then
                        Dim intMsInsured2008 As Long = CType(dr("NUMOF_MSINSURED_YR_MINUS3"), Long)
                        If (intMsInsured2008 <> 0) Then
                            lblms2008.Text = (String.Format("{0:#,#}", intMsInsured2008))
                        End If
                    End If
                    End If



                    ' txtNoOfMsInsured2008.Text = dr("NUMOF_MSINSURED_YR_MINUS3").ToString()

                    If dr("NUMOF_MSINSURED_YR_MINUS4").ToString() = "" Then
                        lblms2007.Text = "N/A"
                Else
                    If lblms2007.Text.ToString() <> "N/A" Then
                        Dim intMsInsured2007 As Long = CType(dr("NUMOF_MSINSURED_YR_MINUS4"), Long)
                        If (intMsInsured2007 <> 0) Then
                            lblms2007.Text = (String.Format("{0:#,#}", intMsInsured2007))
                        End If
                    End If
                    End If



                    If dr("NUMOF_MSINSURED_YR_MINUS5").ToString() = "" Then
                        lblms2006.Text = "N/A"
                Else
                    If lblms2006.Text.ToString() <> "N/A" Then
                        Dim intMsInsured2006 As Long = CType(dr("NUMOF_MSINSURED_YR_MINUS5"), Long)
                        If (intMsInsured2006 <> 0) Then
                            lblms2006.Text = (String.Format("{0:#,#}", intMsInsured2006))
                        End If
                    End If
                    End If

                    If (lblprct2010.Text.ToString() <> "N/A") Then
                        Dim prcentYearMinus1 As Decimal = Convert.ToDecimal(lblprct2010.Text)
                        If (prcentYearMinus1 < 0) Then
                            lblprct2010.ForeColor = Drawing.Color.Red

                        End If
                        lblprct2010.Text = lblprct2010.Text.ToString() & "%"
                    End If
                    If (lblprct2009.Text.ToString() <> "N/A") Then
                        Dim prcentYearMinus2 As Decimal = Convert.ToDecimal(lblprct2009.Text)
                        If (prcentYearMinus2 < 0) Then
                            lblprct2009.ForeColor = Drawing.Color.Red
                        End If
                        lblprct2009.Text = lblprct2009.Text.ToString() & "%"
                    End If
                    If (lblprct2008.Text.ToString() <> "N/A") Then
                        Dim prcentYearMinus3 As Decimal = Convert.ToDecimal(lblprct2008.Text)
                        If (prcentYearMinus3 < 0) Then
                            lblprct2008.ForeColor = Drawing.Color.Red
                        End If
                        lblprct2008.Text = lblprct2008.Text.ToString() & "%"
                    End If
                    If (lblprct2007.Text.ToString() <> "N/A") Then
                        Dim prcentYearMinus4 As Decimal = Convert.ToDecimal(lblprct2007.Text)
                        If (prcentYearMinus4 < 0) Then
                            lblprct2007.ForeColor = Drawing.Color.Red
                        End If
                        lblprct2007.Text = lblprct2007.Text.ToString() & "%"
                    End If
                    If (lblprct2006.Text.ToString() <> "N/A") Then
                        Dim prcentYearMinus5 As Decimal = Convert.ToDecimal(lblprct2006.Text)
                        If (prcentYearMinus5 < 0) Then
                            lblprct2006.ForeColor = Drawing.Color.Red
                        End If
                        lblprct2006.Text = lblprct2006.Text.ToString() & "%"
                    End If

                    If (lbllr2010.Text.ToString() <> "N/A") Then
                        Dim lossRatioYearMinus1 As Decimal = Convert.ToDecimal(lbllr2010.Text)
                        If (lossRatioYearMinus1 < 0) Then
                            lbllr2010.ForeColor = Drawing.Color.Red

                        End If
                        lbllr2010.Text = lbllr2010.Text.ToString() & "%"
                    End If

                    If (lbllr2009.Text.ToString() <> "N/A") Then
                        Dim lossRatioYearMinus2 As Decimal = Convert.ToDecimal(lbllr2009.Text)
                        If (lossRatioYearMinus2 < 0) Then
                            lbllr2009.ForeColor = Drawing.Color.Red

                        End If
                        lbllr2009.Text = lbllr2009.Text.ToString() & "%"
                    End If

                    If (lbllr2008.Text.ToString() <> "N/A") Then
                        Dim lossRatioYearMinus3 As Decimal = Convert.ToDecimal(lbllr2008.Text)
                        If (lossRatioYearMinus3 < 0) Then
                            lbllr2008.ForeColor = Drawing.Color.Red

                        End If
                        lbllr2008.Text = lbllr2008.Text.ToString() & "%"
                    End If

                    If (lbllr2007.Text.ToString() <> "N/A") Then
                        Dim lossRatioYearMinus4 As Decimal = Convert.ToDecimal(lbllr2007.Text)
                        If (lossRatioYearMinus4 < 0) Then
                            lbllr2007.ForeColor = Drawing.Color.Red

                        End If
                        lbllr2007.Text = lbllr2007.Text.ToString() & "%"
                    End If

                    If (lbllr2006.Text.ToString() <> "N/A") Then
                        Dim lossRatioYearMinus5 As Decimal = Convert.ToDecimal(lbllr2006.Text)
                        If (lossRatioYearMinus5 < 0) Then
                            lbllr2006.ForeColor = Drawing.Color.Red

                        End If
                        lbllr2006.Text = lbllr2006.Text.ToString() & "%"
                    End If
            Next

        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try

    End Sub

    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReturn.Click
        Try
            Response.Redirect("RateChangesDashBoard.aspx", False)
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnLink_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLink.Click

        Dim aCookie As HttpCookie = New HttpCookie("userInfo")
        aCookie.Expires = DateTime.Now.AddDays(-1)
        Response.Cookies.Add(aCookie)

        Dim strLogout As String = ConfigurationManager.AppSettings("MidRateReview.logout")
        Dim strLogoutUrl As String = strLogout + "/Account/login.aspx"
        Response.Redirect(strLogoutUrl, False)
    End Sub

    Protected Sub btnMidRateReviewHome_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMidRateReviewHome.Click
       
        Dim strLogout As String = ConfigurationManager.AppSettings("CompareRateChanges")
        Dim strLogoutUrl As String = strLogout + "/CompareRateChanges.aspx"
        Response.Redirect(strLogoutUrl, False)
   
    End Sub
End Class