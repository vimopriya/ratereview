﻿Imports System.Web.UI.DataVisualization.Charting
Imports RateChangesBL
Imports System.Drawing

Partial Public Class CompareCarriers
    Inherits System.Web.UI.Page

    Dim userName As String
    Protected RateChangeId As String = String.Empty
    Private Const ERROR_PAGE As String = "ErrorPage.aspx"

#Region "Page Events"

    ''' <summary>
    ''' Handles all the page load events of this page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim isUserExists As Integer = 0
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        Try

            If Not (Request.QueryString("ID") Is Nothing) Then
                RateChangeId = Request.QueryString("ID").ToString()
                DrawChart(RateChangeId)
            Else
                Response.Write("No company has been selected.")
                CompareChart.Enabled = False
            End If

            menuTabs.Items(0).NavigateUrl = "CompareCarriers.aspx?Id=" + RateChangeId
            menuTabs.Items(1).NavigateUrl = "RateChangesDashBoard.aspx?Mode=ChartView&Id=" + RateChangeId
            menuTabs.Items(0).Selected = True
            linkLogout.Visible = False


            If Not (Request.Cookies("userInfo") Is Nothing) Then
                userName = Convert.ToString(Request.Cookies("userInfo")("userName"))

            End If
            If (userName <> "") Then
                isUserExists = rateChangesBL.IsUserExists(userName)
            End If
            If (isUserExists > 0) Then
                linkLogout.Visible = True
            End If
        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try
    End Sub

    ''' <summary>
    ''' Returns the user back to the Dash board page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>5
    Private Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Dim intCompId As Integer = 0
        If Session.Item("compID") <> Nothing Then
            intCompId = CType(Session.Item("compID"), Integer)
        End If
        If intCompId <> 0 Then
            Response.Redirect("RateChangesDashBoard.aspx?compID=" + intCompId.ToString(), False)

        Else
            Response.Redirect("RateChangesDashBoard.aspx")
        End If

    End Sub

    Protected Overrides Sub OnInit(ByVal e As EventArgs)
        InitializeComponent()
        MyBase.OnInit(e)

    End Sub


    Private Sub InitializeComponent()
        AddHandler CompareChart.Customize, AddressOf CompareChart_Customize
    End Sub
    ''' <summary>
    ''' Customizes the labels on both the Y axis.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub CompareChart_Customize(ByVal sender As Object, ByVal e As System.EventArgs)

        ' Get X and Y axis labels collections
        Dim yAxisLabels As CustomLabelsCollection = CompareChart.ChartAreas("ChartArea1").AxisY.CustomLabels
        Dim y2AxisLabels As CustomLabelsCollection = CompareChart.ChartAreas("ChartArea1").AxisY2.CustomLabels

        Try
            ' Change text of the first Y axis label
            If (yAxisLabels.Count > 0) Then
                yAxisLabels(0).Text = ""
            End If
            If y2AxisLabels.Count > 0 Then
                y2AxisLabels(0).Text = ""
            End If

            ' Change Y axis labels
            Dim labelIndex As Integer
            For labelIndex = 1 To yAxisLabels.Count - 1
                yAxisLabels(labelIndex).Text = yAxisLabels(labelIndex).Text + "%"
            Next labelIndex

            For labelIndex = 1 To y2AxisLabels.Count - 1
                y2AxisLabels(labelIndex).Text = y2AxisLabels(labelIndex).Text + "%"
            Next labelIndex


        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try

    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub linkLogout_Click(ByVal sender As Object, ByVal e As EventArgs) Handles linkLogout.Click

        Dim aCookie As HttpCookie = New HttpCookie("userInfo")
        aCookie.Expires = DateTime.Now.AddDays(-1)
        Response.Cookies.Add(aCookie)


        For Each key As String In Request.Cookies.AllKeys
            Dim cookie As HttpCookie = New HttpCookie(key)
            cookie.Expires = DateTime.UtcNow.AddDays(-7)
            Response.Cookies.Add(cookie)
        Next


        If Not (Request.Cookies("userInfo") Is Nothing) Then
            userName = Convert.ToString(Request.Cookies("userInfo")("userName"))

        End If
        Dim strLogout As String = ConfigurationManager.AppSettings("MidRateReview.logout")
        Dim strLogoutUrl As String = strLogout + "/Account/login.aspx"
        Response.Redirect(strLogoutUrl, False)

    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnError(ByVal args As EventArgs)
        Admin.WriteLog(Server.GetLastError())
        Response.Redirect(ERROR_PAGE)
    End Sub

#End Region

#Region "User Defined Methods"

    ''' <summary>
    ''' Fetches the data for and draws the charts 
    ''' </summary>
    ''' <param name="RateChangeId"></param>
    ''' <remarks></remarks>
    Private Sub DrawChart(ByVal RateChangeId As String)

        Dim RateChangeIds() As String
        Dim dtCompanies As DataTable = New DataTable()
        Dim rate As Graphs = New Graphs
        Dim drUtility() As DataRow
        Dim Companynames As String = String.Empty

        Try

            RateChangeIds = RateChangeId.Split(",")

            CompareChart.ChartAreas("ChartArea1").AxisX.Minimum = Today.Year - 6
            CompareChart.ChartAreas("ChartArea1").AxisX.Maximum = Today.Year + 1
            CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = 0
            CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = 0

            '---[Start] Added By Sushil Dhiver on 22/Feb/2012 at 11:30 am
            CompareChart.ChartAreas("ChartArea1").AxisY2.IntervalAutoMode = IntervalAutoMode.VariableCount
            CompareChart.ChartAreas("ChartArea1").AxisY2.InterlacedColor = Color.BlanchedAlmond

            CompareChart.ChartAreas("ChartArea1").IsSameFontSizeForAllAxes = True


            '---[End] Added By Sushil Dhiver on 22/Feb/2012 at 11:30 am

            CompareChart.Series("Series1").ChartType = SeriesChartType.Column
            CompareChart.Series("Series2").ChartType = SeriesChartType.Column
            CompareChart.Series("Series5").ChartType = SeriesChartType.Column
            'CompareChart.Series("Series7").ChartType = SeriesChartType.Column


            CompareChart.Series("Series3").ChartType = SeriesChartType.Line
            CompareChart.Series("Series4").ChartType = SeriesChartType.Line
            CompareChart.Series("Series6").ChartType = SeriesChartType.Line
            'CompareChart.Series("Series8").ChartType = SeriesChartType.Line
            CompareChart.Series("ZeroLine").ChartType = SeriesChartType.Line

            CompareChart.Series("Series3").YAxisType = AxisType.Secondary
            CompareChart.Series("Series4").YAxisType = AxisType.Secondary


            CompareChart.Series("Series3")("ShowMarkerLines") = "True"
            CompareChart.Series("Series4")("ShowMarkerLines") = "True"
            CompareChart.Series("ZeroLine")("ShowMarkerLines") = "True"


            Dim datapoint As System.Web.UI.DataVisualization.Charting.DataPoint = New System.Web.UI.DataVisualization.Charting.DataPoint
            datapoint.XValue = Today.Year - 6
            datapoint.SetValueY(0)
            CompareChart.Series("ZeroLine").Points.Add(datapoint)
            CompareChart.Series("ZeroLine").ShadowOffset = 0 ' Changed by Sushil Dhiver on 22/Feb/2012 at 12:50 pm
            CompareChart.Series("ZeroLine").ChartType = SeriesChartType.Line

            CompareChart.Series("ZeroLine").Points.AddXY(Today.Year + 1, 0)
            CompareChart.Series("ZeroLine").Color = Color.Silver ' Changed by Sushil Dhiver on 22/Feb/2012 at 12:50 pm
            CompareChart.Series("ZeroLine").Enabled = False

            'dtCompanies = rate.GetDataForGraphs(RateChangeId)

            ' displaying the charts dynamically
            DrawDynamicCharts()

            Return

            drUtility = dtCompanies.Select("RATECHANGES_ID =" + RateChangeIds(0))

            If drUtility.Length > 0 Then

                'If (drUtility(0)("DATEOF_YR_MINUS1_RATEINC").ToString()) = "2001" Then

                '    drUtility(0)("DATEOF_YR_MINUS1_RATEINC") = 0

                'End If
                'If (drUtility(0)("DATEOF_YR_MINUS2_RATEINC").ToString()) = "2001" Then

                '    drUtility(0)("DATEOF_YR_MINUS2_RATEINC") = 0

                'End If

                'If (drUtility(0)("DATEOF_YR_MINUS3_RATEINC").ToString()) = "2001" Then

                '    drUtility(0)("DATEOF_YR_MINUS3_RATEINC") = 0

                'End If

                'If (drUtility(0)("DATEOF_YR_MINUS4_RATEINC").ToString()) = "2001" Then

                '    drUtility(0)("DATEOF_YR_MINUS4_RATEINC") = 0

                'End If

                'If (drUtility(0)("DATEOF_YR_MINUS5_RATEINC").ToString()) = "2001" Then

                '    drUtility(0)("DATEOF_YR_MINUS5_RATEINC") = 0

                'End If


                CompareChart.Series("Series1").LegendText = String.Concat(drUtility(0)("CompanyName"), " Rate Increase Percentage")
                CompareChart.Series("Series3").LegendText = String.Concat(drUtility(0)("CompanyName"), " Loss Ratio")

                ''Fetch points for last 5 years
                'Percentage graph plot.
                CompareChart.Series("Series1").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_CURRENTRATEINCR").ToString()), Double.Parse(drUtility(0)("PERCENT_CURRENT_RATEINC").ToString()))

                If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS1_RATEINC").ToString()) Then

                    If Convert.ToDecimal(drUtility(0)("PERCENT_YR_MINUS1_RATEINC").ToString()) <> 0 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                        If (drUtility(0)("PERCENT_YR_MINUS1_RATEINC") < 0) Then
                            CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                            CompareChart.Series("ZeroLine").Enabled = True
                        End If
                        CompareChart.Series("Series1").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS1_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS1_RATEINC").ToString()))

                    End If

                End If

                If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS2_RATEINC").ToString()) Then

                    If Convert.ToDecimal((drUtility(0)("PERCENT_YR_MINUS2_RATEINC").ToString())) <> 0 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                        If (drUtility(0)("PERCENT_YR_MINUS2_RATEINC") < 0) Then
                            CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                            CompareChart.Series("ZeroLine").Enabled = True

                        End If
                        CompareChart.Series("Series1").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS2_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS2_RATEINC").ToString()))

                    End If

                End If

                If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS3_RATEINC").ToString()) Then

                    If Convert.ToDecimal(drUtility(0)("PERCENT_YR_MINUS3_RATEINC").ToString()) <> 0 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                        If (drUtility(0)("PERCENT_YR_MINUS3_RATEINC") < 0) Then
                            CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                            CompareChart.Series("ZeroLine").Enabled = True
                        End If
                        CompareChart.Series("Series1").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS3_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS3_RATEINC").ToString()))
                    End If

                End If

                If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS4_RATEINC").ToString()) Then

                    If Convert.ToDecimal(drUtility(0)("PERCENT_YR_MINUS4_RATEINC").ToString()) <> 0 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                        If (drUtility(0)("PERCENT_YR_MINUS4_RATEINC") < 0) Then
                            CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                            CompareChart.Series("ZeroLine").Enabled = True
                        End If
                        CompareChart.Series("Series1").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS4_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS4_RATEINC").ToString()))

                    End If

                End If

                If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS5_RATEINC").ToString()) Then

                    If Convert.ToDecimal(drUtility(0)("PERCENT_YR_MINUS5_RATEINC").ToString()) <> 0 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                        If (drUtility(0)("PERCENT_YR_MINUS5_RATEINC") < 0) Then
                            CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                            CompareChart.Series("ZeroLine").Enabled = True
                        End If
                        CompareChart.Series("Series1").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS5_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS5_RATEINC").ToString()))

                    End If

                End If

                '' Loss ratio
                If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS1_LOSSRATIO").ToString()) Then

                    If Convert.ToDecimal(drUtility(0)("LAST_MINUS1_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS1_RATEINC").ToString()) <> 2001 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                        If (drUtility(0)("LAST_MINUS1_LOSSRATIO") < 0) Then
                            CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                            CompareChart.Series("ZeroLine").Enabled = True

                        End If

                        CompareChart.Series("Series3").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS1_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS1_LOSSRATIO").ToString()))

                    End If

                End If

                If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS2_LOSSRATIO").ToString()) Then

                    If Convert.ToDecimal(drUtility(0)("LAST_MINUS2_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS2_RATEINC").ToString()) <> 2001 Then  ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                        If (drUtility(0)("LAST_MINUS2_LOSSRATIO") < 0) Then
                            CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                            CompareChart.Series("ZeroLine").Enabled = True
                        End If
                        CompareChart.Series("Series3").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS2_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS2_LOSSRATIO").ToString()))

                    End If

                End If

                If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS3_LOSSRATIO").ToString()) Then

                    If Convert.ToDecimal(drUtility(0)("LAST_MINUS3_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS3_RATEINC").ToString()) <> 2001 Then  ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                        If (drUtility(0)("LAST_MINUS3_LOSSRATIO") < 0) Then
                            CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                            CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                            CompareChart.Series("ZeroLine").Enabled = True
                        End If
                        CompareChart.Series("Series3").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS3_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS3_LOSSRATIO").ToString()))

                    End If

                End If

                If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS4_LOSSRATIO").ToString()) Then

                    If Convert.ToDecimal(drUtility(0)("LAST_MINUS4_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS4_RATEINC").ToString()) <> 2001 Then  ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                        If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS4_LOSSRATIO").ToString()) Then
                            If (drUtility(0)("LAST_MINUS4_LOSSRATIO") < 0) Then
                                CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                CompareChart.Series("ZeroLine").Enabled = True
                            End If
                            CompareChart.Series("Series3").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS4_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS4_LOSSRATIO").ToString()))
                        End If

                    End If

                    If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS5_LOSSRATIO").ToString()) Then

                        If Convert.ToDecimal(drUtility(0)("LAST_MINUS5_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS5_RATEINC").ToString()) <> 2001 Then  ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                            If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS5_LOSSRATIO").ToString()) Then
                                If (drUtility(0)("LAST_MINUS5_LOSSRATIO") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                            End If
                            CompareChart.Series("Series3").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS5_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS5_LOSSRATIO").ToString()))

                        End If

                    End If
                    If Not String.IsNullOrEmpty(drUtility(0)("COMPANY_ID").ToString()) Then
                        Session("CompId") = drUtility(0)("COMPANY_ID").ToString()
                    End If
                    Companynames = String.Concat(drUtility(0)("CompanyName").ToString(), " - ", drUtility(0)("POLICY_FORMNO").ToString(), " - ", drUtility(0)("POLICY_TYPE").ToString())
                End If

                'Display the 2nd company details only if another company is selected
                If RateChangeIds.Length > 1 Then
                    drUtility = dtCompanies.Select("RATECHANGES_ID =" + RateChangeIds(1))

                    If drUtility.Length > 0 Then
                        CompareChart.Series("Series2").LegendText = String.Concat(drUtility(0)("CompanyName"), " Rate Increase Percentage")
                        CompareChart.Series("Series4").LegendText = String.Concat(drUtility(0)("CompanyName"), " Loss Ratio")

                        ''Fetch points for last 5 years
                        CompareChart.Series("Series2").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_CURRENTRATEINCR").ToString()), Double.Parse(drUtility(0)("PERCENT_CURRENT_RATEINC").ToString()))

                        If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS1_RATEINC").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("PERCENT_YR_MINUS1_RATEINC").ToString()) <> 0 Then  ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm
                                If (drUtility(0)("PERCENT_YR_MINUS1_RATEINC") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If

                                CompareChart.Series("Series2").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS1_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS1_RATEINC").ToString()))

                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS2_RATEINC").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("PERCENT_YR_MINUS2_RATEINC").ToString()) <> 0 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                                If (drUtility(0)("PERCENT_YR_MINUS2_RATEINC") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series("Series2").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS2_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS2_RATEINC").ToString()))

                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS3_RATEINC").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("PERCENT_YR_MINUS3_RATEINC").ToString()) <> 0 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                                If (drUtility(0)("PERCENT_YR_MINUS3_RATEINC") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series("Series2").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS3_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS3_RATEINC").ToString()))

                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS4_RATEINC").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("PERCENT_YR_MINUS4_RATEINC").ToString()) <> 0 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                                If (drUtility(0)("PERCENT_YR_MINUS4_RATEINC") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series("Series2").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS4_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS4_RATEINC").ToString()))

                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS5_RATEINC").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("PERCENT_YR_MINUS5_RATEINC").ToString()) <> 0 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm
                                If (drUtility(0)("PERCENT_YR_MINUS5_RATEINC") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series("Series2").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS5_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS5_RATEINC").ToString()))
                            End If

                        End If

                        '' Loss ratio
                        If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS1_LOSSRATIO").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("LAST_MINUS1_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS1_RATEINC").ToString()) <> 2001 Then   ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm
                                If (drUtility(0)("LAST_MINUS1_LOSSRATIO") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series("Series4").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS1_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS1_LOSSRATIO").ToString()))
                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS2_LOSSRATIO").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("LAST_MINUS2_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS2_RATEINC").ToString()) <> 2001 Then  ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm
                                If (drUtility(0)("LAST_MINUS2_LOSSRATIO") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series("Series4").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS2_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS2_LOSSRATIO").ToString()))
                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS3_LOSSRATIO").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("LAST_MINUS3_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS3_RATEINC").ToString()) <> 2001 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm
                                If (drUtility(0)("LAST_MINUS3_LOSSRATIO") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series("Series4").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS3_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS3_LOSSRATIO").ToString()))

                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS4_LOSSRATIO").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("LAST_MINUS4_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS4_RATEINC").ToString()) <> 2001 Then  ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm
                                If (drUtility(0)("LAST_MINUS4_LOSSRATIO") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series("Series4").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS4_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS4_LOSSRATIO").ToString()))
                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS5_LOSSRATIO").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("LAST_MINUS5_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS5_RATEINC").ToString()) <> 2001 Then  ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                                If (drUtility(0)("LAST_MINUS5_LOSSRATIO") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series("Series4").Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS5_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS5_LOSSRATIO").ToString()))

                            End If

                        End If

                        Companynames = String.Concat(Companynames, " And ", drUtility(0)("CompanyName").ToString(), " - ", drUtility(0)("POLICY_FORMNO").ToString(), " - ", drUtility(0)("POLICY_TYPE").ToString())
                        If Not String.IsNullOrEmpty(drUtility(0)("COMPANY_ID").ToString()) Then
                            Session("CompId") = drUtility(0)("COMPANY_ID").ToString()
                        End If

                    End If
                Else
                    CompareChart.Series("Series2").Enabled = False
                    CompareChart.Series("Series4").Enabled = False

                End If

                headingSpan.InnerHtml = String.Concat("Rate Comparison of ", Companynames)
                Page.Title = String.Concat("Rate Comparison of ", Companynames)
            End If
        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        Finally
            drUtility = Nothing
            RateChangeIds = Nothing
        End Try

    End Sub

    ''' <summary>
    ''' DrawDynamicCharts
    ''' </summary>
    ''' <param name="DrawDynamicCharts"></param>
    ''' <remarks></remarks>
    Private Sub DrawDynamicCharts()
        Dim RateChangeIds() As String
        Dim dtCompanies As DataTable = New DataTable()
        Dim rate As Graphs = New Graphs
        Dim drUtility() As DataRow
        Dim Companynames As String = String.Empty
        Dim SeriesType As String = String.Empty
        Dim SeriesType1 As String = String.Empty

        Try


            RateChangeIds = RateChangeId.Split(",")

            dtCompanies = rate.GetDataForGraphs(RateChangeId)

            If RateChangeIds.Length > 0 Then

                For i = 0 To RateChangeIds.Length - 1

                    drUtility = dtCompanies.Select("RATECHANGES_ID =" + RateChangeIds(i))

                    If i = 0 Then
                        SeriesType = "Series1"
                        SeriesType1 = "Series3"
                    ElseIf i = 1 Then
                        SeriesType = "Series2"
                        SeriesType1 = "Series4"
                    ElseIf i = 2 Then
                        SeriesType = "Series5"
                        SeriesType1 = "Series6"
                    ElseIf i = 3 Then
                        SeriesType = "Series7"
                        SeriesType1 = "Series8"
                    End If

                    If drUtility.Length > 0 Then

                        CompareChart.Series(SeriesType).Enabled = True
                        CompareChart.Series(SeriesType1).Enabled = True

                        CompareChart.Series(SeriesType).LegendText = String.Concat(drUtility(0)("CompanyName"), " Rate Increase Percentage")
                        CompareChart.Series(SeriesType1).LegendText = String.Concat(drUtility(0)("CompanyName"), " Loss Ratio")


                        If (drUtility(0)("DATEOF_YR_MINUS1_RATEINC").ToString()) = "2001" Then

                            drUtility(0)("DATEOF_YR_MINUS1_RATEINC") = 0

                        End If
                        If (drUtility(0)("DATEOF_YR_MINUS2_RATEINC").ToString()) = "2001" Then

                            drUtility(0)("DATEOF_YR_MINUS2_RATEINC") = 0

                        End If

                        If (drUtility(0)("DATEOF_YR_MINUS3_RATEINC").ToString()) = "2001" Then

                            drUtility(0)("DATEOF_YR_MINUS3_RATEINC") = 0

                        End If

                        If (drUtility(0)("DATEOF_YR_MINUS4_RATEINC").ToString()) = "2001" Then

                            drUtility(0)("DATEOF_YR_MINUS4_RATEINC") = 0

                        End If

                        If (drUtility(0)("DATEOF_YR_MINUS5_RATEINC").ToString()) = "2001" Then

                            drUtility(0)("DATEOF_YR_MINUS5_RATEINC") = 0

                        End If

                        ''Fetch points for last 5 years
                        CompareChart.Series(SeriesType).Points.AddXY(Double.Parse(drUtility(0)("DATEOF_CURRENTRATEINCR").ToString()), Double.Parse(drUtility(0)("PERCENT_CURRENT_RATEINC").ToString()))

                        If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS1_RATEINC").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("PERCENT_YR_MINUS1_RATEINC").ToString()) <> 0 Then  ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm
                                If (drUtility(0)("PERCENT_YR_MINUS1_RATEINC") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If

                                CompareChart.Series(SeriesType).Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS1_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS1_RATEINC").ToString()))

                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS2_RATEINC").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("PERCENT_YR_MINUS2_RATEINC").ToString()) <> 0 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                                If (drUtility(0)("PERCENT_YR_MINUS2_RATEINC") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series(SeriesType).Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS2_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS2_RATEINC").ToString()))

                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS3_RATEINC").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("PERCENT_YR_MINUS3_RATEINC").ToString()) <> 0 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                                If (drUtility(0)("PERCENT_YR_MINUS3_RATEINC") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series(SeriesType).Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS3_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS3_RATEINC").ToString()))

                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS4_RATEINC").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("PERCENT_YR_MINUS4_RATEINC").ToString()) <> 0 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                                If (drUtility(0)("PERCENT_YR_MINUS4_RATEINC") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series(SeriesType).Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS4_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS4_RATEINC").ToString()))

                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("PERCENT_YR_MINUS5_RATEINC").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("PERCENT_YR_MINUS5_RATEINC").ToString()) <> 0 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm
                                If (drUtility(0)("PERCENT_YR_MINUS5_RATEINC") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series(SeriesType).Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS5_RATEINC").ToString()), Double.Parse(drUtility(0)("PERCENT_YR_MINUS5_RATEINC").ToString()))
                            End If

                        End If

                        '' Loss ratio
                        If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS1_LOSSRATIO").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("LAST_MINUS1_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS1_RATEINC").ToString()) <> 0 Then   ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm
                                If (drUtility(0)("LAST_MINUS1_LOSSRATIO") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series(SeriesType1).Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS1_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS1_LOSSRATIO").ToString()))
                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS2_LOSSRATIO").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("LAST_MINUS2_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS2_RATEINC").ToString()) <> 0 Then  ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm
                                If (drUtility(0)("LAST_MINUS2_LOSSRATIO") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series(SeriesType1).Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS2_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS2_LOSSRATIO").ToString()))
                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS3_LOSSRATIO").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("LAST_MINUS3_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS3_RATEINC").ToString()) <> 0 Then ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm
                                If (drUtility(0)("LAST_MINUS3_LOSSRATIO") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series(SeriesType1).Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS3_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS3_LOSSRATIO").ToString()))

                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS4_LOSSRATIO").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("LAST_MINUS4_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS4_RATEINC").ToString()) <> 0 Then  ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm
                                If (drUtility(0)("LAST_MINUS4_LOSSRATIO") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series(SeriesType1).Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS4_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS4_LOSSRATIO").ToString()))
                            End If

                        End If

                        If Not String.IsNullOrEmpty(drUtility(0)("LAST_MINUS5_LOSSRATIO").ToString()) Then

                            If Convert.ToDecimal(drUtility(0)("LAST_MINUS5_LOSSRATIO").ToString()) <> 0 And Convert.ToDecimal(drUtility(0)("DATEOF_YR_MINUS5_RATEINC").ToString()) <> 0 Then  ' Added By Sushil Dhiver on 24/Feb/2012 at 7:40 pm

                                If (drUtility(0)("LAST_MINUS5_LOSSRATIO") < 0) Then
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY.Maximum = 100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Minimum = -100
                                    CompareChart.ChartAreas("ChartArea1").AxisY2.Maximum = 100
                                    CompareChart.Series("ZeroLine").Enabled = True
                                End If
                                CompareChart.Series(SeriesType1).Points.AddXY(Double.Parse(drUtility(0)("DATEOF_YR_MINUS5_RATEINC").ToString()), Double.Parse(drUtility(0)("LAST_MINUS5_LOSSRATIO").ToString()))

                            End If

                        End If

                        Companynames = String.Concat(Companynames, " And ", drUtility(0)("CompanyName").ToString(), " - ", drUtility(0)("POLICY_FORMNO").ToString(), " - ", drUtility(0)("POLICY_TYPE").ToString())
                        If Not String.IsNullOrEmpty(drUtility(0)("COMPANY_ID").ToString()) Then
                            Session("CompId") = drUtility(0)("COMPANY_ID").ToString()
                        End If

                    End If

                Next

            End If

        Catch ex As Exception
            Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        Finally
            drUtility = Nothing
            RateChangeIds = Nothing
        End Try


    End Sub


#End Region


End Class