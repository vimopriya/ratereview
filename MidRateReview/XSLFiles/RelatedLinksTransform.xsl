<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" />

  <xsl:template match="/">

    <html>
      <head>
        <link rel="stylesheet" type="text/css" href="../css/main.css" />
      </head>
    </html>
    <ul id="lbptext">
      <xsl:for-each select="rss/channel/item[category='RelatedLinks']">
        <li>

          <xsl:element name="a">
            <xsl:attribute name="href">
              <xsl:value-of select="link" />
            </xsl:attribute>
            <xsl:attribute name="target">blank</xsl:attribute>
            <span class="blog-url">
              <xsl:value-of select="title" />
            </span>
          </xsl:element>
        </li>
      </xsl:for-each>
    </ul>
  </xsl:template>

  <xsl:template name="getDate">
    <xsl:param name="dateTime" />
    <xsl:value-of select="substring($dateTime,9,4)" />
    <xsl:value-of select="substring($dateTime,6,2)" />,
    <xsl:value-of select="substring($dateTime,13,5)" />
  </xsl:template>

</xsl:stylesheet>