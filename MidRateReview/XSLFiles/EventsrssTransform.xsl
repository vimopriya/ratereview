<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" />


  

  <xsl:template match="/" >
   
    <ul >
      <xsl:for-each select="rss/channel/item[category='Events Calendar'][1]">
        <li>
          <xsl:element name="a">
            <xsl:attribute name="href">
              <xsl:value-of select="link" />
            </xsl:attribute>
			<xsl:attribute name="target">blank</xsl:attribute>  
            <span class="blog-url">
              <xsl:value-of select="title" />
            </span>
          </xsl:element>
          <br />

          <xsl:element name ="text">
            <span class="PageText">
              <xsl:value-of select="substring-after(description,'&lt;p&gt;')"  disable-output-escaping="yes"/>
            </span>
          </xsl:element>
        </li>
        
        
      </xsl:for-each>
    </ul>
    
  </xsl:template>

  
</xsl:stylesheet>