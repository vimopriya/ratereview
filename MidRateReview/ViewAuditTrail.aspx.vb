﻿Public Partial Class WebForm3
    Inherits System.Web.UI.Page
    Dim isShowDetail As Boolean = False
    Private Const ERROR_PAGE As String = "ErrorPage.aspx"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Page.IsPostBack) Then
                ApplyPaging()
            Else
                Dim audirTrailDs As New DataSet
                Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
                audirTrailDs = rateChangesBL.GetAuditTrail()
                gridAuditTrail.DataSource = audirTrailDs
                If (audirTrailDs.Tables.Count > 0) Then
                    gridAuditTrail.DataBind()
                End If


            End If
       
        Catch ex As Exception
            RateChangesBL.Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridAuditTrail_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridAuditTrail.PageIndexChanging
        Try
            If e.NewPageIndex <> -1 Then
                gridAuditTrail.PageIndex = e.NewPageIndex
            End If
            btnSearch_Click(sender, e)
        Catch ex As Exception
            RateChangesBL.Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try
    End Sub


    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Response.Redirect("RateChangesDashBoard.aspx")
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click

        Dim audirTrailDs As New DataSet
        Dim strActionBy As String = ""
        Dim strFromDate As String = ""
        Dim strToDate As String = ""
        Try

            strActionBy = txtActionBy.Text.ToString()
            strFromDate = txtFromDate.Text
            strToDate = txtToDate.Text

            Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
            If (txtActionBy.Text = "") Then
                strActionBy = "All"
            End If
            If (txtFromDate.Text = "") Then
                strFromDate = "All"
            End If
            If (txtToDate.Text = "") Then
                strToDate = "All"
            End If
            If (drpListAction.SelectedValue.ToString() = "All" And strActionBy = "All" And strFromDate = "All" And strToDate = "All") Then
                audirTrailDs = rateChangesBL.GetAuditTrail()
            Else
                audirTrailDs = rateChangesBL.GetAuditTrailBySearch(drpListAction.SelectedValue.ToString(), strActionBy, strFromDate, strToDate)

            End If

         
            gridAuditTrail.DataSource = audirTrailDs
            If (audirTrailDs.Tables.Count > 0) Then
                gridAuditTrail.DataBind()
            End If
        Catch ex As Exception
            RateChangesBL.Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try

    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnError(ByVal args As EventArgs)
        RateChangesBL.Admin.WriteLog(Server.GetLastError())
        Response.Redirect(ERROR_PAGE)
    End Sub

    Protected Sub drpListAction_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles drpListAction.SelectedIndexChanged
        Dim audirTrailDs As New DataSet
        Dim strActionBy As String = ""
        Dim strFromDate As String = ""
        Dim strToDate As String = ""
        Try

            strActionBy = txtActionBy.Text.ToString()
            strFromDate = txtFromDate.Text
            strToDate = txtToDate.Text

            Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
            If (txtActionBy.Text = "") Then
                strActionBy = "All"
            End If
            If (txtFromDate.Text = "") Then
                strFromDate = "All"
            End If
            If (txtToDate.Text = "") Then
                strToDate = "All"
            End If
            audirTrailDs = rateChangesBL.GetAuditTrailBySearch(drpListAction.SelectedValue.ToString(), strActionBy, strFromDate, strToDate)

            gridAuditTrail.DataSource = audirTrailDs
            If (audirTrailDs.Tables.Count > 0) Then
                gridAuditTrail.DataBind()
            End If
        Catch ex As Exception
            RateChangesBL.Admin.WriteLog(ex)
            Response.Redirect(ERROR_PAGE)
        End Try
    End Sub
    Sub ApplyPaging()

        Dim row As GridViewRow = gridAuditTrail.BottomPagerRow
        Dim ph As PlaceHolder = New PlaceHolder
        Dim lnkPaging As LinkButton
        Dim lnkFirstPage As LinkButton
        Dim lnkPrevPage As LinkButton
        Dim lnkNextPage As LinkButton
        Dim lnkLastPage As LinkButton
        lnkFirstPage = New LinkButton()
        lnkFirstPage.Text = Server.HtmlEncode("<<First")
        lnkFirstPage.Width = Unit.Pixel(50)
        lnkFirstPage.CommandName = "Page"
        lnkFirstPage.CommandArgument = "First"
        lnkPrevPage = New LinkButton()
        lnkPrevPage.Text = Server.HtmlEncode("<Prev")
        lnkPrevPage.Width = Unit.Pixel(50)
        lnkPrevPage.CommandName = "Page"
        lnkPrevPage.CommandArgument = "Prev"

        If (gridAuditTrail.PageCount <> 0) Then
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkFirstPage)
            ph.Controls.Add(lnkPrevPage)
        End If
        If (gridAuditTrail.PageIndex = 0) Then

            lnkFirstPage.Enabled = False
            lnkPrevPage.Enabled = False
        Else
            lnkPrevPage.Enabled = True
        End If


        Dim i As Integer
        For i = 1 To gridAuditTrail.PageCount

            lnkPaging = New LinkButton()
            lnkPaging.Width = Unit.Pixel(20)
            ' lnkPaging.CssClass = "LinkPaging"
            lnkPaging.Text = i.ToString()
            lnkPaging.CommandName = "Page"
            lnkPaging.CommandArgument = i.ToString()
            ' If (i = gridRateChanges.PageIndex + 1) Then
            ' lnkPaging.BackColor = System.Drawing.Color.Blue
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkPaging)
            ' End If

        Next i

        lnkNextPage = New LinkButton()
        lnkNextPage.Text = Server.HtmlEncode("Next>")
        lnkNextPage.Width = Unit.Pixel(50)
        lnkNextPage.CommandName = "Page"
        lnkNextPage.CommandArgument = "Next"
        lnkLastPage = New LinkButton()
        lnkLastPage.Text = Server.HtmlEncode("Last>>")
        lnkLastPage.Width = Unit.Pixel(50)
        lnkLastPage.CommandName = "Page"
        lnkLastPage.CommandArgument = "Last"
        lnkNextPage.Enabled = True
        lnkLastPage.Enabled = True
        If (gridAuditTrail.PageCount <> 0) Then
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkNextPage)
            ph = CType(row.FindControl("ph"), PlaceHolder)
            ph.Controls.Add(lnkLastPage)
        End If
        If (gridAuditTrail.PageIndex + 1 = gridAuditTrail.PageCount) Then

            lnkNextPage.Enabled = False
            lnkLastPage.Enabled = False
        End If
    End Sub
    
    Protected Sub gridAuditTrail_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles gridAuditTrail.DataBound
        ApplyPaging()
    End Sub

    Protected Sub gridAuditTrail_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gridAuditTrail.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            gridAuditTrail.PagerSettings.Mode = PagerButtons.NumericFirstLast
            gridAuditTrail.PagerSettings.FirstPageText = "First"
            gridAuditTrail.PagerSettings.LastPageText = "Last"
            gridAuditTrail.PagerSettings.NextPageText = "Next"
            gridAuditTrail.PagerSettings.PreviousPageText = "Previous"
        End If

    End Sub

    Private Sub btnLink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLink.Click
        Dim aCookie As HttpCookie = New HttpCookie("userInfo")
        aCookie.Expires = DateTime.Now.AddDays(-1)
        Response.Cookies.Add(aCookie)

        If Not (Request.Cookies("userInfo") Is Nothing) Then
            Dim userName As String = Convert.ToString(Request.Cookies("userInfo")("userName"))

        End If
        Dim strLogout As String = ConfigurationManager.AppSettings("MidRateReview.logout")
        Dim strLogoutUrl As String = strLogout + "/Account/login.aspx"
        Response.Redirect(strLogoutUrl, False)
    End Sub
End Class