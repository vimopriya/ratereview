﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="CompareCarriers.aspx.vb" Inherits="MidWebApp.CompareCarriers" 
    %>
<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <link href="css/grid.css" rel="stylesheet" media="screen" type="text/css" />

    <div id="page_header3">
             <span class="h2-form" id="headingSpan" runat="server"></span>  
             <asp:LinkButton ID="linkLogout" CssClass="logout" runat="server">LogOut</asp:LinkButton>
    </div>    
    <div class="spacer"></div>

       
       <div class="tabwrap col_16x">
           <asp:Button ID="btnReturn" runat="server" CssClass="btn-form left margin0" Text="Return To Search Results" 
     CausesValidation="false"/>
    <asp:Menu id="menuTabs"  CssClass="menuTabs" StaticMenuItemStyle-CssClass="tab-chart" StaticSelectedStyle-CssClass="selectedTab-graph" Orientation="Horizontal"        
            Runat="server"   >
             <Items>
                <asp:MenuItem Value="Graph" Text=""/>
                <asp:MenuItem Value="GridView" Text="" />   
            </Items>
    </asp:Menu>
       </div>
    <div class="col_16x left gutter20-t center">
       <asp:CHART id="CompareChart" runat="server" Height="600px" Width="800px" ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)" ImageType="Png" Palette="BrightPastel" BackColor="#D3DFF0" BorderDashStyle="Solid" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
			<titles>
				<asp:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3"  ForeColor="26, 59, 105"></asp:Title>
			</titles>
			<legends>
				<asp:Legend LegendStyle="Column" IsTextAutoFit="False" Docking="Bottom" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8.25pt, style=Bold" Alignment="Center"></asp:Legend>
			</legends>
			<borderskin SkinStyle="Emboss"></borderskin>
			<series>
				<asp:Series  Name="Series1" IsValueShownAsLabel="True" CustomProperties="LabelStyle=Bottom" BorderColor="180, 26, 59, 105" Color="220, 224, 64, 10"></asp:Series>
				<asp:Series Name="Series2" IsValueShownAsLabel="True" CustomProperties="LabelStyle=Bottom" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"></asp:Series>
				<asp:Series MarkerSize="10" Name="Series3" BorderColor="180, 26, 59, 105" Color="220, 224, 64, 10" MarkerStyle="Circle" ShadowColor="Black" ShadowOffset="1"></asp:Series>
				<asp:Series MarkerSize="10" Name="Series4" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240" MarkerStyle="Circle" ShadowColor="Black" ShadowOffset="1"></asp:Series>
				<asp:Series MarkerSize="10" Name="Series5" IsValueShownAsLabel="True" BorderColor="180, 26, 59, 105" Color="#4cb848" ShadowColor="Black" ShadowOffset="1"></asp:Series>
				<asp:Series MarkerSize="10" Name="Series6" IsValueShownAsLabel="True" BorderColor="180, 26, 59, 105" Color="Purple" MarkerStyle="Circle" ShadowColor="Black" ShadowOffset="1"></asp:Series>
				<%--<asp:Series MarkerSize="10" Name="Series7" IsValueShownAsLabel="True" BorderColor="180, 26, 59, 105" Color="#0024e1" ShadowColor="Black" ShadowOffset="1"></asp:Series>
				<asp:Series MarkerSize="10" Name="Series8" IsValueShownAsLabel="True" BorderColor="180, 26, 59, 105" Color="#b7b7d1" MarkerStyle="Circle" ShadowColor="Black" ShadowOffset="1"></asp:Series>--%>
				<asp:Series MarkerSize="10" Name="ZeroLine"  IsVisibleInLegend = "false" BorderColor="180, 26, 59, 105" Color="220, 224, 64, 10"   ShadowColor="Black" ShadowOffset="1"></asp:Series>
				
			</series>
			<chartareas>
				<asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="64, 165, 191, 228" ShadowColor="Transparent" BackGradientStyle="TopBottom">
					<axisy2 IsLabelAutoFit="False" Interval="10" Title="Loss Ratio" TitleForeColor="180, 26, 59, 105">
						<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
						<majorgrid enabled="False" />
					</axisy2>
					<area3dstyle Rotation="10" Perspective="10" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
					
					<axisy LineColor="64, 64, 64, 64" Title="Rate Increase Percentage" TitleForeColor="180, 26, 59, 105">
						<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
						<MajorGrid LineColor="64, 64, 64, 64" />
					</axisy>
					<axisx LineColor="64, 64, 64, 64" >
						<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
						<MajorGrid LineColor="64, 64, 64, 64" />
					</axisx>
				</asp:ChartArea>
			</chartareas>
   </asp:CHART>
        <ul class="list-hrz right gutter10 links-secondary">
            <li>
               
                <asp:HyperLink ID="hLinkResource" runat="server" NavigateUrl = "~/Resources.aspx" target="_blank">Resources</asp:HyperLink>
              
            </li>
            <li>
             <asp:HyperLink ID="hLinkFAQ" runat="server" NavigateUrl = "/MidBlog/category/FAQ.aspx">FAQ&#39;s</asp:HyperLink>
              
            </li>
            <li>
             <asp:HyperLink ID="hLinkFeedBack" runat="server"  OnClick = "return confirm('All nonpublic personal health information and nonpublic personal financial information shall be held confidential by Mississippi Insurance Department.')" NavigateUrl = "mailto:ratehelp@mid.state.ms.us" target="_blank" >Submit a Comment</asp:HyperLink>
               
            </li>
        </ul>
   </div>

</asp:Content>
