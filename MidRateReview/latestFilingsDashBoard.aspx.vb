﻿Partial Public Class latestFilingsDashBoard
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim rateChangesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        rateChangesDs = rateChangesBL.GetRateFilingsData()




        If (rateChangesDs.Tables.Count > 0) Then
            Dim dr As DataRow
            Dim dt As DataTable = rateChangesDs.Tables(0)
            If Not (LatestFilingsGridView Is Nothing) Then


                For Each dr In dt.Rows
                    If Not IsDBNull(dr("DATE_SUBMITTED")) Then
                        If (dr("DATE_SUBMITTED").ToString() = "01/01/0001") Or (dr("DATE_SUBMITTED").ToString() = "01/01/2001") Then
                            dr("DATE_SUBMITTED") = "N/A"
                        End If
                    Else
                        dr("DATE_SUBMITTED") = "N/A"
                    End If



                Next
            End If
        End If

        LatestFilingsGridView.DataSource = rateChangesDs
        LatestFilingsGridView.DataBind()
        'Dim dr As DataRow
        'Dim dt As DataTable = rateChangesDs.Tables(0)
        'If Not (LatestFilingsGridView Is Nothing) Then


        '    For Each dr In dt.Rows

        '        If (dr("LINK_RATE_INCREASE").ToString() <> "N/A") Then
        '            Dim nameLink As HyperLinkField = TryCast(LatestFilingsGridView.Columns(2), HyperLinkField)
        '            nameLink.NavigateUrl = dr("LINK_RATE_INCREASE").ToString()
        '        End If

        '    Next
        'End If
    End Sub

    Protected Sub LatestFilingsGridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles LatestFilingsGridView.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim rowView As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
            Dim hLink As HyperLink = e.Row.FindControl("hLinkRateInc")
            hLink.Text = rowView("LINK_RATE_INCREASE").ToString().Trim()
            hLink.NavigateUrl = rowView("LINK_RATE_INCREASE").ToString().Trim()
            hLink.Attributes.Add("OnClick", "return confirm('You are now leaving the Mississippi Insurance Department website and being directed to HealthCare.gov, a website operated by\nthe United States Department of Health and Human Services.');")

        End If
        ''Changes text in the first column into HyperLinks
        'Dim nameLink As HyperLinkField = TryCast(LatestFilingsGridView.Columns(2), HyperLinkField)
        'Dim ds As DataSet
        'Dim dr As DataRow
        'ds = LatestFilingsGridView.DataSource
        'Dim dt As DataTable = ds.Tables(0)
        'If Not (LatestFilingsGridView Is Nothing) Then


        '    For Each dr In dt.Rows
        '        ' If e.Row.RowType = DataControlRowType.DataRow Then

        '        nameLink.NavigateUrl = dr("LINK_RATE_INCREASE").ToString()

        '        ' e.Row.Controls.Add(CType(nameLink, System.Web.UI.Control))

        '        ' End If
        '    Next
        'End If

    End Sub


  

    Protected Sub btnMidBlog_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMidBlog.Click
        Dim strMidHome As String = ConfigurationManager.AppSettings("CompareRateChanges")
        Dim strLogoutUrl As String = strMidHome + "/CompareRateChanges.aspx"
        Response.Redirect(strLogoutUrl, False)
    End Sub
End Class