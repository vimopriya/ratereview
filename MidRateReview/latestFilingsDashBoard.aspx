﻿
    <%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile ="~/MasterPage.master" CodeBehind="latestFilingsdashBoard.aspx.vb" Inherits="MidWebApp.latestFilingsDashBoard" 
   title="Mississippi Rate Reviews on HealthCare.gov"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" EnableViewState = "false" runat ="server">
    <link href="css/grid.css" rel="stylesheet" media="screen" type="text/css" />
     <div>
  
    <cc1:toolkitscriptmanager ID="Toolkitscriptmanager1" runat="server"></cc1:toolkitscriptmanager>
    <div id="page_header3">
     <asp:Label ID="lblTitle" runat="server" Text="Mississippi Rate Reviews on HealthCare.gov" cssClass="h2-form" ></asp:Label> 
        <asp:LinkButton ID="btnMidBlog" CssClass="logout" runat="server"  CausesValidation="False" >MID 
        Rate Review Home</asp:LinkButton>  
        
      
</div>
<div class="gridwrap gridnotr">
    <asp:GridView ID="LatestFilingsGridView" runat="server" 
        AutoGenerateColumns="False" CssClass="gridclass w100pc left">
        <Columns>
          
            <asp:BoundField DataField="DATE_SUBMITTED" HeaderText="Date Submitted" ControlStyle-CssClass="txt-left">
            <ControlStyle CssClass="gridheader" />
                
                
 
             </asp:BoundField>
            <asp:BoundField DataField="COMPANY_NAME" HeaderText="Company" ControlStyle-CssClass="txt-left">
             <ControlStyle CssClass="gridheader" />
               
             </asp:BoundField>
            <asp:TemplateField HeaderText="Link To Mississippi Rate Reviews On HealthCare.gov" ControlStyle-CssClass="txt-left">
            <ItemTemplate>
                <asp:HyperLink ID="hLinkRateInc" runat="server" Font-Bold="True"  Text='<%# Eval("LINK_RATE_INCREASE", "{0}") %>'
    NavigateUrl='<%# Container.DataItem("LINK_RATE_INCREASE") %>'  ></asp:HyperLink>
            </ItemTemplate>
            
            </asp:TemplateField>



        </Columns>
    </asp:GridView>
   
    </div>
     
    </asp:Content>

