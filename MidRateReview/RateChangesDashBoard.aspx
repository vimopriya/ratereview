<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile ="~/MasterPage.master" CodeBehind="RateChangesDashBoard.aspx.vb" Inherits="MidWebApp.WebForm2" 
   title="Rate Review Dash Board"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script runat="server">

    Protected Sub gridRateChanges_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" EnableViewState = "false" runat ="server">
    <link href="css/grid.css" rel="stylesheet" media="screen" type="text/css" />
     
    <style type="text/css">

      .LinkPaging
      {
            width:20px;
            background-color:White;
            border: none;
            text-align:center;
            margin-left:8px;
      }
      


</style>    
    <script type="text/javascript">

        function VerifyOptionSelected() {

            var e = document.getElementById("<%= DrpListExport.ClientID %>");             
            var strExportOption = e.options[e.selectedIndex].text;

            if (strExportOption == 'Export To') {
                alert('Please choose an Export option.');
                return false;
            }
            else
               return true;
        }
        
        
        function VerifyCheckedRecords() 
        {
           
            var gridviewArray = new Array("<%= gridRateChanges.ClientID %>");
            // Loop through the grdiview
            var count =0;    
            for (var i = 0; i < gridviewArray.length; i++)                
                 {
                    var GridView = document.getElementById(gridviewArray[i]);
                    if (GridView != undefined)
                     {
                        // Loop through all rows in the gridview and check for checkboxes 
                        //skip header
                         for (var j = 1; j < GridView.rows.length; j++)
                         {
                            var checkboxes = GridView.rows[j].getElementsByTagName('input');
                            for (var k = 0; k < checkboxes.length; k++)
                             {
                                 if (checkboxes[k].type == "checkbox")
                                 {
                                    // if checked, enable the add button and exit
                                    if (checkboxes[k].checked) 
                                    {
                                        count++;
                                    }
                                }
                            }
                        }
                    }
                }

                if (count == 0) {
                    alert('Please select atleast one rate review record.');
                    return false;
                }
                else if (count > 3) {
                    alert('Please select three rate review records to compare.');
                    return false;
                }
                else
                    return true;
        } 
    </script>
    <style type="text/css">

      .LinkPaging
      {
            width:20px;
            background-color:White;
            border: Solid 1px Black;
            text-align:center;
            margin-left:8px;
      }

</style>
    
   
    <cc1:toolkitscriptmanager ID="Toolkitscriptmanager1" runat="server"></cc1:toolkitscriptmanager>
    <div id="page_header3">
     <asp:Label ID="lblTitle" runat="server" Text="Most Recent Rate Changes" cssClass="h2-form" ></asp:Label> 
        <asp:LinkButton ID="linkLogout" CssClass="logout" runat="server"  CausesValidation="False">LogOut</asp:LinkButton>
        <asp:LinkButton ID="btnMidBlog" CssClass="logout" runat="server"  CausesValidation="False" >MID 
        Blog Home</asp:LinkButton>  
         <asp:LinkButton ID="btnMidRateReviewHome" CssClass="logout" runat="server"  CausesValidation="False" >MID 
        Rate Review Home</asp:LinkButton>
</div>

    
    <div class="col_16x left gutter15">
     <asp:Button ID="btnCompareRate" runat="server" CssClass="btn-form left margin0" Text="Return To MID Rate Review Home" 
     CausesValidation="false"/>
    </div>
    
   <div class="col_16x bg-wht rate-changes">

     <asp:Panel ID="menuPanel" runat="server" Visible="false">   
        
        <div class="tabwrap col_16x">
                   
            <asp:Button ID="btnReturn" runat="server" CssClass="btn-form left margin0" Text="Return To Search Results" 
     CausesValidation="false"/>
        <asp:Menu id="menuTabs"  CssClass="menuTabs" StaticMenuItemStyle-CssClass="tab-graph" StaticSelectedStyle-CssClass="selectedTab-chart" Orientation="Horizontal"        
                Runat="server"  Visible="false">
                 <Items>
                   <asp:MenuItem Value="Graph" Text="" />
                    <asp:MenuItem Value="GridView" Text="" />    
                </Items>
        </asp:Menu>
     </div>
     </asp:Panel>
        <ul class="list-hrz">
            <li>
                <label><asp:Label ID="lblSearchCompany" runat="server" 
                    Text="Search By Company"></asp:Label></label>
                   <asp:DropDownList ID="drpListCompany" runat="server"
                    CssClass="dropdown-columnar searchco" AutoPostBack="True" Height="19px">
                    <asp:ListItem>All</asp:ListItem>
                    </asp:DropDownList>
            </li>
            <li>
                <label><asp:Label ID="lblPolicyType" runat="server" 
                    Text="Type Of Insurance"></asp:Label></label>
                    <asp:DropDownList ID="drpListPolicyType" runat="server" Width="234px" 
                      AutoPostBack="True">
                    </asp:DropDownList>
            </li>
            <li class="date2date">
                <label><asp:Label ID="lblFromDate" runat="server" 
                    Text="From Date <span>To Date</span>"></asp:Label></label>

                    <asp:TextBox ID="txtFromDate" runat="server" cssClass="w60"></asp:TextBox>

                <input id="btnPick1" type="button" runat = "server" value = "" class="btn-calendar"/>  
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server"  PopupButtonID = "btnPick1" TargetControlID = "txtFromDate"></cc1:calendarextender>    
                 <span id="hyphenSpan" runat="server">-&nbsp;</span>
                    <asp:TextBox ID="txtToDate" runat="server"  cssClass="w60"></asp:TextBox>

                <input id="btnPick2" type="button" value="" runat = "server" class="btn-calendar" />
                <cc1:CalendarExtender ID="CalendarExtender3" runat="server"  PopupButtonID = "btnPick2" TargetControlID = "txtToDate"></cc1:calendarextender>    


                <asp:Button ID="btnSearch" runat="server" CssClass="btn-form" 
                    Text="Search" />
                    <br />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToValidate="txtFromDate" CssClass="validators" 
                    ErrorMessage="Please Enter Valid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                    
                    <asp:CompareValidator ID="CompareValidator2" runat="server" 
            ControlToValidate="txtToDate" CssClass="validators" 
            ErrorMessage="Please Enter Valid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
             
             
            </li>

            <li class="gutter10-l w100pc">
                <asp:DropDownList ID="DrpListExport" runat="server"   runat="server" Width="162px" 
                    CssClass="dropdown-columnar" AutoPostBack="True" Height="19px">
                    <asp:ListItem>Export To </asp:ListItem>
                    <asp:ListItem>Excel</asp:ListItem>
                    <asp:ListItem>PDF</asp:ListItem>
                    <asp:ListItem>CSV</asp:ListItem>
                    <asp:ListItem>XML</asp:ListItem>
                </asp:DropDownList>
                 <asp:Button ID="btnExport" runat="server" CssClass="btn-wht-form" OnClientClick="return VerifyOptionSelected();"
                Text="Export" />
             <asp:Button ID="btnAddNew" runat="server" CssClass="btn-wht-form" 
                Text="Add New" />
            <asp:Button ID="btnCompare" runat="server" CssClass="btn-wht-form" 
                Text="Compare"  CausesValidation="false" OnClientClick="return VerifyCheckedRecords();"/>
            <asp:Button ID="btnViewAudit" runat="server" CssClass="btn-wht-form" 
                Text="View Audit Trail" />
            <asp:Button ID="btnViewErrorLog" runat="server" CssClass="btn-wht-form" 
                Text="View Error Log" />    
            </li>
          </ul>
    </div>
      <asp:Label ID="lblResult" runat="server" Text=""></asp:Label>
    <div class="gridwrap">
    <asp:GridView ID="gridRateChanges" runat="server" PersistedSelection="true"
        BackColor="White" AutoGenerateColumns="False" 
        AllowSorting="True" 
       ForeColor="Black"
       DataKeyNames = "RATECHANGES_ID"  
       EmptyDataText="No Record Available" CssClass="gridclass"
        OnPageIndexChanging = "gridRateChanges_PageIndexChanging" 
        AllowPaging="True" 
            onselectedindexchanged="gridRateChanges_SelectedIndexChanged" >

        <Columns>
             
              <asp:TemplateField HeaderText="Edit">
              
              <ItemTemplate>
               
              <asp:LinkButton ID="Edit" CommandName="edt" cssClass="btn-edit" runat="server" ToolTip = "Edit"></asp:LinkButton>
                  &nbsp;&nbsp;
                
               </itemTemplate>
              
              </asp:TemplateField>
                 <asp:TemplateField HeaderText="View">
              
              <ItemTemplate>
               
              <asp:LinkButton ID="btnView" CommandName="View" CssClass="btn-view" text = "" runat="server" ToolTip = "View"></asp:LinkButton>
                  &nbsp;&nbsp;
                
               </itemTemplate>
              </asp:TemplateField>
              
               <asp:TemplateField HeaderText="Delete">
               <itemTemplate>
               <asp:LinkButton ID="Delete" CommandName="dlt" runat="server"  CssClass = "btn-delete " ToolTip = "Delete"
OnClientClick = "return confirm('Are you sure you want to delete this Rate Review Record?');"></asp:LinkButton>
               </itemTemplate>
               </asp:TemplateField>
               <asp:TemplateField>
                 <ItemTemplate>
                  <asp:CheckBox  ID="ChartChkBox" runat="server"/>
                  <asp:HiddenField ID="hfKey" runat = "server" Value='<%#Eval("RATECHANGES_ID") %>' /> 
                 </ItemTemplate>
               </asp:TemplateField>  
               <asp:BoundField DataField="Company_Name" HeaderText="Company" SortExpression = "Company_Name">
                <ControlStyle CssClass="gridheader" />
                <HeaderStyle CssClass="gridheader w80" />
            </asp:BoundField>
                                          
              <asp:TemplateField HeaderText="Policy<br /> Form No"  SortExpression="POLICY_FORMNO">
                  <ItemTemplate>
                      <asp:Label ID="lblPolicyForm"  runat="server"   Text='<%# Bind("POLICY_FORMNO") %>'></asp:Label>
                  </ItemTemplate>
                                    <ControlStyle CssClass="gridheader" />
                  <HeaderStyle CssClass="gridheader TextBoxdata w70" />
                  <ItemStyle HorizontalAlign="left" Wrap="True" CssClass  ="w70of"/>
              </asp:TemplateField>
            <asp:BoundField DataField="POLICY_TYPE" HeaderText="Type Of Insurance" SortExpression = "POLICY_TYPE">
                <HeaderStyle CssClass="gridheader w200" />
                <ItemStyle Wrap="true" />
            </asp:BoundField>
            <asp:BoundField DataField="DATEOF_CURRENTRATEINCR" 
                HeaderText="Date Of Current Rate Increase" SortExpression = "DATEOF_CURRENTRATEINCR">
                <HeaderStyle CssClass="gridheader TextBoxdata" />
            </asp:BoundField>
<asp:BoundField DataField="PERCENT_CURRENT_RATEINC" 
                HeaderText="Percentage Of Rate Change" SortExpression = "PERCENT_CURRENT_RATEINC" ItemStyle-HorizontalAlign = "Center">
    <ItemStyle Wrap="False" />
</asp:BoundField>
               <asp:BoundField DataField="DATEOF_YR_MINUS1_RATEINC" HeaderText="2010" SortExpression = "DATEOF_YR_MINUS1_RATEINC" ItemStyle-HorizontalAlign = "Center">
                <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
<asp:BoundField DataField="DATEOF_YR_MINUS2_RATEINC" HeaderText="2009" SortExpression = "DATEOF_YR_MINUS2_RATEINC">
  <HeaderStyle CssClass="gridheader col-date" />
</asp:BoundField>
            <asp:BoundField DataField="DATEOF_YR_MINUS3_RATEINC" HeaderText="2008" SortExpression = "DATEOF_YR_MINUS3_RATEINC">
            <HeaderStyle CssClass="gridheader col-date" />
             </asp:BoundField>
            <asp:BoundField DataField="DATEOF_YR_MINUS4_RATEINC" HeaderText="2007"  SortExpression  = "DATEOF_YR_MINUS4_RATEINC">
            <HeaderStyle CssClass="gridheader col-date" />
             </asp:BoundField>
            <asp:BoundField DataField="DATEOF_YR_MINUS5_RATEINC" HeaderText="2006" SortExpression = "DATEOF_YR_MINUS5_RATEINC">
            <HeaderStyle CssClass="gridheader col-date" />
             </asp:BoundField>
              <asp:TemplateField>
              <HeaderTemplate>
                  <asp:ImageButton ID="btnRight1" runat="server"  
                  onclick = "btnRight1_Click" ImageUrl="images/ico-aro-right.png" />
              </HeaderTemplate>
            </asp:TemplateField>
               <asp:TemplateField>
              <HeaderTemplate>
                  <asp:ImageButton ID="btnLeftPercentage" runat="server" Text="" 
                  onclick = "btnLeftPercentage_Click" ImageUrl="images/ico-aro-left.png" />
              </HeaderTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="PERCENT_YR_MINUS1_RATEINC" HeaderText="2010" ItemStyle-HorizontalAlign = "center" SortExpression = "PERCENT_YR_MINUS1_RATEINC">
                <HeaderStyle CssClass="gridheader col-date" />
                         
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                         
            </asp:BoundField>
            <asp:BoundField DataField="PERCENT_YR_MINUS2_RATEINC" HeaderText="2009" ItemStyle-HorizontalAlign = "center" SortExpression = "PERCENT_YR_MINUS2_RATEINC">
               <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
               </asp:BoundField>
            <asp:BoundField DataField="PERCENT_YR_MINUS3_RATEINC" HeaderText="2008" ItemStyle-HorizontalAlign = "center" SortExpression ="PERCENT_YR_MINUS3_RATEINC" >
             <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
               </asp:BoundField>
            <asp:BoundField DataField="PERCENT_YR_MINUS4_RATEINC" HeaderText="2007" ItemStyle-HorizontalAlign = "center" SortExpression = "PERCENT_YR_MINUS4_RATEINC">
             <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
               </asp:BoundField>
            <asp:BoundField DataField="PERCENT_YR_MINUS5_RATEINC" HeaderText="2006" ItemStyle-HorizontalAlign = "center" SortExpression = "PERCENT_YR_MINUS5_RATEINC">
             <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
               </asp:BoundField>
            
               <asp:TemplateField>
              <HeaderTemplate>
                  <asp:ImageButton ID="btnRightPercentage" runat="server" Text="" 
                  onclick = "btnRightPercentage_Click" ImageUrl="images/ico-aro-right.png" />
              </HeaderTemplate>
            </asp:TemplateField>
             <asp:TemplateField>
              <HeaderTemplate>
                  <asp:ImageButton ID="btnLeftLossRatio" runat="server" Text="" 
                  onclick = "btnLeftLossRatio_Click" ImageUrl="images/ico-aro-left.png" />
              </HeaderTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="LAST_MINUS1_LOSSRATIO" HeaderText="2010" ItemStyle-HorizontalAlign = "center" SortExpression ="LAST_MINUS1_LOSSRATIO" >
                <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="LAST_MINUS2_LOSSRATIO" HeaderText="2009" ItemStyle-HorizontalAlign = "center" SortExpression = "LAST_MINUS2_LOSSRATIO">
             <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
               </asp:BoundField>
            <asp:BoundField DataField="LAST_MINUS3_LOSSRATIO" HeaderText="2008" ItemStyle-HorizontalAlign = "center" SortExpression ="LAST_MINUS3_LOSSRATIO">
             <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
               </asp:BoundField>
            <asp:BoundField DataField="LAST_MINUS4_LOSSRATIO" HeaderText="2007" ItemStyle-HorizontalAlign = "center" SortExpression ="LAST_MINUS4_LOSSRATIO">
             <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
               </asp:BoundField>

            <asp:BoundField DataField="LAST_MINUS5_LOSSRATIO" HeaderText="2006" ItemStyle-HorizontalAlign = "center" SortExpression ="LAST_MINUS5_LOSSRATIO">
             <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
               </asp:BoundField>
               <asp:TemplateField>
              <HeaderTemplate>
                  <asp:ImageButton ID="btnRightLossRatio" runat="server" Text="" 
                  onclick = "btnRightLossRatio_Click" ImageUrl="images/ico-aro-right.png" />
              </HeaderTemplate>
            </asp:TemplateField>
              <asp:TemplateField>
              <HeaderTemplate>
                  <asp:ImageButton ID="btnLeftNumOfInsured" runat="server" Text="" 
                  onclick = "btnLeftNumOfInsured_Click" ImageUrl="images/ico-aro-left.png" />
              </HeaderTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="NUMOF_MSINSURED_YR_MINUS1" HeaderText="2010" ItemStyle-HorizontalAlign = "center" SortExpression ="NUMOF_MSINSURED_YR_MINUS1">
                <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="NUMOF_MSINSURED_YR_MINUS2" HeaderText="2009" ItemStyle-HorizontalAlign = "center" SortExpression ="NUMOF_MSINSURED_YR_MINUS2">
             <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
               </asp:BoundField>
            <asp:BoundField DataField="NUMOF_MSINSURED_YR_MINUS3" HeaderText="2008" ItemStyle-HorizontalAlign = "center" SortExpression ="NUMOF_MSINSURED_YR_MINUS3">
             <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
               </asp:BoundField>
            <asp:BoundField DataField="NUMOF_MSINSURED_YR_MINUS4" HeaderText="2007" ItemStyle-HorizontalAlign = "center" SortExpression ="NUMOF_MSINSURED_YR_MINUS4">
             <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
               </asp:BoundField>
            <asp:BoundField DataField="NUMOF_MSINSURED_YR_MINUS5" HeaderText="2006" ItemStyle-HorizontalAlign = "center" SortExpression = "NUMOF_MSINSURED_YR_MINUS5">
             <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
               </asp:BoundField>
                <asp:BoundField DataField="POLICY_APPROVAL" HeaderText="Policy Approval" ItemStyle-HorizontalAlign = "center" SortExpression ="POLICY_APPROVAL">
             <HeaderStyle CssClass="gridheader col-date" />

<ItemStyle HorizontalAlign="Center"></ItemStyle>
               </asp:BoundField>
               
          <asp:TemplateField HeaderText="Link to MS Rate Reviews On HealthCare.gov">
            <ItemTemplate >
                <asp:HyperLink ID="hLinkRateInc" runat="server" Font-Bold="True"  Text='<%# Eval("LINK_RATE_INCREASE", "{0}") %>'
    NavigateUrl='<%# Container.DataItem("LINK_RATE_INCREASE") %>' ></asp:HyperLink>
            </ItemTemplate>
            
            </asp:TemplateField>
               
        </Columns>
        
        <SelectedRowStyle BorderColor="Blue" BackColor="Fuchsia" />
        
        <HeaderStyle CssClass="gridheader" />
        <EditRowStyle CssClass="griditem" HorizontalAlign="Left" BackColor="#FF66FF" />
         <PagerStyle HorizontalAlign="left" />
          <PagerTemplate>
                        <table>
                              <tr>
                                    <td>
                                          <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                              </tr>
                        </table>
                  </PagerTemplate>
        <AlternatingRowStyle CssClass="rowcoloralt" />
         
    </asp:GridView>
        <p class="txt-gry col_5 gutter10-l left">You are viewing page
        <strong><%=gridRateChanges.PageIndex + 1%></strong>
            of
        <strong><%=gridRateChanges.PageCount%></strong>
        </p>
        <ul class="list-hrz right gutter10 links-secondary">
            <li>
               
                <asp:HyperLink ID="hLinkResource" runat="server" NavigateUrl = "~/Resources.aspx" target="_blank">Resources</asp:HyperLink>
              
            </li>
            <li>
             <asp:HyperLink ID="hLinkFAQ" runat="server" NavigateUrl = "/MidBlog/category/FAQ.aspx" target="_blank" >FAQ's</asp:HyperLink>
              
            </li>
            <li>
             <asp:HyperLink ID="hLinkFeedBack" runat="server"  OnClick = "return confirm('All nonpublic personal health information and nonpublic personal financial information shall be held confidential by Mississippi Insurance Department.')"  NavigateUrl = "mailto:ratehelp@mid.state.ms.us" target="_blank" >Submit 
                a Comment</asp:HyperLink>
               
            </li>
            
            <li>
             <asp:HyperLink ID="hLinkLatestFilings" runat="server" NavigateUrl = "/MidRateChanges/latestFilingsDashBoard.aspx" target="_blank" >Link 
                to MS Rate Reviews On HealthCare.gov</asp:HyperLink>
               
            </li>
        </ul>
        
     </div>
     
 

</asp:Content>
