﻿Public Partial Class HighestRateChange
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PopulateGridView()
    End Sub



    Private Sub PopulateGridView()
        Dim resourcesDs As New DataSet
        Dim rateChangesBL As RateChangesBL.Admin = New RateChangesBL.Admin()
        resourcesDs = rateChangesBL.GetHighestrateChangeData()
        GridViewHighestRateChange.DataSource = resourcesDs
        GridViewHighestRateChange.DataBind()


    End Sub

End Class